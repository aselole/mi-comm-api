<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member_model extends CI_Model{

    public $table = 'members'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
    public function __construct()
    {
        parent::__construct();
        $this->timestamps = FALSE;
        // $this->has_one['user'] = array('foreign_model'=>'user_model','foreign_table'=>'users','foreign_key'=>'id','local_key'=>'user_id');
        // $this->has_one['community'] = array('foreign_model'=>'community_model','foreign_table'=>'communities','foreign_key'=>'id','local_key'=>'community_id');
    }

    public function get_limit_data($limit, $start) {
        $order     = $this->input->post('order');
        $dataorder = array();
        $where     = array();

        $i=1;
        $dataorder[$i++] = 'user_member_jenis';
        $dataorder[$i++] = 'community_nama';
        $dataorder[$i++] = 'user_first_name';
        $dataorder[$i++] = 'user_email';
        $dataorder[$i++] = 'tgl_bergabung';

        $fields = array(
            'members.id',
            'members.community_id',
            'members.user_id',
            'members.tgl_bergabung',
            'members.active',
        );

        if(!empty($this->input->post('member_jenis'))){
            $where['LOWER(users.member_jenis) LIKE'] = '%'.strtolower($this->input->post('member_jenis')).'%';
        }
        if(!empty($this->input->post('community_id'))){
            $where['members.community_id'] = $this->input->post('community_id');
        }
        if(!empty($this->input->post('level_id'))){
            $where['users.level_id'] = $this->input->post('level_id');
        }
        if(!empty($this->input->post('active'))){
            $where['members.active'] = $this->input->post('active');
        }
        if(!empty($this->input->post('first_name'))){
            $where['LOWER(users.first_name) LIKE'] = '%'.strtolower($this->input->post('first_name')).'%';
        }
        if(!empty($this->input->post('email'))){
            $where['LOWER(users.email) LIKE'] = '%'.strtolower($this->input->post('email')).'%';
        }
        if(!empty($this->input->post('tgl_bergabung_start')) && !empty($this->input->post('tgl_bergabung_end'))){
            $date_from_arr = explode('/', $this->input->post('tgl_bergabung_start'));
            $date_from     = date('Y-m-d', strtotime($date_from_arr[2].$date_from_arr[1].$date_from_arr[0]));
            $date_to_arr   = explode('/', $this->input->post('tgl_bergabung_end'));
            $date_to       = date('Y-m-d', strtotime($date_to_arr[2].$date_to_arr[1].$date_to_arr[0]));
            $where['(members.tgl_bergabung >= "'.$date_from.'" and members.tgl_bergabung <= "'.$date_to.'")'] = null;
        }

        $this->where($where);
        // $result['total_rows'] = $this->count_rows();
        $result['total_rows'] = count($this
                                        ->with_community('fields:nama','join:true')
                                        ->with_user('fields:member_jenis, first_name, email, level_id', 'join:true')
                                        ->get_all()
                                    );

        $this->where($where);
        $this->order_by( $dataorder[$order[0]["column"]],  $order[0]["dir"]);
        $this->limit($start, $limit);
        $result['get_db'] = $this
                            ->fields($fields)
                            ->with_user(array('fields'=>'member_jenis, first_name, email, level_id', 'join'=>true, 'with'=>array('relation'=>'level', 'fields'=>'nama', 'join'=>true)))
                            ->with_community('fields:nama|join:true')
                            ->get_all();
        return $result;
    }

    function insert($data){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

}
