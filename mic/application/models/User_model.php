<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model{

    public $table = 'users'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $label = 'first_name'; // If you want, you can set an array with the fields that can be filled by insert/update
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
    public function __construct()
    {
        parent::__construct();
        // $this->has_one['level'] = array('foreign_model'=>'level_model','foreign_table'=>'levels','foreign_key'=>'id','local_key'=>'level_id');
        // $this->has_many['promo_user'] = array('foreign_model'=>'promo_user_model','foreign_table'=>'promos_users','foreign_key'=>'user_id','local_key'=>'id');
    }

    public function get_limit_data($limit, $start) {
        $order     = $this->input->post('order');
        $dataorder = array();
        $where     = array();

        $i=1;
        $dataorder[$i++] = 'level_id';
        $dataorder[$i++] = 'member_jenis';
        $dataorder[$i++] = 'first_name';
        $dataorder[$i++] = 'email';
        $dataorder[$i++] = 'phone';
        $dataorder[$i++] = 'active';
        $dataorder[$i++] = 'last_login';
        if(!empty($this->input->post('id'))){
            $where['id'] = $this->input->post('id');
        }
        if(!empty($this->input->post('level_id'))){
            $where['level_id'] = $this->input->post('level_id');
        }
        if(!empty($this->input->post('member_jenis'))){
            $where['LOWER(users.member_jenis) LIKE'] = '%'.strtolower($this->input->post('member_jenis')).'%';
        }
        if(!empty($this->input->post('first_name'))){
            $where['LOWER(users.first_name) LIKE'] = '%'.strtolower($this->input->post('first_name')).'%';
        }
        if(!empty($this->input->post('username'))){
            $where['LOWER(users.username) LIKE'] = '%'.strtolower($this->input->post('username')).'%';
        }
        if(!empty($this->input->post('email'))){
            $where['LOWER(users.email) LIKE'] = '%'.strtolower($this->input->post('email')).'%';
        }
        if(!empty($this->input->post('phone'))){
            $where['LOWER(users.phone) LIKE'] = '%'.strtolower($this->input->post('phone')).'%';
        }
        if($this->input->post('active') != null){
            $where['users.active'] = $this->input->post('active');
        }
        if(!empty($this->input->post('last_login_start')) && !empty($this->input->post('last_login_end'))){
            $date_from_arr = explode('/', $this->input->post('last_login_start'));
            $date_from     = date('Y-m-d', strtotime($date_from_arr[2].$date_from_arr[1].$date_from_arr[0]));
            $date_to_arr   = explode('/', $this->input->post('last_login_end'));
            $date_to       = date('Y-m-d', strtotime($date_to_arr[2].$date_to_arr[1].$date_to_arr[0]));
            $where['(users.last_login >= "'.$date_from.'" and users.last_login <= "'.$date_to.'")'] = null;
        }

        $this->where($where);
        $result['total_rows'] = $this->count_rows();

        $this->where($where);
        $this->order_by( $dataorder[$order[0]["column"]],  $order[0]["dir"]);
        $this->limit($start, $limit);
        $result['get_db'] = $this
                            ->fields('id, level_id, member_jenis, first_name, username, email, phone, active, last_login')
                            ->with_level('fields:nama|join:true')
                            ->get_all();
        return $result;
    }

    public function send_email_activation($user_id)
    {
        $this->load->model(array('global_model'));
        $fields     = 'users.id, users.first_name, users.email, users.activation_code';
        // $dt         = $this->fields($fields)->with_level('fields:nama', 'join: true')->get($user_id);
        // $data['dt'] = $dt;
        $r = $this->db->where(["id" => $user_id])->get("users")->result();
        $dt = $r[0];
        // print_r($dt);

        $email_to = array($dt->email);
        $subject = 'Aktifasi Akun MiComm';
        // $body_email = $this->load->view('mail_template/activation', $data, true);
        $sql = "select * from master_emails where kode='email_aktifasi'";
        $body_email = $this->db->query($sql)->row()->isi;
        $body_email = str_replace("%nama%", $dt->first_name, $body_email);
        // $body_email = str_replace("%link_aktifasi%", site_url('activation')."/".$dt->id."/".$dt->activation_code, $body_email);
        $body_email = str_replace("%link_aktifasi%", "http://mi-comm.com/activation/".$dt->id."/".$dt->activation_code, $body_email);


        return $this->global_model->send_email($email_to, $subject, $body_email);
    }

    public function upload_pic_user($email=null)
    {
        $config['upload_path'] = './uploads/user/';
        $config['allowed_types'] = 'jpeg|jpg';
        $config['max_size']  = '90000';
        $config['file_name'] = $email.'.jpg';

        $this->load->library('upload', $config);
        $this->upload->overwrite = true;

        if ( ! $this->upload->do_upload('photo')) {
            $error = array('error' => $this->upload->display_errors());
            $result['success'] = false;
            $result['message'] = $error['error'];
        } else {
            $upload_data = $this->upload->data();
            $result = array(
                'success' => true,
                'data' => $upload_data,
            );
        }
        return $result;
    }

    function insert($data){
        $r = $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
}
