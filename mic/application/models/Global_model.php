<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Global_model extends CI_Model{

    public function generateKey($user_id)
    {
        $static_prefix = $this->config->item('static_prefix');
        return md5($static_prefix.$user_id.'key');
    }

    public function generateActivationCode($user_id)
    {
        $static_prefix = $this->config->item('static_prefix');
        return md5($static_prefix.$user_id.'activation_code');
    }

    public function getUserIdBYToken($token)
    {
        $sql = "
        select user_id
        from `keys`
        where `key` = '$token'
        ";
        $hasil = $this->db->query($sql);
        if ($hasil->num_rows() > 0) {
            $user_id = $hasil->row()->user_id;
            return $user_id;
        } else {
            return false;
        }
    }

    public function send_email($email_to, $subject, $body_email)
    {
        $config = array();
        $config['protocol']  = "smtp";
        $config['smtp_host'] = "orotech.xyz";
        // $config['smtp_port'] = 465;
        // $config['smtp_user'] = "ndoro.awank@gmail.com";
        // $config['smtp_pass'] = "restuibu!@#";
        $config['charset']   = "utf-8";
        $config['mailtype']  = "html";
        $config['newline']   = "\r\n";
        $config['wordwrap']  = TRUE;


        // $config['smtp_host'] = "mi-comm.com";
        $config['smtp_port'] = 587;
        $config['smtp_user'] = "info@mi-comm.com";
        $config['smtp_pass'] = "password";

        // $config = array();
        // $config['protocol']  = "smtp";
        // $config['smtp_host'] = "smtp.gmail.com";
        // $config['smtp_port'] = 465;
        // $config['smtp_timeout'] = 7;
        // $config['smtp_user'] = "xxx@gmail.com";
        // $config['smtp_pass'] = "xxx";
        // $config['charset']   = "utf-8";
        // $config['mailtype']  = "html";
        // $config['newline']   = "\r\n";
        // $config['wordwrap']  = TRUE;
        // $config["smtp_crypto"] = "ssl";

        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from('info@mi-comm.com', 'Mi-comm');
        $this->email->to($email_to);

        $this->email->subject($subject);
        $this->email->message($body_email);

        if ($this->email->send()) {
            return true;
        } else {

            return $this->email->print_debugger();
            // return false;
        }
    }

    /*
    * tujuan: kirim Notification Message ke server FCM supaya user dpt notifikasi
    * $to = token firebase
    * $title = judul notifikasi
    * $message = isi notifikasi
    * $data_firebase = custom data yg dibutuhkan oleh aplikasi hp, array of object
    * $member_jenis = menentukan server fcm yang menghandle notifikasi
    */
    public function push_notif_firebase($to, $title, $message, $data_firebase, $member_jenis)
    {
        if ($member_jenis=='micomm') {
            $api_access_key = "AAAA29_sSHE:APA91bEfySDIuCFZpWOHH_RF60qHDL5GugQkLjMyK4WrG_On6FCqdlONfkCPpSJqVJ0aJfsMMyzCnqDM0l0Hk6ZHhAPjEZc8usbVWXS-9tTqAD_ereWnrDK9PLWs_xAdk9b_UP9lQZgP6cJ6m7FfaitNKLQTETFIsw";
        } else {
            $api_access_key = "AAAA29_sSHE:APA91bEfySDIuCFZpWOHH_RF60qHDL5GugQkLjMyK4WrG_On6FCqdlONfkCPpSJqVJ0aJfsMMyzCnqDM0l0Hk6ZHhAPjEZc8usbVWXS-9tTqAD_ereWnrDK9PLWs_xAdk9b_UP9lQZgP6cJ6m7FfaitNKLQTETFIsw";
        }

        $registrationIds = array($to);
        $msg = array(
            'body' => $message,
            'title' => $title,
            'vibrate' => 1,
            'sound' => 1,
            // 'subtitle' => 'subtitle',
            // 'tickerText' => 'ticker text disini..',
            // 'largeIcon' => 'large_icon',
            // 'smallIcon' => 'small_icon'
        );
        $fields = array(
            'registration_ids' => $registrationIds,
            'notification' => $msg,
            'data' => $data_firebase
        );
        $headers = array(
            'Authorization:key='.$api_access_key,
            'Content-Type:application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);
        curl_close($ch);
        // echo $result;
    }



}
