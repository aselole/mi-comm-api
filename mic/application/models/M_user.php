<?php

class M_user extends CI_Model {
	

	private $_table = "users";

	function __construct(){
		parent::__construct();

	}

	function add($data){
		// $data["password"] = 
		$data["password"] = password_hash($data["password"], PASSWORD_DEFAULT);
		$data["active"] = 1;
		$data["level_id"] = 2;
		$data["created_at"] = date("Y-m-d H:i:s");

		$r = $this->db->insert($this->_table, $data);
		if($r == 1){
			return "success";
		}
		return "failed";
	}

}