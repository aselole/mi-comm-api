<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}

	// function add(){
	// 	$data = [
	// 		"email" => $this->input->get_post("email"),
	// 		"first_name" => $this->input->get_post("first_name"),
	// 		"username" => $this->input->get_post("username"),
	// 		"password" => $this->input->get_post("password")
	// 	];
	// 	$this->load->model("m_user");
	// 	$r = $this->m_user->add($data);
	// 	echo $r;
	// }

	function add(){
		// $this->load->model("m_user");
        $this->load->model("user_model");
        $this->load->model("global_model");
        $this->load->model("member_model");

		$this->load->library("bcrypt");

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        $length = 4;
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        $barcode = "3201" . date("Ydm") . $randomString;
        
        $data = [
            "email" => $this->input->get_post("email"),
            "first_name" => $this->input->get_post("first_name"),
            "password" => $this->input->get_post("password"),
            "level_id" => 2,
            "active" => 0,
            "created_at" => date("Y-m-d H:i:s"),
            "member_jenis" => "micomm",
            "card_barcode" => $barcode
        ];

        

        $community_id = 7;

        $result=[];
        $result['success'] = false;
        $result['message'] = 'Terjadi kesalahan.';

        $check_temp_user = $this->db->from("temp_user")->where(["email" => $data["email"]])->get()->result_array();
        if(count($check_temp_user) == 0){
            $result["message"] = "Email ini tidak diperkenankan untuk melakukan registrasi";
            echo json_encode($result);
            return;
        }
        $temp_user = $check_temp_user[0];
        $community_id = $temp_user["kode"];

        $users = $this->db->from("users")->where(["email" => $data["email"]])->select()->get()->result_array();

        if(count($users) > 0) {
            $result["message"] = "Email sudah dipakai";
            echo json_encode($result);
            return;
        }

        $data["password"] = $this->bcrypt->hash($data["password"]);

        $insert_id = $this->user_model->insert($data);
        if ($insert_id) {
            // begin generate key
            $key = $this->global_model->generateKey($insert_id);
            $data_key = array(
                'user_id' => $insert_id,
                'key'     => $key,
            );
            $this->db->insert('keys', $data_key);
            // end generate key

            // generate activation code
            $activation_code = $this->global_model->generateActivationCode($insert_id);
            $data_activation = array(
                'activation_code' => $activation_code,
            );
            $this->db->where('id', $insert_id);
            $this->db->update('users', $data_activation);
            // end generate activation code

            $send_email = $this->user_model->send_email_activation($insert_id);

            // create member
            // $level_id = $this->input->post('level_id');
            $level_id = 2;
            if ($level_id == 2) {
                if ($community_id) {
                    $data = array(
                        'community_id'  => $community_id,
                        'user_id'       => $insert_id,
                        'tgl_bergabung' => date('Y-m-d'),
                    );
                    $this->member_model->insert($data);
                }
            }
            if ($level_id == 3) {
                $community_arr = $this->input->post('a_community_id');
                if (!empty($community_arr) ) {
                    foreach ($community_arr as $key => $v) {
                        $data = array(
                            'community_id'  => $v,
                            'user_id'       => $insert_id,
                            'tgl_bergabung' => date('Y-m-d'),
                        );
                        $this->member_model->insert($data);
                    }
                }
            }

            $result['success'] = true;
            $result['message'] = 'User berhasil ditambahkan.';
            $result['url']     = site_url('admin/user');
            $result["email_status"] = $send_email;
        } else {
            $result['success'] = false;
            $result['message'] = 'Terjadi kesalahan.';
        }
        echo json_encode($result);
    }
}
