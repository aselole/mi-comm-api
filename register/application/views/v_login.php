<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="#" type="image/png">

    <title>Login Admin MIC Database</title>

    <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
	
	<style>
		
	</style>
</head>

<body class="login-body">

<div class="container">

    <form class="form-signin" action="<?= base_url('login/cek_login')?>" method="post">
         
		<div class="form-signin-heading text-center">
		
            <img src="<?= base_url(); ?>assets/images/logo-mic_login.png" alt=""/>
        </div>
		<center><label>Login MIC Database</label></center>
        <div class="login-wrap">
            <input type="text" name="username" class="form-control" placeholder="Username" autofocus required>
            <input type="password" name="password" class="form-control" placeholder="Password" required>
            <button class="btn btn-lg btn-login btn-block" type="submit">
                <h4>LOGIN</h4>
            </button>
			<?php echo $this->session->flashdata('pesan');?>
            

        </div>

        

    </form>

</div>



<!-- Placed js at the end of the document so the pages load faster -->

<!-- Placed js at the end of the document so the pages load faster -->
<script src="<?= base_url(); ?>assets/js/jquery-1.10.2.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/js/modernizr.min.js"></script>

</body>
</html>
