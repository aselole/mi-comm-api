<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">
  <link href="<?=base_url();?>assets/images/logo-mic.png" rel='shortcut icon'>
	
  <title>MIC Database Management - Data Birthday The-Coach</title>
	
   <!-- customize -->
  <link href="<?= base_url(); ?>assets/css_datepicker/datepicker.css" rel="stylesheet">
  
  <!--data table-->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/js/data-tables/DT_bootstrap.css" />
	
  <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/css/style-responsive.css" rel="stylesheet">
<!--pickers css-->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-datepicker/css/datepicker-custom.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-timepicker/css/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-colorpicker/css/colorpicker.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-datetimepicker/css/datetimepicker-custom.css" />
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="sticky-header">

<section>
    <!-- left side start-->
    <div class="left-side sticky-left-side">

        <!--logo and iconic logo start-->
        <div class="logo">
            <a href="<?=base_url("dashboard/index") ?>"><img src="<?= base_url(); ?>assets/images/logo-mic.png" alt=""></a>
        </div>

        <div class="logo-icon text-center">
            <a href="<?=base_url("dashboard/index") ?>"><img src="<?= base_url(); ?>assets/images/logo-mic.png" alt=""></a>
        </div>
        <!--logo and iconic logo end-->


        <div class="left-side-inner">

            <!-- visible to small devices only -->
            <div class="visible-xs hidden-sm hidden-md hidden-lg">
                <div class="media logged-user">
                    <img alt="" src="<?= base_url(); ?>assets/images/admin.png" class="media-object">
                    <div class="media-body">
                        Admin,
                        <?php echo $user ?>
                    </div>
                </div>

                <ul class="nav nav-pills nav-stacked custom-nav">
                 
                  <li><a href="<?=base_url('login/logout');?>"><i class="fa fa-sign-out"></i><span>Logout</span></a></li>
                </ul>
            </div>

            <!--sidebar nav start-->
            <ul class="nav nav-pills nav-stacked custom-nav">
                <li><a href="<?= base_url('dashboard/index'); ?>"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>  
                <li class="menu-list"><a href="#"><i class="fa fa-laptop"></i> <span>Data Anggota</span></a>
                    <ul class="sub-menu-list">
                        <li ><a href="<?= base_url('data_mi_comm/data_anggota'); ?>"> Mi-Comm</a></li>
                        <li><a href="<?= base_url('data_the_coach/data_anggota'); ?>"> The-Coach</a></li>
						<li><a href="<?= base_url('data_yes/data_anggota'); ?>"> Yes</a></li>
                    </ul>
                </li>
				
				<li class="menu-list active"><a href="#"><i class="fa fa-calendar"></i> <span> Birthday</span></a>
                    <ul class="sub-menu-list">
                        <li ><a href="<?= base_url('data_birthday_micomm/data_anggota'); ?>"> Mi-Comm</a></li>
                        <li class="active"><a href="<?= base_url('data_birthday_thecoach/data_anggota'); ?>"> The-Coach</a></li>
						<li><a href="<?= base_url('data_birthday_yes/data_anggota'); ?>"> Yes</a></li>
                    </ul>
                </li>
				<li class="menu-list"><a href="#"><i class="fa fa-folder-open"></i> <span>Data Master</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="<?= base_url('data_perusahaan_micomm/index'); ?>"> Perusahaan Mi-Comm</a></li>
						<li><a href="<?= base_url('data_perusahaan_thecoach/index'); ?>"> Perusahaan The-Coach</a></li>
						<li><a href="<?= base_url('data_perusahaan_yes/index'); ?>"> Perusahaan Yes</a></li>
						<li><a href="<?= base_url('data_training'); ?>"> Training </a></li>
                        <li><a href="<?= base_url('data_event/index'); ?>"> Event</a></li>
                    </ul>
                </li>
				<li><a href="<?= base_url('user/index'); ?>"><i class="fa fa-user"></i> <span>Data User</span></a></li>
                <li><a href="<?= base_url('login/logout'); ?>"><i class="fa fa-sign-in"></i> <span>Logout</span></a></li>
            </ul>
            <!--sidebar nav end-->

        </div>
    </div>
    <!-- left side end-->
    
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <div class="header-section">

        <!--toggle button start-->
        <a class="toggle-btn"><i class="fa fa-bars"></i></a>
        <!--toggle button end-->

        <!--search start-->
       
        <!--search end-->

		<!--notification menu start -->
        <div class="menu-right">
            <ul class="notification-menu">
                
                <li>
                    <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= base_url('assets/images/admin.png') ?>" alt="" />
                        Admin,
                        <?php echo $user ?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                        
                        <li><a href="<?= base_url('login/logout'); ?>"><i class="fa fa-sign-out"></i> Log Out</a></li>
                    </ul>
                </li>

            </ul>
        </div>
        <!--notification menu end -->

        </div>
        <!-- header section end-->

        <!-- page heading start-->
        
        <!-- page heading end-->

        <!--body wrapper start-->
        <div class="wrapper">
             <div class="row">
                <div class="col-sm-12">
				<?php echo $this->session->flashdata('pesan');?>
                <section class="panel">
                <header class="panel-heading">
                   Data Tanggal Lahir Anggota The-Coach
                     <span class="tools pull-right">
						<a href="<?= base_url('data_birthday_thecoach/data_anggota'); ?>" class="fa fa-refresh"></a>
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
						
                     </span>
                </header>
                <div class="panel-body">
                <div class="adv-table editable-table ">
                <div class="clearfix">
                    <form action="<?= base_url('data_birthday_thecoach/pencarian'); ?>" method="post">
					<div class="form-group">
					<span style="color:blue"><strong>Pencarian tanggal lahir : </strong></span>
					</div>
					<div class="btn-group">
						<select class="form-control" id="tanggal" name="tanggal" required >
							<option value="">Tanggal</option>
							<option value="01">1</option>
							<option value="02">2</option>
							<option value="03">3</option>
							<option value="04">4</option>
							<option value="05">5</option>
							<option value="06">6</option>
							<option value="07">7</option>
							<option value="08">8</option>
							<option value="09">9</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
							<option value="13">13</option>
							<option value="14">14</option>
							<option value="15">15</option>
							<option value="16">16</option>
							<option value="17">17</option>
							<option value="18">18</option>
							<option value="19">19</option>
							<option value="20">20</option>
							<option value="21">21</option>
							<option value="22">22</option>
							<option value="23">23</option>
							<option value="24">24</option>
							<option value="25">25</option>
							<option value="26">26</option>
							<option value="27">27</option>
							<option value="28">28</option>
							<option value="29">29</option>
							<option value="30">30</option>
							<option value="31">31</option>
							
						</select>
						
                    </div>
					<div class="btn-group">
						<select class="form-control" name="bulan" required>
							<option value="">Bulan</option>
							<option value="01">Januari</option>
							<option value="02">Februari</option>
							<option value="03">Maret</option>
							<option value="04">April</option>
							<option value="05">Mei</option>
							<option value="06">Juni</option>
							<option value="07">Juli</option>
							<option value="08">Agustus</option>
							<option value="09">September</option>
							<option value="10">Oktober</option>
							<option value="11">November</option>
							<option value="12">Desember</option>
							
						</select>
						
                    </div>
					<div class="btn-group">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                    </div>
					</form>
					
					</br>
                    
                </div>
				
                <div class="space15"></div>
                <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
					<th width="100">No. Anggota</th>
                    <th width="160">Nama Anggota</th>
                    <th width="100">Tanggal Lahir</th>
					<th width="100">Usia Ke-</th>
					<th width="140">Perusahaan</th>
                    <th width="80">Telepon</th>
                    <th width="40">Opsi</th>
                </tr>
                </thead>
                <tbody>
				<?php $hari_ini = date("Y"); ?>
				<?php foreach($list as $row) { ?>
                <tr>
                    
					<td><?php echo $row['no_anggota']; ?></td>
                    <td><?php echo $row['nama_anggota']; ?></td>
					<td align="center"><?php echo date('d-m-Y',strtotime($row['tgllahir_anggota'])); ?></td>
					<td align="center"><?php echo $hari_ini-date('Y',strtotime($row['tgllahir_anggota'])); ?></td>
					<td><?php echo $row['nama_perusahaan']; ?></td>
					<td><?php echo $row['tlp']; ?></td>
					
					<td>
					 <button type="button" data-target="#modalView" data-toggle="modal" onclick="view(<?= $row['id_thecoach'] ?>)" class="btn btn-info btn-sm"><i class="fa fa-search" title="View"></i></button>
					 
					</td>
                </tr>
                <?php } ?>
                </tbody>
				
                </table>
				<div class="form-group">
					
					
					<div class="pull-right">
					<?php
						echo $this->pagination->create_links();
					?>
					</div>
				</div>
                </div>
                </section>
                </div>
                </div>
        </div>
        <!--body wrapper end-->
		<!-- Modal Edit-->
		<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalEdit" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h4 class="modal-title">Edit Anggota Mi-Comm - <span class="edit_ket"></span></h4>
					</div>
					<div class="modal-body">

					<form role="form" method="post" action="<?= base_url('data_mi_comm/update_micomm'); ?>">
					<input type="text" name="id_micomm" value="" id="edit_id_micomm" hidden="">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1"><strong>Identitas</strong></label>
							</div>
						</div>
						
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Event</label>
							   <select id="edit_event" name="event" class="form-control selectpicker" data-live-search="true" title="--Pilih Event--">
									<option value="0">-- Pilih Event --</option>
									<?php foreach ($event as $event) {
										echo "<option value='".$event['id_event']."'>".$event['nama_event']."</option>";
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Perusahaan</label>
								<select id="edit_perusahaan" name="perusahaan" class="form-control" data-live-search="true" title="--Pilih Perusahaan--">
								<option value="">-- Pilih Perusahaan --</option>
									<?php foreach ($perusahaan as $data) {
										echo "<option value='".$data['nama_perusahaan']."'>".$data['nama_perusahaan']."</option>";
									}
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Tempat Lokasi</label>
							  <input class="form-control" id="edit_lokasi" name="lokasi" type="text" placeholder="Tempat lokasi"/>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							   <label for="exampleInputEmail1">Tanggal mulai</label>
							   <input type="text" class="span2 form-control" value="" placeholder="Tanggal mulai" id="edit_tanggal_mulai" name="tanggal_mulai">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							   <label for="exampleInputEmail1">Tanggal berakhir</label>
							   <input type="text" class="span2 form-control" value="" placeholder="Tanggal berakhir" id="edit_tanggal_akhir" name="tanggal_akhir">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Nama Lengkap</label>
							  <input class="form-control" id="edit_nama_lengkap" name="nama_lengkap" type="text" required placeholder="Nama lengkap"/>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Nama Panggilan</label>
							   <input class=" form-control" id="edit_nama_panggilan" name="nama_panggilan" type="text" placeholder="Nama panggilan"/>
							</div>
						</div>
					</div>
					<div class="row">
						
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Tempat Lahir</label>
							   <input class=" form-control" id="edit_tempat_lahir" name="tempat_lahir" type="text" placeholder="Tempat lahir"/>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Tanggal lahir</label>
							   <input type="text" class="span2 form-control" value="" placeholder="Tanggal lahir" id="edit_tgl_lahir" name="tgl_lahir">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Agama</label>
							  <input class=" form-control" id="edit_agama" name="agama" type="text" required placeholder="Agama"/>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Alamat</label>
							   <input class=" form-control" id="edit_alamat" placeholder="Alamat" name="alamat">
							</div>
						</div>
						
					</div>	
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Nomor Telepon</label>
							   <input class=" form-control" id="edit_telepon" name="telepon" type="text" placeholder="Nomor telepon"/>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							   <label for="exampleInputEmail1">Nomor Handphone</label>
							   <input class=" form-control" id="edit_no_hp" placeholder="Nomor Handphone" required name="no_hp">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							   <label for="exampleInputEmail1">Nomor Handphone 2</label>
							   <input class=" form-control" id="edit_no_hp2" name="no_hp2" type="text" placeholder="Nomor Handphone 2"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Email Pribadi</label>
							   <input class=" form-control" id="edit_email_pribadi" name="email_pribadi" type="email" placeholder="E-mail pribadi"/>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							   <label for="exampleInputEmail1">Nama Group WA</label>
							   <input class=" form-control" id="edit_group_wa" placeholder="Nama Group WA" name="group_wa">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							   <label for="exampleInputEmail1">Akun Instagram</label>
							   <input class=" form-control" id="edit_instagram" placeholder="Akun Instagram" name="instagram" type="text">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
							   <label for="exampleInputEmail1">PIN BBM</label>
							   <input class=" form-control" id="edit_bbm" placeholder="PIN BBM" name="pin_bb">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							   <label for="exampleInputEmail1">ID Line</label>
							   <input class=" form-control" id="edit_line" placeholder="ID Line" name="id_line">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							   <label for="exampleInputEmail1">ID Facebook</label>
							   <input class=" form-control" id="edit_facebook" placeholder="ID Facebook" name="id_facebook">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							   <label for="exampleInputEmail1">ID Twitter</label>
							   <input class=" form-control" id="edit_twitter" name="id_twitter" type="text" placeholder="ID Twitter"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1"><strong>Perusahaan</strong></label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
							   <label for="exampleInputEmail1">Alamat Penempatan</label>
							   <input class=" form-control" id="edit_alamat_penempatan" placeholder="Alamat penempatan" name="alamat_penempatan">
							</div>
						</div>
						
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Email Kantor</label>
							   <input class=" form-control" id="edit_email_kantor" name="email_kantor" type="email" placeholder="E-mail kantor"/>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Posisi</label>
							   <input class=" form-control" id="edit_posisi" name="posisi" type="text" placeholder="Posisi"/>
							</div>
						</div> 
					</div>

					<div class="modal-footer">
						<button type="submit" class="btn btn-success">Simpan</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
					</div>
					</form>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Modal View-->
		<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalView" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h4 class="modal-title">Detail Anggota The-Coach - <span class="view_ket"></span></h4>
					</div>
					<input type="text" name="id_micomm" value="" class="view_id_micomm" hidden="">
					<div class="modal-body">
					<header class="panel-heading">
						Identitas
					</header>
						<table class="table table-bordered table-striped table-condensed">
							
							<tbody>
							<tr>
								<td>Perusahaan</td>
								<td><span class="view_perusahaan"></span></td>
							</tr>
							<tr>
								<td width="350">Nomor Anggota</td>
								<td><span class="view_no_anggota"></span></td>
							</tr>
							<tr>
								<td>Nama Anggota</td>
								<td><span class="view_nama_anggota"></span></td>
							</tr>
							<tr>
								
								<td>Tanggal Lahir Anggota</td>
								<td><span class="view_tgllahir_anggota"></span></td>
							</tr>
							<tr>
								<td>Nama Pasangan</td>
								<td><span class="view_nama_pasangan"></span></td>
							</tr>
							<tr>
								
								<td>Tanggal Lahir Pasangan</td>
								<td><span class="view_tgllahir_pasangan"></span></td>
							</tr>
							<tr>
								<td>Alamat Anggota</td>
								<td><span class="view_alamat"></span></td>
							</tr>
							<tr>
								<td>Kota</td>
								<td><span class="view_kota"></span></td>
							</tr>
							<tr>
								<td>Agama</td>
								<td><span class="view_agama"></span></td>
							</tr>
							<tr>
								<td>Pendidikan terakhir</td>
								<td><span class="view_pendidikan"></span></td>
							</tr>
							<tr>
								<td>Nomor Telepon</td>
								<td><span class="view_telepon"></span></td>
							</tr>
							<tr>
								<td>Nomor Handphone</td>
								<td><span class="view_hp1"></span></td>
							</tr>
							<tr>
								<td>Nomor Handphone 2</td>
								<td><span class="view_hp2"></span></td>
							</tr>
							<tr>
								<td>PIN BBM</td>
								<td><span class="view_bbm"></span></td>
								
							</tr>
							<tr>
							   
								<td>E-mail</td>
								<td><span class="view_email"></span></td>
							</tr>
							<tr>
								<td>Kegiatan</td>
								<td><span class="view_kegiatan"></span></td>
							</tr>
							
							
							</tbody>
						</table>
					

					<div class="modal-footer">
						<button type="button" class="btn btn-info" data-dismiss="modal">Tutup</button>
					</div>
					</form>
					</div>
				</div>
			</div>
		</div>
		
		
		<!-- Modal hapus -->
		<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalHapus" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h4 class="modal-title">Peringatan !!!</h4>
					</div>
					<div class="modal-body">

					<p class="no-margin">Apakah anda yakin menghapus data anggota Mi-Comm <span id="delete_keterangan"></span> ?</p>

					<div class="modal-footer">
						<form role="form" method="post" action="<?= base_url('data_mi_comm/hapus_micomm'); ?>">
						<input type="hidden" class="form-control kd_hidden" name="id_micomm">
						<button type="submit" class="btn btn-success">Iya</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button>
					</div>
					</form>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Modal Excel-->
		<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalExcel" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h4 class="modal-title">Import Excel</h4>
					</div>
					<form class="form-horizontal" action="<?= base_url('data_mi_comm/do_upload') ?>" method="post" enctype="multipart/form-data">
					<div class="modal-body">

						<div class="form-group">
							<div class="col-md-5 col-sm-6">
								<label>Pilih file excel yang diupload</label>
							</div>
							<div class="col-md-3 col-sm-6">
                                <input  type="file" name="file" class="default" />
							</div>
                        </div>

					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-success">Upload</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
						
					</div>
					</form>
				</div>
			</div>
		</div>
		<!-- modal -->

        <!--footer section start-->
        <footer>
            2016 &copy; MIC Transformer
        </footer>
        <!--footer section end-->


    </div>
    <!-- main content end-->
</section>

<!-- Placed js at the end of the document so the pages load faster -->
<script src="<?= base_url(); ?>assets/js/jquery-1.10.2.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/js/modernizr.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.nicescroll.js"></script>

<!--data table-->
<script type="text/javascript" src="<?= base_url(); ?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/data-tables/DT_bootstrap.js"></script>

<!--common scripts for all pages-->
<script src="<?= base_url(); ?>assets/js/scripts.js"></script>

<!--script for editable table-->
<script src="<?= base_url(); ?>assets/js/editable-table.js"></script>

<!--pickers plugins-->
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="<?= base_url(); ?>assets/js_datepicker/bootstrap-datepicker.js"></script>

<!--pickers initialization-->
<script src="<?= base_url(); ?>assets/js/pickers-init.js"></script>

<script>
	if (top.location != location) {
    top.location.href = document.location.href ;
  }
		$(function(){
			window.prettyPrint && prettyPrint();
			$('input[name="tanggal_mulai"]').datepicker({
				format: 'dd-mm-yyyy'
			});
			$('input[name="tanggal_akhir"]').datepicker({
				format: 'dd-mm-yyyy'
			});
			$('input[name="tgl_lahir"]').datepicker({
				format: 'dd-mm-yyyy'
			});
			$('#dpYears').datepicker();
			$('#dpMonths').datepicker();
			
			
			var startDate = new Date(2012,1,20);
			var endDate = new Date(2012,1,25);
			$('#dp4').datepicker()
				.on('changeDate', function(ev){
					if (ev.date.valueOf() > endDate.valueOf()){
						$('#alert').show().find('strong').text('The start date can not be greater then the end date');
					} else {
						$('#alert').hide();
						startDate = new Date(ev.date);
						$('#startDate').text($('#dp4').data('date'));
					}
					$('#dp4').datepicker('hide');
				});
			$('#dp5').datepicker()
				.on('changeDate', function(ev){
					if (ev.date.valueOf() < startDate.valueOf()){
						$('#alert').show().find('strong').text('The end date can not be less then the start date');
					} else {
						$('#alert').hide();
						endDate = new Date(ev.date);
						$('#endDate').text($('#dp5').data('date'));
					}
					$('#dp5').datepicker('hide');
				});

        // disabling dates
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#dpd1').datepicker({
          onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
          }
          checkin.hide();
          $('#dpd2')[0].focus();
        }).data('datepicker');
        var checkout = $('#dpd2').datepicker({
          onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          checkout.hide();
        }).data('datepicker');
		});
	</script>

<!-- END JAVASCRIPTS -->
<script>
    jQuery(document).ready(function() {
        EditableTable.init();
    });
</script>
<script>
function edit(x){
	var nomor=x;
	$('#edit_id_thecoach').val(nomor);
	
	$.ajax({
		url   : "<?= base_url("data_the_coach/cekData") ?>",
		type: "get",
		data: "id="+nomor,
		dataType: "json",
		success: function(data){
			
			$("#edit_no_anggota").val(data.no_anggota);
			$("#edit_perusahaan").val(data.perusahaan);
			$("#edit_nama_anggota").val(data.nama_anggota);
			$("#edit_jenis_kelamin").val(data.jenis_kelamin);
			$("#edit_tgllahir_anggota").val(data.tgllahir_anggota);
			$("#edit_alamat").val(data.alamat);
			$("#edit_kota").val(data.kota);
			$("#edit_telepon").val(data.tlp);
			$("#edit_no_hp").val(data.hp1);
			$("#edit_no_hp2").val(data.hp2);
			$("#edit_bbm").val(data.bbm);
			$("#edit_email").val(data.email);
			$("#edit_agama").val(data.agama);
			$("#edit_kegiatan").val(data.kegiatan);
			$("#edit_status_pensiun").val(data.status_pensiun);
			$("#edit_status_usaha").val(data.status_usaha);
			$("#edit_keterangan").val(data.keterangan);
			$(".edit_ket").html(data.nama_anggota);
			
		}
	});
	//$("#modalEdit").modal("show");
}

function view(x){
	var nomor=x;
	$('.view_id_thecoach').val(nomor);
	
	$.ajax({
		url   : "<?= base_url("data_the_coach/getView") ?>",
		type: "get",
		data: "id="+nomor,
		dataType: "json",
		success: function(data){
			
			$(".view_no_anggota").html(data.no_anggota);
			$(".view_perusahaan").html(data.perusahaan);
			$(".view_nama_anggota").html(data.nama_anggota);
			$(".view_tgllahir_anggota").html(data.tgllahir_anggota);
			$(".view_nama_pasangan").html(data.nama_pasangan);
			$(".view_tgllahir_pasangan").html(data.tgllahir_pasangan);
			$(".view_alamat").html(data.alamat);
			$(".view_kota").html(data.kota);
			$(".view_telepon").html(data.tlp);
			$(".view_hp1").html(data.hp1);
			$(".view_hp2").html(data.hp2);
			$(".view_bbm").html(data.bbm);
			$(".view_email").html(data.email);
			$(".view_agama").html(data.agama);
			$(".view_kegiatan").html(data.kegiatan);
			$(".view_pendidikan").html(data.pendidikan);
			$(".view_ket").html(data.nama_anggota);
			
		}
	});
	
}
	
	function hapus(x){
	var nomor = x;
	$.ajax({
	type  : "get",
	url   : "<?= base_url("data_the_coach/cekData") ?>",
	data  : "id="+nomor,
	dataType: "json",
	success : function(data){
	  $("#delete_keterangan").html(data.nama_anggota);
	}
	});
	$("input.kd_hidden").val(nomor);
	//$("#modalHapus").modal("show");
	
}

$('form select[name="tanggal"]').on('change invalid', function() {
    var textfield = $(this).get(0);
    
    // hapus dulu pesan yang sudah ada
    textfield.setCustomValidity('');
    
    if (!textfield.validity.valid) {
      textfield.setCustomValidity('Tanggal tidak boleh kosong!');  
    }
});

$('form select[name="bulan"]').on('change invalid', function() {
    var textfield = $(this).get(0);
    
    // hapus dulu pesan yang sudah ada
    textfield.setCustomValidity('');
    
    if (!textfield.validity.valid) {
      textfield.setCustomValidity('Bulan tidak boleh kosong!');  
    }
});

function selectOption(index){ 
  document.getElementById("tanggal").options.selectedIndex = index;
}
	
</script>

</body>
</html>
