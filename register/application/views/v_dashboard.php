<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="keywords" content="admin, dashboard, bootstrap, template, flat, modern, theme, responsive, fluid, retina, backend, html5, css, css3">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">
  <link href="<?=base_url();?>assets/images/logo-mic.png" rel='shortcut icon'>

  <title>MIC Database Management - Dashboard</title>

  <!--icheck-->
  <link href="<?=base_url(); ?>assets/js/iCheck/skins/minimal/minimal.css" rel="stylesheet">
  <link href="<?=base_url(); ?>assets/js/iCheck/skins/square/square.css" rel="stylesheet">
  <link href="<?=base_url(); ?>assets/js/iCheck/skins/square/red.css" rel="stylesheet">
  <link href="<?=base_url(); ?>assets/js/iCheck/skins/square/blue.css" rel="stylesheet">

  <!--dashboard calendar-->
  <link href="<?=base_url(); ?>assets/css/clndr.css" rel="stylesheet">

  <!--Morris Chart CSS -->
  <link rel="<?=base_url(); ?>assets/stylesheet" href="js/morris-chart/morris.css">

  <!--common-->
  <link href="<?=base_url(); ?>assets/css/style.css" rel="stylesheet">
  <link href="<?=base_url(); ?>assets/css/style-responsive.css" rel="stylesheet">




  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="sticky-header">

<section>
    <!-- left side start-->
    <div class="left-side sticky-left-side">

        <!--logo and iconic logo start-->
        <div class="logo">
            <a href="<?=base_url("dashboard/index") ?>"><img src="<?= base_url(); ?>assets/images/logo-mic.png" alt=""></a>
        </div>

        <div class="logo-icon text-center">
            <a href="<?=base_url("dashboard/index") ?>"><img src="<?= base_url(); ?>assets/images/logo-mic.png" alt=""></a>
        </div>
        <!--logo and iconic logo end-->

        <div class="left-side-inner">

            <!-- visible to small devices only -->
            <div class="visible-xs hidden-sm hidden-md hidden-lg">
                <div class="media logged-user">
                    <img alt="" src="<?= base_url(); ?>assets/images/admin.png" class="media-object">
                    <div class="media-body">
                        Admin,
                        <?php echo $user ?>
                    </div>
                </div>

                <ul class="nav nav-pills nav-stacked custom-nav">
                 
                  <li><a href="<?=base_url('login/logout');?>"><i class="fa fa-sign-out"></i><span>Logout</span></a></li>
                </ul>
            </div>

            <!--sidebar nav start-->
            <ul class="nav nav-pills nav-stacked custom-nav">
                <li class="active"><a href="<?= base_url('dashboard/index'); ?>"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>  
                <li class="menu-list"><a href="#"><i class="fa fa-laptop"></i> <span>Data Anggota</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="<?= base_url('data_mi_comm/data_anggota'); ?>"> Mi-Comm</a></li>
                        <li><a href="<?= base_url('data_the_coach/data_anggota'); ?>"> The-Coach</a></li>
						<li><a href="<?= base_url('data_yes/data_anggota'); ?>"> Yes</a></li>
                    </ul>
                </li>
				
				<li class="menu-list"><a href="#"><i class="fa fa-calendar"></i> <span> Birthday</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="<?= base_url('data_birthday_micomm/data_anggota'); ?>"> Mi-Comm</a></li>
                        <li><a href="<?= base_url('data_birthday_thecoach/data_anggota'); ?>"> The-Coach</a></li>
						<li><a href="<?= base_url('data_birthday_yes/data_anggota'); ?>"> Yes</a></li>
                    </ul>
                </li>
				<li class="menu-list"><a href="#"><i class="fa fa-folder-open"></i> <span>Data Master</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="<?= base_url('data_perusahaan_micomm/index'); ?>"> Perusahaan Mi-Comm</a></li>
						<li><a href="<?= base_url('data_perusahaan_thecoach/index'); ?>"> Perusahaan The-Coach</a></li>
						<li><a href="<?= base_url('data_perusahaan_yes/index'); ?>"> Perusahaan Yes</a></li>
						<li><a href="<?= base_url('data_training'); ?>"> Training </a></li>
                        <li><a href="<?= base_url('data_event/index'); ?>"> Event</a></li>
                    </ul>
                </li>
				<li><a href="<?= base_url('user/index'); ?>"><i class="fa fa-user"></i> <span>Data User</span></a></li>
                <li><a href="<?= base_url('login/logout'); ?>"><i class="fa fa-sign-in"></i> <span>Logout</span></a></li>

            </ul>
            <!--sidebar nav end-->

        </div>
    </div>
    <!-- left side end-->
    
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <div class="header-section">

            <!--toggle button start-->
            <a class="toggle-btn"><i class="fa fa-bars"></i></a>
            <!--toggle button end-->

            

<!--notification menu start -->
        <div class="menu-right">
            <ul class="notification-menu">
                
                <li>
                    <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= base_url('assets/images/admin.png') ?>" alt="" />
						Admin,
                        <?php echo $user ?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                        
                        <li><a href="<?= base_url('login/logout'); ?>"><i class="fa fa-sign-out"></i> Log Out</a></li>
                    </ul>
                </li>

            </ul>
        </div>
        <!--notification menu end -->

        </div>
        <!-- header section end-->

        <!-- page heading start-->
		<?php echo $this->session->flashdata('pesan');?>
        <!-- page heading end-->

        <!--body wrapper start-->
        <div class="wrapper">
          <div class="row states-info">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Dashboard</div>
					<div class="panel-body">
						<div class="col-md-4">
							<a href="<?=base_url("data_mi_comm/data_anggota") ?>">
							<div class="panel red-bg">
								<div class="panel-body">
									<div class="row">
										<div class="col-xs-4">
											<i class="fa fa-user"></i>
										</div>
										<div class="col-xs-8">
											<span class="state-title"> Total Data Anggota Mi-Comm </span>
											<h4>
											<?php foreach ($micomm as $data) { ?>
												<?php echo $data["jumlah"] ?> Data
											<?php } ?>
											</h4>
										</div>
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-md-4">
							<a href="<?=base_url("data_the_coach/data_anggota") ?>">
							<div class="panel blue-bg">
								<div class="panel-body">
									<div class="row">
										<div class="col-xs-4">
											<i class="fa fa-user"></i>
										</div>
										<div class="col-xs-8">
											<span class="state-title"> Total Data Anggota The-Coach  </span>
											<h4>
											<?php foreach ($thecoach as $data) { ?>
												<?php echo $data["jumlah"] ?> Data
											<?php } ?>
											</h4>
										</div>
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-md-4">
							<a href="<?=base_url("data_yes/data_anggota") ?>">
							<div class="panel green-bg">
								<div class="panel-body">
									<div class="row">
										<div class="col-xs-4">
											<i class="fa fa-user"></i>
										</div>
										<div class="col-xs-8">
											<span class="state-title">  Total Data Anggota Mi-Comm Yes  </span>
											<h4>
											<?php foreach ($yes as $data) { ?>
												<?php echo $data["jumlah"] ?> Data
											<?php } ?>
											</h4>
										</div>
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-md-4">
							<a href="<?=base_url("data_perusahaan_micomm") ?>">
							<div class="panel yellow-bg">
								<div class="panel-body">
									<div class="row">
										<div class="col-xs-4">
											<i class="fa fa-building-o"></i>
										</div>
										<div class="col-xs-8">
											<span class="state-title">  Total Data Perusahaan Anggota Mi-Comm  </span>
											<h4>
											<?php foreach ($p_micomm as $data) { ?>
												<?php echo $data["jumlah"] ?> Data
											<?php } ?>
											</h4>
										</div>
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-md-4">
							<a href="<?=base_url("data_perusahaan_thecoach") ?>">
							<div class="panel green-bg">
								<div class="panel-body">
									<div class="row">
										<div class="col-xs-4">
											<i class="fa fa-building-o"></i>
										</div>
										<div class="col-xs-8">
											<span class="state-title">  Total Data Perusahaan Anggota The-Coach </span>
											<h4>
											<?php foreach ($p_thecoach as $data) { ?>
												<?php echo $data["jumlah"] ?> Data
											<?php } ?>
											</h4>
										</div>
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-md-4">
							<a href="<?=base_url("data_perusahaan_yes") ?>">
							<div class="panel red-bg">
								<div class="panel-body">
									<div class="row">
										<div class="col-xs-4">
											<i class="fa fa-building-o"></i>
										</div>
										<div class="col-xs-8">
											<span class="state-title">  Total Data Perusahaan Anggota Yes </span>
											<h4>
											<?php foreach ($p_yes as $data) { ?>
												<?php echo $data["jumlah"] ?> Data
											<?php } ?>
											</h4>
										</div>
									</div>
								</div>
							</div>
							</a>
						</div>
						
					</div>
					<br><br><br><br><br><br><br><br><br><br><br><br>
                </div>
				
			</div>
            
            

            

           

            
        </div>
        <!--body wrapper end-->

        <!--footer section start-->
        <footer>
            2016 &copy; MIC Transformer
        </footer>
        <!--footer section end-->


    </div>
    <!-- main content end-->
</section>

<!-- Placed js at the end of the document so the pages load faster -->
<script src="<?= base_url(); ?>assets/js/jquery-1.10.2.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/js/modernizr.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.nicescroll.js"></script>

<!--easy pie chart-->
<script src="<?= base_url(); ?>assets/js/easypiechart/jquery.easypiechart.js"></script>
<script src="<?= base_url(); ?>assets/js/easypiechart/easypiechart-init.js"></script>

<!--Sparkline Chart-->
<script src="<?= base_url(); ?>assets/js/sparkline/jquery.sparkline.js"></script>
<script src="<?= base_url(); ?>assets/js/sparkline/sparkline-init.js"></script>

<!--icheck -->
<script src="<?= base_url(); ?>assets/js/iCheck/jquery.icheck.js"></script>
<script src="<?= base_url(); ?>assets/js/icheck-init.js"></script>

<!-- jQuery Flot Chart-->
<script src="<?= base_url(); ?>assets/js/flot-chart/jquery.flot.js"></script>
<script src="<?= base_url(); ?>assets/js/flot-chart/jquery.flot.tooltip.js"></script>
<script src="<?= base_url(); ?>assets/js/flot-chart/jquery.flot.resize.js"></script>


<!--Morris Chart-->
<script src="<?= base_url(); ?>assets/js/morris-chart/morris.js"></script>
<script src="<?= base_url(); ?>assets/js/morris-chart/raphael-min.js"></script>

<!--Calendar-->
<script src="<?= base_url(); ?>assets/js/calendar/clndr.js"></script>
<script src="<?= base_url(); ?>assets/js/calendar/evnt.calendar.init.js"></script>
<script src="<?= base_url(); ?>assets/js/calendar/moment-2.2.1.js"></script>


<!--common scripts for all pages-->
<script src="<?= base_url(); ?>assets/js/scripts.js"></script>

<!--Dashboard Charts-->
<script src="<?= base_url(); ?>assets/js/dashboard-chart-init.js"></script>


</body>
</html>
