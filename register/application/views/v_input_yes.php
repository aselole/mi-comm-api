<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">
  <link href="<?=base_url();?>assets/images/logo-mic.png" rel='shortcut icon'>

  <title>MIC Database Management - Data Yes</title>

  <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/css/style-responsive.css" rel="stylesheet">
  <!-- customize -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/css_select/bootstrap-select.css">
  <link href="<?= base_url(); ?>assets/css_datepicker/datepicker.css" rel="stylesheet">
  <!--pickers css-->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-datepicker/css/datepicker-custom.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-timepicker/css/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-colorpicker/css/colorpicker.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-datetimepicker/css/datetimepicker-custom.css" />
  <!--multi-select-->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/jquery-multi-select/css/multi-select.css" />

    <!--file upload-->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/bootstrap-fileupload.min.css" />

    <!--tags input-->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/jquery-tags-input/jquery.tagsinput.css" />
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="sticky-header">

<section>
    <!-- left side start-->
    <div class="left-side sticky-left-side">

        <!--logo and iconic logo start-->
        <div class="logo">
            <a href="<?=base_url("dashboard/index") ?>"><img src="<?= base_url(); ?>assets/images/logo-mic.png" alt=""></a>
        </div>

        <div class="logo-icon text-center">
            <a href="<?=base_url("dashboard/index") ?>"><img src="<?= base_url(); ?>assets/images/logo-mic.png" alt=""></a>
        </div>
        <!--logo and iconic logo end-->


        <div class="left-side-inner">

            <!-- visible to small devices only -->
            <div class="visible-xs hidden-sm hidden-md hidden-lg">
                <div class="media logged-user">
                    <img alt="" src="<?= base_url(); ?>assets/images/admin.png" class="media-object">
                    <div class="media-body">
                        Admin,
                        <?php echo $user ?>
                    </div>
                </div>

                <ul class="nav nav-pills nav-stacked custom-nav">
                 
                  <li><a href="<?=base_url('login/logout');?>"><i class="fa fa-sign-out"></i><span>Logout</span></a></li>
                </ul>
            </div>

            <!--sidebar nav start-->
            <ul class="nav nav-pills nav-stacked custom-nav">
                <li><a href="<?= base_url('dashboard/index'); ?>"><i class="fa fa-home"></i> <span>Dashboard</span></a>  
                <li class="menu-list active"><a href="#"><i class="fa fa-laptop"></i> <span>Data Anggota</span></a>
                    <ul class="sub-menu-list">
						<li><a href="<?= base_url('data_mi_comm/data_anggota'); ?>"> Mi-Comm</a></li>
                        <li ><a href="<?= base_url('data_the_coach/data_anggota'); ?>"> The-Coach</a></li>
						<li class="active"><a href="<?= base_url('data_yes/data_anggota'); ?>"> Yes</a></li>
						</ul>
                </li>
			
				<li class="menu-list"><a href="#"><i class="fa fa-calendar"></i> <span> Birthday</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="<?= base_url('data_birthday_micomm/data_anggota'); ?>"> Mi-Comm</a></li>
                        <li><a href="<?= base_url('data_birthday_thecoach/data_anggota'); ?>"> The-Coach</a></li>
						<li><a href="<?= base_url('data_birthday_yes/data_anggota'); ?>"> Yes</a></li>
                    </ul>
                </li>
				<li class="menu-list"><a href="#"><i class="fa fa-folder-open"></i> <span>Data Master</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="<?= base_url('data_perusahaan_micomm/index'); ?>"> Perusahaan Mi-Comm</a></li>
						<li><a href="<?= base_url('data_perusahaan_thecoach/index'); ?>"> Perusahaan The-Coach</a></li>
						<li><a href="<?= base_url('data_perusahaan_yes/index'); ?>"> Perusahaan Yes</a></li>
						<li><a href="<?= base_url('data_training'); ?>"> Training </a></li>
                        <li><a href="<?= base_url('data_event/index'); ?>"> Event</a></li>
                    </ul>
                </li>
				<li><a href="<?= base_url('user/index'); ?>"><i class="fa fa-user"></i> <span>Data User</span></a></li>
                <li><a href="<?= base_url('login/logout'); ?>"><i class="fa fa-sign-in"></i> <span>Logout</span></a></li>

            </ul>
            <!--sidebar nav end-->

        </div>
    </div>
    <!-- left side end-->
    
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <div class="header-section">

        <!--toggle button start-->
        <a class="toggle-btn"><i class="fa fa-bars"></i></a>
        <!--toggle button end-->

       

        <!--notification menu start -->
        <d<!--notification menu start -->
        <div class="menu-right">
            <ul class="notification-menu">
                
                <li>
                    <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= base_url('assets/images/admin.png') ?>" alt="" />
						Admin,
                        <?php echo $user ?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                        
                        <li><a href="<?= base_url('login/logout'); ?>"><i class="fa fa-sign-out"></i> Log Out</a></li>
                    </ul>
                </li>

            </ul>
        </div>
        <!--notification menu end -->

        </div>
        <!-- header section end-->

        <!-- page heading start-->
       
        <!-- page heading end-->

        <!--body wrapper start-->
        <div class="wrapper">
            
            <div class="row">
                <div class="col-lg-12">
				<?php echo $this->session->flashdata('pesan');?>
                    <section class="panel">
                        <header class="panel-heading">
                            Form Anggota Yes
                        </header>
                        <div class="panel-body">
                            <div class="form">
                                <form class="cmxform form-horizontal adminex-form" method="post" action="<?= base_url('data_yes/tambah_yes'); ?>" enctype="multipart/form-data">
									
									
									<div class="form-group">
											<div class="col-md-12"><label class="control-label"><strong>Identitas</strong></label></div>
									</div>
									<div class="form-group">
										
										<div class="col-md-6 col-sm-12"><input class=" form-control" id="lastname" name="nama_lengkap" type="text" required placeholder="Nama lengkap"/></div>
										<div class="col-md-6 col-sm-12">
                                            <input class=" form-control" id="lastname" name="panggilan" type="text" placeholder="Panggilan"/>
                                        </div>
										</br></br></br>
										<div class="col-md-6 col-sm-12"><input class=" form-control" id="lastname" name="perguruan_tinggi" type="text" placeholder="Perguruan tinggi"/></div>
										<div class="col-md-6 col-sm-12"><input class=" form-control" id="lastname" name="fakultas" type="text" placeholder="Fakultas"/></div>
										</br></br></br>
										<div class="col-md-6 col-sm-12">
										  <select id="lunch" name="perusahaan" class="form-control selectpicker" data-live-search="true" title="--Pilih Perusahaan--">
											<option value="0">-- Pilih Perusahaan --</option>
												<?php foreach ($perusahaan as $data) {
													echo "<option value='".$data['nama_perusahaan']."'>".$data['nama_perusahaan']."</option>";
												}
												?>
										  </select>
										</div>
										<div class="col-md-6 col-sm-12"><input class=" form-control" id="lastname" name="agama" type="text" placeholder="Agama"/></div></br></br></br>
										<div class="col-md-6 col-sm-12">
                                            <input class=" form-control" id="lastname" name="tempat_lahir" type="text" placeholder="Tempat lahir"/>
                                        </div>
										<div class="col-md-6 col-sm-12">
											<input type="text" class="span2 form-control" value="" placeholder="Tanggal lahir" id="dp1" name="tgl_lahir">
										</div>
										</br></br></br>
										<div class="col-md-6 col-sm-12">
                                            <input class="form-control" id="lastname" name="alamat" type="text" placeholder="Alamat" required/>
                                        </div>
										<div class="col-md-6 col-sm-12">
                                            <input class=" form-control" id="lastname" name="telepon" type="text" placeholder="Telepon"/>
                                        </div>
										</br></br></br>
										<div class="col-md-6 col-sm-12">
                                            <input class=" form-control" id="lastname" name="no_hp" type="text" placeholder="Nomor handphone" required/>
                                        </div>
										<div class="col-md-6 col-sm-12">
                                            <input class=" form-control" id="lastname" name="bbm" type="text" placeholder="PIN BBM"/>
                                        </div>
										</br></br></br>
										<div class="col-md-6 col-sm-12"><input class=" form-control" id="lastname" name="line" type="text" placeholder="ID Line"/></div>
										<div class="col-md-6 col-sm-12"><input class=" form-control" id="lastname" name="facebook" type="text" placeholder="Akun Facebook"/></div>
										</br></br></br>
										<div class="col-md-6 col-sm-12"><input class=" form-control" id="lastname" name="twitter" type="text" placeholder="Akun Twitter"/></div>
										<div class="col-md-6 col-sm-12"><input class=" form-control" id="lastname" name="email" type="text" placeholder="Email"/></div>
										</br></br>
										</br>
										
									</div>

									<div class="form-group">
                                        <div class="col-lg-12">
                                            <button class="btn btn-success btn-block m-t-5" type="submit" name="submit">Simpan</button>
                                            
                                        </div>
                                    </div>
								
                                </form>
								
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <!--body wrapper end-->

        <!--footer section start-->
        <footer>
            2016 &copy; MIC Transformer
        </footer>
        <!--footer section end-->


    </div>
    <!-- main content end-->
</section>

<!-- Placed js at the end of the document so the pages load faster -->
<script src="<?= base_url(); ?>assets/js/jquery-1.10.2.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/js/modernizr.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.nicescroll.js"></script>

<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?= base_url(); ?>assets/js/validation-init.js"></script>

<!--common scripts for all pages-->
<script src="<?= base_url(); ?>assets/js/scripts.js"></script>

<!--pickers plugins-->
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>

<!--pickers initialization-->
<script src="<?= base_url(); ?>assets/js/pickers-init.js"></script>

<!--multi-select-->
<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery-multi-select/js/jquery.quicksearch.js"></script>
<script src="<?= base_url(); ?>assets/js/multi-select-init.js"></script>
<!--spinner-->
<script type="text/javascript" src="<?= base_url(); ?>assets/js/fuelux/js/spinner.min.js"></script>
<script src="<?= base_url(); ?>assets/js/spinner-init.js"></script>
<!--file upload-->
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-fileupload.min.js"></script>
<!-- customize -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/js_select/bootstrap-select.js"></script>
<script src="<?= base_url(); ?>assets/js_datepicker/bootstrap-datepicker.js"></script>
<script>
$(document).ready(function() {
	$(".dp-date-picker").datepicker({
		format:"dd-mm-yyyy"
	})
});

$(document).ready(function () {
    var mySelect = $('#first-disabled2');

    $('#special').on('click', function () {
      mySelect.find('option:selected').prop('disabled', true);
      mySelect.selectpicker('refresh');
    });

    $('#special2').on('click', function () {
      mySelect.find('option:disabled').prop('disabled', false);
      mySelect.selectpicker('refresh');
    });

    $('#basic2').selectpicker({
      liveSearch: true,
      maxOptions: 1
    });
  });

</script>

<script>
	if (top.location != location) {
    top.location.href = document.location.href ;
  }
		$(function(){
			window.prettyPrint && prettyPrint();
			$('#dp1').datepicker({
				format: 'dd-mm-yyyy'
			});
			$('#dp2').datepicker();
			$('#dp3').datepicker();
			$('#dp3').datepicker();
			$('#dpYears').datepicker();
			$('#dpMonths').datepicker();
			
			
			var startDate = new Date(2012,1,20);
			var endDate = new Date(2012,1,25);
			$('#dp4').datepicker()
				.on('changeDate', function(ev){
					if (ev.date.valueOf() > endDate.valueOf()){
						$('#alert').show().find('strong').text('The start date can not be greater then the end date');
					} else {
						$('#alert').hide();
						startDate = new Date(ev.date);
						$('#startDate').text($('#dp4').data('date'));
					}
					$('#dp4').datepicker('hide');
				});
			$('#dp5').datepicker()
				.on('changeDate', function(ev){
					if (ev.date.valueOf() < startDate.valueOf()){
						$('#alert').show().find('strong').text('The end date can not be less then the start date');
					} else {
						$('#alert').hide();
						endDate = new Date(ev.date);
						$('#endDate').text($('#dp5').data('date'));
					}
					$('#dp5').datepicker('hide');
				});

        // disabling dates
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#dpd1').datepicker({
          onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
          }
          checkin.hide();
          $('#dpd2')[0].focus();
        }).data('datepicker');
        var checkout = $('#dpd2').datepicker({
          onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          checkout.hide();
        }).data('datepicker');
		});
	</script>

</body>
</html>
