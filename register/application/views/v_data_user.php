<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">
  <link href="<?=base_url();?>assets/images/logo-mic.png" rel='shortcut icon'>

  <title>MIC Database Management - Data User</title>

  <!--data table-->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/js/data-tables/DT_bootstrap.css" />

  <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/css/style-responsive.css" rel="stylesheet">
<!--pickers css-->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-datepicker/css/datepicker-custom.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-timepicker/css/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-colorpicker/css/colorpicker.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-datetimepicker/css/datetimepicker-custom.css" />
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="sticky-header">

<section>
    <!-- left side start-->
    <div class="left-side sticky-left-side">

        <!--logo and iconic logo start-->
        <div class="logo">
            <a href="<?=base_url("dashboard/index") ?>"><img src="<?= base_url(); ?>assets/images/logo-mic.png" alt=""></a>
        </div>

        <div class="logo-icon text-center">
            <a href="<?=base_url("dashboard/index") ?>"><img src="<?= base_url(); ?>assets/images/logo-mic.png" alt=""></a>
        </div>
        <!--logo and iconic logo end-->


        <div class="left-side-inner">

            <!-- visible to small devices only -->
            <div class="visible-xs hidden-sm hidden-md hidden-lg">
                <div class="media logged-user">
                    <img alt="" src="<?= base_url(); ?>assets/images/admin.png" class="media-object">
                    <div class="media-body">
                        Admin,
                        <?php echo $user ?>
                    </div>
                </div>

                <ul class="nav nav-pills nav-stacked custom-nav">
                 
                  <li><a href="<?=base_url('login/logout');?>"><i class="fa fa-sign-out"></i><span>Logout</span></a></li>
                </ul>
            </div>

            <!--sidebar nav start-->
            <ul class="nav nav-pills nav-stacked custom-nav">
                <li><a href="<?= base_url('dashboard/index'); ?>"><i class="fa fa-home"></i> <span>Dashboard</span></a>  
                <li class="menu-list"><a href="#"><i class="fa fa-laptop"></i> <span>Data Anggota</span></a>
                    <ul class="sub-menu-list">
						<li><a href="<?= base_url('data_mi_comm/data_anggota'); ?>"> Mi-Comm</a></li>
                        <li><a href="<?= base_url('data_the_coach/data_anggota'); ?>"> The-Coach</a></li>
						<li><a href="<?= base_url('data_yes/data_anggota'); ?>"> Yes</a></li>
						</ul>
                </li>
				
				<li class="menu-list"><a href="#"><i class="fa fa-calendar"></i> <span> Birthday</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="<?= base_url('data_birthday_micomm/data_anggota'); ?>"> Mi-Comm</a></li>
                        <li><a href="<?= base_url('data_birthday_thecoach/data_anggota'); ?>"> The-Coach</a></li>
						<li><a href="<?= base_url('data_birthday_yes/data_anggota'); ?>"> Yes</a></li>
                    </ul>
                </li>
				<li class="menu-list"><a href="#"><i class="fa fa-folder-open"></i> <span>Data Master</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="<?= base_url('data_perusahaan_micomm/index'); ?>"> Perusahaan Mi-Comm</a></li>
						<li><a href="<?= base_url('data_perusahaan_thecoach/index'); ?>"> Perusahaan The-Coach</a></li>
						<li><a href="<?= base_url('data_perusahaan_yes/index'); ?>"> Perusahaan Yes</a></li>
						<li><a href="<?= base_url('data_training'); ?>"> Training </a></li>
                        <li><a href="<?= base_url('data_event/index'); ?>"> Event</a></li>
                    </ul>
                </li>
				<li class="active"><a href="<?= base_url('user/index'); ?>"><i class="fa fa-user"></i> <span>Data User</span></a></li>
                <li><a href="<?= base_url('login/logout'); ?>"><i class="fa fa-sign-in"></i> <span>Logout</span></a></li>

            </ul>
            <!--sidebar nav end-->

        </div>
    </div>
    <!-- left side end-->
    
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <div class="header-section">

        <!--toggle button start-->
        <a class="toggle-btn"><i class="fa fa-bars"></i></a>
        <!--toggle button end-->

        <!--search start-->
       
        <!--search end-->

        <!--notification menu start -->
        <div class="menu-right">
            <ul class="notification-menu">
                
                <li>
                    <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= base_url('assets/images/admin.png') ?>" alt="" />
						Admin,
                        <?php echo $user ?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                        
                        <li><a href="<?= base_url('login/logout'); ?>"><i class="fa fa-sign-out"></i> Log Out</a></li>
                    </ul>
                </li>

            </ul>
        </div>
        <!--notification menu end -->

        </div>
        <!-- header section end-->

        <!-- page heading start-->
        
        <!-- page heading end-->

        <!--body wrapper start-->
        <div class="wrapper">
             <div class="row">
                <div class="col-sm-12">
				<?php echo $this->session->flashdata('pesan');?>
                <section class="panel">
                <header class="panel-heading">
                   Data User
                    <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                     </span>
                </header>
                <div class="panel-body">
                <div class="adv-table editable-table ">
                <div class="clearfix">
                    <div class="btn-group">
                        <button type="button" data-target="#modalTambah" data-toggle="modal" class="btn btn-primary">Tambah <i class="fa fa-plus" title="Update"></i></button>
                    </div>
                    
                </div>
                <div class="space15"></div>
                <table class="table table-striped table-hover table-bordered" id="editable-sample">
                <thead>
                <tr>
                    <th>No.</th>
                    <th width="200">Nama</th>
                    <th>Username</th>
					<th>Password</th>
					<th width="100">Level</th>
					<th>Last Login</th>
                    <th width="100">Opsi</th>
                </tr>
                </thead>
                <tbody>
				<?php $nomer=1; 
				$date=date_create("d-m-Y");
				?>
				<?php foreach($list as $row) { ?>
                <tr>
                    <td><?php echo $nomer; ?></td>
                    <td><?php echo $row['nama']; ?></td>
                    <td><?php echo $row['username']; ?></td>
					<td><?php echo $row['password']; ?></td>
					<td><?php echo $row['level']; ?></td>
					<td><?php echo date("d-m-Y H:i:s",strtotime($row['last_login'])); ?> WIB</td>
                    <td>
					<input type="text" id="id_user" value="<?php echo $row['iduser']; ?>" hidden="">
					<button type="button" onclick="edit(<?= $row['iduser']; ?>)" class="btn btn-success btn-sm"><i class="fa fa-edit" title="Update"></i></button>
					<a type="button" onclick="hapus(<?= $row['iduser']; ?>)" class="btn btn-danger btn-sm"><i class="fa fa-trash-o" title="Hapus"></i></a>
					</td>
                </tr>
                <?php $nomer++; } ?>
                </tbody>
                </table>
                </div>
                </div>
                </section>
                </div>
                </div>
        </div>
        <!--body wrapper end-->
		
		<!-- Modal Tambah-->
		<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalTambah" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h4 class="modal-title">Tambah User</h4>
					</div>
					<div class="modal-body">

					<form role="form" method="post" action="<?= base_url('user/tambah_user'); ?>">
						<div class="form-group">
							<label for="exampleInputPassword1">Nama</label>
							<input class="form-control" name="nama" type="text" required/>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Username</label>
							<input class="form-control " name="username" type="text" required/>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Password</label>
							<input class="form-control " name="password" type="password" required />
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Level</label>
							<input class="form-control " name="level" type="text" value="Admin" required readonly/>
						</div>

					<div class="modal-footer">
						<button type="submit" class="btn btn-success">Simpan</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
					</div>
					</form>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Modal Edit-->
		<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalEdit" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h4 class="modal-title">Edit User - <span class="edit_ket"></span></h4>
					</div>
					<div class="modal-body">

					<form role="form" method="post" action="<?= base_url('user/update_user'); ?>">
					<input type="text" name="iduser" value="" id="edit_id_user" hidden="">
						<div class="form-group">
							<label for="exampleInputPassword1">Nama</label>
							<input class="form-control" name="nama" id="edit_nama" type="text" required/>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Username</label>
							<input class="form-control " name="username" id="edit_username" type="text" required/>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Password</label>
							<input class="form-control " name="password" id="edit_password" type="password" required />
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Level</label>
							<input class="form-control " name="level" id="edit_level" type="text" value="Admin" required readonly/>
						</div>

					<div class="modal-footer">
						<button type="submit" class="btn btn-success">Simpan</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
					</div>
					</form>
					</div>
				</div>
			</div>
		</div>
		
		
		<!-- Modal hapus -->
		<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_delete" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h4 class="modal-title">Peringatan !!!</h4>
					</div>
					<div class="modal-body">
						<p class="no-margin">Apakah anda yakin menghapus data user <span id="delete_keterangan"></span> ?</p>
					</div>
					<div class="modal-footer">
						<form method="post" action="<?= base_url("user/hapus_user") ?>">
						<input type="hidden" class="form-control kd_hidden" name="id_user">
						<button type="submit" class="btn btn-success">Ya</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button>
					</form>
					</div>
				</div>
			</div>
		</div>

        <!--footer section start-->
        <footer>
            2016 &copy; MIC Transformer
        </footer>
        <!--footer section end-->


    </div>
    <!-- main content end-->
</section>

<!-- Placed js at the end of the document so the pages load faster -->
<script src="<?= base_url(); ?>assets/js/jquery-1.10.2.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/js/modernizr.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.nicescroll.js"></script>

<!--data table-->
<script type="text/javascript" src="<?= base_url(); ?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/data-tables/DT_bootstrap.js"></script>

<!--common scripts for all pages-->
<script src="<?= base_url(); ?>assets/js/scripts.js"></script>

<!--script for editable table-->
<script src="<?= base_url(); ?>assets/js/editable-table.js"></script>

<!--pickers plugins-->
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>

<!--pickers initialization-->
<script src="<?= base_url(); ?>assets/js/pickers-init.js"></script>

<script>
$(document).ready(function() {
	$(".dp-date-picker").datepicker({
		format:"dd-mm-yyyy"
	})
});
</script>

<!-- END JAVASCRIPTS -->
<script>
    jQuery(document).ready(function() {
        EditableTable.init();
    });
</script>
<script>
function edit(x){
	var nomor=x;
	$('#edit_id_user').val(nomor);
	
	$.ajax({
		url : "<?= base_url("user/cekUser") ?>",
		type: "get",
		data: "id="+nomor,
		dataType: "json",
		success: function(data){
			$("#edit_nama").val(data.nama);
			$("#edit_username").val(data.username);
			$("#edit_password").val(data.password);
			$("#edit_level").val(data.level);
			
			$(".edit_ket").html(data.username);
		}
	});
	$("#modalEdit").modal("show");
}

function hapus(x){
	var nomor = x;
	$.ajax({
	type  : "get",
	url   : "<?= base_url("user/cekUser") ?>",
	data  : "id="+nomor,
	dataType: "json",
	success : function(data){
	  $("#delete_keterangan").html(data.nama);
	}
	});
	$("input.kd_hidden").val(nomor);
	$("#modal_delete").modal("show");
	
}

function getdata() {
	var nama = $("select[name=nama_jabatan]").val();
		if (nama=="Kepala Dinas Tenaga Kerja dan Transmigrasi")
		{
			//var nama_kep 	= "Dr. Ir. H. MOCH. SALEH UDIN, MM.";
			//var golongan 	= "Pembina Utama Muda";
			//var nip 		= "19650318 199003 1 005";
			var level		= "1";
			//alert(nama_kep);
			//$("input[name=nama_pejabat]").val(nama_kep);
			//$("input[name=golongan]").val(golongan);
			//$("input[name=nip]").val(nip);
			$("select[name=level]").val(level);
		}
		else if (nama=="Kabid Perlindungan Tenaga Kerja")
		{
			//var nama_kep 	= "Drs. H. IMAM DJUPRIADI, MM.";
			//var golongan 	= "Pembina Tk. I";
			//var nip 		= "19610414 198603 1 023";
			var level		= "2";
			//alert(nama_kep);
			//$("input[name=nama_pejabat]").val(nama_kep);
			//$("input[name=golongan]").val(golongan);
			//$("input[name=nip]").val(nip);
			$("select[name=level]").val(level);
		}
		else if (nama=="Kasi Jamsostek")
		{
			//var nama_kep 	= "MAHDI WIDJOJO KUSUMO, SE.";
			//var golongan 	= "Penata Tk. I";
			//var nip 		= "19701122 199803 1 002";
			var level		= "3";
			//alert(nama_kep);
			//$("input[name=nama_pejabat]").val(nama_kep);
			//$("input[name=golongan]").val(golongan);
			//$("input[name=nip]").val(nip);
			$("select[name=level]").val(level);
		}
}	

</script>

</body>
</html>
