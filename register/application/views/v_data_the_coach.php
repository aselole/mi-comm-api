<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">
  <link href="<?=base_url();?>assets/images/logo-mic.png" rel='shortcut icon'>
	
  <title>MIC Database Management - Data The Coach</title>

  <!--data table-->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/js/data-tables/DT_bootstrap.css" />

  <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/css/style-responsive.css" rel="stylesheet">
<!--pickers css-->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-datepicker/css/datepicker-custom.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-timepicker/css/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-colorpicker/css/colorpicker.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-datetimepicker/css/datetimepicker-custom.css" />
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="sticky-header">

<section>
    <!-- left side start-->
    <div class="left-side sticky-left-side">

        <!--logo and iconic logo start-->
        <div class="logo">
            <a href="<?=base_url("dashboard/index") ?>"><img src="<?= base_url(); ?>assets/images/logo-mic.png" alt=""></a>
        </div>

        <div class="logo-icon text-center">
            <a href="<?=base_url("dashboard/index") ?>"><img src="<?= base_url(); ?>assets/images/logo-mic.png" alt=""></a>
        </div>
        <!--logo and iconic logo end-->


        <div class="left-side-inner">

            <!-- visible to small devices only -->
            <div class="visible-xs hidden-sm hidden-md hidden-lg">
                <div class="media logged-user">
                    <img alt="" src="<?= base_url(); ?>assets/images/admin.png" class="media-object">
                    <div class="media-body">
                        Admin,
                        <?php echo $user ?>
                    </div>
                </div>

                <ul class="nav nav-pills nav-stacked custom-nav">
                 
                  <li><a href="<?=base_url('login/logout');?>"><i class="fa fa-sign-out"></i><span>Logout</span></a></li>
                </ul>
            </div>

            <!--sidebar nav start-->
            <ul class="nav nav-pills nav-stacked custom-nav">
                <li><a href="<?= base_url('dashboard/index'); ?>"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>  
                <li class="menu-list active"><a href="#"><i class="fa fa-laptop"></i> <span>Data Anggota</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="<?= base_url('data_mi_comm/data_anggota'); ?>"> Mi-Comm</a></li>
                        <li class="active"><a href="<?= base_url('data_the_coach/data_anggota'); ?>"> The-Coach</a></li>
						<li><a href="<?= base_url('data_yes/data_anggota'); ?>"> Yes</a></li>
                    </ul>
                </li>
				
				<li class="menu-list"><a href="#"><i class="fa fa-calendar"></i> <span> Birthday</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="<?= base_url('data_birthday_micomm/data_anggota'); ?>"> Mi-Comm</a></li>
                        <li><a href="<?= base_url('data_birthday_thecoach/data_anggota'); ?>"> The-Coach</a></li>
						<li><a href="<?= base_url('data_birthday_yes/data_anggota'); ?>"> Yes</a></li>
                    </ul>
                </li>
				<li class="menu-list"><a href="#"><i class="fa fa-folder-open"></i> <span>Data Master</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="<?= base_url('data_perusahaan_micomm/index'); ?>"> Perusahaan Mi-Comm</a></li>
						<li><a href="<?= base_url('data_perusahaan_thecoach/index'); ?>"> Perusahaan The-Coach</a></li>
						<li><a href="<?= base_url('data_perusahaan_yes/index'); ?>"> Perusahaan Yes</a></li>
						<li><a href="<?= base_url('data_training'); ?>"> Training </a></li>
                        <li><a href="<?= base_url('data_event/index'); ?>"> Event</a></li>
                    </ul>
                </li>
				<li><a href="<?= base_url('user/index'); ?>"><i class="fa fa-user"></i> <span>Data User</span></a></li>
                <li><a href="<?= base_url('login/logout'); ?>"><i class="fa fa-sign-in"></i> <span>Logout</span></a></li>
            </ul>
            <!--sidebar nav end-->

        </div>
    </div>
    <!-- left side end-->
    
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <div class="header-section">

        <!--toggle button start-->
        <a class="toggle-btn"><i class="fa fa-bars"></i></a>
        <!--toggle button end-->

        <!--search start-->
       
        <!--search end-->

		<!--notification menu start -->
        <div class="menu-right">
            <ul class="notification-menu">
                
                <li>
                    <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= base_url('assets/images/admin.png') ?>" alt="" />
						Admin,
                        <?php echo $user ?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                        
                        <li><a href="<?= base_url('login/logout'); ?>"><i class="fa fa-sign-out"></i> Log Out</a></li>
                    </ul>
                </li>

            </ul>
        </div>
        <!--notification menu end -->

        </div>
        <!-- header section end-->

        <!-- page heading start-->
        
        <!-- page heading end-->

        <!--body wrapper start-->
        <div class="wrapper">
             <div class="row">
                <div class="col-sm-12">
				<?php echo $this->session->flashdata('pesan');?>
                <section class="panel">
                <header class="panel-heading">
                   Data Anggota The-Coach
                     <span class="tools pull-right">
						<a href="<?= base_url('data_the_coach/data_anggota'); ?>" class="fa fa-refresh"></a>
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                     </span>
                </header>
                <div class="panel-body">
                <div class="adv-table editable-table ">
                <div class="clearfix">
				<div class="row">
					<div class="col-sm-6">
                    <div class="btn-group">
                        <a href="<?= base_url('data_the_coach/input_data'); ?>" class="btn btn-primary">
                            Tambah Data <i class="fa fa-plus"></i>
                        </a>
                    </div>
					<div class="btn-group">
                        <a data-target="#modalExcel" data-toggle="modal" class="btn btn-success">
							Import Excel <i class="fa fa-file"></i>
						</a>
                    </div>
					<div class="btn-group">
                        <a href="<?=base_url("export_thecoach/export") ?>" class="btn btn-warning">
							Export Excel <i class="fa fa-file-text"></i>
						</a>
                    </div>
					</div>
					<div class="col-sm-6">
						<form action="<?= base_url('data_the_coach/pencarian'); ?>" method="post">
						
						<div class="btn-group pull-right">
							<button type="submit" class="btn btn-info">
								<i class="fa fa-search"></i>
							</button>
						</div>
						<div class="btn-group pull-right">
							<input type="text" class="form-control" name="cari" placeholder="Pencarian" />
							
						</div>
						</form>
					</div> 
				</div>
					</br>
                    
                </div>
                <div class="space15"></div>
                <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th width="60">ID</th>
					<th width="100">No. Anggota</th>
                    <th width="160">Nama Anggota</th>
					<th width="100">Email</th>
                    <th width="100">Telepon</th>
					
                    <th width="80">Opsi</th>
                </tr>
                </thead>
                <tbody>
				
				<?php foreach($list as $row) { ?>
                <tr>
                    <td><?php echo $row['id_thecoach']; ?></td>
					<td><?php echo $row['no_anggota']; ?></td>
                    <td><?php echo $row['nama_anggota']; ?></td>
					<td><?php echo $row['email']; ?></td>
					<td><?php echo $row['tlp']; ?></td>
					
					<td>
					<button type="button" data-target="#modalView" data-toggle="modal" onclick="view(<?= $row['id_thecoach'] ?>)" class="btn btn-info btn-sm"><i class="fa fa-search" title="View"></i></button>
					 <button type="button" data-target="#modalEdit" data-toggle="modal" onclick="edit(<?= $row['id_thecoach'] ?>)" class="btn btn-success btn-sm"><i class="fa fa-edit" title="Update"></i></button>
					<a type="button" data-target="#modalHapus" data-toggle="modal" onclick="hapus(<?= $row['id_thecoach']; ?>)" class="btn btn-danger btn-sm"><i class="fa fa-trash-o" title="Hapus"></i> </a>
					</td>
                </tr>
                <?php } ?>
                </tbody>
                </table>
				<div class="form-group">
					<?php foreach ($total as $data) { ?>
					<div class="pull-left">
						<button data-original-title="Tooltip on top" type="button" class="btn btn-default tooltips" data-toggle="tooltip" data-placement="top" title=""><?php echo $data->jumlah ?> entries</button>
							
					 </div>			
					<?php } ?>
					
					<div class="pull-right">
					<?php
						echo $this->pagination->create_links();
					?>
					</div>
				</div>
                </div>
                </section>
                </div>
                </div>
        </div>
        <!--body wrapper end-->
		<!-- Modal Edit-->
		<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalEdit" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h4 class="modal-title">Edit Anggota The-Coach - <span class="edit_ket"></span></h4>
					</div>
					<div class="modal-body">

					<form role="form" method="post" action="<?= base_url('data_the_coach/update_thecoach'); ?>">
					<input type="text" name="id_thecoach" value="" id="edit_id_thecoach" hidden="">
					
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Pilih Perusahaan</label>
							   <select required id="edit_perusahaan" name="perusahaan" class="form-control selectpicker" data-live-search="true" title="--Pilih Perusahaan--">
									<option value="0">-- Pilih Perusahaan --</option>
									<?php foreach ($perusahaan as $data) {
										echo "<option value='".$data['nama_perusahaan']."'>".$data['nama_perusahaan']."</option>";
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Nomor Anggota</label>
							  <input required class="form-control" id="edit_no_anggota" name="no_anggota" type="text" required placeholder="Nomor anggota"/>
							</div>
						</div>
						
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Nama Lengkap</label>
							   <input required class=" form-control" id="edit_nama_anggota" name="nama_anggota" type="text" placeholder="Nama lengkap"/>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Tanggal Lahir Anggota </label>
							   <input type="text" class="span2 form-control" value="" placeholder="Tanggal lahir anggota" id="edit_tgllahir_anggota" name="tgllahir_anggota">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Nama Pasangan</label>
							   <input required class=" form-control" id="edit_nama_pasangan" name="nama_pasangan" type="text" placeholder="Nama pasangan"/>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Tanggal Lahir Pasangan</label>
							   <input type="text" class="span2 form-control" value="" placeholder="Tanggal lahir anggota" id="edit_tgllahir_pasangan" name="tgllahir_pasangan">
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Alamat Anggota</label>
							   <input class=" form-control" id="edit_alamat" name="alamat" type="text" placeholder="Alamat"/>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Kota</label>
							   <input type="text" class="span2 form-control" value="" placeholder="Kota" id="edit_kota" name="kota">
							</div>
						</div>
					</div>
					<div class="row">	
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Agama</label>
							   <input class=" form-control" id="edit_agama" name="agama" type="text" placeholder="Agama"/>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Pendidikan Terakhir</label>
							   <input class="form-control" id="edit_pendidikan" placeholder="Pendidikan terakhir" name="pendidikan" type="text" >
							</div>
						</div>
					</div>
					
					
					
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Nomor Telepon</label>
							  <input class=" form-control" id="edit_telepon" name="telepon" type="text" placeholder="Nomor telepon"/>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Nomor Handphone</label>
							   <input type="text" class="form-control" id="edit_no_hp" placeholder="Nomor handphone" name="no_hp">
							</div>
						</div>
						
					</div>	
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
							   <label for="exampleInputEmail1">Nomor Handphone 2</label>
							   <input class="form-control" id="edit_no_hp2" name="no_hp2" type="text" placeholder="Nomor handphone 2"/>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							   <label for="exampleInputEmail1">PIN BBM</label>
							   <input type="text" class=" form-control" id="edit_bbm" placeholder="PIN BBM" name="bbm">
							</div>
						</div>
                                                <div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Nama Group WA</label>
							   <input type="text" class=" form-control" id="edit_wa" placeholder="Nama Group WA" name="group_wa">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Email</label>
							   <input class="form-control" id="edit_email" name="email" type="email" placeholder="E-mail"/>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Kegiatan</label>
							   <input class="form-control" id="edit_kegiatan" placeholder="Kegiatan" name="kegiatan" type="text">
							</div>
						</div>
						
					</div>
					
					

					<div class="modal-footer">
						<button type="submit" class="btn btn-success">Simpan</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
					</div>
					</form>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Modal View-->
		<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalView" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h4 class="modal-title">Detail Anggota The-Coach - <span class="view_ket"></span></h4>
					</div>
					<input type="text" name="id_micomm" value="" class="view_id_micomm" hidden="">
					<div class="modal-body">
					<header class="panel-heading">
						Identitas
					</header>
						<table class="table table-bordered table-striped table-condensed">
							
							<tbody>
							<tr>
								<td>Perusahaan</td>
								<td><span class="view_perusahaan"></span></td>
							</tr>
							<tr>
								<td width="350">Nomor Anggota</td>
								<td><span class="view_no_anggota"></span></td>
							</tr>
							<tr>
								<td>Nama Anggota</td>
								<td><span class="view_nama_anggota"></span></td>
							</tr>
							<tr>
								
								<td>Tanggal Lahir Anggota</td>
								<td><span class="view_tgllahir_anggota"></span></td>
							</tr>
							<tr>
								<td>Nama Pasangan</td>
								<td><span class="view_nama_pasangan"></span></td>
							</tr>
							<tr>
								
								<td>Tanggal Lahir Pasangan</td>
								<td><span class="view_tgllahir_pasangan"></span></td>
							</tr>
							<tr>
								<td>Alamat Anggota</td>
								<td><span class="view_alamat"></span></td>
							</tr>
							<tr>
								<td>Kota</td>
								<td><span class="view_kota"></span></td>
							</tr>
							<tr>
								<td>Agama</td>
								<td><span class="view_agama"></span></td>
							</tr>
							<tr>
								<td>Pendidikan terakhir</td>
								<td><span class="view_pendidikan"></span></td>
							</tr>
							<tr>
								<td>Nomor Telepon</td>
								<td><span class="view_telepon"></span></td>
							</tr>
							<tr>
								<td>Nomor Handphone</td>
								<td><span class="view_hp1"></span></td>
							</tr>
							<tr>
								<td>Nomor Handphone 2</td>
								<td><span class="view_hp2"></span></td>
							</tr>
							<tr>
								<td>PIN BBM</td>
								<td><span class="view_bbm"></span></td>
								
							</tr>
                                                        <tr>
								<td>Nama Group WA</td>
								<td><span class="view_wa"></span></td>
								
							</tr>
							<tr>
							   
								<td>E-mail</td>
								<td><span class="view_email"></span></td>
							</tr>
							<tr>
								<td>Kegiatan</td>
								<td><span class="view_kegiatan"></span></td>
							</tr>
							
							
							</tbody>
						</table>
					

					<div class="modal-footer">
						<button type="button" class="btn btn-info" data-dismiss="modal">Tutup</button>
					</div>
					</form>
					</div>
				</div>
			</div>
		</div>
		
		
		
		
		
		<!-- Modal hapus -->
		<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalHapus" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h4 class="modal-title">Peringatan !!!</h4>
					</div>
					<div class="modal-body">

					<p class="no-margin">Apakah anda yakin menghapus data anggota The-Coach <span id="delete_keterangan"></span> ?</p>

					<div class="modal-footer">
						<form role="form" method="post" action="<?= base_url('data_the_coach/hapus_thecoach'); ?>">
						<input type="hidden" class="form-control kd_hidden" name="id_thecoach">
						<button type="submit" class="btn btn-success">Iya</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button>
					</div>
					</form>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Modal Excel-->
		<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalExcel" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h4 class="modal-title">Import Excel</h4>
					</div>
					
					<div class="modal-body">
					<form class="form-horizontal" action="<?= base_url('data_the_coach/do_upload') ?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<div class="col-md-5 col-sm-6">
								<label>Pilih file excel yang diupload</label>
							</div>
							<div class="col-md-3 col-sm-6">
                                <input class="btn" type="file" name="userfile" id="exampleInputFile">
							</div>
                        </div>

					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-success">Upload</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
						
					</div>
					</form>
				</div>
			</div>
		</div>
		<!-- modal -->
        <!--footer section start-->
        <footer>
            2016 &copy; MIC Transformer
        </footer>
        <!--footer section end-->


    </div>
    <!-- main content end-->
</section>

<!-- Placed js at the end of the document so the pages load faster -->
<script src="<?= base_url(); ?>assets/js/jquery-1.10.2.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/js/modernizr.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.nicescroll.js"></script>

<!--data table-->
<script type="text/javascript" src="<?= base_url(); ?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/data-tables/DT_bootstrap.js"></script>

<!--common scripts for all pages-->
<script src="<?= base_url(); ?>assets/js/scripts.js"></script>

<!--script for editable table-->
<script src="<?= base_url(); ?>assets/js/editable-table.js"></script>

<!--pickers plugins-->
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>

<!--pickers initialization-->
<script src="<?= base_url(); ?>assets/js/pickers-init.js"></script>

<script>
	if (top.location != location) {
    top.location.href = document.location.href ;
  }
		$(function(){
			window.prettyPrint && prettyPrint();
			$('input[name="tgllahir_anggota"]').datepicker({
				format: 'dd-mm-yyyy'
			});
			$('input[name="tgllahir_pasangan"]').datepicker({
				format: 'dd-mm-yyyy'
			});
			$('input[name="tgl_lahir"]').datepicker({
				format: 'dd-mm-yyyy'
			});
			$('#dpYears').datepicker();
			$('#dpMonths').datepicker();
			
			
			var startDate = new Date(2012,1,20);
			var endDate = new Date(2012,1,25);
			$('#dp4').datepicker()
				.on('changeDate', function(ev){
					if (ev.date.valueOf() > endDate.valueOf()){
						$('#alert').show().find('strong').text('The start date can not be greater then the end date');
					} else {
						$('#alert').hide();
						startDate = new Date(ev.date);
						$('#startDate').text($('#dp4').data('date'));
					}
					$('#dp4').datepicker('hide');
				});
			$('#dp5').datepicker()
				.on('changeDate', function(ev){
					if (ev.date.valueOf() < startDate.valueOf()){
						$('#alert').show().find('strong').text('The end date can not be less then the start date');
					} else {
						$('#alert').hide();
						endDate = new Date(ev.date);
						$('#endDate').text($('#dp5').data('date'));
					}
					$('#dp5').datepicker('hide');
				});

        // disabling dates
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#dpd1').datepicker({
          onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
          }
          checkin.hide();
          $('#dpd2')[0].focus();
        }).data('datepicker');
        var checkout = $('#dpd2').datepicker({
          onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          checkout.hide();
        }).data('datepicker');
		});
	</script>

<script>
$(document).ready(function() {
	$(".dp-date-picker").datepicker({
		format:"dd-mm-yyyy"
	})
});
</script>

<!-- END JAVASCRIPTS -->
<script>
    jQuery(document).ready(function() {
        EditableTable.init();
    });
</script>
<script>
function edit(x){
	var nomor=x;
	$('#edit_id_thecoach').val(nomor);
	
	$.ajax({
		url   : "<?= base_url("data_the_coach/cekData") ?>",
		type: "get",
		data: "id="+nomor,
		dataType: "json",
		success: function(data){
			
			$("#edit_no_anggota").val(data.no_anggota);
			$("#edit_perusahaan").val(data.perusahaan);
			$("#edit_nama_anggota").val(data.nama_anggota);
			$("#edit_tgllahir_anggota").val(data.tgllahir_anggota);
			$("#edit_nama_pasangan").val(data.nama_pasangan);
			$("#edit_tgllahir_pasangan").val(data.tgllahir_pasangan);
			$("#edit_alamat").val(data.alamat);
			$("#edit_kota").val(data.kota);
			$("#edit_telepon").val(data.tlp);
			$("#edit_no_hp").val(data.hp1);
			$("#edit_no_hp2").val(data.hp2);
			$("#edit_bbm").val(data.bbm);
			$("#edit_email").val(data.email);
			$("#edit_agama").val(data.agama);
			$("#edit_kegiatan").val(data.kegiatan);
			$("#edit_pendidikan").val(data.pendidikan);
                        $("#edit_wa").val(data.group_wa);
			$(".edit_ket").html(data.nama_anggota);
			
		}
	});
	//$("#modalEdit").modal("show");
}

function view(x){
	var nomor=x;
	$('.view_id_thecoach').val(nomor);
	
	$.ajax({
		url   : "<?= base_url("data_the_coach/getView") ?>",
		type: "get",
		data: "id="+nomor,
		dataType: "json",
		success: function(data){
			
			$(".view_no_anggota").html(data.no_anggota);
			$(".view_perusahaan").html(data.perusahaan);
			$(".view_nama_anggota").html(data.nama_anggota);
			$(".view_tgllahir_anggota").html(data.tgllahir_anggota);
			$(".view_nama_pasangan").html(data.nama_pasangan);
			$(".view_tgllahir_pasangan").html(data.tgllahir_pasangan);
			$(".view_alamat").html(data.alamat);
			$(".view_kota").html(data.kota);
			$(".view_telepon").html(data.tlp);
			$(".view_hp1").html(data.hp1);
			$(".view_hp2").html(data.hp2);
			$(".view_bbm").html(data.bbm);
			$(".view_email").html(data.email);
			$(".view_agama").html(data.agama);
			$(".view_kegiatan").html(data.kegiatan);
			$(".view_pendidikan").html(data.pendidikan);
                        $(".view_wa").html(data.group_wa);
			$(".view_ket").html(data.nama_anggota);
			
		}
	});
	
}
	
	function hapus(x){
	var nomor = x;
	$.ajax({
	type  : "get",
	url   : "<?= base_url("data_the_coach/cekData") ?>",
	data  : "id="+nomor,
	dataType: "json",
	success : function(data){
	  $("#delete_keterangan").html(data.nama_anggota);
	}
	});
	$("input.kd_hidden").val(nomor);
	//$("#modalHapus").modal("show");
	
}
	
</script>

</body>
</html>
