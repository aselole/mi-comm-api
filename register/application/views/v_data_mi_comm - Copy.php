<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">
  <link href="<?=base_url();?>assets/images/logo-mic.png" rel='shortcut icon'>
	
  <title>MIC Database Management - Data Mi-Comm</title>
	
   <!-- customize -->
  <link href="<?= base_url(); ?>assets/css_datepicker/datepicker.css" rel="stylesheet">
  
  <!--data table-->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/js/data-tables/DT_bootstrap.css" />
	
  <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/css/style-responsive.css" rel="stylesheet">
<!--pickers css-->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-datepicker/css/datepicker-custom.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-timepicker/css/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-colorpicker/css/colorpicker.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-datetimepicker/css/datetimepicker-custom.css" />
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="sticky-header">

<section>
    <!-- left side start-->
    <div class="left-side sticky-left-side">

        <!--logo and iconic logo start-->
        <div class="logo">
            <a href="index.html"><img src="<?= base_url(); ?>assets/images/logo-mic.png" alt=""></a>
        </div>

        <div class="logo-icon text-center">
            <a href="index.html"><img src="<?= base_url(); ?>assets/images/logo-mic.png" alt=""></a>
        </div>
        <!--logo and iconic logo end-->


        <div class="left-side-inner">

            <!-- visible to small devices only -->
            <div class="visible-xs hidden-sm hidden-md hidden-lg">
                <div class="media logged-user">
                    <img alt="" src="<?= base_url(); ?>assets/images/admin.png" class="media-object">
                    <div class="media-body">
                        Admin,
                        <?php echo $user ?>
                    </div>
                </div>

                <ul class="nav nav-pills nav-stacked custom-nav">
                 
                  <li><a href="<?=base_url('login/logout');?>"><i class="fa fa-sign-out"></i><span>Logout</span></a></li>
                </ul>
            </div>

            <!--sidebar nav start-->
            <ul class="nav nav-pills nav-stacked custom-nav">
                <li><a href="<?= base_url('index.php/dashboard/index'); ?>"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>  
                <li class="menu-list active"><a href="#"><i class="fa fa-laptop"></i> <span>Data Anggota</span></a>
                    <ul class="sub-menu-list">
                        <li class="active"><a href="<?= base_url('data_mi_comm/data_anggota'); ?>"> Mi-Comm</a></li>
                        <li><a href="<?= base_url('data_the_coach/data_anggota'); ?>"> The-Coach</a></li>
						<li><a href="<?= base_url('data_yes/data_anggota'); ?>"> Yes</a></li>
                    </ul>
                </li>
				
				<li class="menu-list"><a href="#"><i class="fa fa-calendar"></i> <span> Birthday</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="#"> Mi-Comm</a></li>
                        <li><a href="#"> The-Coach</a></li>
						<li><a href="#"> Yes</a></li>
                    </ul>
                </li>
				<li class="menu-list"><a href="#"><i class="fa fa-folder-open"></i> <span>Data Master</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="<?= base_url('data_perusahaan_micomm/index'); ?>"> Perusahaan Mi-Comm</a></li>
						<li><a href="<?= base_url('data_perusahaan_thecoach/index'); ?>"> Perusahaan The-Coach</a></li>
						<li><a href="<?= base_url('data_perusahaan_yes/index'); ?>"> Perusahaan Yes</a></li>
                        <li><a href="<?= base_url('data_event/index'); ?>"> Event</a></li>
                    </ul>
                </li>
				<li><a href="<?= base_url('user/index'); ?>"><i class="fa fa-user"></i> <span>Data User</span></a></li>
                <li><a href="<?= base_url('login/logout'); ?>"><i class="fa fa-sign-in"></i> <span>Logout</span></a></li>
            </ul>
            <!--sidebar nav end-->

        </div>
    </div>
    <!-- left side end-->
    
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <div class="header-section">

        <!--toggle button start-->
        <a class="toggle-btn"><i class="fa fa-bars"></i></a>
        <!--toggle button end-->

        <!--search start-->
       
        <!--search end-->

		<!--notification menu start -->
        <div class="menu-right">
            <ul class="notification-menu">
                
                <li>
                    <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= base_url('assets/images/admin.png') ?>" alt="" />
                        Admin,
                        <?php echo $user ?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                        
                        <li><a href="<?= base_url('index.php/login/logout'); ?>"><i class="fa fa-sign-out"></i> Log Out</a></li>
                    </ul>
                </li>

            </ul>
        </div>
        <!--notification menu end -->

        </div>
        <!-- header section end-->

        <!-- page heading start-->
        
        <!-- page heading end-->

        <!--body wrapper start-->
        <div class="wrapper">
             <div class="row">
                <div class="col-sm-12">
				<?php echo $this->session->flashdata('pesan');?>
                <section class="panel">
                <header class="panel-heading">
                   Data Anggota Mi-Comm
                     <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                     </span>
                </header>
                <div class="panel-body">
                <div class="adv-table editable-table ">
                <div class="clearfix">
                    <div class="btn-group">
                        <a href="<?= base_url('data_mi_comm/input_data'); ?>" class="btn btn-primary">
                            Tambah Data <i class="fa fa-plus"></i>
                        </a>
                    </div>
					<div class="btn-group">
						<a href="#" data-toggle="modal" class="btn btn-success">
                            Import Excel <i class="fa fa-file"></i>
                        </a>
                    </div>
					<div class="btn-group pull-right">
                        <form action="index.html" method="post">
							<input type="text" class="form-control" name="cari" placeholder="Pencarian" />
						</form>
                    </div>
					</br></br>
                    
                </div>
                <div class="space15"></div>
                <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th width="60">ID</th>
                    <th width="220">Nama Lengkap</th>
					<th width="150">Perusahaan</th>
                    <th width="150">Handphone</th>
                    <th width="20">E-mail Pribadi</th>
                    <th width="110">Opsi</th>
                </tr>
                </thead>
                <tbody>
				
				<?php foreach($list as $row) { ?>
                <tr>
                    <td><?php echo $row['id_micomm']; ?></td>
					<td><?php echo $row['nama_lengkap']; ?></td>
                    <td><?php echo $row['perusahaan']; ?></td>
					<!--<td><?php echo $row['alamat']; ?></td>-->
					<td><?php echo $row['no_hp']; ?></td>
					<td><?php echo $row['email_pribadi']; ?></td>
					<td>
					 <button type="button" data-target="#modalView" data-toggle="modal" onclick="view(<?= $row['id_micomm'] ?>)" class="btn btn-info btn-sm"><i class="fa fa-search" title="View"></i></button>
					 <button type="button" data-target="#modalEdit" data-toggle="modal" onclick="edit(<?= $row['id_micomm'] ?>)" class="btn btn-success btn-sm"><i class="fa fa-edit" title="Update"></i></button>
					<a type="button" data-target="#modalHapus" data-toggle="modal" onclick="hapus(<?= $row['id_micomm']; ?>)" class="btn btn-danger btn-sm"><i class="fa fa-trash-o" title="Hapus"></i> </a>
					</td>
                </tr>
                <?php } ?>
                </tbody>
                </table>
				<div class="form-group">
					<?php foreach ($total as $data) { ?>
					<div class="pull-left">
						<button data-original-title="Tooltip on top" type="button" class="btn btn-default tooltips" data-toggle="tooltip" data-placement="top" title=""><?php echo $data->jumlah ?> entries</button>
							
					 </div>			
					<?php } ?>
					
					<div class="pull-right">
					<?php
						echo $this->pagination->create_links();
					?>
					</div>
				</div>
                </div>
                </section>
                </div>
                </div>
        </div>
        <!--body wrapper end-->
		<!-- Modal Edit-->
		<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalEdit" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h4 class="modal-title">Edit Anggota Mi-Comm - <span class="edit_ket"></span></h4>
					</div>
					<div class="modal-body">

					<form role="form" method="post" action="<?= base_url('data_mi_comm/update_micomm'); ?>">
					<input type="text" name="id_micomm" value="" id="edit_id_micomm" hidden="">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1"><strong>Identitas</strong></label>
							</div>
						</div>
						
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Event</label>
							   <select id="edit_event" name="event" class="form-control selectpicker" data-live-search="true" title="--Pilih Event--">
									<option value="0">-- Pilih Event --</option>
									<?php foreach ($event as $event) {
										echo "<option value='".$event['id_event']."'>".$event['nama_event']."</option>";
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Perusahaan</label>
								<select id="edit_perusahaan" name="perusahaan" class="form-control" data-live-search="true" title="--Pilih Perusahaan--">
								<option value="">-- Pilih Perusahaan --</option>
									<?php foreach ($perusahaan as $data) {
										echo "<option value='".$data['nama_perusahaan']."'>".$data['nama_perusahaan']."</option>";
									}
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Tempat Lokasi</label>
							  <input class="form-control" id="edit_lokasi" name="lokasi" type="text" placeholder="Tempat lokasi"/>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							   <label for="exampleInputEmail1">Tanggal Mulai</label>
							   <input type="text" class="span2 form-control" value="" placeholder="Tanggal mulai" id="edit_tanggal_mulai" name="tanggal_mulai">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							   <label for="exampleInputEmail1">Tanggal Selesai</label>
							   <input type="text" class="span2 form-control" value="" placeholder="Tanggal Selesai" id="edit_tanggal_akhir" name="tanggal_selesai">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Nama Lengkap</label>
							  <input class="form-control" id="edit_nama_lengkap" name="nama_lengkap" type="text" required placeholder="Nama lengkap"/>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Nama Panggilan</label>
							   <input class=" form-control" id="edit_nama_panggilan" name="nama_panggilan" type="text" placeholder="Nama panggilan"/>
							</div>
						</div>
					</div>
					<div class="row">
						
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Tempat Lahir</label>
							   <input class=" form-control" id="edit_tempat_lahir" name="tempat_lahir" type="text" placeholder="Tempat lahir"/>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Tanggal lahir</label>
							   <input type="text" class="span2 form-control" value="" placeholder="Tanggal lahir" id="edit_tgl_lahir" name="tgl_lahir">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Agama</label>
							  <input class=" form-control" id="edit_agama" name="agama" type="text" required placeholder="Agama"/>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Alamat</label>
							   <input class=" form-control" id="edit_alamat" placeholder="Alamat" name="alamat">
							</div>
						</div>
						
					</div>	
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Nomor Telepon</label>
							   <input class=" form-control" id="edit_telepon" name="telepon" type="text" placeholder="Nomor telepon"/>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							   <label for="exampleInputEmail1">Nomor Handphone</label>
							   <input class=" form-control" id="edit_no_hp" placeholder="Nomor Handphone" required name="no_hp">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							   <label for="exampleInputEmail1">Nomor Handphone 2</label>
							   <input class=" form-control" id="edit_no_hp2" name="no_hp2" type="text" placeholder="Nomor Handphone 2"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Email Pribadi</label>
							   <input class=" form-control" id="edit_email_pribadi" name="email_pribadi" type="email" placeholder="E-mail pribadi"/>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							   <label for="exampleInputEmail1">Nama Group WA</label>
							   <input class=" form-control" id="edit_group_wa" placeholder="Nama Group WA" name="group_wa">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							   <label for="exampleInputEmail1">Akun Instagram</label>
							   <input class=" form-control" id="edit_instagram" placeholder="Akun Instagram" name="instagram" type="text">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
							   <label for="exampleInputEmail1">PIN BBM</label>
							   <input class=" form-control" id="edit_bbm" placeholder="PIN BBM" name="pin_bb">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							   <label for="exampleInputEmail1">ID Line</label>
							   <input class=" form-control" id="edit_line" placeholder="ID Line" name="id_line">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							   <label for="exampleInputEmail1">ID Facebook</label>
							   <input class=" form-control" id="edit_facebook" placeholder="ID Facebook" name="id_facebook">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							   <label for="exampleInputEmail1">ID Twitter</label>
							   <input class=" form-control" id="edit_twitter" name="id_twitter" type="text" placeholder="ID Twitter"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1"><strong>Perusahaan</strong></label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
							   <label for="exampleInputEmail1">Alamat Penempatan</label>
							   <input class=" form-control" id="edit_alamat_penempatan" placeholder="Alamat penempatan" name="alamat_penempatan">
							</div>
						</div>
						
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Email Kantor</label>
							   <input class=" form-control" id="edit_email_kantor" name="email_kantor" type="email" placeholder="E-mail kantor"/>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							   <label for="exampleInputEmail1">Posisi</label>
							   <input class=" form-control" id="edit_posisi" name="posisi" type="text" placeholder="Posisi"/>
							</div>
						</div> 
					</div>

					<div class="modal-footer">
						<button type="submit" class="btn btn-success">Simpan</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
					</div>
					</form>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Modal View-->
		<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalView" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h4 class="modal-title">Detail Anggota Mi-Comm - <span class="view_ket"></span></h4>
					</div>
					<input type="text" name="id_micomm" value="" class="view_id_micomm" hidden="">
					<div class="modal-body">
					<header class="panel-heading">
						Identitas
					</header>
						<table class="table table-bordered table-striped table-condensed">
							
							<tbody>
							<tr>
								<td width="350">Event</td>
								<td><span class="view_event"></span></td>
							</tr>
							<tr>
								<td>Pelatihan</td>
								<td><span class="view_training"></span></td>
							</tr>
							<tr>
								<td>Lokasi</td>
								<td><span class="view_lokasi"></span></td>
							</tr>
							<tr>
								<td>Tanggal Mulai</td>
								<td><span class="view_tgl_mulai"></span></td>
								
							</tr>
							<tr>
								
								<td>Tanggal Selesai</td>
								<td><span class="view_tgl_selesai"></span></td>
							</tr>
							<tr>
								<td>Nama Lengkap</td>
								<td><span class="view_nama_lengkap"></span></td>
							</tr>
							<tr>
								<td>Nama Panggilan</td>
								<td><span class="view_nama_panggilan"></span></td>
							</tr>
							<tr>
								<td>Tempat Lahir</td>
								<td><span class="view_tempat_lahir"></span></td>
								
							</tr>
							<tr>
							   
								<td>Tanggal Lahir</td>
								<td><span class="view_tgl_lahir"></span></td>
							</tr>
							<tr>
								<td>Agama</td>
								<td><span class="view_agama"></span></td>
							</tr>
							<tr>
								<td>Alamat</td>
								<td><span class="view_alamat"></span></td>
							</tr>
							<tr>
								<td>Nomor telepon</td>
								<td><span class="view_telepon"></span></td>
							</tr>
							<tr>
								<td>Nomor Handphone</td>
								<td><span class="view_no_hp"></span></td>
								
							</tr>
							<tr>
							   
								<td>Nomor Handphone 2</td>
								<td><span class="view_no_hp2"></span></td>
							</tr>
							<tr>
								<td>E-mail Pribadi</td>
								<td><span class="view_email_pribadi"></span></td>
							</tr>
							<tr>
								<td>Nama Group Wa</td>
								<td><span class="view_group_wa"></span></td>
							</tr>
							<tr>
								<td>Akun Instagram</td>
								<td><span class="view_instagram"></span></td>
							</tr>
							<tr>
								<td>PIN BBM</td>
								<td><span class="view_bbm"></span></td>
							</tr>
							<tr>
								<td>ID Line</td>
								<td><span class="view_line"></span></td>
							</tr>
							<tr>
								<td>Akun Facebook</td>
								<td><span class="view_facebook"></span></td>
							</tr>
							<tr>
								<td>Akun Twitter</td>
								<td><span class="view_twitter"></span></td>
							</tr>
							</tbody>
						</table>
					<header class="panel-heading">
						Perusahaan
					</header>
					<table class="table table-bordered table-striped table-condensed">
						<tbody>
						<tr>
                            <td width="350">Nama Perusahaan</td>
                            <td><span class="view_perusahaan"></span></td>
                        </tr>
						<tr>
                            <td>Alamat Penempatan</td>
                            <td><span class="view_alamat_penempatan"></span></td>
                        </tr>
						<tr>
                            <td>E-mail Kantor</td>
                            <td><span class="view_email_kantor"></span></td>
                        </tr>
						<tr>
                            <td>Posisi</td>
                            <td><span class="view_posisi"></span></td>
                        </tr>
                        
                        </tbody>
                    </table>

					<div class="modal-footer">
						<button type="button" class="btn btn-info" data-dismiss="modal">Tutup</button>
					</div>
					</form>
					</div>
				</div>
			</div>
		</div>
		
		
		<!-- Modal hapus -->
		<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalHapus" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h4 class="modal-title">Peringatan !!!</h4>
					</div>
					<div class="modal-body">

					<p class="no-margin">Apakah anda yakin menghapus data anggota Mi-Comm <span id="delete_keterangan"></span> ?</p>

					<div class="modal-footer">
						<form role="form" method="post" action="<?= base_url('data_mi_comm/hapus_micomm'); ?>">
						<input type="hidden" class="form-control kd_hidden" name="id_micomm">
						<button type="submit" class="btn btn-success">Iya</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button>
					</div>
					</form>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Modal Excel-->
		<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalExcel" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h4 class="modal-title">Import Excel</h4>
					</div>
					<form class="form-horizontal" action="<?= base_url('data_mi_comm/do_upload') ?>" method="post" enctype="multipart/form-data">
					<div class="modal-body">

						<div class="form-group">
							<div class="col-md-5 col-sm-6">
								<label>Pilih file excel yang diupload</label>
							</div>
							<div class="col-md-3 col-sm-6">
                                <input  type="file" name="file" class="default" />
							</div>
                        </div>

					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-success">Upload</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
						
					</div>
					</form>
				</div>
			</div>
		</div>
		<!-- modal -->

        <!--footer section start-->
        <footer>
            2016 &copy; MIC Transformer
        </footer>
        <!--footer section end-->


    </div>
    <!-- main content end-->
</section>

<!-- Placed js at the end of the document so the pages load faster -->
<script src="<?= base_url(); ?>assets/js/jquery-1.10.2.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/js/modernizr.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.nicescroll.js"></script>

<!--data table-->
<script type="text/javascript" src="<?= base_url(); ?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/data-tables/DT_bootstrap.js"></script>

<!--common scripts for all pages-->
<script src="<?= base_url(); ?>assets/js/scripts.js"></script>

<!--script for editable table-->
<script src="<?= base_url(); ?>assets/js/editable-table.js"></script>

<!--pickers plugins-->
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="<?= base_url(); ?>assets/js_datepicker/bootstrap-datepicker.js"></script>

<!--pickers initialization-->
<script src="<?= base_url(); ?>assets/js/pickers-init.js"></script>

<script>
	if (top.location != location) {
    top.location.href = document.location.href ;
  }
		$(function(){
			window.prettyPrint && prettyPrint();
			$('input[name="tanggal_mulai"]').datepicker({
				format: 'dd-mm-yyyy'
			});
			$('input[name="tanggal_akhir"]').datepicker({
				format: 'dd-mm-yyyy'
			});
			$('input[name="tgl_lahir"]').datepicker({
				format: 'dd-mm-yyyy'
			});
			$('#dpYears').datepicker();
			$('#dpMonths').datepicker();
			
			
			var startDate = new Date(2012,1,20);
			var endDate = new Date(2012,1,25);
			$('#dp4').datepicker()
				.on('changeDate', function(ev){
					if (ev.date.valueOf() > endDate.valueOf()){
						$('#alert').show().find('strong').text('The start date can not be greater then the end date');
					} else {
						$('#alert').hide();
						startDate = new Date(ev.date);
						$('#startDate').text($('#dp4').data('date'));
					}
					$('#dp4').datepicker('hide');
				});
			$('#dp5').datepicker()
				.on('changeDate', function(ev){
					if (ev.date.valueOf() < startDate.valueOf()){
						$('#alert').show().find('strong').text('The end date can not be less then the start date');
					} else {
						$('#alert').hide();
						endDate = new Date(ev.date);
						$('#endDate').text($('#dp5').data('date'));
					}
					$('#dp5').datepicker('hide');
				});

        // disabling dates
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#dpd1').datepicker({
          onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
          }
          checkin.hide();
          $('#dpd2')[0].focus();
        }).data('datepicker');
        var checkout = $('#dpd2').datepicker({
          onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          checkout.hide();
        }).data('datepicker');
		});
	</script>

<!-- END JAVASCRIPTS -->
<script>
    jQuery(document).ready(function() {
        EditableTable.init();
    });
</script>
<script>
function edit(x){
	var nomor=x;
	$('#edit_id_micomm').val(nomor);
	
	$.ajax({
		url   : "<?= base_url("data_mi_comm/cekData") ?>",
		type: "get",
		data: "id="+nomor,
		dataType: "json",
		success: function(data){
			
			$("#edit_event").val(data.id_event);
			$("#edit_training").val(data.training);
			$("#edit_lokasi").val(data.lokasi);
			$("#edit_tanggal_mulai").val(data.tanggal_mulai);
			$("#edit_tanggal_selesai").val(data.tanggal_selesai);
			$("#edit_tempat_tanggal").val(data.tempat_tanggal);
			$("#edit_nama_lengkap").val(data.nama_lengkap);
			$("#edit_nama_panggilan").val(data.nama_panggilan);
			$("#edit_tempat_lahir").val(data.tempat_lahir);
			$("#edit_tgl_lahir").val(data.tgl_lahir);
			$("#edit_agama").val(data.agama);
			$("#edit_alamat").val(data.alamat);
			$("#edit_telepon").val(data.no_telepon);
			$("#edit_no_hp").val(data.no_hp);
			$("#edit_no_hp2").val(data.no_hp2);
			$("#edit_email_pribadi").val(data.email_pribadi);
			$("#edit_group_wa").val(data.group_wa);
			$("#edit_instagram").val(data.instagram);
			$("#edit_bbm").val(data.pin_bb);
			$("#edit_line").val(data.id_line);
			$("#edit_facebook").val(data.id_facebook);
			$("#edit_twitter").val(data.id_twitter);
			$("#edit_perusahaan").val(data.perusahaan);
			$("#edit_email_kantor").val(data.email_kantor);
			$("#edit_alamat_penempatan").val(data.alamat_penempatan);
			$("#edit_posisi").val(data.posisi);
			$(".edit_ket").html(data.nama_lengkap);
			
		}
	});
	//$("#modalEdit").modal("show");
}

function view(x){
	var nomor=x;
	$('.view_id_micomm').val(nomor);
	
	$.ajax({
		url   : "<?= base_url("data_mi_comm/getView") ?>",
		type: "get",
		data: "id="+nomor,
		dataType: "json",
		success: function(data){
			
			$(".view_event").html(data.nama_event);
			$(".view_training").html(data.training);
			$(".view_lokasi").html(data.lokasi);
			$(".view_tgl_mulai").html(data.tanggal_mulai);
			$(".view_tgl_selesai").html(data.tanggal_selesai);
			$(".view_tempat_tanggal").html(data.tempat_tanggal);
			$(".view_nama_lengkap").html(data.nama_lengkap);
			$(".view_nama_panggilan").html(data.nama_panggilan);
			$(".view_tempat_lahir").html(data.tempat_lahir);
			$(".view_tgl_lahir").html(data.tgl_lahir);
			$(".view_agama").html(data.agama);
			$(".view_alamat").html(data.alamat);
			$(".view_telepon").html(data.no_telepon);
			$(".view_no_hp").html(data.no_hp);
			$(".view_no_hp2").html(data.no_hp2);
			$(".view_email_pribadi").html(data.email_pribadi);
			$(".view_group_wa").html(data.group_wa);
			$(".view_instagram").html(data.instagram);
			$(".view_bbm").html(data.pin_bb);
			$(".view_line").html(data.id_line);
			$(".view_facebook").html(data.id_facebook);
			$(".view_twitter").html(data.id_twitter);
			$(".view_perusahaan").html(data.perusahaan);
			$(".view_email_kantor").html(data.email_kantor);
			$(".view_alamat_penempatan").html(data.alamat_penempatan);
			$(".view_posisi").html(data.posisi);
			$(".view_ket").html(data.nama_lengkap);
			
		}
	});
	
}
	
	function hapus(x){
	var nomor = x;
	$.ajax({
	type  : "get",
	url   : "<?= base_url("data_mi_comm/cekData") ?>",
	data  : "id="+nomor,
	dataType: "json",
	success : function(data){
	  $("#delete_keterangan").html(data.nama_lengkap);
	}
	});
	$("input.kd_hidden").val(nomor);
	//$("#modalHapus").modal("show");
	
}
	
</script>

</body>
</html>
