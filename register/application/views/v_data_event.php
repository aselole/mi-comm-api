<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">
  <link href="<?=base_url();?>assets/images/logo-mic.png" rel='shortcut icon'>

  <title>MIC Database Management - Data Event</title>

  <!--data table-->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/js/data-tables/DT_bootstrap.css" />

  <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/css/style-responsive.css" rel="stylesheet">
<!--pickers css-->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-datepicker/css/datepicker-custom.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-timepicker/css/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-colorpicker/css/colorpicker.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/bootstrap-datetimepicker/css/datetimepicker-custom.css" />
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="sticky-header">

<section>
    <!-- left side start-->
    <div class="left-side sticky-left-side">

        <!--logo and iconic logo start-->
        <div class="logo">
            <a href="<?=base_url("dashboard/index") ?>"><img src="<?= base_url(); ?>assets/images/logo-mic.png" alt=""></a>
        </div>

        <div class="logo-icon text-center">
            <a href="<?=base_url("dashboard/index") ?>"><img src="<?= base_url(); ?>assets/images/logo-mic.png" alt=""></a>
        </div>
        <!--logo and iconic logo end-->


        <div class="left-side-inner">

            <!-- visible to small devices only -->
            <div class="visible-xs hidden-sm hidden-md hidden-lg">
                <div class="media logged-user">
                    <img alt="" src="<?= base_url(); ?>assets/images/admin.png" class="media-object">
                    <div class="media-body">
                        Admin,
                        <?php echo $user ?>
                    </div>
                </div>

                <ul class="nav nav-pills nav-stacked custom-nav">
                 
                  <li><a href="<?=base_url('login/logout');?>"><i class="fa fa-sign-out"></i><span>Logout</span></a></li>
                </ul>
            </div>

            <!--sidebar nav start-->
            <ul class="nav nav-pills nav-stacked custom-nav">
                <li><a href="<?= base_url('dashboard/index'); ?>"><i class="fa fa-home"></i> <span>Dashboard</span></a>  
                <li class="menu-list"><a href="#"><i class="fa fa-laptop"></i> <span>Data Anggota</span></a>
                    <ul class="sub-menu-list">
						<li><a href="<?= base_url('data_mi_comm/data_anggota'); ?>"> Mi-Comm</a></li>
                        <li><a href="<?= base_url('data_the_coach/data_anggota'); ?>"> The-Coach</a></li>
						<li><a href="<?= base_url('data_yes/data_anggota'); ?>"> Yes</a></li>
						</ul>
                </li>
		
				<li class="menu-list"><a href="#"><i class="fa fa-calendar"></i> <span> Birthday</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="<?= base_url('data_birthday_micomm/data_anggota'); ?>"> Mi-Comm</a></li>
                        <li><a href="<?= base_url('data_birthday_thecoach/data_anggota'); ?>"> The-Coach</a></li>
						<li><a href="<?= base_url('data_birthday_yes/data_anggota'); ?>"> Yes</a></li>
                    </ul>
                </li>
				<li class="menu-list"><a href="#"><i class="fa fa-folder-open"></i> <span>Data Master</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="<?= base_url('data_perusahaan_micomm/index'); ?>"> Perusahaan Mi-Comm</a></li>
						<li><a href="<?= base_url('data_perusahaan_thecoach/index'); ?>"> Perusahaan The-Coach</a></li>
						<li><a href="<?= base_url('data_perusahaan_yes/index'); ?>"> Perusahaan Yes</a></li>
						<li><a href="<?= base_url('data_training'); ?>"> Training </a></li>
                        <li><a href="<?= base_url('data_event/index'); ?>"> Event</a></li>
                    </ul>
                </li>
				<li><a href="<?= base_url('user/index'); ?>"><i class="fa fa-user"></i> <span>Data User</span></a></li>
                <li><a href="<?= base_url('login/logout'); ?>"><i class="fa fa-sign-in"></i> <span>Logout</span></a></li>

            </ul>
            <!--sidebar nav end-->

        </div>
    </div>
    <!-- left side end-->
    
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <div class="header-section">

        <!--toggle button start-->
        <a class="toggle-btn"><i class="fa fa-bars"></i></a>
        <!--toggle button end-->

        <!--search start-->
       
        <!--search end-->

        <!--notification menu start -->
        <div class="menu-right">
            <ul class="notification-menu">
                
                <li>
                    <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= base_url('assets/images/admin.png') ?>" alt="" />
						Admin,
                        <?php echo $user ?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                        
                        <li><a href="<?= base_url('login/logout'); ?>"><i class="fa fa-sign-out"></i> Log Out</a></li>
                    </ul>
                </li>

            </ul>
        </div>
        <!--notification menu end -->

        </div>
        <!-- header section end-->

        <!-- page heading start-->
        
        <!-- page heading end-->

        <!--body wrapper start-->
        <div class="wrapper">
             <div class="row">
                <div class="col-sm-12">
				<?php echo $this->session->flashdata('pesan');?>
                <section class="panel">
                <header class="panel-heading">
                   Data Event
                    <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                     </span>
                </header>
                <div class="panel-body">
                <div class="adv-table editable-table ">
                <div class="clearfix">
                    <div class="btn-group">
                        <button type="button" data-target="#modalTambah" data-toggle="modal" class="btn btn-primary">Tambah <i class="fa fa-plus" title="Update"></i></button>
                    </div>
                    
                </div>
                <div class="space15"></div>
                <table class="table table-striped table-hover table-bordered" id="editable-sample">
                <thead>
                <tr>
                    <th width="30">No.</th>
                    <th width="300">Nama Event</th>
                    <th>Tanggal Mulai</th>
                    <th>Tanggal Selesai</th>
                    <th>Lokasi Event</th>
                    <th width="150">Opsi</th>
                </tr>
                </thead>
                <tbody>
				<?php $nomer=1;
				?>
				<?php foreach($list as $row) { ?>
                <tr>
                    <td><?php echo $nomer; ?></td>
                    <td><?php echo $row['nama_event']; ?></td>
                    <td><?php echo $row['tgl_mulai']; ?></td>
                    <td><?php echo $row['tgl_selesai']; ?></td>
                    <td><?php echo $row['lokasi_event']; ?></td>
                    <td>
					<button type="button" data-target="#modalView" data-toggle="modal" onclick="view(<?= $row['id_event'] ?>)" class="btn btn-info btn-sm"><i class="fa fa-search" title="View"></i></button>
					 <button type="button" data-target="#modalEdit" data-toggle="modal" onclick="edit(<?= $row['id_event'] ?>)" class="btn btn-success btn-sm"><i class="fa fa-edit" title="Update"></i></button>
					<a type="button" data-target="#modalHapus" data-toggle="modal" onclick="hapus(<?= $row['id_event']; ?>)" class="btn btn-danger btn-sm"><i class="fa fa-trash-o" title="Hapus"></i> </a>
					</td>
                </tr>
                <?php $nomer++; } ?>
                </tbody>
                </table>
                </div>
                </div>
                </section>
                </div>
                </div>
        </div>
        <!--body wrapper end-->
		
		<!-- Modal Tambah-->
		<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalTambah" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h4 class="modal-title">Tambah Event</h4>
					</div>
					<div class="modal-body">

					<form role="form" method="post" action="<?= base_url('data_event/tambah_event'); ?>">
					
						<div class="form-group">
							<label for="exampleInputPassword1">Pilih Pelatihan</label>
							<select id="lunch" name="training" class="form-control selectpicker" data-live-search="true" title="--Pilih Pelatihan--">
								<option value="0">-- Pilih Pelatihan --</option>
								<?php foreach ($training as $training) {
									echo "<option value='".$training['id_training']."'>".$training['nama_training']."</option>";
								}
								?>
							</select>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Nama Event</label>
							<input class="form-control" name="nama_event" type="text" required/>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Tanggal Mulai</label>
							<input class="form-control " name="tgl_mulai" type="text"/>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Tanggal Selesai</label>
							<input class="form-control " name="tgl_selesai" type="text"/>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Lokasi Event</label>
							<input class="form-control " name="lokasi" type="text"/>
						</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-success">Simpan</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
					</div>
					</form>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Modal Edit-->
		<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalEdit" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h4 class="modal-title">Edit Event - <span class="edit_ket"></span></h4>
					</div>
					<div class="modal-body">

					<form role="form" method="post" action="<?= base_url('data_event/update_event'); ?>">
					<input type="text" name="id_event" value="" id="edit_id_event" hidden="">
						<div class="form-group">
							<label for="exampleInputPassword1">Pilih Pelatihan</label>
							<select id="edit_id_training" name="training" class="form-control selectpicker" data-live-search="true" title="--Pilih Pelatihan--">
								<option value="0">-- Pilih Pelatihan --</option>
								<?php foreach ($training2 as $data) {
									echo "<option value='".$data['id_training']."'>".$data['nama_training']."</option>";
								}
								?>
							</select>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Nama Event</label>
							<input class="form-control" name="nama_event" type="text" id="edit_nama_event" required/>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Tanggal Mulai</label>
							<input class="form-control " name="tgl_mulai" id="edit_tgl_mulai" type="text"/>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Tanggal Selesai</label>
							<input class="form-control " name="tgl_selesai" id="edit_tgl_selesai" type="text"/>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Lokasi Event</label>
							<input class="form-control " name="lokasi" id="edit_lokasi" type="text"/>
						</div>

					<div class="modal-footer">
						<button type="submit" class="btn btn-success">Simpan</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
					</div>
					</form>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Modal View-->
		<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalView" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h4 class="modal-title">Detail Data Event - <span class="view_ket"></span></h4>
					</div>
					<input type="text" name="id_event" value="" class="view_id_event" hidden="">
					<div class="modal-body">
					<!--<header class="panel-heading">
						Identitas
					</header>-->
						<table class="table table-bordered table-striped table-condensed">
							
							<tbody>
							<tr>
								<td width="350">Nama Pelatihan</td>
								<td><span class="view_nama_training"></span></td>
							</tr>
							<tr>
								<td>Nama Event</td>
								<td><span class="view_nama_event"></span></td>
							</tr>
							<tr>
								<td>Tanggal Mulai</td>
								<td><span class="view_tgl_mulai"></span></td>
							</tr>
							<tr>
								<td>Tanggal Selesai</td>
								<td><span class="view_tgl_selesai"></span></td>
							</tr>
							<tr>
								<td>Lokasi Event</td>
								<td><span class="view_lokasi_event"></span></td>
							</tr>
							
							
							</tbody>
						</table>
					

					<div class="modal-footer">
						<button type="button" class="btn btn-info" data-dismiss="modal">Tutup</button>
					</div>
					</form>
					</div>
				</div>
			</div>
		</div>
		
		
		<!-- Modal hapus -->
		<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalHapus" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h4 class="modal-title">Peringatan !!!</h4>
					</div>
					<div class="modal-body">
						<p class="no-margin">Apakah anda yakin menghapus data pelatihan <span id="delete_keterangan"></span> ?</p>
					</div>
					<div class="modal-footer">
						<form method="post" action="<?= base_url("data_event/hapus_event") ?>">
						<input type="hidden" class="form-control kd_hidden" name="id_event">
						<button type="submit" class="btn btn-success">Ya</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button>
					</form>
					</div>
				</div>
			</div>
		</div>

        <!--footer section start-->
        <footer>
            2016 &copy; MIC Transformer
        </footer>
        <!--footer section end-->


    </div>
    <!-- main content end-->
</section>

<!-- Placed js at the end of the document so the pages load faster -->
<script src="<?= base_url(); ?>assets/js/jquery-1.10.2.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/js/modernizr.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.nicescroll.js"></script>

<!--data table-->
<script type="text/javascript" src="<?= base_url(); ?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/data-tables/DT_bootstrap.js"></script>

<!--common scripts for all pages-->
<script src="<?= base_url(); ?>assets/js/scripts.js"></script>

<!--script for editable table-->
<script src="<?= base_url(); ?>assets/js/editable-table.js"></script>

<!--pickers plugins-->
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>

<!--pickers initialization-->
<script src="<?= base_url(); ?>assets/js/pickers-init.js"></script>

<script>
$(document).ready(function() {
	$(".dp-date-picker").datepicker({
		format:"dd-mm-yyyy"
	})
});
</script>
<script>
	if (top.location != location) {
    top.location.href = document.location.href ;
  }
		$(function(){
			window.prettyPrint && prettyPrint();
			$('input[name="tgl_mulai"]').datepicker({
				format: 'dd-mm-yyyy'
			});
			$('input[name="tgl_selesai"]').datepicker({
				format: 'dd-mm-yyyy'
			});
			
			$('#dpYears').datepicker();
			$('#dpMonths').datepicker();
			
			
			var startDate = new Date(2012,1,20);
			var endDate = new Date(2012,1,25);
			$('#dp4').datepicker()
				.on('changeDate', function(ev){
					if (ev.date.valueOf() > endDate.valueOf()){
						$('#alert').show().find('strong').text('The start date can not be greater then the end date');
					} else {
						$('#alert').hide();
						startDate = new Date(ev.date);
						$('#startDate').text($('#dp4').data('date'));
					}
					$('#dp4').datepicker('hide');
				});
			$('#dp5').datepicker()
				.on('changeDate', function(ev){
					if (ev.date.valueOf() < startDate.valueOf()){
						$('#alert').show().find('strong').text('The end date can not be less then the start date');
					} else {
						$('#alert').hide();
						endDate = new Date(ev.date);
						$('#endDate').text($('#dp5').data('date'));
					}
					$('#dp5').datepicker('hide');
				});

        // disabling dates
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#dpd1').datepicker({
          onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
          }
          checkin.hide();
          $('#dpd2')[0].focus();
        }).data('datepicker');
        var checkout = $('#dpd2').datepicker({
          onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          checkout.hide();
        }).data('datepicker');
		});
</script>

<!-- END JAVASCRIPTS -->
<script>
    jQuery(document).ready(function() {
        EditableTable.init();
    });
</script>
<script>
function edit(x){
	var nomor=x;
	$('#edit_id_event').val(nomor);
	
	$.ajax({
		url   : "<?= base_url("data_event/cekData") ?>",
		type: "get",
		data: "id="+nomor,
		dataType: "json",
		success: function(data){
			$("#edit_id_training").val(data.id_training);
			$("#edit_nama_event").val(data.nama_event);
			$("#edit_tgl_mulai").val(data.tgl_mulai);
			$("#edit_tgl_selesai").val(data.tgl_selesai);
			$("#edit_lokasi").val(data.lokasi_event);
			$(".edit_ket").html(data.nama_event);
			
		}
	});
	//$("#modalEdit").modal("show");
}

function view(x){
	var nomor=x;
	$('.view_id_event').val(nomor);
	
	$.ajax({
		url   : "<?= base_url("data_event/getView") ?>",
		type: "get",
		data: "id="+nomor,
		dataType: "json",
		success: function(data){
			
			$(".view_nama_training").html(data.nama_training);
			$(".view_nama_event").html(data.nama_event);
			$(".view_tgl_mulai").html(data.tgl_mulai);
			$(".view_tgl_selesai").html(data.tgl_selesai);
			$(".view_deskripsi").html(data.deskripsi);
			$(".view_lokasi_event").html(data.lokasi_event);
			$(".view_ket").html(data.nama_event);
			
		}
	});
	
}
	
	function hapus(x){
	var nomor = x;
	$.ajax({
	type  : "get",
	url   : "<?= base_url("data_event/cekData") ?>",
	data  : "id="+nomor,
	dataType: "json",
	success : function(data){
	  $("#delete_keterangan").html(data.nama_event);
	}
	});
	$("input.kd_hidden").val(nomor);
	//$("#modalHapus").modal("show");
	
}
	
</script>

</body>
</html>
