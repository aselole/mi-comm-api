<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Phpexcel_micomm extends CI_Model {

	public function upload_data($filename){
        ini_set('memory_limit', '-1');
        $inputFileName = './assets/uploaddata/'.$filename;
        try {
        $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
        } catch(Exception $e) {
        die('Error loading file :' . $e->getMessage());
        }

        $worksheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $numRows = count($worksheet);

        for ($i=3; $i < ($numRows+1) ; $i++) { 
        	if($worksheet[$i]["A"]==null) break;
			//$chq_date = str_replace('/', '-', $worksheet[$i]["D"]);
			//$tgllahir_anggota = date("Y-m-d", strtotime($chq_date));
			//$chq_date2 = str_replace('/', '-', $worksheet[$i]["F"]);
			//$tgllahir_pasangan = date("Y-m-d", strtotime($chq_date2));
			
			$data = array(
                    "id_event"=> $worksheet[$i]["A"],
					"lokasi"=> $worksheet[$i]["B"],
					"tanggal_mulai"=> $worksheet[$i]["C"],
					"tanggal_selesai"=> $worksheet[$i]["D"],
					"perusahaan"=> $worksheet[$i]["E"],
					"posisi"=> $worksheet[$i]["F"],
					"nama_lengkap"=> $worksheet[$i]["G"],
					"nama_panggilan"=> $worksheet[$i]["H"],
					"jenis_kelamin"=> $worksheet[$i]["I"],
					"tempat_lahir"=> $worksheet[$i]["J"],
					"tgl_lahir"=> $worksheet[$i]["K"],
					"agama"=> $worksheet[$i]["L"],
					"alamat_penempatan"=> $worksheet[$i]["M"],
					"alamat"=> $worksheet[$i]["N"],
					"no_telepon"=> $worksheet[$i]["O"],
					"no_hp"=> $worksheet[$i]["P"],
					"no_wa"=> $worksheet[$i]["Q"],
					"pin_bb"=> $worksheet[$i]["R"],
					"id_line"=> $worksheet[$i]["S"],
					"id_facebook"=> $worksheet[$i]["T"],
					"id_twitter"=> $worksheet[$i]["U"],
					"instagram"=> $worksheet[$i]["V"],
					"email_kantor"=> $worksheet[$i]["W"],
					"email_pribadi"=> $worksheet[$i]["X"],
					"group_wa"=> $worksheet[$i]["Y"]
					
                );
				
	        $this->db->insert('micomm', $data);
	    }
    }

}

/* End of file Phpexcel_model.php */
/* Location: ./application/models/Phpexcel_model.php */