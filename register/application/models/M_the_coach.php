<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class M_the_coach extends CI_Model
{
	public function get_all_data($config){
		$hasilquery=$this->db->get('tampil_the_coach', $config['per_page'], $this->uri->segment(3));
        if ($hasilquery->num_rows() > 0) {
            foreach ($hasilquery->result_array() as $value) {
                $data[]=$value;
            }
            return $data;
        }
	}
	
	function cari_data($pencarian,$offset,$total){
                 $this->db->or_like(array('nama_anggota' => $pencarian, 'no_anggota' => $pencarian));
                
		 $query = $this->db->get('tampil_the_coach',$total,$offset);
				
		 $result['data'] = $query->result_array(); 
		 return $result;
   }
   
   function cari_tanggal($pencarian,$offset,$total){
       if ($pencarian){
		  $this->db->like('tgllahir_anggota',$pencarian);
		 }     
		 $result['total_rows'] = $this->db->count_all_results('tampil_the_coach');
		  
		 if ($pencarian){
		  $this->db->like('tgllahir_anggota',$pencarian);
		 }
		 $query = $this->db->get('tampil_the_coach',$total,$offset);
				
		 $result['data'] = $query->result_array(); 
		 return $result;
   }
	
	public function update_data($data,$id){
		$this->db->update("thecoach",$data,$id);
	}
	public function delete_data($id){
		$this->db->where('id_thecoach',$id);
		$this->db->delete('thecoach'); 
	}
	
	public function get_perusahaan(){
		return $this->db->get_where('view_perusahaan_thecoach');
	}
 
	public function insert_data($data){
		return $this->db->insert('thecoach', $data); 
	}
	
	public function jumlah_data_thecoach(){
		return $this->db->get('hitung_data_thecoach');
	}
	
	function getDataTheCoach($whr=array()){
		$this->db->where($whr);
		return $this->db->get('thecoach')->row_array();
	}
	
	function getViewTheCoach($whr=array()){
		$this->db->where($whr);
		return $this->db->get_where('view_thecoach')->row_array();
	}
}