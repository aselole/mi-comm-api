<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class M_perusahaan_yes extends CI_Model
{
	public function get_all_data(){
		return $this->db->get('perusahaan_yes');
	}
	
	public function update_data($data,$id){
		$this->db->update("perusahaan_yes",$data,$id);
	}
	public function delete_data($id){
		$this->db->where('kode',$id);
		$this->db->delete('perusahaan_yes'); 
	}
	
	public function insert_data($data){
		return $this->db->insert('perusahaan_yes', $data); 
	}
	
	
	function getDataYes($whr=array()){
		$this->db->where($whr);
		return $this->db->get_where('view_perusahaan_yes')->row_array();
	}
	
	function getViewYes($whr=array()){
		$this->db->where($whr);
		return $this->db->get('perusahaan_yes')->row_array();
	}
	
	public function jumlah_data(){
		return $this->db->get('hitung_perusahaan_yes');
	}
}