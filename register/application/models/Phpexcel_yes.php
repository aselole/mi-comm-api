<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Phpexcel_yes extends CI_Model {

	public function upload_data($filename){
        ini_set('memory_limit', '-1');
        $inputFileName = './assets/uploaddata/'.$filename;
        try {
        $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
        } catch(Exception $e) {
        die('Error loading file :' . $e->getMessage());
        }

        $worksheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $numRows = count($worksheet);

        for ($i=2; $i < ($numRows+1) ; $i++) { 
        	if($worksheet[$i]["A"]==null) break;
			//$chq_date = str_replace('/', '-', $worksheet[$i]["H"]);
			//$tgl_lahir = date("Y-m-d", strtotime($chq_date));
			
			$data = array(
					"perusahaan"=> $worksheet[$i]["A"],
					"perguruan_tinggi"=> $worksheet[$i]["B"],
					"nama_lengkap"=> $worksheet[$i]["C"],
					"panggilan"=> $worksheet[$i]["D"],
					"fakultas"=> $worksheet[$i]["E"],
					"agama"=> $worksheet[$i]["F"],
					"tempat_lahir"=> $worksheet[$i]["G"],
					"tgl_lahir"=> $worksheet[$i]["H"],
					"alamat"=> $worksheet[$i]["I"],
					"tlp"=> $worksheet[$i]["J"],
					"no_hp"=> $worksheet[$i]["K"],
					"bbm"=> $worksheet[$i]["L"],
					"line"=> $worksheet[$i]["M"],
					"facebook"=> $worksheet[$i]["N"],
					"twitter"=> $worksheet[$i]["O"],
					"email"=> $worksheet[$i]["P"]	
                );
				
	        $this->db->insert('yes', $data);
	    }
    }

}

/* End of file Phpexcel_model.php */
/* Location: ./application/models/Phpexcel_model.php */