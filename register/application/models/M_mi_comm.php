<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class M_mi_comm extends CI_Model
{
	public function __construct()
    {
		// Call the CI_Model constructor
		parent::__construct();
		
    }
	
	public function get_all_data($config){
		$hasilquery=$this->db->get('micomm', $config['per_page'], $this->uri->segment(3));
        if ($hasilquery->num_rows() > 0) {
            foreach ($hasilquery->result_array() as $value) {
                $data[]=$value;
            }
            return $data;
        }
	}
	
	function cari_data($pencarian,$offset,$total){
                 $this->db->or_like(array('nama_lengkap' => $pencarian, 'group_wa' => $pencarian, 'alamat_penempatan' => $pencarian, 'id_event' => $pencarian, 'perusahaan' => $pencarian));
               
		 $query = $this->db->get('micomm',$total,$offset);
				
		 $result['data'] = $query->result_array(); 
		 return $result;
   }
   
   function cari_event($event,$offset,$total){
                 $this->db->or_like(array('id_event' => $event));
               
		 $query = $this->db->get('micomm',$total,$offset);
				
		 $result['data'] = $query->result_array(); 
		 return $result;
   }
   
   function cari_topik($topik,$offset,$total){
                 $this->db->or_like(array('topik' => $topik));
               
		 $query = $this->db->get('micomm',$total,$offset);
				
		 $result['data'] = $query->result_array(); 
		 return $result;
   }
   
   function cari_tanggal($pencarian,$offset,$total){
       if ($pencarian){
		  $this->db->like('tgl_lahir',$pencarian);
		 }     
		 $result['total_rows'] = $this->db->count_all_results('micomm');
		  
		 if ($pencarian){
		  $this->db->like('tgl_lahir',$pencarian);
		 }
		 $query = $this->db->get('micomm',$total,$offset);
				
		 $result['data'] = $query->result_array(); 
		 return $result;
   }
	
	public function update_data($data,$id){
		$this->db->update("micomm",$data,$id);
	}
	public function delete_data($id){
		$this->db->where('id_micomm',$id);
		$this->db->delete('micomm'); 
	}
	
	public function get_event(){
		return $this->db->get_where('tampil_event');
	}
	
	public function get_topik(){
		return $this->db->get_where('tampil_topik');
	}
	
	public function get_training(){
		return $this->db->get_where('tampil_training');
	}
	
	public function get_perusahaan(){
		return $this->db->get_where('view_perusahaan_micomm');
	}
 
	public function insert_data($data){
		return $this->db->insert('micomm', $data); 
	}
	
	public function jumlah_data_micomm(){
		return $this->db->get('hitung_data_micomm');
	}
	
	function getDataMicomm($whr=array()){
		$this->db->where($whr);
		return $this->db->get('view_micomm')->row_array();
	}
	
	function getViewMicomm($whr=array()){
		$this->db->where($whr);
		return $this->db->get_where('view_micomm')->row_array();
	}
	
	function tampil_event($id) {
		return $this->db->get_where("view_event",array("id_event"=>$id))->row();
	}
}