<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_training extends CI_Model
{	

	public function get_all_data(){
		return $this->db->query("SELECT * FROM training");
	}
	
	public function update_data($data,$id){
		$this->db->update("training",$data,$id);
	}
	public function delete_data($id){
		$this->db->where('id_training',$id);
		$this->db->delete('training'); 
	}
	
	public function insert_data($data){
		return $this->db->insert('training', $data); 
	}
	
	function getDataTraining($whr=array()){
		$this->db->where($whr);
		return $this->db->get('training')->row_array();
	}
	
	function getViewTraining($whr=array()){
		$this->db->where($whr);
		return $this->db->get('training')->row_array();
	}

}