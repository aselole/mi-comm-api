<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_user extends CI_Model
{	

	public function get_all_data(){
		return $this->db->query("SELECT * FROM users");
	}
	
	public function update_data($data,$id){
		$this->db->update("users",$data,$id);
	}
	public function delete_data($id){
		$this->db->where('iduser',$id);
		$this->db->delete('users'); 
	}
	
	public function insert_data($data){
		return $this->db->insert('users', $data); 
	}
	
	function getDataUser($whr=array()){
		$this->db->where($whr);
		return $this->db->get('users')->row_array();
	}

}