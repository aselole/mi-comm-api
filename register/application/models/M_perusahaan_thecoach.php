<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class M_perusahaan_thecoach extends CI_Model
{
	public function get_all_data(){
		return $this->db->get('perusahaan_thecoach');
	}
	
	public function update_data($data,$id){
		$this->db->update("perusahaan_thecoach",$data,$id);
	}
	public function delete_data($id){
		$this->db->where('kode',$id);
		$this->db->delete('perusahaan_thecoach'); 
	}
	
	public function insert_data($data){
		return $this->db->insert('perusahaan_thecoach', $data); 
	}
	
	
	function getDataTheCoach($whr=array()){
		$this->db->where($whr);
		return $this->db->get_where('view_perusahaan_thecoach')->row_array();
	}
	
	function getViewTheCoach($whr=array()){
		$this->db->where($whr);
		return $this->db->get('perusahaan_thecoach')->row_array();
	}
	
	public function jumlah_data(){
		return $this->db->get('hitung_perusahaan_thecoach');
	}
}