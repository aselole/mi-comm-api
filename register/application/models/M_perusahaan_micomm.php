<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class M_perusahaan_micomm extends CI_Model
{
	public function get_all_data(){
		return $this->db->get('perusahaan_micomm');
	}
	
	public function update_data($data,$id){
		$this->db->update("perusahaan_micomm",$data,$id);
	}
	public function delete_data($id){
		$this->db->where('kode',$id);
		$this->db->delete('perusahaan_micomm'); 
	}
	
	public function insert_data($data){
		return $this->db->insert('perusahaan_micomm', $data); 
	}
	
	
	function getDataMicomm($whr=array()){
		$this->db->where($whr);
		return $this->db->get_where('view_perusahaan_micomm')->row_array();
	}
	
	function getViewMicomm($whr=array()){
		$this->db->where($whr);
		return $this->db->get('perusahaan_micomm')->row_array();
	}
	
	public function jumlah_data(){
		return $this->db->get('hitung_perusahaan_micomm');
	}
}