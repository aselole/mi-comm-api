<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class M_yes extends CI_Model
{
	public function get_all_data($config){
		$hasilquery=$this->db->get('yes', $config['per_page'], $this->uri->segment(3));
        if ($hasilquery->num_rows() > 0) {
            foreach ($hasilquery->result_array() as $value) {
                $data[]=$value;
            }
            return $data;
        }
	}
	
	function cari_data($pencarian,$offset,$total){
       
                 $this->db->or_like(array('nama_lengkap' => $pencarian, 'perusahaan' => $pencarian));
		
		 $query = $this->db->get('yes',$total,$offset);
				
		 $result['data'] = $query->result_array(); 
		 return $result;
   }
   
   function cari_tanggal($pencarian,$offset,$total){
       if ($pencarian){
		  $this->db->like('tgl_lahir',$pencarian);
		 }     
		 $result['total_rows'] = $this->db->count_all_results('yes');
		  
		 if ($pencarian){
		  $this->db->like('tgl_lahir',$pencarian);
		 }
		 $query = $this->db->get('yes',$total,$offset);
				
		 $result['data'] = $query->result_array(); 
		 return $result;
   }
	
	public function update_data($data,$id){
		$this->db->update("yes",$data,$id);
	}
	public function delete_data($id){
		$this->db->where('id_yes',$id);
		$this->db->delete('yes'); 
	}
	
	public function get_event(){
		return $this->db->get_where('tampil_event');
	}
 
	public function get_perusahaan(){
		return $this->db->get_where('view_perusahaan_yes');
	}
	
	public function insert_data($data){
		return $this->db->insert('yes', $data); 
	}
	
	public function jumlah_data_yes(){
		return $this->db->get('hitung_data_yes');
	}
	
	function getDataYes($whr=array()){
		$this->db->where($whr);
		return $this->db->get('yes')->row_array();
	}
	
	function getViewYes($whr=array()){
		$this->db->where($whr);
		return $this->db->get('view_yes')->row_array();
	}
}