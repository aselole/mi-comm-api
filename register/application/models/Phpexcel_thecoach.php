<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Phpexcel_thecoach extends CI_Model {

	public function upload_data($filename){
        ini_set('memory_limit', '-1');
        $inputFileName = './assets/uploaddata/'.$filename;
        try {
        $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
        } catch(Exception $e) {
        die('Error loading file :' . $e->getMessage());
        }

        $worksheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $numRows = count($worksheet);

        for ($i=2; $i < ($numRows+1) ; $i++) { 
        	if($worksheet[$i]["A"]==null) break;
			$chq_date = str_replace('/', '-', $worksheet[$i]["D"]);
			$tgllahir_anggota = date("Y-m-d", strtotime($chq_date));
			$chq_date2 = str_replace('/', '-', $worksheet[$i]["F"]);
			$tgllahir_pasangan = date("Y-m-d", strtotime($chq_date2));
			
			$data = array(
                    "perusahaan"=> $worksheet[$i]["A"],
					"no_anggota"=> $worksheet[$i]["B"],
					"nama_anggota"=> $worksheet[$i]["C"],
					"tgllahir_anggota"=> $tgllahir_anggota,
					"nama_pasangan"=> $worksheet[$i]["E"],
					"tgllahir_pasangan"=> $tgllahir_pasangan,
					"alamat"=> $worksheet[$i]["G"],
					"kota"=> $worksheet[$i]["H"],
					"tlp"=> $worksheet[$i]["I"],
					"hp1"=> $worksheet[$i]["J"],
					"hp2"=> $worksheet[$i]["K"],
					"bbm"=> $worksheet[$i]["L"],
					"email"=> $worksheet[$i]["M"],
					"pendidikan"=> $worksheet[$i]["N"],
					"agama"=> $worksheet[$i]["O"],
					"kegiatan"=> $worksheet[$i]["P"],
					
                );
				
	        $this->db->insert('thecoach', $data);
	    }
    }

}

/* End of file Phpexcel_model.php */
/* Location: ./application/models/Phpexcel_model.php */