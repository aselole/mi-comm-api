<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class M_mi_comm extends CI_Model
{
	public function get_all_data($config){
		$hasilquery=$this->db->get('micomm', $config['per_page'], $this->uri->segment(3));
        if ($hasilquery->num_rows() > 0) {
            foreach ($hasilquery->result_array() as $value) {
                $data[]=$value;
            }
            return $data;
        }
	}
	
	public function update_data($data,$id){
		$this->db->update("micomm",$data,$id);
	}
	public function delete_data($id){
		$this->db->where('id_micomm',$id);
		$this->db->delete('micomm'); 
	}
	
	public function get_event(){
		return $this->db->get_where('tampil_event');
	}
	
	public function get_training(){
		return $this->db->get_where('tampil_training');
	}
	
	public function get_perusahaan(){
		return $this->db->get_where('tampil_perusahaan_micomm');
	}
 
	public function insert_data($data){
		return $this->db->insert('micomm', $data); 
	}
	
	public function jumlah_data_micomm(){
		return $this->db->get('hitung_data_micomm');
	}
	
	function getDataMicomm($whr=array()){
		$this->db->where($whr);
		return $this->db->get('micomm')->row_array();
	}
	
	function getViewMicomm($whr=array()){
		$this->db->where($whr);
		return $this->db->get_where('view_micomm')->row_array();
	}
}