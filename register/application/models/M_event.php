<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_event extends CI_Model
{	

	public function get_all_data(){
		return $this->db->query("SELECT * FROM event");
	}
	
	public function update_data($data,$id){
		$this->db->update("event",$data,$id);
	}
	public function delete_data($id){
		$this->db->where('id_event',$id);
		$this->db->delete('event'); 
	}
	
	public function insert_data($data){
		return $this->db->insert('event', $data); 
	}
	
	function getDataEvent($whr=array()){
		$this->db->where($whr);
		return $this->db->get('event')->row_array();
	}
	
	function getViewEvent($whr=array()){
		$this->db->where($whr);
		return $this->db->get('view_event')->row_array();
	}
	
	public function get_training(){
		return $this->db->get_where('tampil_training');
	}

}