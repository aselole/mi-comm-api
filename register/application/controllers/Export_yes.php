<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Export_yes extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->library(array('Export/PHPExcel','Export/PHPExcel/IOFactory.php'));
        $this->load->model('m_export_yes'); // memanggil model 
    }
    public function export(){
        $ambildata = $this->m_export_yes->export_yes();
         
        if(count($ambildata)>0){
            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()
                  ->setCreator("MIC Transformer") //creator
                    ->setTitle("Data Yes");  //file title
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Sample Sheet'); //sheet title
             
            $objget->getStyle("A1:Q1")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '92d050')
                    ),
                    'font' => array(
                        'color' => array('rgb' => '000000')
                    )
                )
            );
 
            //table header
            $cols = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q");
             
            $val = array("ID", "Perusahaan", "Perguruan Tinggi", "Nama Lengkap", "Nama Panggilan", "Fakultas", "Agama", "Tempat Lahir ", "Tanggal Lahir", "Alamat", "Telepon", "Handphone", "BBM", "Line", "Facebook", "Twitter", "Email");

            
            for ($a=0;$a<17; $a++) 
            {
                $objset->setCellValue($cols[$a].'1', $val[$a]);
                 
                //Setting lebar cell
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(50);
                $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(50); 
            
             
                $style = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                );
                $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
            }
             
            $baris  = 2;
            foreach ($ambildata as $frow){
                 
                //pemanggilan sesuaikan dengan nama kolom tabel
                $objset->setCellValue("A".$baris, $frow->id_yes); 
                $objset->setCellValue("B".$baris, $frow->perusahaan); 
                $objset->setCellValue("C".$baris, $frow->perguruan_tinggi);
				$objset->setCellValue("D".$baris, $frow->nama_lengkap); 
				$objset->setCellValue("E".$baris, $frow->panggilan); 
                $objset->setCellValue("F".$baris, $frow->fakultas); 
                $objset->setCellValue("G".$baris, $frow->agama);
				$objset->setCellValue("H".$baris, $frow->tempat_lahir); 
                $objset->setCellValue("I".$baris, $frow->tgl_lahir); 
                $objset->setCellValue("J".$baris, $frow->alamat);
				$objset->setCellValue("K".$baris, $frow->tlp); 
                $objset->setCellValue("L".$baris, $frow->no_hp); 
                $objset->setCellValue("M".$baris, $frow->bbm);
				$objset->setCellValue("N".$baris, $frow->line); 
                $objset->setCellValue("O".$baris, $frow->facebook); 
                $objset->setCellValue("P".$baris, $frow->twitter);
				$objset->setCellValue("Q".$baris, $frow->email); 
              
                 
                //Set number value
                $objPHPExcel->getActiveSheet()->getStyle('Q1:Q'.$baris)->getNumberFormat()->setFormatCode('0');
                 
                $baris++;
            }
             
            $objPHPExcel->getActiveSheet()->setTitle('Data Yes');
 
            $objPHPExcel->setActiveSheetIndex(0);  
            $filename = urlencode("Data_Yes_".date("Y-m-d H:i:s").".xls");
               
              header('Content-Type: application/vnd.ms-excel'); //mime type
              header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
              header('Cache-Control: max-age=0'); //no cache
 
            $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');                
            $objWriter->save('php://output');
        }else{
            redirect('Excel');
        }
    }
}