<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_mi_comm extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (isset($this->session->userdata['login'])) {
		$username = ($this->session->userdata['login']['nama']);
		} else {
		redirect("login/index");
		}
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->helper('form','file');
		$this->load->model('m_mi_comm');
		$this->load->helper('url');
        $this->load->library('pagination');
        $this->load->database();
	}

	public function input_data()
	{
		$data['user']= $this->session->userdata['login']['nama'];
		$data['event'] = $this->m_mi_comm->get_event()->result_array();
		$data['perusahaan'] = $this->m_mi_comm->get_perusahaan()->result_array();
		$this->load->view('v_input_mi_comm',$data);
		$this->load->helper("url","form");
	}
	
    function data_anggota(){
		$data['user']= $this->session->userdata['login']['nama'];
		$data['event'] = $this->m_mi_comm->get_event()->result_array();
		$data['perusahaan'] = $this->m_mi_comm->get_perusahaan()->result_array();
		$data['perusahaan2'] = $this->m_mi_comm->get_perusahaan()->result_array();
		$data['event1'] = $this->m_mi_comm->get_event()->result_array();
		$config['base_url']= base_url()."data_mi_comm/data_anggota";
		$config['total_rows']= $this->db->query("SELECT * FROM micomm")->num_rows();
		$config['per_page']=10;
		$config['num_links'] = 2;
		$config['uri_segment']=3;
 
        //Tambahan untuk styling
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
 
        $config['first_link']='<strong>Pertama</strong>';
        $config['last_link']='<strong>Terakhir</strong>';
        $config['next_link']='<i class="fa fa-angle-double-right"></i>';
        $config['prev_link']='<i class="fa fa-angle-double-left"></i>';
        $this->pagination->initialize($config);
 
        $data['list']=$this->m_mi_comm->get_all_data($config);
		$data['total']=$this->m_mi_comm->jumlah_data_micomm()->result();
        $this->load->view('v_data_mi_comm', $data);
    }
	
	function cekData(){
    	$arr = array(
			"id_micomm"=>$this->input->get("id")
			);		
		$r = $this->m_mi_comm->getDataMicomm($arr);
		echo json_encode($r);
    }
	
	function getView(){
    	$arr = array(
			"id_micomm"=>$this->input->get("id")
			);		
		$r = $this->m_mi_comm->getViewMicomm($arr);
		echo json_encode($r);
    }
	
	public function tambah_micomm()
	{
		$chq_date2 = str_replace('/', '-', $this->input->post('tgl_lahir'));
		$tgl_lahir = date("Y-m-d", strtotime($chq_date2));
		//$data['event'] = $this->m_mi_comm->get_event()->result_array();
		/*//Check whether user upload picture
            if(!empty($_FILES['picture']['name'])){
                $config['upload_path'] = 'assets/foto/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $_FILES['picture']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('picture')){
                    $uploadData = $this->upload->data();
                    $picture = $uploadData['file_name'];
                }else{
                    $picture = '';
                }
            }else{
                $picture = '';
            }*/
			
		$data = array('nama_lengkap' => $this->input->post('nama_lengkap'),
					'nama_panggilan' => $this->input->post('nama_panggilan'),
					'agama' => $this->input->post('agama'),
					'tempat_lahir' => $this->input->post('tempat_lahir'),
					'tgl_lahir' => $tgl_lahir,
					'no_telepon' => $this->input->post('telepon'),
					'no_hp' => $this->input->post('no_hp'),
					'no_hp2' => $this->input->post('no_hp2'),
					'alamat' => $this->input->post('alamat'),
					'email_pribadi' => $this->input->post('email_pribadi'),
					'pin_bb' => $this->input->post('pin_bb'),
					'id_line' => $this->input->post('id_line'),
					'id_facebook' => $this->input->post('id_facebook'),
					'id_twitter' => $this->input->post('id_twitter'),
					'instagram' => $this->input->post('instagram'),
					'group_wa' => $this->input->post('group_wa'),
					//'picture' => $picture,
					'perusahaan' => $this->input->post('perusahaan'),
					'email_kantor' => $this->input->post('email_kantor'),
					'posisi' => $this->input->post('posisi'),
					'id_event' => $this->input->post('event'),
					'tempat_tanggal' => $this->input->post('lokasi').", ".$this->input->post('tanggal_mulai')." s/d ".$this->input->post('tanggal_akhir')
					);
		$res = $this->m_mi_comm->insert_data($data);	
		if($res>=1){
			$this->session->set_flashdata('pesan',"<div class='alert alert-success'>Data Anggota Berhasil Disimpan</div>");	
			redirect($_SERVER['HTTP_REFERER']);
		}else{
			$this->session->set_flashdata('pesan',"<div class='alert alert-danger'>Data Anggota Gagal Disimpan</div>");
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	public function update_micomm()
	{
		$chq_date2 = str_replace('/', '-', $this->input->post('tgl_lahir') );
		$tgl_lahir = date("Y-m-d", strtotime($chq_date2));
		//$data['event'] = $this->m_mi_comm->get_event()->result_array();
		/*//Check whether user upload picture
            if(!empty($_FILES['picture']['name'])){
                $config['upload_path'] = 'assets/foto/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $_FILES['picture']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('picture')){
                    $uploadData = $this->upload->data();
                    $picture = $uploadData['file_name'];
                }else{
                    $picture = '';
                }
            }else{
                $picture = '';
            }*/
		$id['id_micomm'] = $this->input->post('id_micomm');
		$data = array('nama_lengkap' => $this->input->post('nama_lengkap'),
					'nama_panggilan' => $this->input->post('nama_panggilan'),
					'agama' => $this->input->post('agama'),
					'tempat_lahir' => $this->input->post('tempat_lahir'),
					'tgl_lahir' => $tgl_lahir,
					'no_telepon' => $this->input->post('telepon'),
					'no_hp' => $this->input->post('no_hp'),
					'no_hp2' => $this->input->post('no_hp2'),
					'alamat' => $this->input->post('alamat'),
					'email_pribadi' => $this->input->post('email_pribadi'),
					'pin_bb' => $this->input->post('pin_bb'),
					'id_line' => $this->input->post('id_line'),
					'id_facebook' => $this->input->post('id_facebook'),
					'id_twitter' => $this->input->post('id_twitter'),
					'instagram' => $this->input->post('instagram'),
					'group_wa' => $this->input->post('group_wa'),
					//'picture' => $picture,
					'perusahaan' => $this->input->post('perusahaan'),
					'email_kantor' => $this->input->post('email_kantor'),
					'posisi' => $this->input->post('posisi'),
					'id_event' => $this->input->post('event'),
					'training' => $this->input->post('training'),
					'tempat_tanggal' => $this->input->post('lokasi').", ".$this->input->post('tanggal_mulai')." s/d ".$this->input->post('tanggal_akhir')
					);
		$res = $this->m_mi_comm->update_data($data,$id);	
		if($res>=1){
			$this->session->set_flashdata('pesan',"<div class='alert alert-danger'>Data Anggota Gagal Disimpan</div>");
			redirect($_SERVER['HTTP_REFERER']);
		}else{
			$this->session->set_flashdata('pesan',"<div class='alert alert-success'>Data Anggota Berhasil Disimpan</div>");	
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	public function hapus_micomm($id)
	{
		$id = $this->input->post('id_micomm');
		$res = $this->m_mi_comm->delete_data($id);
		if($res>=0){
		$this->session->set_flashdata('pesan',"<div class='alert alert-success'>Data Anggota Berhasil Dihapus</div>");
		redirect($_SERVER['HTTP_REFERER']);
		}else{
		$this->session->set_flashdata('pesan',"<div class='alert alert-success'>Data Anggota Gagal Dihapus</div>");	
		redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	public function do_upload(){
        $fileName = time().$_FILES['file']['name'];
         
        $config['upload_path'] = './assets/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
         
        if(! $this->upload->do_upload('file') )
        $this->upload->display_errors();
             
        $media = $this->upload->data('file');
        $inputFileName = './assets/'.$media['file_name'];
         
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
             
            for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                                                 
                //Sesuaikan sama nama kolom tabel di database                                
                 $data = array(
                    "perusahaan"=> $rowData[0][0],
					"pelatihan"=> $rowData[0][1],
                    "tempat_tanggal"=> $rowData[0][2],
                    "nama_lengkap"=> $rowData[0][3],
                    "nama_panggilan"=> $rowData[0][4],
					"posisi"=> $rowData[0][5],
                    "jenis_kelamin"=> $rowData[0][6],
					"tempat_lahir"=> $rowData[0][7],
					
					"alamat_penempatan"=> $rowData[0][8],
					"alamat"=> $rowData[0][9],
					"agama"=> $rowData[0][10],
					"no_telepon"=> $rowData[0][12],
					"no_hp"=> $rowData[0][13],
					"wa"=> $rowData[0][14],
					"pin_bb"=> $rowData[0][15],
					"id_line"=> $rowData[0][16],
					"id_facebook"=> $rowData[0][17],
					"id_twitter"=> $rowData[0][18],
					"instagram"=> $rowData[0][19],
					"email_kantor"=> $rowData[0][20],
					"email_pribadi"=> $rowData[0][21],
					"group_wa"=> $rowData[0][22]
                );

                //sesuaikan nama dengan nama tabel
                $insert = $this->db->insert("micomm",$data);
                delete_files($media['file_path']);
                     
            }
        redirect($_SERVER['HTTP_REFERER']);
    }
	
}
