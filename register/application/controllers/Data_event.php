<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_event extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (isset($this->session->userdata['login'])) {
		$username = ($this->session->userdata['login']['nama']);
		} else {
		redirect("login/index");
		}
		$this->load->helper('form');
		$this->load->model('m_event');
		$this->load->helper('url');
        $this->load->library('pagination');
        $this->load->database();
	}
	
	function index(){
		$data['training'] = $this->m_event->get_training()->result_array();
		$data['training2'] = $this->m_event->get_training()->result_array();
		$data['list'] = $this->m_event->get_all_data()->result_array();
		$data['user'] = $this->session->userdata['login']['nama'];
        $this->load->view('v_data_event', $data);
    }
	
	
	function cekData(){
    	$arr = array(
			"id_event"=>$this->input->get("id")
			);		
		$r = $this->m_event->getDataEvent($arr);
		echo json_encode($r);
    }
	
	function getView(){
    	$arr = array(
			"id_event"=>$this->input->get("id")
			);		
		$r = $this->m_event->getViewEvent($arr);
		echo json_encode($r);
    }
	
	public function tambah_event()
	{
		$chq_date1 = str_replace('/', '-', $this->input->post('tgl_mulai'));
		$tgl_mulai = date("Y-m-d", strtotime($chq_date1));
		$chq_date2 = str_replace('/', '-', $this->input->post('tgl_selesai'));
		$tgl_selesai = date("Y-m-d", strtotime($chq_date2));
		$data = array('id_training' => $this->input->post('training'),
					'nama_event' => $this->input->post('nama_event'),
					'tgl_mulai' => $tgl_mulai,
					'tgl_selesai' => $tgl_selesai,
					'lokasi_event' => $this->input->post('lokasi')
					);
		$res = $this->m_event->insert_data($data);	
		if($res>=1){
			$this->session->set_flashdata('pesan',"<div class='alert alert-success'>Data Event Berhasil Disimpan</div>");	
			redirect('data_event', 'refresh');
		}else{
			$this->session->set_flashdata('pesan',"<div class='alert alert-danger'>Data Event Gagal Disimpan</div>");
			redirect('data_event', 'refresh');
		}
	}
	
	public function update_event()
	{
	
		$chq_date1 = str_replace('/', '-', $this->input->post('tgl_mulai'));
		$tgl_mulai = date("Y-m-d", strtotime($chq_date1));
		$chq_date2 = str_replace('/', '-', $this->input->post('tgl_selesai'));
		$tgl_selesai = date("Y-m-d", strtotime($chq_date2));
		$id['id_event'] = $this->input->post('id_event');
		$data = array('id_training' => $this->input->post('training'),
					'nama_event' => $this->input->post('nama_event'),
					'tgl_mulai' => $tgl_mulai,
					'tgl_selesai' => $tgl_selesai,
					'lokasi_event' => $this->input->post('lokasi')
					);
		$res = $this->m_event->update_data($data,$id);	
		if($res>=1){
			$this->session->set_flashdata('pesan',"<div class='alert alert-danger'>Data Event Gagal Diupdate</div>");
		    redirect('data_event', 'refresh');
		}else{
			$this->session->set_flashdata('pesan',"<div class='alert alert-success'>Data Event Berhasil Diupdate</div>");	
			redirect('data_event', 'refresh');
		}
	}
	
	public function hapus_event($id)
	{
		$id = $this->input->post('id_event');
		$res = $this->m_event->delete_data($id);
		if($res>=0){
		$this->session->set_flashdata('pesan',"<div class='alert alert-success alert-block fade in'>Data Event Berhasil Dihapus</div>");
		    redirect('data_event', 'refresh');
		}else{
		$this->session->set_flashdata('pesan',"<div class='alert alert-danger alert-block fade in'>Data Event Gagal Dihapus</div>");	
		    redirect('data_event', 'refresh');
		}
	}
	
	public function do_upload(){
		$config['upload_path'] = './assets/uploaddata/';
        $config['allowed_types'] = 'xlsx|xls';
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload()){
			$error = array('error' => $this->upload->display_errors());
		}
		else{
			$data = array('upload_data' => $this->upload->data());
            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $filename = $upload_data['file_name'];
            $this->phpexcel_model->upload_data($filename);
           // unlink('./assets/uploaddata/'.$filename);
			delete_files($media['file_path']);
            redirect('data_event', 'refresh');
		}
	}
	
}
