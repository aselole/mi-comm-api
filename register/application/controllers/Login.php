<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
		parent::__construct();
		
		$this->load->model('m_login');
		$this->load->library('form_validation');
		$this->load->library('session');
	}
 
	function index(){
		$this->load->view('v_login');
	}
 
	public function cek_login(){
		$user	=$this->input->post('username');
		$pass	=$this->input->post('password');
		$passwordx = md5($pass);
		if(($user=='')||($passwordx=='')){
			$this->session->set_flashdata('pesan',"<div class='alert alert-block alert-danger fade in'><center>Username dan Password Salah !</center></div>");
			redirect(base_url("login/index"));
		}
		else {
			$sql=$this->db->query("select * from users where username='$user' AND password='$passwordx'");
			if($sql->num_rows()>=1){
				$data	=$sql->row();
				$timezone = "Asia/Jakarta";
				if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);
				$now = date('Y-m-d H:i:s');
				$sesi	=array(
						'sesi_id' => $data->iduser,
						'username' => $data->username,
						'password' => $data->password,
						'nama' => $data->nama);
				$userdata1 = array('last_login' => $now);
				$this->session->set_userdata('login',$sesi);
				$this->db->where('username', $data->username);
				$this->db->update('users', $userdata1);
				//$this->db->update('users', $sesi);
				$this->session->set_flashdata('pesan',"<div class='alert alert-block alert-success fade in'>Berhasil login, Selamat datang..</div>");
				redirect(base_url("dashboard/index"));
				
			}else{
				$this->session->set_flashdata('pesan',"<div class='alert alert-block alert-danger fade in'><center>Username dan Password Salah !</center></div>");
				redirect(base_url("login/index"));
				
			}
		}
		
	}
	
	function logout(){
		$this->session->sess_destroy();
        redirect(base_url("login/index"));
	}
}
