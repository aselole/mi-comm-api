<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_yes extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (isset($this->session->userdata['login'])) {
		$username = ($this->session->userdata['login']['nama']);
		} else {
		redirect("login/index");
		}
		$this->load->library("PHPExcel");
		$this->load->model("phpexcel_yes");
		$this->load->helper('url');
        $this->load->helper('form');
		$this->load->model('m_yes');
        $this->load->library('pagination');
        $this->load->database();
	}

	public function input_data()
	{
		$data['user'] = $this->session->userdata['login']['nama'];
		$data['perusahaan'] = $this->m_yes->get_perusahaan()->result_array();
		//$data['event'] = $this->m_the_coach->get_event()->result_array();
		$this->load->view('v_input_yes',$data);
		$this->load->helper("url","form");
	}
	
    function data_anggota(){
		$data['perusahaan'] = $this->m_yes->get_perusahaan()->result_array();
		$data['user'] = $this->session->userdata['login']['nama'];
		$config['base_url']= base_url()."data_yes/data_anggota";
		$config['total_rows']= $this->db->query("SELECT * FROM yes")->num_rows();
		$config['per_page']=10;
		$config['num_links'] = 2;
		$config['uri_segment']=3;
 
        //Tambahan untuk styling
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
 
        $config['first_link']='<strong>Pertama</strong>';
        $config['last_link']='<strong>Terakhir</strong>';
        $config['next_link']='<i class="fa fa-angle-double-right"></i>';
        $config['prev_link']='<i class="fa fa-angle-double-left"></i>';
        $this->pagination->initialize($config);
 
        $data['list']=$this->m_yes->get_all_data($config);
		$data['total']=$this->m_yes->jumlah_data_yes()->result();
        $this->load->view('v_data_yes', $data);
    }
	
	function pencarian() {
		$data['perusahaan'] = $this->m_yes->get_perusahaan()->result_array();
		$data['user'] = $this->session->userdata['login']['nama'];
		$data['total']=$this->m_yes->jumlah_data_yes()->result();
		$pencarian = $this->input->post('cari');
		$offset = $this->uri->segment(2, 0);
		$total = 5000;
		$result = $this->m_yes->cari_data($pencarian,$offset,$total);
		$config['base_url']= base_url()."data_yes/data_anggota";
		$config['total_rows']= $total;
		$config['per_page']=5000;
		$config['num_links'] = 2;
		$config['uri_segment']=3;
 
        //Tambahan untuk styling
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
 
        $config['first_link']='<strong>Pertama</strong>';
        $config['last_link']='<strong>Terakhir</strong>';
        $config['next_link']='<i class="fa fa-angle-double-right"></i>';
        $config['prev_link']='<i class="fa fa-angle-double-left"></i>';
        $this->pagination->initialize($config);
		$data['list'] = $result['data'];
	   
		$this->load->view('v_data_yes',$data);
	}
	
	function cekData(){
    	$arr = array(
			"id_yes"=>$this->input->get("id")
			);		
		$r = $this->m_yes->getDataYes($arr);
		echo json_encode($r);
    }
	
	function getView(){
    	$arr = array(
			"id_yes"=>$this->input->get("id")
			);		
		$r = $this->m_yes->getViewYes($arr);
		echo json_encode($r);
    }
	
	public function tambah_yes()
	{
		$chq_date2 = str_replace('/', '-', $this->input->post('tgl_lahir') );
		$tgl_lahir = date("Y-m-d", strtotime($chq_date2));
		
		$data = array('nama_lengkap' => $this->input->post('nama_lengkap'),
					'panggilan' => $this->input->post('panggilan'),
					'perguruan_tinggi' => $this->input->post('perguruan_tinggi'),
					'fakultas' => $this->input->post('fakultas'),
					'perusahaan' => $this->input->post('perusahaan'),
					'agama' => $this->input->post('agama'),
					'tempat_lahir' => $this->input->post('tempat_lahir'),
					'tgl_lahir' => $tgl_lahir,
					'alamat' => $this->input->post('alamat'),
					'tempat_lahir' => $this->input->post('tempat_lahir'),
					'tlp' => $this->input->post('telepon'),
					'no_hp' => $this->input->post('no_hp'),
					'bbm' => $this->input->post('bbm'),
					'line' => $this->input->post('line'),
					'facebook' => $this->input->post('facebook'),
					'twitter' => $this->input->post('twitter'),
					'email' => $this->input->post('email')
					);
		$res = $this->m_yes->insert_data($data);	
		if($res>=1){
			$this->session->set_flashdata('pesan',"<div class='alert alert-success'>Data Anggota Berhasil Disimpan</div>");	
			redirect('data_yes/input_data', 'refresh');
		}else{
			$this->session->set_flashdata('pesan',"<div class='alert alert-danger'>Data Anggota Gagal Disimpan</div>");
			redirect('data_yes/input_data', 'refresh');
		}
	}
	
	public function update_yes()
	{
		$chq_date2 = str_replace('/', '-', $this->input->post('tgl_lahir') );
		$tgl_lahir = date("Y-m-d", strtotime($chq_date2));
		
		$id['id_yes'] = $this->input->post('id_yes');
		$data = array('nama_lengkap' => $this->input->post('nama_lengkap'),
					'panggilan' => $this->input->post('panggilan'),
					'perguruan_tinggi' => $this->input->post('perguruan_tinggi'),
					'fakultas' => $this->input->post('fakultas'),
					'perusahaan' => $this->input->post('perusahaan'),
					'agama' => $this->input->post('agama'),
					'tempat_lahir' => $this->input->post('tempat_lahir'),
					'tgl_lahir' => $tgl_lahir,
					'alamat' => $this->input->post('alamat'),
					'tempat_lahir' => $this->input->post('tempat_lahir'),
					'tlp' => $this->input->post('telepon'),
					'no_hp' => $this->input->post('no_hp'),
					'bbm' => $this->input->post('bbm'),
					'line' => $this->input->post('line'),
					'facebook' => $this->input->post('facebook'),
					'twitter' => $this->input->post('twitter'),
					'email' => $this->input->post('email')
					);
		$res = $this->m_yes->update_data($data,$id);	
		if($res>=1){
			$this->session->set_flashdata('pesan',"<div class='alert alert-danger'>Data Anggota Gagal Disimpan</div>");
			redirect('data_yes', 'refresh');
		}else{
			$this->session->set_flashdata('pesan',"<div class='alert alert-success'>Data Anggota Berhasil Disimpan</div>");	
			redirect('data_yes', 'refresh');
		}
	}
	
	public function hapus_yes($id)
	{
		$id = $this->input->post('id_yes');
		$res = $this->m_yes->delete_data($id);
		if($res>=0){
		$this->session->set_flashdata('pesan',"<div class='alert alert-success alert-block fade in'>Data Anggota Berhasil Dihapus</div>");
		redirect('data_yes', 'refresh');
		}else{
		$this->session->set_flashdata('pesan',"<div class='alert alert-danger alert-block fade in'>Data Anggota Gagal Dihapus</div>");	
		redirect('data_yes', 'refresh');
		}
	}
	
	public function do_upload(){
		$config['upload_path'] = './assets/uploaddata/';
        $config['allowed_types'] = 'xlsx|xls';
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload()){
			$error = array('error' => $this->upload->display_errors());
		}
		else{
			$data = array('upload_data' => $this->upload->data());
            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $filename = $upload_data['file_name'];
            $this->phpexcel_yes->upload_data($filename);
           // unlink('./assets/uploaddata/'.$filename);
			delete_files($media['file_path']);
            redirect('data_yes', 'refresh');
		}
	}
	
}
