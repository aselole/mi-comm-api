<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_training extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (isset($this->session->userdata['login'])) {
		$username = ($this->session->userdata['login']['nama']);
		} else {
		redirect("login/index");
		}
		$this->load->helper('form');
		$this->load->model('m_training');
		$this->load->helper('url');
        $this->load->library('pagination');
        $this->load->database();
	}
	
	function index(){
		$data['list'] = $this->m_training->get_all_data()->result_array();
		$data['user'] = $this->session->userdata['login']['nama'];
        $this->load->view('v_data_training', $data);
    }
	
	
	function cekData(){
    	$arr = array(
			"id_training"=>$this->input->get("id")
			);		
		$r = $this->m_training->getDataTraining($arr);
		echo json_encode($r);
    }
	
	function getView(){
    	$arr = array(
			"id_training"=>$this->input->get("id")
			);		
		$r = $this->m_training->getViewTraining($arr);
		echo json_encode($r);
    }
	
	public function tambah_training()
	{
		
		$data = array('nama_training' => $this->input->post('nama_training'),
					'deskripsi' => $this->input->post('deskripsi')
					);
		$res = $this->m_training->insert_data($data);	
		if($res>=1){
			$this->session->set_flashdata('pesan',"<div class='alert alert-success'>Data Pelatihan Berhasil Disimpan</div>");	
			redirect('data_training', 'refresh');
		}else{
			$this->session->set_flashdata('pesan',"<div class='alert alert-danger'>Data Pelatihan Gagal Disimpan</div>");
			redirect('data_training', 'refresh');
		}
	}
	
	public function update_training()
	{
	
		$id['id_training'] = $this->input->post('id_training');
		$data = array('nama_training' => $this->input->post('nama_training'),
					'deskripsi' => $this->input->post('deskripsi')
					);
		$res = $this->m_training->update_data($data,$id);	
		if($res>=1){
			$this->session->set_flashdata('pesan',"<div class='alert alert-danger'>Data Pelatihan Gagal Diupdate</div>");
			redirect('data_training', 'refresh');
		}else{
			$this->session->set_flashdata('pesan',"<div class='alert alert-success'>Data Pelatihan Berhasil Diupdate</div>");	
			redirect('data_training', 'refresh');
		}
	}
	
	public function hapus_training($id)
	{
		$id = $this->input->post('id_training');
		$res = $this->m_training->delete_data($id);
		if($res>=0){
		$this->session->set_flashdata('pesan',"<div class='alert alert-success alert-block fade in'>Data Pelatihan Berhasil Dihapus</div>");
		redirect('data_training', 'refresh');
		}else{
		$this->session->set_flashdata('pesan',"<div class='alert alert-danger alert-block fade in'>Data Pelatihan Gagal Dihapus</div>");	
		redirect('data_training', 'refresh');
		}
	}
	
	public function do_upload(){
		$config['upload_path'] = './assets/uploaddata/';
        $config['allowed_types'] = 'xlsx|xls';
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload()){
			$error = array('error' => $this->upload->display_errors());
		}
		else{
			$data = array('upload_data' => $this->upload->data());
            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $filename = $upload_data['file_name'];
            $this->phpexcel_model->upload_data($filename);
           // unlink('./assets/uploaddata/'.$filename);
			delete_files($media['file_path']);
            redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
}
