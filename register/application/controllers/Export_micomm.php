<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Export_micomm extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->library(array('Export/PHPExcel','Export/PHPExcel/IOFactory.php'));
        $this->load->model('m_export_micomm'); // memanggil model 
    }
    public function export(){
        $ambildata = $this->m_export_micomm->export_micomm();
         
        if(count($ambildata)>0){
            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()
                  ->setCreator("MIC Transformer") //creator
                    ->setTitle("Data Micomm");  //file title
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Sample Sheet'); //sheet title
             
            $objget->getStyle("A1:V1")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '92d050')
                    ),
                    'font' => array(
                        'color' => array('rgb' => '000000')
                    )
                )
            );
 
            //table header
            $cols = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V");
             
            $val = array("ID", "Perusahaan", "Posisi", "Nama", "Panggilan", "JK", "Alamat", "Alamat Penempatan", "Agama", "Tempat Lahir", "Tanggal Lahir", "Telepon", "Mobile", "WA", "BBM",	"Line", "Facebook", "Twitter", "Instagram", "Email kantor", "Email Pribadi", "Group WA");

            
            for ($a=0;$a<22; $a++) 
            {
                $objset->setCellValue($cols[$a].'1', $val[$a]);
                 
                //Setting lebar cell
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(50);
                $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(50); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(50); 
             
                $style = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                );
                $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
            }
             
            $baris  = 2;
            foreach ($ambildata as $frow){
                 
                //pemanggilan sesuaikan dengan nama kolom tabel
                $objset->setCellValue("A".$baris, $frow->id_micomm); 
                $objset->setCellValue("B".$baris, $frow->perusahaan); 
                $objset->setCellValue("C".$baris, $frow->posisi);
				$objset->setCellValue("D".$baris, $frow->nama_lengkap); 
				$objset->setCellValue("E".$baris, $frow->nama_panggilan); 
                $objset->setCellValue("F".$baris, $frow->jenis_kelamin); 
                $objset->setCellValue("G".$baris, $frow->alamat);
				$objset->setCellValue("H".$baris, $frow->alamat_penempatan); 
                $objset->setCellValue("I".$baris, $frow->agama); 
                $objset->setCellValue("J".$baris, $frow->tempat_lahir);
				$objset->setCellValue("K".$baris, $frow->tgl_lahir); 
                $objset->setCellValue("L".$baris, $frow->no_telepon); 
                $objset->setCellValue("M".$baris, $frow->no_hp);
				$objset->setCellValue("N".$baris, $frow->no_wa); 
                $objset->setCellValue("O".$baris, $frow->pin_bb); 
                $objset->setCellValue("P".$baris, $frow->id_line);
				$objset->setCellValue("Q".$baris, $frow->id_facebook); 
                $objset->setCellValue("R".$baris, $frow->id_twitter); 
                $objset->setCellValue("S".$baris, $frow->instagram);
				$objset->setCellValue("T".$baris, $frow->email_kantor); 
                $objset->setCellValue("U".$baris, $frow->email_pribadi); 
                $objset->setCellValue("V".$baris, $frow->group_wa);
                 
                //Set number value
                $objPHPExcel->getActiveSheet()->getStyle('V1:V'.$baris)->getNumberFormat()->setFormatCode('0');
                 
                $baris++;
            }
             
            $objPHPExcel->getActiveSheet()->setTitle('Data Micomm');
 
            $objPHPExcel->setActiveSheetIndex(0);  
            $filename = urlencode("Data_Micomm_".date("Y-m-d H:i:s").".xls");
               
              header('Content-Type: application/vnd.ms-excel'); //mime type
              header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
              header('Cache-Control: max-age=0'); //no cache
 
            $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');                
            $objWriter->save('php://output');
        }else{
            redirect('Excel');
        }
    }
}