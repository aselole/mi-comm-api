<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_mi_comm extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (isset($this->session->userdata['login'])) {
		$username = ($this->session->userdata['login']['nama']);
		} else {
		redirect("login/index");
		}
		$this->load->library("PHPExcel");
		$this->load->model("phpexcel_micomm");
		$this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('file');
		$this->load->model('m_mi_comm');
        $this->load->library('pagination');
        $this->load->database();
	}

	public function input_data()
	{
		$data['user']= $this->session->userdata['login']['nama'];
		$data['event'] = $this->m_mi_comm->get_event()->result_array();
		$data['perusahaan'] = $this->m_mi_comm->get_perusahaan()->result_array();
		$this->load->view('v_input_mi_comm',$data);
		$this->load->helper("url","form");
	}
	
    function data_anggota(){
		$data['user']= $this->session->userdata['login']['nama'];
		$data['event'] = $this->m_mi_comm->get_event()->result_array();
		$data['topik'] = $this->m_mi_comm->get_topik()->result_array();
		$data['perusahaan'] = $this->m_mi_comm->get_perusahaan()->result_array();
		$data['perusahaan2'] = $this->m_mi_comm->get_perusahaan()->result_array();
		$data['event1'] = $this->m_mi_comm->get_event()->result_array();
		$config['base_url']= base_url()."data_mi_comm/data_anggota";
		$config['total_rows']= $this->db->query("SELECT * FROM micomm")->num_rows();
		$config['per_page']=10;
		$config['num_links'] = 2;
		$config['uri_segment']=3;
 
        //Tambahan untuk styling
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
 
        $config['first_link']='<strong>Pertama</strong>';
        $config['last_link']='<strong>Terakhir</strong>';
        $config['next_link']='<i class="fa fa-angle-double-right"></i>';
        $config['prev_link']='<i class="fa fa-angle-double-left"></i>';
        $this->pagination->initialize($config);
 
        $data['list']=$this->m_mi_comm->get_all_data($config);
		$data['total']=$this->m_mi_comm->jumlah_data_micomm()->result();
        $this->load->view('v_data_mi_comm', $data);
    }
	
	function pencarian() {
		$data['user']= $this->session->userdata['login']['nama'];
		$data['event'] = $this->m_mi_comm->get_event()->result_array();
		$data['perusahaan'] = $this->m_mi_comm->get_perusahaan()->result_array();
		$data['perusahaan2'] = $this->m_mi_comm->get_perusahaan()->result_array();
		$data['event1'] = $this->m_mi_comm->get_event()->result_array();
		$data['topik'] = $this->m_mi_comm->get_topik()->result_array();
		$data['total']=$this->m_mi_comm->jumlah_data_micomm()->result();
		$pencarian = $this->input->post('cari');
		$offset = $this->uri->segment(2, 0);
		$total = 5000;
		$result = $this->m_mi_comm->cari_data($pencarian,$offset,$total);
		$config['base_url']= base_url()."data_mi_comm/data_anggota";
		$config['total_rows']= $total;
		$config['per_page']=5000;
		$config['num_links'] = 2;
		$config['uri_segment']=3;
 
        //Tambahan untuk styling
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
 
        $config['first_link']='<strong>Pertama</strong>';
        $config['last_link']='<strong>Terakhir</strong>';
        $config['next_link']='<i class="fa fa-angle-double-right"></i>';
        $config['prev_link']='<i class="fa fa-angle-double-left"></i>';
        $this->pagination->initialize($config);
		$data['list'] = $result['data'];
	   
		$this->load->view('v_data_mi_comm',$data);
	}
	
	function cekData(){
    	$arr = array(
			"id_micomm"=>$this->input->get("id")
			);		
		$r = $this->m_mi_comm->getDataMicomm($arr);
		echo json_encode($r);
    }
	
	function getView(){
    	$arr = array(
			"id_micomm"=>$this->input->get("id")
			);		
		$r = $this->m_mi_comm->getViewMicomm($arr);
		echo json_encode($r);
    }
	
	public function get_event($id)
	{
		$data = $this->m_mi_comm->tampil_event($id);
		echo json_encode($data);
	}
	
	
	public function tambah_micomm()
	{
		$chq_date2 = str_replace('/', '-', $this->input->post('tgl_lahir'));
		$tgl_lahir = date("Y-m-d", strtotime($chq_date2));
		$chq_date3 = str_replace('/', '-', $this->input->post('tanggal_mulai'));
		$tanggal_mulai = date("Y-m-d", strtotime($chq_date3));
		$chq_date3 = str_replace('/', '-', $this->input->post('tanggal_selesai'));
		$tanggal_selesai = date("Y-m-d", strtotime($chq_date3));
		
		$data = array('nama_lengkap' => $this->input->post('nama_lengkap'),
					'nama_panggilan' => $this->input->post('nama_panggilan'),
					'agama' => $this->input->post('agama'),
					'tempat_lahir' => $this->input->post('tempat_lahir'),
					'tgl_lahir' => $tgl_lahir,
					'jenis_kelamin' => $this->input->post('jenis_kelamin'),
					'no_telepon' => $this->input->post('telepon'),
					'no_hp' => $this->input->post('no_hp'),
					'no_wa' => $this->input->post('no_wa'),
					'alamat' => $this->input->post('alamat'),
					'email_pribadi' => $this->input->post('email_pribadi'),
					'pin_bb' => $this->input->post('pin_bb'),
					'id_line' => $this->input->post('id_line'),
					'id_facebook' => $this->input->post('id_facebook'),
					'id_twitter' => $this->input->post('id_twitter'),
					'instagram' => $this->input->post('instagram'),
					'group_wa' => $this->input->post('group_wa'),
					'alamat_penempatan' => $this->input->post('alamat_penempatan'),
					'perusahaan' => $this->input->post('perusahaan'),
					'email_kantor' => $this->input->post('email_kantor'),
					'posisi' => $this->input->post('posisi'),
					'id_event' => $this->input->post('event'),
					'lokasi' => $this->input->post('lokasi'),
					'topik' => $this->input->post('topik'),
					'hobby' => $this->input->post('hobby'),
					'tanggal_mulai' => $tanggal_mulai,
					'tanggal_selesai' => $tanggal_selesai
					);
		$res = $this->m_mi_comm->insert_data($data);	
		if($res>=1){
			$this->session->set_flashdata('pesan',"<div class='alert alert-success'>Data Anggota Berhasil Disimpan</div>");	
			redirect('data_mi_comm/input_data', 'refresh');
		}else{
			$this->session->set_flashdata('pesan',"<div class='alert alert-danger'>Data Anggota Gagal Disimpan</div>");
			redirect('data_mi_comm/input_data', 'refresh');
		}
	}
	
	public function update_micomm()
	{
		$chq_date2 = str_replace('/', '-', $this->input->post('tgl_lahir'));
		$tgl_lahir = date("Y-m-d", strtotime($chq_date2));
		$chq_date3 = str_replace('/', '-', $this->input->post('tanggal_mulai'));
		$tanggal_mulai = date("Y-m-d", strtotime($chq_date3));
		$chq_date3 = str_replace('/', '-', $this->input->post('tanggal_selesai'));
		$tanggal_selesai = date("Y-m-d", strtotime($chq_date3));
		//$data['event'] = $this->m_mi_comm->get_event()->result_array();
		/*//Check whether user upload picture
            if(!empty($_FILES['picture']['name'])){
                $config['upload_path'] = 'assets/foto/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $_FILES['picture']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('picture')){
                    $uploadData = $this->upload->data();
                    $picture = $uploadData['file_name'];
                }else{
                    $picture = '';
                }
            }else{
                $picture = '';
            }*/
		$id['id_micomm'] = $this->input->post('id_micomm');
		$data = array('nama_lengkap' => $this->input->post('nama_lengkap'),
					'nama_panggilan' => $this->input->post('nama_panggilan'),
					'agama' => $this->input->post('agama'),
					'tempat_lahir' => $this->input->post('tempat_lahir'),
					'tgl_lahir' => $tgl_lahir,
					'jenis_kelamin' => $this->input->post('jenis_kelamin'),
					'no_telepon' => $this->input->post('telepon'),
					'no_hp' => $this->input->post('no_hp'),
					'no_wa' => $this->input->post('no_wa'),
					'alamat' => $this->input->post('alamat'),
					'email_pribadi' => $this->input->post('email_pribadi'),
					'pin_bb' => $this->input->post('pin_bb'),
					'id_line' => $this->input->post('id_line'),
					'id_facebook' => $this->input->post('id_facebook'),
					'id_twitter' => $this->input->post('id_twitter'),
					'instagram' => $this->input->post('instagram'),
					'group_wa' => $this->input->post('group_wa'),
					//'picture' => $picture,
					'alamat_penempatan' => $this->input->post('alamat_penempatan'),
					'perusahaan' => $this->input->post('perusahaan'),
					'email_kantor' => $this->input->post('email_kantor'),
					'posisi' => $this->input->post('posisi'),
					'id_event' => $this->input->post('event'),
					'lokasi' => $this->input->post('lokasi'),
					'topik' => $this->input->post('topik'),
					'hobby' => $this->input->post('hobby'),
					'tanggal_mulai' => $tanggal_mulai,
					'tanggal_selesai' => $tanggal_selesai
					);
		$res = $this->m_mi_comm->update_data($data,$id);	
		if($res>=1){
			$this->session->set_flashdata('pesan',"<div class='alert alert-danger'>Data Anggota Gagal Disimpan</div>");
			redirect('data_mi_comm/data_anggota', 'refresh');
		}else{
			$this->session->set_flashdata('pesan',"<div class='alert alert-success'>Data Anggota Berhasil Disimpan</div>");	
			redirect('data_mi_comm/data_anggota', 'refresh');
		}
	}
	
	public function hapus_micomm($id)
	{
		$id = $this->input->post('id_micomm');
		$res = $this->m_mi_comm->delete_data($id);
		if($res>=0){
		$this->session->set_flashdata('pesan',"<div class='alert alert-success'>Data Anggota Berhasil Dihapus</div>");
		redirect('data_mi_comm/data_anggota', 'refresh');
		}else{
		$this->session->set_flashdata('pesan',"<div class='alert alert-success'>Data Anggota Gagal Dihapus</div>");	
		redirect('data_mi_comm/data_anggota', 'refresh');
		}
	}
	
	public function do_upload(){
		$config['upload_path'] = './assets/uploaddata/';
        $config['allowed_types'] = 'xlsx|xls';
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload()){
			$error = array('error' => $this->upload->display_errors());
		}
		else{
			$data = array('upload_data' => $this->upload->data());
            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $filename = $upload_data['file_name'];
            $this->phpexcel_micomm->upload_data($filename);
           // unlink('./assets/uploaddata/'.$filename);
			delete_files($media['file_path']);
            redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function export()
	{
		$this->load->plugin('to_excel');
		$query = $this->db->query("select * from micomm");
		$nama = "excel_micomm";
		to_excel($query, $nama);
	}
	
	public function sortir_event() {
		
		$data['user']= $this->session->userdata['login']['nama'];
		$data['event'] = $this->m_mi_comm->get_event()->result_array();
		$data['topik'] = $this->m_mi_comm->get_topik()->result_array();
		$data['perusahaan'] = $this->m_mi_comm->get_perusahaan()->result_array();
		$data['perusahaan2'] = $this->m_mi_comm->get_perusahaan()->result_array();
		$data['event1'] = $this->m_mi_comm->get_event()->result_array();
		$data['total']=$this->m_mi_comm->jumlah_data_micomm()->result();
		$event = $this->input->post('event');
		$offset = $this->uri->segment(2, 0);
		$total = 5000;
		$result = $this->m_mi_comm->cari_event($event,$offset,$total);
		$config['base_url']= base_url()."data_mi_comm/data_anggota";
		$config['total_rows']= $total;
		$config['per_page']=5000;
		$config['num_links'] = 2;
		$config['uri_segment']=3;
 
        //Tambahan untuk styling
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
 
        $config['first_link']='<strong>Pertama</strong>';
        $config['last_link']='<strong>Terakhir</strong>';
        $config['next_link']='<i class="fa fa-angle-double-right"></i>';
        $config['prev_link']='<i class="fa fa-angle-double-left"></i>';
        $this->pagination->initialize($config);
		$data['list'] = $result['data'];
	   
		$this->load->view('v_data_mi_comm',$data);
		
	}
	
	public function sortir_topik() {
		
		$data['user']= $this->session->userdata['login']['nama'];
		$data['event'] = $this->m_mi_comm->get_event()->result_array();
		$data['topik'] = $this->m_mi_comm->get_topik()->result_array();
		$data['perusahaan'] = $this->m_mi_comm->get_perusahaan()->result_array();
		$data['perusahaan2'] = $this->m_mi_comm->get_perusahaan()->result_array();
		$data['event1'] = $this->m_mi_comm->get_event()->result_array();
		$data['total']=$this->m_mi_comm->jumlah_data_micomm()->result();
		$topik = $this->input->post('topik');
		$offset = $this->uri->segment(2, 0);
		$total = 5000;
		$result = $this->m_mi_comm->cari_topik($topik,$offset,$total);
		$config['base_url']= base_url()."data_mi_comm/data_anggota";
		$config['total_rows']= $total;
		$config['per_page']=5000;
		$config['num_links'] = 2;
		$config['uri_segment']=3;
 
        //Tambahan untuk styling
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
 
        $config['first_link']='<strong>Pertama</strong>';
        $config['last_link']='<strong>Terakhir</strong>';
        $config['next_link']='<i class="fa fa-angle-double-right"></i>';
        $config['prev_link']='<i class="fa fa-angle-double-left"></i>';
        $this->pagination->initialize($config);
		$data['list'] = $result['data'];
	   
		$this->load->view('v_data_mi_comm',$data);
		
	}
}
