<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_birthday_thecoach extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (isset($this->session->userdata['login'])) {
		$username = ($this->session->userdata['login']['nama']);
		} else {
		redirect("login/index");
		}
		
		$this->load->helper('form','file');
		$this->load->model('m_the_coach');
		$this->load->helper('url');
        $this->load->library('pagination');
        $this->load->database();
	}

	
    function data_anggota(){
		$data['user']= $this->session->userdata['login']['nama'];
		$config['base_url']= base_url()."data_birthday_thecoach/data_anggota";
		$config['total_rows']= $this->db->query("SELECT * FROM tampil_the_coach")->num_rows();
		$config['per_page']=10;
		$config['num_links'] = 2;
		$config['uri_segment']=3;
 
        //Tambahan untuk styling
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
 
        $config['first_link']='<strong>Pertama</strong>';
        $config['last_link']='<strong>Terakhir</strong>';
        $config['next_link']='<i class="fa fa-angle-double-right"></i>';
        $config['prev_link']='<i class="fa fa-angle-double-left"></i>';
        $this->pagination->initialize($config);
 
        $data['list']=$this->m_the_coach->get_all_data($config);
		$data['total']=$this->m_the_coach->jumlah_data_thecoach()->result();
        $this->load->view('v_data_birthday_thecoach', $data);
    }
	
	function pencarian() {
		$data['user']= $this->session->userdata['login']['nama'];
		$data['total']=$this->m_the_coach->jumlah_data_thecoach()->result();
		$data['perusahaan'] = $this->m_the_coach->get_perusahaan()->result_array();
		$pencarian = $this->input->post('bulan')."-".$this->input->post('tanggal');
		$offset = $this->uri->segment(2, 0);
		$total = 5000;
		$result = $this->m_the_coach->cari_tanggal($pencarian,$offset,$total);
		$config['base_url']= base_url()."data_birthday_thecoach/data_anggota";
		$config['total_rows']= $total;
		$config['per_page']=5000;
		$config['num_links'] = 2;
		$config['uri_segment']=3;
 
        //Tambahan untuk styling
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
 
        $config['first_link']='<strong>Pertama</strong>';
        $config['last_link']='<strong>Terakhir</strong>';
        $config['next_link']='<i class="fa fa-angle-double-right"></i>';
        $config['prev_link']='<i class="fa fa-angle-double-left"></i>';
        $this->pagination->initialize($config);
		$data['list'] = $result['data'];
	   
		$this->load->view('v_data_birthday_thecoach',$data);
	}
}
