<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		if (isset($this->session->userdata['login'])) {
		$username = ($this->session->userdata['login']['nama']);
		} else {
		redirect("login/index");
		}
		$this->load->model('m_mi_comm');
		$this->load->model('m_the_coach');
		$this->load->model('m_yes');
		$this->load->model('m_perusahaan_micomm');
		$this->load->model('m_perusahaan_thecoach');
		$this->load->model('m_perusahaan_yes');
		$this->load->helper('form');
$this->load->library('form_validation');
$this->load->library('encrypt');
		//$this->load->model('m_perusahaan');
	}
 
	public function index()
	{
		$data['user'] = $this->session->userdata['login']['nama'];
		$data['micomm'] = $this->m_mi_comm->jumlah_data_micomm()->result_array();
		$data['thecoach'] = $this->m_the_coach->jumlah_data_thecoach()->result_array();
		$data['yes'] = $this->m_yes->jumlah_data_yes()->result_array();
		$data['p_micomm'] = $this->m_perusahaan_micomm->jumlah_data()->result_array();
 		$data['p_thecoach'] = $this->m_perusahaan_thecoach->jumlah_data()->result_array();
		$data['p_yes'] = $this->m_perusahaan_yes->jumlah_data()->result_array();
		$this->load->view('v_dashboard',$data);
	}

	
	
}