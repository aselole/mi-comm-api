<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_perusahaan_micomm extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (isset($this->session->userdata['login'])) {
		$username = ($this->session->userdata['login']['nama']);
		} else {
		redirect("login/index");
		}
		$this->load->helper('form');
		$this->load->model('m_perusahaan_micomm');
		$this->load->helper('url');
        $this->load->library('pagination');
        $this->load->database();
	}

	
    function index(){
		$data['user'] = $this->session->userdata['login']['nama'];
		$data['list'] = $this->m_perusahaan_micomm->get_all_data()->result_array();
        $this->load->view('v_data_perusahaan_micomm', $data);
    }
	
	function cekData(){
    	$arr = array(
			"kode"=>$this->input->get("id")
			);		
		$r = $this->m_perusahaan_micomm->getDataMicomm($arr);
		echo json_encode($r);
    }
	
	function getView(){
    	$arr = array(
			"kode"=>$this->input->get("id")
			);		
		$r = $this->m_perusahaan_micomm->getViewMicomm($arr);
		echo json_encode($r);
    }
	
	public function tambah_perusahaan()
	{
	
		$data = array(
					'nama_perusahaan' => $this->input->post('nama_perusahaan'),
					'bidang' => $this->input->post('bidang')
					);
		$res = $this->m_perusahaan_micomm->insert_data($data);	
		if($res>=1){
			$this->session->set_flashdata('pesan',"<div class='alert alert-success'>Data Perusahaan Berhasil Disimpan</div>");	
			redirect('data_perusahaan_micomm', 'refresh');
		}else{
			$this->session->set_flashdata('pesan',"<div class='alert alert-danger'>Data Perusahaan Gagal Disimpan</div>");
		    redirect('data_perusahaan_micomm', 'refresh');
		}
	}
	
	public function update_perusahaan()
	{
		
		$id['kode'] = $this->input->post('kode');
		$data = array('nama_perusahaan' => $this->input->post('nama_perusahaan'),
					'bidang' => $this->input->post('bidang')
					);
		$res = $this->m_perusahaan_micomm->update_data($data,$id);	
		if($res>=1){
			$this->session->set_flashdata('pesan',"<div class='alert alert-danger'>Data Perusahaan Gagal Diupdate</div>");
			redirect('data_perusahaan_micomm', 'refresh');
		}else{
			$this->session->set_flashdata('pesan',"<div class='alert alert-success'>Data Perusahaan Berhasil Diupdate</div>");	
			redirect('data_perusahaan_micomm', 'refresh');
		}
	}
	
	public function hapus_perusahaan($id)
	{
		$id = $this->input->post('kode');
		$res = $this->m_perusahaan_micomm->delete_data($id);
		if($res>=0){
		$this->session->set_flashdata('pesan',"<div class='alert alert-success alert-block fade in'>Data Perusahaan Berhasil Dihapus</div>");
		redirect('data_perusahaan_micomm', 'refresh');
		}else{
		$this->session->set_flashdata('pesan',"<div class='alert alert-danger alert-block fade in'>Data Perusahaan Gagal Dihapus</div>");	
		redirect('data_perusahaan_micomm', 'refresh');
		}
	}
	
	public function do_upload(){
		$config['upload_path'] = './assets/uploaddata/';
        $config['allowed_types'] = 'xlsx|xls';
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload()){
			$error = array('error' => $this->upload->display_errors());
		}
		else{
			$data = array('upload_data' => $this->upload->data());
            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $filename = $upload_data['file_name'];
            $this->phpexcel_model->upload_data($filename);
           // unlink('./assets/uploaddata/'.$filename);
			delete_files($media['file_path']);
            redirect('data_perusahaan_micomm', 'refresh');
		}
	}
	
}
