<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_the_coach extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (isset($this->session->userdata['login'])) {
		$username = ($this->session->userdata['login']['nama']);
		} else {
		redirect("login/index");
		}
		$this->load->library("PHPExcel");
		$this->load->model("phpexcel_thecoach");
		$this->load->helper('form');
		$this->load->model('m_the_coach');
		$this->load->helper('url');
        $this->load->library('pagination');
        $this->load->database();
	}

	public function input_data()
	{
		$data['user'] = $this->session->userdata['login']['nama'];
		$data['perusahaan'] = $this->m_the_coach->get_perusahaan()->result_array();
		$this->load->view('v_input_the_coach',$data);
		$this->load->helper("url","form");
	}
	
    function data_anggota(){
		$data['user']= $this->session->userdata['login']['nama'];
		$data['perusahaan'] = $this->m_the_coach->get_perusahaan()->result_array();
		$config['base_url']= base_url()."data_the_coach/data_anggota";
		$config['total_rows']= $this->db->query("SELECT * FROM tampil_the_coach")->num_rows();
		$config['per_page']=10;
		$config['num_links'] = 2;
		$config['uri_segment']=3;
 
        //Tambahan untuk styling
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
 
        $config['first_link']='<strong>Pertama</strong>';
        $config['last_link']='<strong>Terakhir</strong>';
        $config['next_link']='<i class="fa fa-angle-double-right"></i>';
        $config['prev_link']='<i class="fa fa-angle-double-left"></i>';
        $this->pagination->initialize($config);
 
        $data['list']=$this->m_the_coach->get_all_data($config);
		$data['total']=$this->m_the_coach->jumlah_data_thecoach()->result();
        $this->load->view('v_data_the_coach', $data);
    }
	
	function pencarian() {
		$data['user']= $this->session->userdata['login']['nama'];
		$data['total']=$this->m_the_coach->jumlah_data_thecoach()->result();
		$data['perusahaan'] = $this->m_the_coach->get_perusahaan()->result_array();
		$pencarian = $this->input->post('cari');
		$offset = $this->uri->segment(2, 0);
		$total = 5000;
		$result = $this->m_the_coach->cari_data($pencarian,$offset,$total);
		$config['base_url']= base_url()."data_the_coach/data_anggota";
		$config['total_rows']= $total;
		$config['per_page']=5000;
		$config['num_links'] = 2;
		$config['uri_segment']=3;
 
        //Tambahan untuk styling
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
 
        $config['first_link']='<strong>Pertama</strong>';
        $config['last_link']='<strong>Terakhir</strong>';
        $config['next_link']='<i class="fa fa-angle-double-right"></i>';
        $config['prev_link']='<i class="fa fa-angle-double-left"></i>';
        $this->pagination->initialize($config);
		$data['list'] = $result['data'];
	   
		$this->load->view('v_data_the_coach',$data);
	}
	
	function cekData(){
    	$arr = array(
			"id_thecoach"=>$this->input->get("id")
			);		
		$r = $this->m_the_coach->getDataTheCoach($arr);
		echo json_encode($r);
    }
	
	function getView(){
    	$arr = array(
			"id_thecoach"=>$this->input->get("id")
			);		
		$r = $this->m_the_coach->getViewTheCoach($arr);
		echo json_encode($r);
    }
	
	public function tambah_thecoach()
	{
		$chq_date2 = str_replace('/', '-', $this->input->post('tgllahir_anggota'));
		$tgl_lahir_anggota = date("Y-m-d", strtotime($chq_date2));
		$chq_date = str_replace('/', '-', $this->input->post('tgllahir_pasangan'));
		$tgl_lahir_pasangan = date("Y-m-d", strtotime($chq_date));
		
		$data = array('no_anggota' => $this->input->post('no_anggota'),
					'perusahaan' => $this->input->post('perusahaan'),
					'nama_anggota' => $this->input->post('nama_anggota'),
					'tgllahir_anggota' => $tgl_lahir_anggota,
					'nama_pasangan' => $this->input->post('nama_pasangan'),
					'tgllahir_pasangan' => $tgl_lahir_pasangan,
					'alamat' => $this->input->post('alamat'),
					'kota' => $this->input->post('kota'),
					'agama' => $this->input->post('agama'),
					'tlp' => $this->input->post('telepon'),
					'hp1' => $this->input->post('no_hp'),
					'hp2' => $this->input->post('no_hp2'),
					'bbm' => $this->input->post('bbm'),
					'email' => $this->input->post('email'),
					'pendidikan' => $this->input->post('pendidikan'),
					'kegiatan' => $this->input->post('kegiatan'),
                                        'group_wa' => $this->input->post('group_wa')
					);
		$res = $this->m_the_coach->insert_data($data);	
		if($res>=1){
			$this->session->set_flashdata('pesan',"<div class='alert alert-success'>Data Anggota Berhasil Disimpan</div>");	
			redirect('data_the_coach/input_data', 'refresh');
		}else{
			$this->session->set_flashdata('pesan',"<div class='alert alert-danger'>Data Anggota Gagal Disimpan</div>");
			redirect('data_the_coach/input_data', 'refresh');
		}
	}
	
	public function update_thecoach()
	{
		$chq_date2 = str_replace('/', '-', $this->input->post('tgllahir_anggota'));
		$tgl_lahir_anggota = date("Y-m-d", strtotime($chq_date2));
		$chq_date = str_replace('/', '-', $this->input->post('tgllahir_pasangan'));
		$tgl_lahir_pasangan = date("Y-m-d", strtotime($chq_date));
		$id['id_thecoach'] = $this->input->post('id_thecoach');
		$data = array('no_anggota' => $this->input->post('no_anggota'),
					'perusahaan' => $this->input->post('perusahaan'),
					'nama_anggota' => $this->input->post('nama_anggota'),
					'tgllahir_anggota' => $tgl_lahir_anggota,
					'nama_pasangan' => $this->input->post('nama_pasangan'),
					'tgllahir_pasangan' => $tgl_lahir_pasangan,
					'alamat' => $this->input->post('alamat'),
					'kota' => $this->input->post('kota'),
					'agama' => $this->input->post('agama'),
					'tlp' => $this->input->post('telepon'),
					'hp1' => $this->input->post('no_hp'),
					'hp2' => $this->input->post('no_hp2'),
					'bbm' => $this->input->post('bbm'),
					'email' => $this->input->post('email'),
					'pendidikan' => $this->input->post('pendidikan'),
					'kegiatan' => $this->input->post('kegiatan'),
                                        'group_wa' => $this->input->post('group_wa')
					);
		$res = $this->m_the_coach->update_data($data,$id);
		if($res>=1){
		$this->session->set_flashdata('pesan',"<div class='alert alert-danger'>Data Anggota Gagal Diupdate</div>");
		redirect('data_the_coach', 'refresh');
		}else{
		$this->session->set_flashdata('pesan',"<div class='alert alert-success'>Data Anggota Berhasil Diupdate</div>");	
		rredirect('data_the_coach', 'refresh');
		}
	}
	
	public function hapus_thecoach($id)
	{
		$id = $this->input->post('id_thecoach');
		$res = $this->m_the_coach->delete_data($id);
		if($res>=0){
		$this->session->set_flashdata('pesan',"<div class='alert alert-success alert-block fade in'>Data Anggota Berhasil Dihapus</div>");
		redirect('data_the_coach', 'refresh');
		}else{
		$this->session->set_flashdata('pesan',"<div class='alert alert-danger alert-block fade in'>Data Anggota Gagal Dihapus</div>");	
		redirect('data_the_coach', 'refresh');
		}
	}
	
	public function do_upload(){
		$config['upload_path'] = './assets/uploaddata/';
        $config['allowed_types'] = 'xlsx|xls';
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload()){
			$error = array('error' => $this->upload->display_errors());
		}
		else{
			$data = array('upload_data' => $this->upload->data());
            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $filename = $upload_data['file_name'];
            $this->phpexcel_thecoach->upload_data($filename);
           // unlink('./assets/uploaddata/'.$filename);
			delete_files($media['file_path']);
            redirect('data_the_coach', 'refresh');
		}
	
	}
}
