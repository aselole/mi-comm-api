<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('m_user');
		if (isset($this->session->userdata['login'])) {
		$username = ($this->session->userdata['login']['nama']);
		} else {
		redirect("login/index");
		}
	}
 
	public function index()
	{
		$data['list'] = $this->m_user->get_all_data()->result_array();
		$data['user']= $this->session->userdata['login']['nama'];
		$this->load->view('v_data_user',$data);
	}
	
	function tambah_user() {
		$data = array('nama' => $this->input->post('nama'),
					'username' => $this->input->post('username'),
					'password' => md5($this->input->post('password')),
					'level' => $this->input->post('level'),
					"last_login" => ""
					);
		$res = $this->m_user->insert_data($data);	
		if($res>=1){
			$this->session->set_flashdata('pesan',"<div class='alert alert-success'>Data User Berhasil Disimpan</div>");	
			redirect($_SERVER['HTTP_REFERER']);
		}else{
			$this->session->set_flashdata('pesan',"<div class='alert alert-danger'>Data User Gagal Disimpan</div>");
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	public function update_user()
	{

		$id['iduser'] = $this->input->post('iduser');
		$data = array('nama' => $this->input->post('nama'),
					'username' => $this->input->post('username'),
					'password' => md5($this->input->post('password')),
					'level' => $this->input->post('level')
					);
		$res = $this->m_user->update_data($data,$id);
		if($res>=1){
		$this->session->set_flashdata('pesan',"<div class='alert alert-danger'>Data User Gagal Diupdate</div>");
		redirect($_SERVER['HTTP_REFERER']);
		}else{
		$this->session->set_flashdata('pesan',"<div class='alert alert-success'>Data User Berhasil Diupdate</div>");	
		redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	public function hapus_user($id)
	{
		$id = $this->input->post('id_user');
		$res = $this->m_user->delete_data($id);
		if($res>=0){
		$this->session->set_flashdata('pesan',"<div class='alert alert-success'>Data User Berhasil Dihapus</div>");
		redirect($_SERVER['HTTP_REFERER']);
		}else{
		$this->session->set_flashdata('pesan',"<div class='alert alert-success'>Data User Gagal Dihapus</div>");	
		redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	function cekUser(){
    	$arr = array(
			"iduser"=>$this->input->get("id")
			);		
		$r = $this->m_user->getDataUser($arr);
		echo json_encode($r);
    }
	
}