<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Issue_comment_model extends MY_Model{

    public $table = 'issue_comments'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
    public function __construct()
    {
        parent::__construct();
        $this->timestamps = FALSE;
        $this->has_many['issue_comment_child'] = array('foreign_model'=>'issue_comment_model','foreign_table'=>'issue_comments','foreign_key'=>'parent_id','local_key'=>'id');
        $this->has_one['user'] = array('foreign_model'=>'user_model','foreign_table'=>'users','foreign_key'=>'id','local_key'=>'user_id');
        $this->has_one['issue'] = array('foreign_model'=>'issue_model','foreign_table'=>'issues','foreign_key'=>'id','local_key'=>'issue_id');
    }

}
