<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Voucher_model extends MY_Model{

    public $table = 'vouchers'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
    public function __construct()
    {
        parent::__construct();
        $this->timestamps = FALSE;
        $this->has_one['promo'] = array('foreign_model'=>'promo_model','foreign_table'=>'promos','foreign_key'=>'id','local_key'=>'promo_id');
    }

    public function ajax_list()
    {
        $dataorder    = array();
        $dataorder[1] = "merchant_nama";
        $dataorder[2] = "promo_judul";
        $dataorder[3] = "promo_is_global";
        $dataorder[4] = "promo_valid_until";
        $dataorder[5] = "kode";
        $dataorder[6] = "is_used";

        $start = intval($_POST['start']);
        $sEcho = intval($_POST['draw']);

        $order  = $this->input->post('order');
        $search = $this->input->post("search");

        $query = "
        select
            a.*,
            b.judul as promo_judul,
            b.is_global as promo_is_global,
            b.valid_until as promo_valid_until,
            b.merchant_id,
            c.nama as merchant_nama
        from vouchers a
        join promos b on (a.promo_id=b.id)
        join merchants c on (b.merchant_id=c.id)
        ";

        if(!empty($this->input->post('merchant_nama'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " LOWER(replace(c.nama, '''', '')) LIKE '%".strtolower($this->input->post('merchant_nama'))."%' ";
        }
        if(!empty($this->input->post('promo_judul'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " LOWER(replace(b.judul, '''', '')) LIKE '%".strtolower($this->input->post('promo_judul'))."%' ";
        }
        if(!empty($this->input->post('promo_is_global'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " b.is_global = $this->input->post('promo_is_global') ";
        }
        if(!empty($this->input->post('valid_until_start')) && !empty($this->input->post('valid_until_end'))){
            $date_from_arr = explode('/', $this->input->post('valid_until_start'));
            $date_from     = date('Y-m-d', strtotime($date_from_arr[2].$date_from_arr[1].$date_from_arr[0]));
            $date_to_arr   = explode('/', $this->input->post('valid_until_end'));
            $date_to       = date('Y-m-d', strtotime($date_to_arr[2].$date_to_arr[1].$date_to_arr[0]));
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " (b.valid_until between '$date_from' and '$date_to') ";
        }
        if(!empty($this->input->post('kode'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " LOWER(replace(a.kode, '''', '')) LIKE '%".strtolower($this->input->post('kode'))."%' ";
        }
        if(!empty($this->input->post('is_used'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " a.is_used = '".$this->input->post('is_used')."'";
        }

        if($order){
            $query .= "order by ".$dataorder[$order[0]["column"]]." ".$order[0]["dir"];
        }

        $iTotalRecords  = $this->db->query("SELECT COUNT(*) AS numrows FROM (".$query.") A")->row()->numrows;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($_REQUEST['start']);
        $query          .= " LIMIT ". ($start) .",".($iDisplayLength);

        $data = $this->db->query($query)->result();
        $i=$iDisplayStart+1;
        $result = array();
        $date_now = date('Y-m-d');
        foreach ($data as $d) {

            $id = $d->id;

            $delete = '';
            if ($d->is_used=='t') {
                $delete='<a href="#" class="btn btn-sm btn-outline red" onclick="event.preventDefault();btn_delete('.$d->id.')" title="delete">
                <i class="fa fa-trash-o fa-lg"></i>
                </a> ';
            }

            $r = array();
            $r[0] = $i++;
            $r[1] = $d->merchant_nama;
            $r[2] = $d->promo_judul;
            $r[3] = $d->promo_is_global;
            $r[4] = date('d/m/Y', strtotime($d->promo_valid_until));
            $r[5] = $d->kode;
            $r[6] = $d->is_used;
            $r[7] = $delete;
            array_push($result, $r);
        }

        $records["data"] = $result;
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return $records;
    }

    public function create()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Tambah User';
        $data['judul'] = '<i class="fa fa-plus"></i> Tambah User';

        $data['mode'] = 'create';
        $data['method'] = 'post';
        $data['action'] = site_url('admin/user/store');

        $date_now = date('Y-m-d');
        $data['promos'] = $this
                            ->promo_model
                            ->fields('id, judul')
                            ->where(array('active'=>'y', 'valid_until >='=>$date_now))
                            ->get_all();

        $this->template->load('layouts/template','voucher/form', $data);
    }

}
