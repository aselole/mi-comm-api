<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Issue_model extends MY_Model{

    public $table = 'issues'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
    public function __construct()
    {
        parent::__construct();
        $this->has_one['author'] = array('foreign_model'=>'user_model','foreign_table'=>'users','foreign_key'=>'id','local_key'=>'author_id');
        $this->has_one['community'] = array('foreign_model'=>'community_model','foreign_table'=>'communities','foreign_key'=>'id','local_key'=>'community_id');
        $this->has_many['issue_comment'] = array('foreign_model'=>'issue_comment_model','foreign_table'=>'issue_comments','foreign_key'=>'issue_id','local_key'=>'id');
    }

    public function get_limit_data($limit, $start) {
        $order     = $this->input->post('order');
        $dataorder = array();
        $where     = array();
        $fields = '
        issues.id,
        issues.community_id,
        issues.judul,
        issues.body,
        issues.author_id,
        issues.status,
        issues.published_at,
        issues.active
        ';

        $i=1;
        $dataorder[$i++] = 'community_nama';
        $dataorder[$i++] = 'judul';
        $dataorder[$i++] = 'body';
        $dataorder[$i++] = 'author_first_name';
        $dataorder[$i++] = 'status';
        $dataorder[$i++] = 'published_at';
        $dataorder[$i++] = 'active';
        if(!empty($this->input->post('community_id'))){
            $where['community_id'] = $this->input->post('community_id');
        }
        if(!empty($this->input->post('judul'))){
            $where['LOWER(issues.judul) LIKE'] = '%'.strtolower($this->input->post('judul')).'%';
        }
        if(!empty($this->input->post('body'))){
            $where['LOWER(issues.body) LIKE'] = '%'.strtolower($this->input->post('body')).'%';
        }
        if(!empty($this->input->post('author_first_name'))){
            $where['LOWER(users.first_name) LIKE'] = '%'.strtolower($this->input->post('author_first_name')).'%';
        }
        if(!empty($this->input->post('status'))){
            $where['issues.status'] = $this->input->post('status');
        }
        if(!empty($this->input->post('published_at_start')) && !empty($this->input->post('published_at_end'))){
            $date_from_arr = explode('/', $this->input->post('published_at_start'));
            $date_from     = date('Y-m-d', strtotime($date_from_arr[2].$date_from_arr[1].$date_from_arr[0]));
            $date_to_arr   = explode('/', $this->input->post('published_at_end'));
            $date_to       = date('Y-m-d', strtotime($date_to_arr[2].$date_to_arr[1].$date_to_arr[0]));
            $where['(issues.published_at >= "'.$date_from.'" and issues.published_at <= "'.$date_to.'")'] = null;
        }
        if(!empty($this->input->post('active'))){
            $where['issues.active'] = $this->input->post('active');
        }

        $this->where($where);
        $result['total_rows'] = count($this
                                        ->with_community('fields:nama|join:true')
                                        ->with_author('fields:first_name|join:true')
                                        ->get_all()
                                    );

        $this->where($where);
        $this->order_by( $dataorder[$order[0]["column"]],  $order[0]["dir"]);
        $this->limit($start, $limit);
        $result['get_db'] = $this
                            ->fields($fields)
                            ->with_community('fields:nama|join:true')
                            ->with_author('fields:first_name|join:true')
                            ->get_all();
        return $result;
    }

    public function upload_pic_issue()
    {
        $config['upload_path'] = './uploads/issue/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['max_size']  = '90000';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('featured_img')) {
            $error = array('error' => $this->upload->display_errors());
            $result['success'] = false;
            $result['message'] = $error['error'];
        } else {
            $upload_data = $this->upload->data();
            $result = array(
                'success' => true,
                'data' => $upload_data,
            );
        }
        return $result;
    }

    public function get_issue_comunity($start = 0 , $limit = 6)
    {
        $start = $start * $limit;

        $this->load->model(array('member_model'));

        $issues = false;
        $member = $this
                    ->member_model
                    ->fields('community_id, user_id')
                    ->where(array('active' => 'y', 'user_id' => logged_user('id')))
                    ->get();
        if ( logged_user('level_id') == 2 )
        {
            $where_author_id = ' community_id = '.$member->community_id;
        }
        else
        {
            $where_author_id = ' author_id = '.logged_user('id');

        }

        if ( $member ) {
            $sql = "SELECT
                    	a.id,
                    	a.community_id,
                    	a.judul,
                    	a.body,
                    	a.featured_img,
                    	a.status,
                    	a.published_at,
                    	(SELECT count(b.issue_id)) AS jumlah_komentar,
                    	c.nama AS community_nama,
                    	users.first_name,
                    	users.last_name
                    FROM
                    	issues AS a
                    LEFT JOIN issue_comments AS b ON (b.issue_id = a.id)
                    LEFT JOIN communities AS c ON (a.community_id = c.id)
                    LEFT JOIN users ON a.author_id = users.id
                    WHERE
                    	$where_author_id
                    AND STATUS IN ('open', 'closed')
                    AND a.active = 'y'
                    GROUP BY
                    	a.id
                    ORDER BY
                    	published_at DESC limit $start , $limit
                    ";
            $issues = $this->db->query($sql)->result();
        }

        return $issues;
    }

    public function get_data()
    {
        $where_author_id = ' author_id = '.logged_user('id');

        $sql = "SELECT
                    a.id,
                    a.community_id,
                    a.judul,
                    a.body,
                    a.featured_img,
                    a.status,
                    a.published_at,
                    (SELECT count(b.issue_id)) AS jumlah_komentar,
                    c.nama AS community_nama,
                    users.first_name,
                    users.last_name
                FROM
                    issues AS a
                LEFT JOIN issue_comments AS b ON (b.issue_id = a.id)
                LEFT JOIN communities AS c ON (a.community_id = c.id)
                LEFT JOIN users ON a.author_id = users.id
                WHERE
                    $where_author_id
                AND STATUS IN ('open', 'closed')
                AND a.active = 'y'
                GROUP BY
                    a.id
                ORDER BY
                published_at DESC ";

        return $this->db->query($sql)->result();
    }

}
