<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Community_model extends MY_Model{

    public $table = 'communities'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
    public function __construct()
    {
        parent::__construct();
        $this->has_one['company'] = array('foreign_model'=>'company_model','foreign_table'=>'company','foreign_key'=>'id','local_key'=>'company_id');
    }

    public function get_limit_data($limit, $start) {
        $order     = $this->input->post('order');
        $dataorder = array();
        $where     = array();

        $i=1;
        $dataorder[$i++] = 'member_jenis';
        $dataorder[$i++] = 'company_nama';
        $dataorder[$i++] = 'nama';
        $dataorder[$i++] = 'ket';
        $dataorder[$i++] = 'alamat';
        $dataorder[$i++] = 'email';
        $dataorder[$i++] = 'telepon1';
        $dataorder[$i++] = 'telepon2';
        $dataorder[$i++] = 'created_at';
        if(!empty($this->input->post('member_jenis'))){
            $where['LOWER(member_jenis) LIKE'] = '%'.strtolower($this->input->post('member_jenis')).'%';
        }
        if(!empty($this->input->post('company_id'))){
            $where['company_id'] = $this->input->post('company_id');
        }
        if(!empty($this->input->post('nama'))){
            $where['LOWER(communities.nama) LIKE'] = '%'.strtolower($this->input->post('nama')).'%';
        }
        if(!empty($this->input->post('ket'))){
            $where['LOWER(communities.ket) LIKE'] = '%'.strtolower($this->input->post('ket')).'%';
        }
        if(!empty($this->input->post('alamat'))){
            $where['LOWER(alamat) LIKE'] = '%'.strtolower($this->input->post('alamat')).'%';
        }
        if(!empty($this->input->post('email'))){
            $where['LOWER(email) LIKE'] = '%'.strtolower($this->input->post('email')).'%';
        }
        if(!empty($this->input->post('telepon1'))){
            $where['LOWER(telepon1) LIKE'] = '%'.strtolower($this->input->post('telepon1')).'%';
        }
        if(!empty($this->input->post('telepon2'))){
            $where['LOWER(telepon2) LIKE'] = '%'.strtolower($this->input->post('telepon2')).'%';
        }
        if(!empty($this->input->post('created_at_start')) && !empty($this->input->post('created_at_end'))){
            $date_from_arr = explode('/', $this->input->post('created_at_start'));
            $date_from     = date('Y-m-d', strtotime($date_from_arr[2].$date_from_arr[1].$date_from_arr[0]));
            $date_to_arr   = explode('/', $this->input->post('created_at_end'));
            $date_to       = date('Y-m-d', strtotime($date_to_arr[2].$date_to_arr[1].$date_to_arr[0]));
            $where['(communities.created_at >= "'.$date_from.'" and communities.created_at <= "'.$date_to.'")'] = null;
        }

        $this->where($where);
        $result['total_rows'] = $this->count_rows();

        $this->where($where);
        $this->order_by( $dataorder[$order[0]["column"]],  $order[0]["dir"]);
        $this->limit($start, $limit);
        $result['get_db'] = $this
                            ->fields('id, member_jenis, nama, ket, alamat, email, telepon1, telepon2, created_at')
                            ->with_company('fields:nama|join:true')
                            ->get_all();
        return $result;
    }

    public function upload_pic_community()
    {
        $config['upload_path'] = './uploads/community/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']  = '90000';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('logo')) {
            $error = array('error' => $this->upload->display_errors());
            $result['success'] = false;
            $result['message'] = $error['error'];
        } else {
            $upload_data = $this->upload->data();
            $result = array(
                'success' => true,
                'data' => $upload_data,
            );
        }
        return $result;
    }

}
