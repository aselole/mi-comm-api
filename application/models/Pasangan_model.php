<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasangan_model extends MY_Model{

    public $table = 'pasangans'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
    public function __construct()
    {
        parent::__construct();
        $this->timestamps = false;
    }

    public function ajax_list()
    {
        $dataorder    = array();
        $dataorder[1] = "company_nama";
        $dataorder[2] = "community_nama";
        $dataorder[3] = "utama_nama";
        $dataorder[4] = "utama_status";
        $dataorder[5] = "pasangan_nama";
        $dataorder[6] = "pasangan_status";

        $start = intval($_POST['start']);
        $sEcho = intval($_POST['draw']);

        $order  = $this->input->post('order');
        $search = $this->input->post("search");

        $query = "
        select
            a.*,
            b.first_name as utama_nama,
            c.first_name as pasangan_nama,
            d.id as community_id,
            d.nama as community_nama,
            e.id as company_id,
            e.nama as company_nama
        from pasangans a
        join users b on (a.utama_user_id=b.id)
        join users c on (a.pasangan_user_id=c.id)
        join members z on (a.utama_user_id=z.user_id)
        join communities d on (z.community_id=d.id)
        join company e on (d.company_id=e.id)
        ";

        if(!empty($this->input->post('company_id'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " e.id = ".$this->input->post('company_id')." ";
        }
        if(!empty($this->input->post('community_id'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " d.id = ".$this->input->post('community_id')." ";
        }
        if(!empty($this->input->post('utama_nama'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " LOWER(replace(b.first_name, '''', '')) LIKE '%".strtolower($this->input->post('utama_nama'))."%' ";
        }
        if(!empty($this->input->post('utama_status'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " LOWER(replace(a.utama_status, '''', '')) LIKE '%".strtolower($this->input->post('utama_status'))."%' ";
        }
        if(!empty($this->input->post('pasangan_nama'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " LOWER(replace(b.first_name, '''', '')) LIKE '%".strtolower($this->input->post('pasangan_nama'))."%' ";
        }
        if(!empty($this->input->post('pasangan_status'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " LOWER(replace(a.pasangan_status, '''', '')) LIKE '%".strtolower($this->input->post('pasangan_status'))."%' ";
        }

        if($order){
            $query .= "order by ".$dataorder[$order[0]["column"]]." ".$order[0]["dir"];
        }

        $iTotalRecords  = $this->db->query("SELECT COUNT(*) AS numrows FROM (".$query.") A")->row()->numrows;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($_REQUEST['start']);
        $query          .= " LIMIT ". ($start) .",".($iDisplayLength);

        $data = $this->db->query($query)->result();
        $i=$iDisplayStart+1;
        $result = array();
        foreach ($data as $d) {

            $id = $d->id;

            $edit='<a href="'.site_url('admin/pasangan/edit/').$id.'" class="btn btn-sm btn-outline green" title="abaikan">
            <i class="fa fa-pencil fa-lg"></i>
            </a> ';

            $delete='<a href="#" class="btn btn-sm btn-outline red" onclick="event.preventDefault();btn_delete('.$d->id.')" title="delete">
            <i class="fa fa-trash-o fa-lg"></i>
            </a> ';

            $r = array();
            $r[0] = $i++;
            $r[1] = $d->company_nama;
            $r[2] = $d->community_nama;
            $r[3] = $d->utama_nama;
            $r[4] = $d->utama_status;
            $r[5] = $d->pasangan_nama;
            $r[6] = $d->pasangan_status;
            $r[7] = $edit.$delete;
            array_push($result, $r);
        }

        $records["data"] = $result;
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return $records;
    }

}
