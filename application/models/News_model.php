<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News_model extends MY_Model{

    public $table = 'news'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
    public function __construct()
    {
        parent::__construct();
        $this->has_one['user'] = array('foreign_model'=>'user_model','foreign_table'=>'users','foreign_key'=>'id','local_key'=>'author_id');
        $this->has_many['image_news'] = array('foreign_model'=>'image_news_model','foreign_table'=>'image_news','foreign_key'=>'news_id','local_key'=>'id');

    }

    public function get_limit_data($limit, $start) {
        $order     = $this->input->post('order');
        $dataorder = array();
        $where     = array();

        $i=1;
        $dataorder[$i++] = 'for_whom';
        $dataorder[$i++] = 'judul';
        $dataorder[$i++] = 'body';
        $dataorder[$i++] = 'user_first_name';
        $dataorder[$i++] = 'created_at';
        $dataorder[$i++] = 'published_at';
        $dataorder[$i++] = 'active';
        if(!empty($this->input->post('for_whom'))){
            $where['LOWER(news.for_whom) LIKE'] = '%'.strtolower($this->input->post('for_whom')).'%';
        }
        if(!empty($this->input->post('judul'))){
            $where['LOWER(news.judul) LIKE'] = '%'.strtolower($this->input->post('judul')).'%';
        }
        if(!empty($this->input->post('body'))){
            $where['LOWER(news.body) LIKE'] = '%'.strtolower($this->input->post('body')).'%';
        }
        if(!empty($this->input->post('active'))){
            $where['news.active'] = $this->input->post('active');
        }
        if(!empty($this->input->post('created_at_start')) && !empty($this->input->post('created_at_end'))){
            $date_from_arr = explode('/', $this->input->post('created_at_start'));
            $date_from     = date('Y-m-d', strtotime($date_from_arr[2].$date_from_arr[1].$date_from_arr[0]));
            $date_to_arr   = explode('/', $this->input->post('created_at_end'));
            $date_to       = date('Y-m-d', strtotime($date_to_arr[2].$date_to_arr[1].$date_to_arr[0]));
            $where['(news.created_at >= "'.$date_from.'" and news.created_at <= "'.$date_to.'")'] = null;
        }
        if(!empty($this->input->post('published_at_start')) && !empty($this->input->post('published_at_end'))){
            $date_from_arr = explode('/', $this->input->post('published_at_start'));
            $date_from     = date('Y-m-d', strtotime($date_from_arr[2].$date_from_arr[1].$date_from_arr[0]));
            $date_to_arr   = explode('/', $this->input->post('published_at_end'));
            $date_to       = date('Y-m-d', strtotime($date_to_arr[2].$date_to_arr[1].$date_to_arr[0]));
            $where['(news.published_at >= "'.$date_from.'" and news.published_at <= "'.$date_to.'")'] = null;
        }

        $this->where($where);
        $result['total_rows'] = $this->count_rows();

        $this->where($where);
        $this->order_by( $dataorder[$order[0]["column"]],  $order[0]["dir"]);
        $this->limit($start, $limit);
        $result['get_db'] = $this
                            ->fields('id, for_whom, judul, body, active, published_at, created_at')
                            ->with_user('fields:first_name|join:true')
                            ->get_all();
        return $result;
    }

    public function jumlah_data_pagination()
    {
        $sql = "
        select
            a.*, b.image
        from news a
        left join image_news b on (b.news_id=a.id and b.is_featured='y')
        order by a.published_at desc
        ";
        return $this->db->query($sql)->num_rows();
    }

    public function data_pagination($number, $offset)
    {
        if (!$offset) {
            $offset = 1;
        }
        $sql = "
        select
            a.*, b.image
        from news a
        left join image_news b on (b.news_id=a.id and b.is_featured='y')
        where 
            for_whom='micomm' and
            active='y'
        order by a.published_at desc
        limit $number
        offset $offset
        ";
        return $this->db->query($sql);
    }

}
