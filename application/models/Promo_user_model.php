<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo_user_model extends MY_Model{

    public $table = 'promos_users'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
    public function __construct()
    {
        parent::__construct();
        $this->timestamps = FALSE;
        $this->has_one['promo'] = array('foreign_model'=>'promo_model','foreign_table'=>'promos','foreign_key'=>'id','local_key'=>'promo_id');
        $this->has_one['user'] = array('foreign_model'=>'user_model','foreign_table'=>'users','foreign_key'=>'id','local_key'=>'user_id');
    }

    public function get_limit_data($limit, $start) {
        $order     = $this->input->post('order');
        $dataorder = array();
        $where     = array();
        $fields = array(
            'promos_users.id',
            'promos_users.promo_id',
            'promos_users.user_id',
            'promos_users.is_claimed',
            'promos_users.claimed_at',
            'promos_users.active',
            'promos_users.created_at',
        );

        $i=1;
        $dataorder[$i++] = 'promo_judul';
        $dataorder[$i++] = 'user_first_name';
        $dataorder[$i++] = 'created_at';
        $dataorder[$i++] = 'is_claimed';
        $dataorder[$i++] = 'claimed_at';
        $dataorder[$i++] = 'active';
        if(!empty($this->input->post('promo_judul'))){
            $where['LOWER(promos.judul) LIKE'] = '%'.strtolower($this->input->post('promo_judul')).'%';
        }
        if(!empty($this->input->post('user_first_name'))){
            $where['LOWER(users.first_name) LIKE'] = '%'.strtolower($this->input->post('user_first_name')).'%';
        }
        if(!empty($this->input->post('created_at_start')) && !empty($this->input->post('created_at_end'))){
            $date_from_arr = explode('/', $this->input->post('created_at_start'));
            $date_from     = date('Y-m-d', strtotime($date_from_arr[2].$date_from_arr[1].$date_from_arr[0]));
            $date_to_arr   = explode('/', $this->input->post('created_at_end'));
            $date_to       = date('Y-m-d', strtotime($date_to_arr[2].$date_to_arr[1].$date_to_arr[0]));
            $where['(date(promos_users.created_at) >= "'.$date_from.'" and date(promos_users.created_at) <= "'.$date_to.'")'] = null;
        }
        if($this->input->post('is_claimed')!='all'){
            if ($this->input->post('is_claimed')=='') {
                $where['is_claimed'] = null;
            } else {
                $where['is_claimed'] = $this->input->post('is_claimed');
            }
        }
        if(!empty($this->input->post('claimed_at_start')) && !empty($this->input->post('claimed_at_end'))){
            $date_from_arr = explode('/', $this->input->post('claimed_at_start'));
            $date_from     = date('Y-m-d', strtotime($date_from_arr[2].$date_from_arr[1].$date_from_arr[0]));
            $date_to_arr   = explode('/', $this->input->post('claimed_at_end'));
            $date_to       = date('Y-m-d', strtotime($date_to_arr[2].$date_to_arr[1].$date_to_arr[0]));
            $where['(claimed_at >= "'.$date_from.'" and claimed_at <= "'.$date_to.'")'] = null;
        }
        if(!empty($this->input->post('active'))){
            $where['LOWER(active) LIKE'] = '%'.strtolower($this->input->post('active')).'%';
        }

        $this->where($where);
        // $result['total_rows'] = $this->count_rows();
        $result['total_rows'] = count($this->with_promo('fields:judul','join:true')->with_user('fields:first_name', 'join:true')->get_all());

        $this->where($where);
        $this->order_by( $dataorder[$order[0]["column"]],  $order[0]["dir"]);
        $this->limit($start, $limit);
        $result['get_db']=$this->fields($fields)->with_promo('fields:judul','join:true')->with_user('fields:first_name', 'join:true')
                            ->get_all();
        return $result;
    }

    public function list_dashboard()
    {
        $between = 'claimed_at between \''.date('Y-m-d 00:01:01').'\' and \''.date('Y-m-d 23:59:59').'\'';
        $sql = "SELECT
                    p.id,
                    p.promo_id,
                    p.user_id,
                    p.is_claimed,
                    p.claimed_at,
                    promos.judul,
                    users.first_name,
                    vouchers.kode,
                    merchants.nama AS merchant_nama
                FROM
                    promos_users AS p
                LEFT JOIN promos ON p.promo_id = promos.id
                LEFT JOIN users ON p.user_id = users.id
                LEFT JOIN vouchers ON p.voucher_id = vouchers.id
                LEFT JOIN merchants ON promos.merchant_id = merchants.id
                WHERE
                    is_claimed = 'y'
                    AND
                    $between
                ORDER BY
                    claimed_at DESC";
        $db = $this->db->query($sql);
        if ($db->num_rows() == 0) {
            $res = FALSE;
        }else{
            $res = $db->result();
        }
        return $res;
    }
}
