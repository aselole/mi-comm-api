<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo_claim_model extends MY_Model{

    public $table = 'promos_claim'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
    public function __construct()
    {
        parent::__construct();
        $this->timestamps = FALSE;
        $this->has_one['promo'] = array('foreign_model'=>'promo_model','foreign_table'=>'promos','foreign_key'=>'id','local_key'=>'promo_id');
        $this->has_one['user'] = array('foreign_model'=>'user_model','foreign_table'=>'users','foreign_key'=>'id','local_key'=>'user_id');
    }

    public function ajax_list()
    {
        $dataorder    = array();
        $dataorder[1] = "promo_judul";
        $dataorder[2] = "merchant_nama";
        $dataorder[3] = "user_first_name";
        $dataorder[4] = "claimed_at";

        $start = intval($_POST['start']);
        $sEcho = intval($_POST['draw']);

        $order  = $this->input->post('order');
        $search = $this->input->post("search");

        $query = "
        select
            a.*,
            b.judul as promo_judul,
            c.first_name as user_first_name,
            d.nama as merchant_nama
        from promos_claim a
        join promos b on (a.promo_id=b.id)
        join users c on (a.user_id=c.id)
        join merchants d on (b.merchant_id=d.id)
        ";

        if(!empty($this->input->post('promo_judul'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " LOWER(replace(promo_judul, '''', '')) LIKE '%".strtolower($this->input->post('promo_judul'))."%' ";
        }
        if(!empty($this->input->post('merchant_nama'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " LOWER(replace(merchant_nama, '''', '')) LIKE '%".strtolower($this->input->post('merchant_nama'))."%' ";
        }
        if(!empty($this->input->post('user_first_name'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " LOWER(replace(user_first_name, '''', '')) LIKE '%".strtolower($this->input->post('user_first_name'))."%' ";
        }
        if(!empty($this->input->post('claimed_at_start')) && !empty($this->input->post('claimed_at_end'))){
            $date_from_arr = explode('/', $this->input->post('claimed_at_start'));
            $date_from     = date('Y-m-d', strtotime($date_from_arr[2].$date_from_arr[1].$date_from_arr[0]));
            $date_to_arr   = explode('/', $this->input->post('claimed_at_end'));
            $date_to       = date('Y-m-d', strtotime($date_to_arr[2].$date_to_arr[1].$date_to_arr[0]));
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " (a.claimed_at between '$date_from' and '$date_to') ";
        }

        if($order){
            $query .= "order by ".$dataorder[$order[0]["column"]]." ".$order[0]["dir"];
        }

        $iTotalRecords  = $this->db->query("SELECT COUNT(*) AS numrows FROM (".$query.") A")->row()->numrows;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($_REQUEST['start']);
        $query          .= " LIMIT ". ($start) .",".($iDisplayLength);

        $data = $this->db->query($query)->result();
        $i=$iDisplayStart+1;
        $result = array();
        foreach ($data as $d) {

            $id = $d->id;

            $view='<a href="#" class="btn btn-xs btn-outline green" onclick="event.preventDefault();btn_view('.$d->id.')" title="view">
            <i class="fa fa-search fa-lg"></i>
            </a> ';

            $r = array();
            $r[0] = $i++;
            $r[1] = $d->promo_judul;
            $r[2] = $d->merchant_nama;
            $r[3] = $d->user_first_name;
            $r[4] = date('d/m/Y', strtotime($d->claimed_at));
            $r[5] = $view;
            array_push($result, $r);
        }

        $records["data"] = $result;
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return $records;
    }

    public function get_limit_data($limit, $start) {
        $order     = $this->input->post('order');
        $dataorder = array();
        $where     = array();
        $fields = array(
            'promos_claim.id',
            'promos_claim.promo_id',
            'promos_claim.user_id',
            'promos_claim.is_claimed',
            'promos_claim.claimed_at',
        );

        $i=1;
        $dataorder[$i++] = 'promo_judul';
        $dataorder[$i++] = 'merchant_nama';
        $dataorder[$i++] = 'user_first_name';
        $dataorder[$i++] = 'claimed_at';
        if(!empty($this->input->post('promo_judul'))){
            $where['LOWER(promos.judul) LIKE'] = '%'.strtolower($this->input->post('promo_judul')).'%';
        }
        if(!empty($this->input->post('merchant_nama'))){
            $where['LOWER(merchants.nama) LIKE'] = '%'.strtolower($this->input->post('merchant_nama')).'%';
        }
        if(!empty($this->input->post('user_first_name'))){
            $where['LOWER(users.first_name) LIKE'] = '%'.strtolower($this->input->post('user_first_name')).'%';
        }
        if(!empty($this->input->post('claimed_at_start')) && !empty($this->input->post('claimed_at_end'))){
            $date_from_arr = explode('/', $this->input->post('claimed_at_start'));
            $date_from     = date('Y-m-d', strtotime($date_from_arr[2].$date_from_arr[1].$date_from_arr[0]));
            $date_to_arr   = explode('/', $this->input->post('claimed_at_end'));
            $date_to       = date('Y-m-d', strtotime($date_to_arr[2].$date_to_arr[1].$date_to_arr[0]));
            $where['(claimed_at >= "'.$date_from.'" and claimed_at <= "'.$date_to.'")'] = null;
        }

        $this->where($where);
        // $result['total_rows'] = $this->count_rows();
        $result['total_rows'] = count($this->with_promo('fields:judul','join:true')->with_user('fields:first_name', 'join:true')->get_all());

        $this->where($where);
        $this->order_by( $dataorder[$order[0]["column"]],  $order[0]["dir"]);
        $this->limit($start, $limit);
        $result['get_db']=$this->fields($fields)->with_promo('fields:judul','join:true')->with_user('fields:first_name', 'join:true')
                            ->get_all();
        return $result;
    }

    public function list_dashboard()
    {
        $between = 'claimed_at between \''.date('Y-m-d 00:01:01').'\' and \''.date('Y-m-d 23:59:59').'\'';
        $sql = "SELECT
                    p.id,
                    p.promo_id,
                    p.user_id,
                    p.is_claimed,
                    p.claimed_at,
                    promos.judul,
                    users.first_name,
                    vouchers.kode,
                    merchants.nama AS merchant_nama
                FROM
                    promos_claim AS p
                LEFT JOIN promos ON p.promo_id = promos.id
                LEFT JOIN users ON p.user_id = users.id
                LEFT JOIN vouchers ON p.voucher_id = vouchers.id
                LEFT JOIN merchants ON promos.merchant_id = merchants.id
                WHERE
                    is_claimed = 'y'
                    AND
                    $between
                ORDER BY
                    claimed_at DESC";
        $db = $this->db->query($sql);
        if ($db->num_rows() == 0) {
            $res = FALSE;
        }else{
            $res = $db->result();
        }
        return $res;
    }

}
