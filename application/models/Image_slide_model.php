<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Image_slide_model extends MY_Model {

	public $table = 'image_slides'; 
	public $primary_key = 'id';
	
    public function get_limit_data($limit, $start) {
        $order     = $this->input->post('order');
        $dataorder = array();
        $where     = array();

        $i=1;
        $dataorder[$i++] = 'file';
        $dataorder[$i++] = 'order';
        $dataorder[$i++] = 'publish';

        $result['total_rows'] = $this->count_rows();

        $this->order_by( $dataorder[$order[0]["column"]],  $order[0]["dir"]);
        $this->limit($start, $limit);
        $result['get_db'] = $this ->get_all();
        return $result;
    }

}

/* End of file Slider_model.php */
/* Location: ./application/models/Slider_model.php */