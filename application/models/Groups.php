<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Groups extends MY_Model{

    protected $_table      = 'groups';
    protected $primary_key = 'id';
    protected $return_type = 'array';
    // public function __construct()
    // {
    //     parent::__construct();
    //     //Codeigniter : Write Less Do More
    //     $this->tablename = "groups";
    // }
    //
    public function list()
    {
        $dataorder    = array();
        $dataorder[1] = "name";
        $dataorder[2] = "description";

        $start = intval($_POST['start']);
        $sEcho = intval($_POST['draw']);

        $order  = $this->input->post('order');
        $search = $this->input->post("search");

        $query = "
        select
        *
        from groups
        ";

        if (!empty($search)) {
            $s_search = str_replace("'","",$search["value"]);
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " ( ";
            $query .= " LOWER(replace(name, '''', '')) LIKE '%".strtolower($s_search)."%' ";
            $query .= " OR LOWER(replace(description, '''', '')) LIKE '%".strtolower($s_search)."%' ";
            $query .= " ) ";
        }

        if($order){
            $query .= "order by ".$dataorder[$order[0]["column"]]." ".$order[0]["dir"];
        }

        $iTotalRecords  = $this->db->query("SELECT COUNT(*) AS numrows FROM (".$query.") A")->row()->numrows;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        // $iDisplayStart  = intval($_REQUEST['start']);
        $query          .= " LIMIT ". ($start) .",".($iDisplayLength);

        $data = $this->db->query($query)->result();
        $i = 0;
        $result = array();
        foreach ($data as $d) {
            $i++;
            $id = $d->id;

            $view = '';
            $edit = '';
            $delete = '';

            $view='<a href="#" onclick="event.preventDefault();btn_view('.$id.')" title="view">
            <i class="fa fa-search"></i>
            </a> ';

            $edit='<a href="#" onclick="event.preventDefault();btn_edit('.$id.')" title="edit">
            <i class="fa fa-pencil"></i>
            </a> ';

            $delete='<a href="#" onclick="event.preventDefault();btn_delete('.$id.')" title="delete">
            <i class="fa fa-times"></i>
            </a> ';


            $r = array();
            $r[0] = $i;
            $r[1] = $d->name;
            $r[2] = $d->description;
            $r[3] = $edit;
            array_push($result, $r);
        }

        $records["data"] = $result;
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return $records;
    }
    //
    // public function getById($id)
    // {
    //     $this->db->where('id', $id);
    //     return $this->db->get($this->tablename);
    // }
    //
    // public function store()
    // {
    //     $data = array(
    //         'name'        => $this->input->post('name'),
    //         'description' => $this->input->post('description'),
    //     );
    //     $group = $this->db->insert($this->tablename, $data);
    //     if ($group) {
    //         $insert_id = $this->db->insert_id();
    //         $result['success']   = true;
    //         $result['insert_id'] = $insert_id;
    //         $result['message']   = 'New Group has been saved successfully.';
    //     }
    //     return $result;
    // }
    //
    // public function update($id)
    // {
    //     $data = array(
    //         'name'        => $this->input->post('name'),
    //         'description' => $this->input->post('description'),
    //     );
    //     $this->db->where('id', $id);
    //     $group = $this->db->update($this->tablename, $data);
    //     if ($group) {
    //         $result['success']   = true;
    //         $result['message']   = 'New Group has been updated successfully.';
    //     }
    //     return $result;
    // }
}
