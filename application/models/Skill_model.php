<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skill_model extends MY_Model{

    public $table = 'skills';
    public $primary_key = 'id';
    public $label = 'id';
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array('id'); // ...Or you can set an array with the fields that cannot be filled by insert/update

    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
    }

    public function get_limit_data($limit, $start) {
        $order     = $this->input->post('order');
        $dataorder = array();
        $where     = array();

        $i=1;
        $dataorder[$i++] = 'nama';
        $dataorder[$i++] = 'ket';
        if(!empty($this->input->post('id'))){
            $where['id'] = $this->input->post('id');
        }
        if(!empty($this->input->post('nama'))){
            $where['LOWER(nama) LIKE'] = '%'.strtolower($this->input->post('nama')).'%';
        }
        if(!empty($this->input->post('ket'))){
            $where['LOWER(ket) LIKE'] = '%'.strtolower($this->input->post('ket')).'%';
        }

        $this->where($where);
        $result['total_rows'] = $this->count_rows();

        $this->where($where);
        $this->order_by( $dataorder[$order[0]["column"]],  $order[0]["dir"]);
        $this->limit($start, $limit);
        $result['get_db']=$this
                            ->get_all();
        return $result;
    }

}
