<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo_model extends MY_Model{

    public $table = 'promos'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
    public function __construct()
    {
        parent::__construct();
        $this->has_one['merchant'] = array('foreign_model'=>'merchant_model','foreign_table'=>'merchants','foreign_key'=>'id','local_key'=>'merchant_id');
        $this->has_many['promo_user'] = array('foreign_model'=>'promo_user_model','foreign_table'=>'promos_users','foreign_key'=>'promo_id','local_key'=>'id');
        $this->has_many['promo_claim'] = array('foreign_model'=>'promo_claim_model','foreign_table'=>'promos_claim','foreign_key'=>'promo_id','local_key'=>'id');
    }

    public function get_limit_data($limit, $start) {
        $order     = $this->input->post('order');
        $dataorder = array();
        $where     = array();
        $fields = '
        promos.id,
        promos.merchant_id,
        promos.judul,
        promos.body,
        promos.base_price,
        promos.discount_price,
        promos.featured_img,
        promos.published_at,
        promos.valid_until,
        promos.is_global,
        promos.active,
        promos.created_at
        ';

        $i=1;
        $dataorder[$i++] = 'is_global';
        $dataorder[$i++] = 'judul';
        $dataorder[$i++] = 'valid_until';
        $dataorder[$i++] = 'is_active';
        $dataorder[$i++] = 'created_at';
        if(!empty($this->input->post('is_global'))){
            $where['LOWER(is_global) LIKE'] = '%'.strtolower($this->input->post('is_global')).'%';
        }
        if(!empty($this->input->post('merchant_id'))){
            $where['merchant_id'] = $this->input->post('merchant_id');
        }
        if(!empty($this->input->post('judul'))){
            $where['LOWER(judul) LIKE'] = '%'.strtolower($this->input->post('judul')).'%';
        }
        if(!empty($this->input->post('valid_until_start')) && !empty($this->input->post('valid_until_end'))){
            $date_from_arr = explode('/', $this->input->post('valid_until_start'));
            $date_from     = date('Y-m-d', strtotime($date_from_arr[2].$date_from_arr[1].$date_from_arr[0]));
            $date_to_arr   = explode('/', $this->input->post('valid_until_end'));
            $date_to       = date('Y-m-d', strtotime($date_to_arr[2].$date_to_arr[1].$date_to_arr[0]));
            $where['(valid_until >= "'.$date_from.'" and valid_until <= "'.$date_to.'")'] = null;
        }
        if(!empty($this->input->post('active'))){
            $where['LOWER(promos.active) = '] = strtolower($this->input->post('active'));
        }
        if(!empty($this->input->post('published_at_start')) && !empty($this->input->post('published_at_end'))){
            $date_from_arr = explode('/', $this->input->post('published_at_start'));
            $date_from     = date('Y-m-d', strtotime($date_from_arr[2].$date_from_arr[1].$date_from_arr[0]));
            $date_to_arr   = explode('/', $this->input->post('published_at_end'));
            $date_to       = date('Y-m-d', strtotime($date_to_arr[2].$date_to_arr[1].$date_to_arr[0]));
            $where['(published_at >= "'.$date_from.'" and published_at <= "'.$date_to.'")'] = null;
        }

        $this->where($where);
        $result['total_rows'] = count($this
                                        ->with_merchant('fields:nama','join:true')
                                        ->get_all()
                                    );
        $this->where($where);
        $this->order_by( $dataorder[$order[0]["column"]],  $order[0]["dir"]);
        $this->limit($start, $limit);
        $result['get_db']=$this
                            ->fields($fields)
                            ->with_merchant('fields:nama|join:true')
                            ->get_all();
        return $result;
    }

    public function promo_voucher($limit, $start) {
        $order     = $this->input->post('order');
        $dataorder = array();
        $where     = array();
        $fields = '
        promos.id,
        promos.merchant_id,
        promos.judul,
        promos.body,
        promos.base_price,
        promos.discount_price,
        promos.featured_img,
        promos.valid_until,
        promos.is_global,
        promos.active,
        promos.created_at
        ';

        $where['promos.active'] = 'y';
        $date_now = date('Y-m-d');
        $where['valid_until >='] = $date_now;

        $i=1;
        $dataorder[$i++] = 'is_global';
        $dataorder[$i++] = 'judul';
        $dataorder[$i++] = 'valid_until';
        $dataorder[$i++] = 'is_active';
        $dataorder[$i++] = 'created_at';
        if(!empty($this->input->post('is_global'))){
            $where['LOWER(is_global) LIKE'] = '%'.strtolower($this->input->post('is_global')).'%';
        }
        if(!empty($this->input->post('merchant_id'))){
            $where['merchant_id'] = $this->input->post('merchant_id');
        }
        if(!empty($this->input->post('judul'))){
            $where['LOWER(judul) LIKE'] = '%'.strtolower($this->input->post('judul')).'%';
        }
        // if(!empty($this->input->post('valid_until_start')) && !empty($this->input->post('valid_until_end'))){
        //     $date_from_arr = explode('/', $this->input->post('valid_until_start'));
        //     $date_from     = date('Y-m-d', strtotime($date_from_arr[2].$date_from_arr[1].$date_from_arr[0]));
        //     $date_to_arr   = explode('/', $this->input->post('valid_until_end'));
        //     $date_to       = date('Y-m-d', strtotime($date_to_arr[2].$date_to_arr[1].$date_to_arr[0]));
        //     $where['(valid_until >= "'.$date_from.'" and valid_until <= "'.$date_to.'")'] = null;
        // }
        // if(!empty($this->input->post('created_at_start')) && !empty($this->input->post('created_at_end'))){
        //     $date_from_arr = explode('/', $this->input->post('created_at_start'));
        //     $date_from     = date('Y-m-d', strtotime($date_from_arr[2].$date_from_arr[1].$date_from_arr[0]));
        //     $date_to_arr   = explode('/', $this->input->post('created_at_end'));
        //     $date_to       = date('Y-m-d', strtotime($date_to_arr[2].$date_to_arr[1].$date_to_arr[0]));
        //     $where['(created_at >= "'.$date_from.'" and created_at <= "'.$date_to.'")'] = null;
        // }

        $this->where($where);
        $result['total_rows'] = count($this
                                        ->with_merchant('fields:nama','join:true')
                                        ->get_all()
                                    );
        $this->where($where);
        $this->order_by( $dataorder[$order[0]["column"]],  $order[0]["dir"]);
        $this->limit($start, $limit);
        $result['get_db']=$this
                            ->fields($fields)
                            ->with_merchant('fields:nama|join:true')
                            ->get_all();
        return $result;
    }


    public function list_private ($start = 0 , $limit = 6 )
    {
        $start = $start * $limit;

        $date_now         = date('Y-m-d');
        $batas_date_valid = date('Y-m-d', strtotime($date_now. ' - 7 days'));

        if (logged_user()) {
            $sql = "
                    select
                        a.promo_id,
                        a.is_claimed,
                        a.claimed_at,
                        b.id,
                        b.judul,
                        b.body,
                        b.merchant_id,
                        m.nama as merchant_nama,
                        b.featured_img,
                        b.valid_until,
                        b.created_at,
                        b.published_at
                    from promos_users a
                    join promos b on (a.promo_id=b.id)
                    JOIN merchants m ON (m.id = b.merchant_id)
                    where
                        b.is_global='t' and
                        a.user_id=".logged_user('id')." and
                        b.valid_until >= '$batas_date_valid' and
                        b.active='y' and
                        b.published_at <= '$date_now' and
                        a.active='y'
                        ORDER BY published_at DESC limit $start , $limit
                    ";
            $list_promo_private = $this->db->query($sql);
            if ($list_promo_private->num_rows() == 0) {
                $res = FALSE;
            }else{
                $res = $list_promo_private->result();
            }
        } else {
            $res = FALSE;
        }

        return $res ;
    }

    public function list_global ($start = 0 , $limit = 6 )
    {
        $start = $start * $limit;

        $date_now         = date('Y-m-d');
        $batas_date_valid = date('Y-m-d', strtotime($date_now. ' - 7 days'));
        $user_id = logged_user('id');

        if (logged_user('id')) {
            $sql = "
            select
            a.*,
            b.nama as merchant_nama,
            c.is_claimed
            from promos a
            join merchants b on (a.merchant_id=b.id)
            left join promos_claim c on (c.promo_id=a.id and user_id=$user_id)
            where
                a.valid_until >= '$batas_date_valid' and
                a.published_at <= '$date_now' and
                a.is_global = 'y' and
                a.active = 'y'
            LIMIT $limit OFFSET $start
            ";
        } else {
            $sql = "
            select
            a.*,
            b.nama as merchant_nama,
            c.is_claimed
            from promos a
            join merchants b on (a.merchant_id=b.id)
            left join promos_claim c on (c.promo_id=a.id and user_id=0)
            where
                a.valid_until >= '$batas_date_valid' and
                a.published_at <= '$date_now' and
                a.is_global = 'y' and
                a.active = 'y'
            LIMIT $limit OFFSET $start
            ";
        }
        $promos = $this->db->query($sql);
        // $promos = $this->promo_model
        //                         ->with_merchant()
        //                         ->with_promo_claim()
        //                         ->limit($limit,$start)
        //                         ->order_by('published_at', 'desc')
        //                         ->get_all( array(
        //                             'valid_until >= "'.$batas_date_valid.'"' => null,
        //                             'published_at <=' => $date_now,
        //                             'is_global' => 'y',
        //                             'active'    => 'y',
        //                             ));
        // // echo $this->db->last_query();
        // $promoss = array();
        // if ($promos) {
        //     foreach ($promos as $p) {
        //         if (isset($p->promo_claim))
        //         {
        //             foreach ($p->promo_claim as $k => $v) {
        //                 if (logged_user('id') == $v->user_id ){
        //                     $p->is_claimed = TRUE;
        //                 }
        //             }
        //         }
        //
        //         $promoss[] = $p;
        //     }
        // }
        // var_dump($promos);
        return $promos ;
    }


}
