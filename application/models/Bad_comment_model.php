<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bad_comment_model extends MY_Model{

    public $table = 'bad_comments'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
    public function __construct()
    {
        parent::__construct();
        $this->timestamps = FALSE;

        $this->has_one['user'] = array('foreign_model'=>'user_model','foreign_table'=>'users','foreign_key'=>'id','local_key'=>'pelapor_id');
        $this->has_one['issue_comment'] = array('foreign_model'=>'issue_comment_model','foreign_table'=>'issue_comments','foreign_key'=>'id','local_key'=>'issue_comment_id');
    }

    public function ajax_list()
    {
        $dataorder    = array();
        $dataorder[1] = "community_nama";
        $dataorder[2] = "issue_judul";
        $dataorder[3] = "comment";
        $dataorder[4] = "issue_comment_created_at";
        $dataorder[5] = "user_first_name";

        $start = intval($_POST['start']);
        $sEcho = intval($_POST['draw']);

        $order  = $this->input->post('order');
        $search = $this->input->post("search");

        $query = "
        select
            a.*,
            b.comment,
            b.created_at as issue_comment_created_at,
            c.judul as issue_judul,
            d.first_name as user_first_name,
            e.nama as community_nama
        from bad_comments a
        join issue_comments b on (a.issue_comment_id=b.id)
        join issues c on (b.issue_id=c.id)
        join users d on (a.pelapor_id=d.id)
        join communities e on (c.community_id=e.id)
        ";

        if(!empty($this->input->post('community_nama'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " LOWER(replace(e.nama, '''', '')) LIKE '%".strtolower($this->input->post('community_nama'))."%' ";
        }
        if(!empty($this->input->post('issue_judul'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " LOWER(replace(c.judul, '''', '')) LIKE '%".strtolower($this->input->post('issue_judul'))."%' ";
        }
        if(!empty($this->input->post('comment'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " LOWER(replace(b.comment, '''', '')) LIKE '%".strtolower($this->input->post('comment'))."%' ";
        }
        if(!empty($this->input->post('issue_comment_created_at_start')) && !empty($this->input->post('issue_comment_created_at_end'))){
            $date_from_arr = explode('/', $this->input->post('issue_comment_created_at_start'));
            $date_from     = date('Y-m-d', strtotime($date_from_arr[2].$date_from_arr[1].$date_from_arr[0]));
            $date_to_arr   = explode('/', $this->input->post('issue_comment_created_at_end'));
            $date_to       = date('Y-m-d', strtotime($date_to_arr[2].$date_to_arr[1].$date_to_arr[0]));
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " (b.created_at between '$date_from' and '$date_to') ";
        }
        if(!empty($this->input->post('user_first_name'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " LOWER(replace(d.first_name, '''', '')) LIKE '%".strtolower($this->input->post('user_first_name'))."%' ";
        }

        if($order){
            $query .= "order by ".$dataorder[$order[0]["column"]]." ".$order[0]["dir"];
        }

        $iTotalRecords  = $this->db->query("SELECT COUNT(*) AS numrows FROM (".$query.") A")->row()->numrows;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($_REQUEST['start']);
        $query          .= " LIMIT ". ($start) .",".($iDisplayLength);

        $data = $this->db->query($query)->result();
        $i=$iDisplayStart+1;
        $result = array();
        foreach ($data as $d) {

            $id = $d->id;

            $abaikan='<a href="#" class="btn btn-sm btn-outline green" onclick="event.preventDefault();btn_abaikan('.$d->id.')" title="abaikan">
            <i class="fa fa-check fa-lg"></i>
            </a> ';

            $delete='<a href="#" class="btn btn-sm btn-outline red" onclick="event.preventDefault();btn_delete('.$d->id.')" title="delete">
            <i class="fa fa-trash-o fa-lg"></i>
            </a> ';

            $r = array();
            $r[0] = $i++;
            $r[1] = $d->community_nama;
            $r[2] = $d->issue_judul;
            $r[3] = $d->comment;
            $r[4] = date('d/m/Y H:i', strtotime($d->issue_comment_created_at));
            $r[5] = $d->user_first_name;
            $r[6] = $abaikan.$delete;
            array_push($result, $r);
        }

        $records["data"] = $result;
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return $records;
    }

}
