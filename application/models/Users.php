<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Model{

    protected $_table      = 'users';
    protected $primary_key = 'id';
    protected $return_type = 'array';

    protected $after_get = array('remove_sensitive_data');

    protected function remove_sensitive_data($user)
    {
        unset($user['password']);
        unset($user['salt']);
        unset($user['activation_code']);
        unset($user['forgotten_password_code']);
        unset($user['forgotten_password_time']);
        unset($user['remember_code']);
        return $user;
    }

    public function list()
    {
        $dataorder    = array();
        $dataorder[1] = "group_name";
        $dataorder[2] = "first_name";
        $dataorder[3] = "username";
        $dataorder[4] = "email";
        $dataorder[5] = "phone";
        $dataorder[6] = "created_on";
        $dataorder[7] = "last_login";

        $start = intval($_POST['start']);
        $sEcho = intval($_POST['draw']);

        $order  = $this->input->post('order');
        $search = $this->input->post("search");

        $query = "
        select
            a.*,
            b.name as group_name,
            c.username,
            c.email,
            c.phone,
            c.first_name,
            c.created_on,
            c.last_login,
            c.active
        from users_groups a
        join groups b on (a.group_id = b.id)
        join users c on (a.user_id = c.id)
        where
            active = 1
        ";

        if (!empty($search)) {
            $s_search = str_replace("'","",$search["value"]);
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " ( ";
            $query .= " LOWER(replace(b.name, '''', '')) LIKE '%".strtolower($s_search)."%' ";
            $query .= " OR LOWER(replace(username, '''', '')) LIKE '%".strtolower($s_search)."%' ";
            $query .= " OR LOWER(replace(email, '''', '')) LIKE '%".strtolower($s_search)."%' ";
            $query .= " OR LOWER(replace(phone, '''', '')) LIKE '%".strtolower($s_search)."%' ";
            $query .= " OR LOWER(replace(first_name, '''', '')) LIKE '%".strtolower($s_search)."%' ";
            $query .= " ) ";
        }

        if($order){
            $query .= "order by ".$dataorder[$order[0]["column"]]." ".$order[0]["dir"];
        }

        $iTotalRecords  = $this->db->query("SELECT COUNT(*) AS numrows FROM (".$query.") A")->row()->numrows;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $query          .= " LIMIT ". ($start) .",".($iDisplayLength);

        $data = $this->db->query($query)->result();
        $i = 0;
        $result = array();
        foreach ($data as $d) {
            $i++;
            $id = $d->user_id;

            $view = '';
            $edit = '';
            $delete = '';

            $view='<a href="#" onclick="event.preventDefault();btn_view('.$id.')" title="view">
            <i class="fa fa-search"></i>
            </a> ';

            $edit='<a href="#" onclick="event.preventDefault();btn_edit('.$id.')" title="edit">
            <i class="fa fa-pencil"></i>
            </a> ';

            if ($d->group_name != 'admin') {
                $delete='<a href="#" onclick="event.preventDefault();btn_delete('.$id.')" title="delete">
                <i class="fa fa-times"></i>
                </a> ';
            }


            $r = array();
            $r[0] = $i;
            $r[1] = $d->group_name;
            $r[2] = $d->first_name;
            $r[3] = $d->username;
            $r[4] = $d->email;
            $r[5] = $d->phone;
            $r[6] = date('d/m/Y H:i:s', $d->created_on);
            $r[7] = date('d/m/Y H:i:s', $d->last_login);
            $r[8] = $edit.$delete;
            array_push($result, $r);
        }

        $records["data"] = $result;
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return $records;
    }
}
