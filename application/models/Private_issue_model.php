<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Private_issue_model extends MY_Model{

    public $table = 'private_issues'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
    public function __construct()
    {
        parent::__construct();
        $this->has_one['member'] = array('foreign_model'=>'user_model','foreign_table'=>'users','foreign_key'=>'id','local_key'=>'user_member_id');
        $this->has_one['trainer'] = array('foreign_model'=>'user_model','foreign_table'=>'users','foreign_key'=>'id','local_key'=>'user_trainer_id');
        $this->has_many['private_issue_comment'] = array('foreign_model'=>'private_issue_comment_model','foreign_table'=>'private_issue_comments','foreign_key'=>'private_issue_id','local_key'=>'id');
    }

    public function ajax_list()
    {
        $dataorder    = array();
        $dataorder[1] = "judul";
        $dataorder[2] = "body";
        $dataorder[3] = "member_first_name";
        $dataorder[4] = "trainer_first_name";
        $dataorder[5] = "status";
        $dataorder[6] = "created_at";
        $dataorder[7] = "active";

        $start = intval($_POST['start']);
        $sEcho = intval($_POST['draw']);

        $order  = $this->input->post('order');
        $search = $this->input->post("search");

        $query = "
        select
            a.*,
            b.first_name as member_first_name,
            c.first_name as trainer_first_name
        from private_issues a
        join users b on (a.user_member_id=b.id)
        join users c on (a.user_trainer_id=c.id)
        ";

        if(!empty($this->input->post('judul'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " LOWER(replace(a.judul, '''', '')) LIKE '%".strtolower($this->input->post('judul'))."%' ";
        }
        if(!empty($this->input->post('body'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " LOWER(replace(a.comment, '''', '')) LIKE '%".strtolower($this->input->post('body'))."%' ";
        }
        if(!empty($this->input->post('member_first_name'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " LOWER(replace(b.first_name, '''', '')) LIKE '%".strtolower($this->input->post('member_first_name'))."%' ";
        }
        if(!empty($this->input->post('trainer_first_name'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " LOWER(replace(c.first_name, '''', '')) LIKE '%".strtolower($this->input->post('trainer_first_name'))."%' ";
        }
        if(!empty($this->input->post('status'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " LOWER(replace(a.status, '''', '')) LIKE '%".strtolower($this->input->post('status'))."%' ";
        }
        if(!empty($this->input->post('created_at_start')) && !empty($this->input->post('created_at_end'))){
            $date_from_arr = explode('/', $this->input->post('created_at_start'));
            $date_from     = date('Y-m-d', strtotime($date_from_arr[2].$date_from_arr[1].$date_from_arr[0]));
            $date_to_arr   = explode('/', $this->input->post('created_at_end'));
            $date_to       = date('Y-m-d', strtotime($date_to_arr[2].$date_to_arr[1].$date_to_arr[0]));
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " (a.created_at between '$date_from' and '$date_to') ";
        }
        if(!empty($this->input->post('active'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " LOWER(replace(a.active, '''', '')) LIKE '%".strtolower($this->input->post('active'))."%' ";
        }

        if($order){
            $query .= "order by ".$dataorder[$order[0]["column"]]." ".$order[0]["dir"];
        }

        $iTotalRecords  = $this->db->query("SELECT COUNT(*) AS numrows FROM (".$query.") A")->row()->numrows;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($_REQUEST['start']);
        $query          .= " LIMIT ". ($start) .",".($iDisplayLength);

        $data = $this->db->query($query)->result();
        $i=$iDisplayStart+1;
        $result = array();
        foreach ($data as $d) {

            $id = $d->id;

            $comment='<a href="'.site_url('admin/private_issue/comment/'.$d->id).'" class="btn btn-sm btn-outline green" title="comment">
            <i class="fa fa-comment-o fa-lg"></i>
            </a> ';

            $delete='<a href="#" class="btn btn-sm btn-outline red" onclick="event.preventDefault();btn_delete('.$d->id.')" title="delete">
            <i class="fa fa-trash-o fa-lg"></i>
            </a> ';

            $r = array();
            $r[0] = $i++;
            $r[1] = $d->judul;
            $r[2] = substr($d->body, 0, 100);
            $r[3] = $d->member_first_name;
            $r[4] = $d->trainer_first_name;
            $r[5] = $d->status;
            $r[6] = date('d/m/Y H:i', strtotime($d->created_at));
            $r[7] = $d->active;
            $r[8] = $comment.$delete;
            array_push($result, $r);
        }

        $records["data"] = $result;
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return $records;
    }

    public function get_data ($start = 0 , $limit = 100 )
    {
        $start = $start * $limit;

        $id = logged_user('id');
        if (logged_user('level_id') == 2) {
            $where_user_id = "user_member_id = $id";
        } else {
            $where_user_id = "user_trainer_id = $id";
        }

        $sql = "SELECT
                	a.id,
                	a.user_member_id,
                	a.user_trainer_id,
                	a.judul,
                	a.body,
                	a.`status`,
                	a.active,
                	a.created_at,
                	a.updated_at,
                	users.first_name as member_name,
                	users.last_name,
                    u.first_name as trainer_name,
                	(
                		SELECT
                			COUNT(*)
                		FROM
                			private_issue_comments
                		WHERE
                			private_issue_id = a.id
                		AND user_id != $id
                		AND `read` = 0
                	) as un_read,
                	(
                		SELECT
                			kk.created_at
                		FROM
                			private_issue_comments kk
                		WHERE
                			kk.private_issue_id = a.id
                		-- AND kk.user_id != $id
                		-- AND kk.`read` = 0
                		ORDER BY
                			kk.created_at DESC
                		LIMIT 1
                	) AS comment_update
                FROM
                	private_issues a
                LEFT JOIN users ON a.user_member_id = users.id
                LEFT JOIN users u ON a.user_trainer_id = u.id
                WHERE
                	$where_user_id
                ORDER BY
                	comment_update DESC
                LIMIT $start , $limit
                ";
        $issues = $this->db->query($sql);
        if ($issues->num_rows() == 0) {
            return FALSE;
        } else {
            return $issues->result();
        }
    }

    public function count_unread_msg()
    {
        if ( !logged_user() )
        {
            return 0;
        }
        $id = logged_user('id');
        if (logged_user('level_id') == 2) {
            $where_user_id = "user_member_id = $id";
        } else {
            $where_user_id = "user_trainer_id = $id";
        }

        $sql = "SELECT
            	(
            		SELECT
            			COUNT(*)
            		FROM
            			private_issue_comments
            		WHERE
            			private_issue_id = a.id
            		AND user_id != $id
            		AND `read` = 0
            	) AS un_read
            FROM
            	private_issues a
            WHERE
            	$where_user_id
            ";
        $issues = $this->db->query($sql)->result();

        $cc = 0;
        foreach ( $issues as $k )
        {
            if ( $k->un_read > 0 )
            {
                $cc++;
            }
        }

        return $cc;

    }

    // public function send_notif_firebase($private_issue_id, $user_from, $comment, $data_firebase)
    // {
    //     $this->load->model(array('global_model'));
    //
    //
    //     // var_dump($message);
    //
    // }


}
