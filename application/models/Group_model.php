<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group_model extends MY_Model{

    public $table = 'groups';
    public $primary_key = 'id';
    public $label = 'id';
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array('id'); // ...Or you can set an array with the fields that cannot be filled by insert/update

    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
    }

    public function get_limit_data($limit, $start) {
        $order     = $this->input->post('order');
        $dataorder = array();
        $where     = array();

        $i=1;
        $dataorder[$i++] = 'name';
        $dataorder[$i++] = 'description';
        if(!empty($this->input->post('id'))){
            $where['id'] = $this->input->post('id');
        }
        if(!empty($this->input->post('name'))){
            $where['LOWER(name) LIKE'] = '%'.strtolower($this->input->post('name')).'%';
        }
        if(!empty($this->input->post('description'))){
            $where['LOWER(description) LIKE'] = '%'.strtolower($this->input->post('description')).'%';
        }

        $this->where($where);
        $result['total_rows'] = $this->count_rows();

        $this->where($where);
        $this->order_by( $dataorder[$order[0]["column"]],  $order[0]["dir"]);
        $this->limit($start, $limit);
        $result['get_db']=$this
                            ->get_all();
        return $result;
    }

}
