<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Birthday_model extends MY_Model{

    public $table = 'users'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
    public function __construct()
    {
        parent::__construct();
        $this->timestamps = FALSE;
    }

    public function ajax_list()
    {
        $dataorder    = array();
        $dataorder[1] = "first_name";
        $dataorder[2] = "community_nama";
        $dataorder[3] = "company_nama";
        $dataorder[4] = "next_birthday";
        $dataorder[5] = "days";
        $dataorder[6] = "yang_ke";

        $start = intval($_POST['start']);
        $sEcho = intval($_POST['draw']);

        $order  = $this->input->post('order');
        $search = $this->input->post("search");

        $query = "
        SELECT
            a.*,
            DATE_ADD(
        		a.date_birthday,
        		INTERVAL
        	       IF (
        		         DAYOFYEAR(a.date_birthday) >= DAYOFYEAR(CURDATE()),
        		         YEAR (CURDATE()) - YEAR (a.date_birthday),
        		         YEAR (CURDATE()) - YEAR (a.date_birthday) + 1
        	       ) YEAR
        	) AS next_birthday,
        	DATEDIFF(
        		CURDATE(),
        		(
        			DATE_ADD(
        				a.date_birthday,
        				INTERVAL
                			IF (
                				DAYOFYEAR(a.date_birthday) >= DAYOFYEAR(CURDATE()),
                				YEAR (CURDATE()) - YEAR (a.date_birthday),
                				YEAR (CURDATE()) - YEAR (a.date_birthday) + 1
                			) YEAR
        			)
        		)
        	) AS days,
        	YEAR (CURDATE()) - YEAR (a.date_birthday) AS yang_ke,
            c.nama as community_nama,
            d.nama as company_nama
        FROM users a
        JOIN members b on (a.id=b.user_id)
        JOIN communities c on (b.community_id=c.id)
        JOIN company d on (c.company_id=d.id)
        WHERE
        	DATE_ADD(
        		date_birthday,
        		INTERVAL YEAR (CURDATE()) - YEAR (date_birthday) +
        	IF (
        		DAYOFYEAR(CURDATE()) > DAYOFYEAR(date_birthday),
        		1,
        		0
        	) YEAR
        	) BETWEEN CURDATE()
        AND DATE_ADD(CURDATE(), INTERVAL 7 DAY)
        AND a.level_id = 2
        ";

        if(!empty($this->input->post('first_name'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " LOWER(replace(a.first_name, '''', '')) LIKE '%".strtolower($this->input->post('first_name'))."%' ";
        }
        if(!empty($this->input->post('community_nama'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " LOWER(replace(c.nama, '''', '')) LIKE '%".strtolower($this->input->post('community_nama'))."%' ";
        }
        if(!empty($this->input->post('company_nama'))){
            $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
            $query .= " LOWER(replace(d.nama, '''', '')) LIKE '%".strtolower($this->input->post('company_nama'))."%' ";
        }
        // if(!empty($this->input->post('next_birthday_start')) && !empty($this->input->post('next_birthday_end'))){
        //     $date_from_arr = explode('/', $this->input->post('next_birthday_start'));
        //     $date_from     = date('Y-m-d', strtotime($date_from_arr[2].$date_from_arr[1].$date_from_arr[0]));
        //     $date_to_arr   = explode('/', $this->input->post('next_birthday_end'));
        //     $date_to       = date('Y-m-d', strtotime($date_to_arr[2].$date_to_arr[1].$date_to_arr[0]));
        //     $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
        //     $query .= " (next_birthday between '$date_from' and '$date_to') ";
        // }
        // if(!empty($this->input->post('days'))){
        //     $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
        //     $query .= " LOWER(replace(days, '''', '')) LIKE '%".strtolower($this->input->post('days'))."%' ";
        // }
        // if(!empty($this->input->post('yang_ke'))){
        //     $query .= preg_match("/WHERE/i", $query) ? " AND " : " WHERE ";
        //     $query .= " LOWER(replace(yang_ke, '''', '')) LIKE '%".strtolower($this->input->post('yang_ke'))."%' ";
        // }

        if($order){
            $query .= "order by ".$dataorder[$order[0]["column"]]." ".$order[0]["dir"];
        }

        $iTotalRecords  = $this->db->query("SELECT COUNT(*) AS numrows FROM (".$query.") A")->row()->numrows;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($_REQUEST['start']);
        $query          .= " LIMIT ". ($start) .",".($iDisplayLength);

        $data = $this->db->query($query)->result();
        $i=$iDisplayStart+1;
        $result = array();
        foreach ($data as $d) {

            $id = $d->id;

            $edit='<a href="#" class="btn btn-sm btn-outline green" onclick="event.preventDefault();btn_edit('.$d->id.')" title="hadiah">
            <i class="fa fa-gift fa-lg"></i>
            </a> ';

            $r = array();
            $r[0] = $i++;
            $r[1] = $d->first_name;
            $r[2] = $d->community_nama;
            $r[3] = $d->company_nama;
            $r[4] = date('d F Y', strtotime($d->next_birthday));
            $r[5] = '<b>'.$d->days.'</b>';
            $r[6] = '<b>'.$d->yang_ke.' tahun</b>';
            $r[7] = $edit;
            array_push($result, $r);
        }

        $records["data"] = $result;
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return $records;
    }

}
