<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partner_model extends MY_Model{

    public $table = 'partners'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
    public function __construct()
    {
        parent::__construct();
    }

    public function get_limit_data($limit, $start) {
        $order     = $this->input->post('order');
        $dataorder = array();
        $where     = array();

        $i=1;
        $dataorder[$i++] = 'nama';
        $dataorder[$i++] = 'alamat';
        $dataorder[$i++] = 'telepon1';
        $dataorder[$i++] = 'email';
        $dataorder[$i++] = 'active';

        if(!empty($this->input->post('nama'))){
            $where['LOWER(nama) LIKE'] = '%'.strtolower($this->input->post('nama')).'%';
        }
        if(!empty($this->input->post('alamat'))){
            $where['LOWER(alamat) LIKE'] = '%'.strtolower($this->input->post('alamat')).'%';
        }
        if(!empty($this->input->post('telepon1'))){
            $where['LOWER(telepon1) LIKE'] = '%'.strtolower($this->input->post('telepon1')).'%';
        }
        if(!empty($this->input->post('email'))){
            $where['LOWER(email) LIKE'] = '%'.strtolower($this->input->post('email')).'%';
        }
        if(!empty($this->input->post('active'))){
            $where['LOWER(active) LIKE'] = '%'.strtolower($this->input->post('active')).'%';
        }

        $this->where($where);
        $result['total_rows'] = $this->count_rows();

        $this->where($where);
        $this->order_by( $dataorder[$order[0]["column"]],  $order[0]["dir"]);
        $this->limit($start, $limit);
        $result['get_db'] = $this
                            ->get_all();
        return $result;
    }

}
