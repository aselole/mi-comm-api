<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form extends CI_Controller {

	public function index()
	{
			
	}

	public function dd_user()
	{
		$this->load->model('user_model');
		$limit   =$this->input->post('limit');
		$page    =$this->input->post('page')-1;
		if ($this->input->post('q')) {
			$this->db->like($this->user_model->label, $this->input->post('q'));
		}
        if (logged_user('level_id') == 2) {
			$this->db->where('level_id', 3);
        } else {
			$this->db->where('level_id', 2);
        }
		$this->db->limit($limit,($page*$limit));
		$data_db =$this->user_model->get_all();
		$res     =array();
		if ($data_db) {
			foreach ($data_db as $r) {
				$item=array();
				$item['id']    = $r->id;
				$item['title'] = $r->first_name;
				$item['img']   = get_user_img($r->photo);

				$res[] = $item;
			}
		}
		$output["items"]=$res;
		$this->db->where('level_id', 3);
		$output["total_count"]=$this->user_model->count_rows();
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}


}

/* End of file Form.php */
/* Location: ./application/controllers/Form.php */
