<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Community extends REST_Controller{

    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
        $this->load->model(array('global_model', 'member_model', 'user_model'));
    }

    public function list_get()
    {
        $token = null;
        $headers = apache_request_headers();
        foreach ($headers as $header => $value) {
            $header = strtolower($header);
            if ($header=='x-api-key') {
                $token = $value;
            }
        }

        // convert token to user_id
        $user_id = $this->global_model->getUserIdBYToken($token);
        if (!$user_id) {
            $result = array(
                'success' => false,
                'message' => 'Token mismatch',
            );
            $this->response($result, 400);
        }

        $user = $this->user_model->fields('level_id')->get($user_id);
        if ($user->level_id != 3) {
            $result = array(
                'success' => false,
                'message' => 'Anda bukan trainer',
            );
            $this->response($result, 400);
        }

        $sql = "
        select
            b.id,
            b.nama,
            b.ket,
            b.logo
        from members a
        join communities b on (a.community_id=b.id)
        where
            a.user_id = $user_id AND
            a.active = 'y'
        ";
        $communities = $this->db->query($sql);
        $data = [];
        foreach ($communities->result() as $r) {
            $r->ket = $r->ket==null ? '' : $r->ket;
            array_push($data, $r);
        }
        // $communities = $this
        //                 ->member_model
        //                 ->fields('community_id, tgl_bergabung')
        //                 ->where(array('active'=>'y', 'user_id'=>$user_id))
        //                 ->with_community(array(
        //                     'fields' => 'nama',
        //                     'with' => array(
        //                         'relation' => 'company',
        //                         'fields' => 'nama,alamat'
        //                     )
        //                 ))
        //                 ->get_all();
        $result['jml_communities'] = $communities->num_rows();
        $result['communities'] = $data;
        $this->response($result, 200);
    }

    public function detail_get()
    {
        $id = $this->get('id');
        if (!$id) {
            $result = array(
                'success' => false,
                'message' => 'Bad Request',
            );
            $this->response($result, 400);
        }

        $sql = "
        select
            a.id,
            a.nama,
            a.ket,
            a.alamat,
            a.email,
            a.telepon1,
            a.logo
        from communities a
        where
            a.id = $id
        ";
        $community = $this->db->query($sql)->row();
        $community->ket = $community->ket==null ? '' : $community->ket;
        $community->logo = $community->logo==null ? '' : $community->logo;

        $sql_member = "
        select
            a.user_id,
            a.tgl_bergabung,
            b.first_name as user_first_name,
            b.photo as user_photo,
            b.level_id,
            c.nama as level_nama
        from members a
        join users b on (a.user_id=b.id)
        join levels c on (b.level_id=c.id)
        where
            a.community_id = $id AND
            a.active = 'y'
        ";
        $members = $this->db->query($sql_member)->result();
        $fix_members = [];
        foreach ($members as $r) {
            $r->user_photo = $r->user_photo==null ? '' : $r->user_photo;
            array_push($fix_members, $r);
        }
        // $community = $this
        //                 ->member_model
        //                 ->fields('community_id, tgl_bergabung')
        //                 ->where(array('active'=>'y'))
        //                 ->with_community(array(
        //                     'fields' => 'nama',
        //                     'with' => array(
        //                         'relation' => 'company',
        //                         'fields' => 'nama,alamat'
        //                     )
        //                 ))
        //                 ->get();
        $result['community'] = $community;
        $result['members'] = $fix_members;
        $this->response($result, 200);
    }

    public function my_community_get()
    {
        $token = null;
        $headers = apache_request_headers();
        foreach ($headers as $header => $value) {
            $header = strtolower($header);
            if ($header=='x-api-key') {
                $token = $value;
            }
        }

        // convert token to user_id
        $user_id = $this->global_model->getUserIdBYToken($token);
        if (!$user_id) {
            $result = array(
                'success' => false,
                'message' => 'Token mismatch',
            );
            $this->response($result, 400);
        }
        $user = $this->user_model->fields('level_id')->get($user_id);
        if ($user->level_id == 2) {
            // member
            $community_id = $this->member_model->where('user_id', $user_id)->get()->community_id;
            $sql = "
            select
                a.id,
                a.nama,
                a.ket,
                a.logo
            from communities a
            where
                a.id = $community_id
            ";
            $community = $this->db->query($sql)->row();
            $community->ket = $community->ket==null ? '' : $community->ket;
            $community->logo = $community->logo==null ? '' : $community->logo;

            $sql_member = "
            select
                a.user_id,
                a.tgl_bergabung,
                b.first_name as user_first_name,
                b.photo as user_photo,
                b.level_id,
                c.nama as level_nama
            from members a
            join users b on (a.user_id=b.id)
            join levels c on (b.level_id=c.id)
            where
                a.community_id = $community_id AND
                a.active = 'y'
            ";
            $members = $this->db->query($sql_member)->result();
            $fix_members = [];
            foreach ($members as $r) {
                $r->user_photo = $r->user_photo==null ? '' : $r->user_photo;
                $r->tgl_bergabung = $r->tgl_bergabung==null ? '' : $r->tgl_bergabung;
                array_push($fix_members, $r);
            }
            $community->members = $fix_members;
            $result['community'] = $community;
            // $result['members'] = $fix_members;
        } elseif ($user->level_id == 3) {
            // trainer
            $sql = "
            select
                b.id,
                b.nama,
                b.ket,
                b.logo
            from members a
            join communities b on (a.community_id=b.id)
            where
                a.user_id = $user_id AND
                a.active = 'y'
            ";
            $communities = $this->db->query($sql);
            $data = [];
            foreach ($communities->result() as $r) {
                $r->ket = $r->ket==null ? '' : $r->ket;
                $r->logo = $r->logo==null ? '' : $r->logo;
                $community_id = $r->id;
                $sql_member = "
                select
                    a.user_id,
                    a.tgl_bergabung,
                    b.first_name as user_first_name,
                    b.photo as user_photo,
                    b.level_id,
                    c.nama as level_nama
                from members a
                join users b on (a.user_id=b.id)
                join levels c on (b.level_id=c.id)
                where
                    a.community_id = $community_id AND
                    a.active = 'y'
                ";
                $members = $this->db->query($sql_member)->result();
                $fix_members = [];
                foreach ($members as $rr) {
                    $rr->user_photo = $rr->user_photo==null ? '' : $rr->user_photo;
                    $rr->tgl_bergabung = $rr->tgl_bergabung==null ? '' : $rr->tgl_bergabung;
                    array_push($fix_members, $rr);
                }
                $r->members = $fix_members;
                array_push($data, $r);
            }
            $fix_communities = $data;
            $result['communities'] = $fix_communities;
            $result['jml_communities'] = $communities->num_rows();
        }

        // $community = $this
        //                 ->member_model
        //                 ->fields('community_id, tgl_bergabung')
        //                 ->where(array('active'=>'y'))
        //                 ->with_community(array(
        //                     'fields' => 'nama',
        //                     'with' => array(
        //                         'relation' => 'company',
        //                         'fields' => 'nama,alamat'
        //                     )
        //                 ))
        //                 ->get();

        $this->response($result, 200);
    }

}
