<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Home extends REST_Controller{

    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
        $this->load->model(array('global_model', 'promo_model', 'promo_user_model', 'user_model'));
    }

    public function notif_get()
    {
        $token = null;
        $headers = apache_request_headers();
        foreach ($headers as $header => $value) {
            $header = strtolower($header);
            if ($header=='x-api-key') {
                $token = $value;
            }
        }

        // convert token to user_id
        $user_id = $this->global_model->getUserIdBYToken($token);
        if (!$user_id) {
            $result = array(
                'success' => false,
                'message' => 'Token mismatch',
            );
            $this->response($result, 400);
        }

        $user = $this->user_model->get($user_id);

        $date_now = date('Y-m-d');
        $sql_promo_user = "
        select
            a.promo_id,
            b.judul,
            b.discount_price,
            b.published_at,
            b.valid_until
        from promos_users a
        join promos b on (a.promo_id=b.id)
        where
            a.active = 'y' AND
            a.user_id = $user_id AND
            a.read = 0 AND
            a.is_claimed is null AND
            b.active = 'y' AND
            b.valid_until >= '$date_now' AND
            b.published_at <= '$date_now'
        ";
        $data_promo_user = $this->db->query($sql_promo_user);
        $promo_user = [];
        foreach ($data_promo_user->result() as $r) {
            $r->discount_price = $r->discount_price == null ? '' : $r->discount_price;
            array_push($promo_user, $r);
        }
        $result['promo_user'] = $promo_user;
        $result['jml_promo_user'] = $data_promo_user->num_rows();


        $sql_private_issue = "
            select id from private_issues where user_trainer_id = $user_id || user_member_id = $user_id
        ";
        $private_issues = $this->db->query($sql_private_issue);
        $ar_private_issue_id = [];
        foreach ($private_issues->result() as $r) {
            $ar_private_issue_id[] = $r->id;
        }

        $str_private_issue_id = count($str_private_issue_id) > 0 ? implode(',', $ar_private_issue_id) : 0;

        $sql_private_issue_comment = "
        select
            a.private_issue_id,
            a.user_id,
            c.first_name,
            a.comment,
            a.created_at
        from private_issue_comments a
        join private_issues b on (a.private_issue_id=b.id)
        join users c on (a.user_id=c.id)
        where
            a.private_issue_id in ($str_private_issue_id) AND
            a.user_id != $user_id AND
            a.read = 0 AND
            b.status = 'open'
        ";
        $private_issue_comment = $this->db->query($sql_private_issue_comment);
        $result['private_issue_comment'] = $private_issue_comment->result();
        $result['jml_private_issue_comment'] = $private_issue_comment->num_rows();
        $this->response($result, 200);
    }

}
