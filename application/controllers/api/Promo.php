<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Promo extends REST_Controller{

    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
        $this->load->model(array('promo_model', 'voucher_model', 'promo_claim_model', 'promo_user_model', 'global_model'));
    }

    public function list_promo_get()
    {
        $token = null;
        $headers = apache_request_headers();
        foreach ($headers as $header => $value) {
            $header = strtolower($header);
            if ($header=='x-api-key') {
                $token = $value;
            }
        }

        $fields = 'id, judul, featured_img, valid_until, created_at, published_at';
        $is_global = $this->input->get('is_global');
        if (!$is_global) {
            $result = array(
                'success' => false,
                'message' => 'Bad Request',
            );
            $this->response($result, 400);
        }

        // convert token to user_id
        $user_id = $this->global_model->getUserIdBYToken($token);
        if (!$user_id) {
            $result = array(
                'success' => false,
                'message' => 'Token mismatch',
            );
            $this->response($result, 400);
        }

        $date_now = date('Y-m-d');
        $batas_date_valid = date('Y-m-d', strtotime($date_now. ' + 7 days'));

        if ($is_global=='y') {
            // list promo global
            $sql = "
            select
                a.id as promo_id,
                a.judul,
                a.featured_img,
                a.valid_until,
                a.created_at,
                a.published_at,
                b.is_claimed,
                b.claimed_at
            from promos a
            left join promos_claim b on (a.id=b.promo_id)
            where
                a.is_global='y' and
                (b.user_id=$user_id or b.user_id is null) and
                (a.valid_until <= '$batas_date_valid' or a.valid_until >= '$date_now') and
                a.published_at <= '$date_now' and
                a.active='y'
            order by a.published_at desc
            ";
            $list_promo_global = $this->db->query($sql);
            if ($list_promo_global->num_rows() == 0) {
                $result = array(
                    'success' => true,
                    'message' => 'Tidak ada promo',
                );
                $this->response($result, 400);
            } else {
                $ar_list_promo_global = array();
                foreach ($list_promo_global->result() as $r) {
                    $r->is_claimed = $r->is_claimed == null ? '' : $r->is_claimed;
                    $r->claimed_at = $r->claimed_at == null ? '' : $r->claimed_at;
                    array_push($ar_list_promo_global, $r);
                }
                $result = $ar_list_promo_global;
            }
        } elseif ($is_global=='t') {
            // list promo private
            $sql = "
            select
                a.id as promo_user_id,
                a.promo_id,
                a.is_claimed,
                a.claimed_at,
                a.read,
                b.judul,
                b.featured_img,
                b.valid_until,
                b.created_at,
                b.published_at
            from promos_users a
            join promos b on (a.promo_id=b.id)
            where
                b.is_global='t' and
                a.user_id=$user_id and
                (b.valid_until <= '$batas_date_valid' or b.valid_until >= '$date_now') and
                b.published_at <= '$date_now' and
                b.active='y' and
                a.active='y'
            ";
            $list_promo_private = $this->db->query($sql);
            if ($list_promo_private->num_rows() == 0) {
                $result = array(
                    'success' => true,
                    'message' => 'Tidak ada promo',
                );
                $this->response($result, 400);
            } else {
                $ar_list_promo_private = array();
                foreach ($list_promo_private->result() as $r) {
                    $r->is_claimed = $r->is_claimed ? $r->is_claimed : '';
                    $r->claimed_at = $r->claimed_at ? $r->claimed_at : '';
                    array_push($ar_list_promo_private, $r);
                }
                $result = $ar_list_promo_private;
            }
        } else {
            // bad request
            $result = array(
                'success' => false,
                'message' => 'Bad Request',
            );
            $this->response($result, 400);
        }
        $this->response($result);
    }

    public function detail_get()
    {
        $token = null;
        $headers = apache_request_headers();
        foreach ($headers as $header => $value) {
            $header = strtolower($header);
            if ($header=='x-api-key') {
                $token = $value;
            }
        }

        $promo_id = $this->input->get('promo_id');
        if (!$promo_id) {
            $result = array(
                'success' => false,
                'message' => 'Bad Request',
            );
            $this->response($result, 400);
        }

        // convert token to user_id
        $user_id = $this->global_model->getUserIdBYToken($token);
        if (!$user_id) {
            $result = array(
                'success' => false,
                'message' => 'Token mismatch',
            );
            $this->response($result, 400);
        }

        $date_now = date('Y-m-d');
        $batas_date_valid = date('Y-m-d', strtotime($date_now. ' + 7 days'));

        $exist = $this->db->query("
        select
            id,
            is_global
        from promos
        where
            id=$promo_id and
            (valid_until <= '$batas_date_valid' or valid_until >= '$date_now') and
            published_at <= '$date_now' and
            active='y'
        ");
        if ($exist->num_rows() == 0) {
            $result = array(
                'success' => false,
                'message' => 'Data tidak ditemukan',
            );
            $this->response($result, 200);
        } else {
            $is_global = $exist->row()->is_global;
            if ($is_global=='y') {
                // list promo global
                $sql = "
                select
                    a.id as promo_id,
                    a.judul,
                    a.body,
                    a.featured_img,
                    a.valid_until,
                    a.is_global,
                    a.created_at,
                    a.published_at,
                    b.is_claimed,
                    b.claimed_at
                from promos a
                left join promos_claim b on (a.id=b.promo_id)
                where
                    a.is_global='y' and
                    (b.user_id=$user_id or b.user_id is null) and
                    (a.valid_until <= '$batas_date_valid' or a.valid_until >= '$date_now') and
                    a.active='y' and
                    a.id = $promo_id
                ";
                $list_promo_global = $this->db->query($sql);
                $row = $list_promo_global->row();
                $row->is_claimed = $row->is_claimed == null ? '' : $row->is_claimed;
                $row->claimed_at = $row->claimed_at == null ? '' : $row->claimed_at;
                $result = $row;
            } elseif ($is_global=='t') {
                // list promo private
                $sql = "
                select
                    a.id as promo_user_id,
                    a.promo_id,
                    a.is_claimed,
                    a.claimed_at,
                    a.read,
                    b.judul,
                    b.body,
                    b.featured_img,
                    b.valid_until,
                    b.is_global,
                    b.created_at,
                    b.published_at
                from promos_users a
                join promos b on (a.promo_id=b.id)
                where
                    b.is_global='t' and
                    a.user_id=$user_id and
                    (b.valid_until <= '$batas_date_valid' or b.valid_until >= '$date_now') and
                    b.published_at <= '$date_now' and
                    b.active='y' and
                    a.active='y' and
                    a.promo_id = $promo_id
                ";
                $list_promo_private = $this->db->query($sql);
                if ($list_promo_private->num_rows() == 0) {
                    $result = array(
                        'success' => false,
                        'message' => 'Data tidak ditemukan',
                    );
                    $this->response($result, 200);
                } else {
                    $row = $list_promo_private->row();
                    $row->is_claimed = $row->is_claimed == null ? '' : $row->is_claimed;
                    $row->claimed_at = $row->claimed_at == null ? '' : $row->claimed_at;
                    $result = $row;

                    // jadikan read
                    if ($row->read == 0) {
                        $data = array('read' => 1);
                        $update = $this->promo_user_model->update($data, $row->promo_user_id);
                    }
                }
            }
        }
        $this->response($result);
    }

    public function claim_post()
    {
        $token = null;
        $headers = apache_request_headers();
        foreach ($headers as $header => $value) {
            $header = strtolower($header);
            if ($header=='x-api-key') {
                $token = $value;
            }
        }

        // convert token to user_id
        $user_id = $this->global_model->getUserIdBYToken($token);
        if (!$user_id) {
            $result = array(
                'success' => false,
                'message' => 'Token mismatch',
            );
            $this->response($result, 400);
        }

        $validation = array(
			array(
					"field" => "promo_id",
					"label" => "promo_id",
					"rules" => "required"
				),
			array(
					"field" => "kode",
					"label" => "Kode Voucher",
					"rules" => "required"
				)
		);
		$this->form_validation->set_rules($validation);

		if ($this->form_validation->run()) {
            $promo_id = $this->post('promo_id');
            $kode     = strtolower($this->post('kode'));
            $date_now = date('Y-m-d');

            $detail_promo = $this
                            ->promo_model
                            ->fields('id, is_global')
                            ->where('valid_until >=', $date_now)
                            ->where('active', 'y')
                            ->get($promo_id);
            if (!$detail_promo) {
                $result = array(
                    'success' => false,
                    'message' => 'Promo sudah kadaluarsa.',
                );
                $this->response($result, 400);
            }
            $get_voucher = $this->db->query("select * from vouchers where LOWER(kode) = '$kode'");
            $detail_voucher = $get_voucher->row();

            if ($get_voucher->num_rows() == 0 || $detail_voucher->promo_id != $promo_id) {
                $result = array(
                    'success' => false,
                    'message' => 'Kode Voucher salah.',
                );
                $this->response($result, 400);
            }
            if ($detail_voucher->is_used == 'y') {
                $result = array(
                    'success' => false,
                    'message' => 'Kode Voucher sudah dipakai.',
                );
                $this->response($result, 400);
            }

            if ($detail_promo->is_global=='y') {
                // promo global
                $cek_promo_claim = $this->promo_claim_model->where(array('promo_id'=>$promo_id, 'user_id'=>$user_id))->get();
                if ($cek_promo_claim) {
                    $result = array(
                        'success' => false,
                        'message' => 'Anda sudah pernah melakukan claim terhadap promo ini.',
                    );
                    $this->response($result, 400);
                }
                $data = array(
                    'promo_id'   => $promo_id,
                    'user_id'    => $user_id,
                    'is_claimed' => 'y',
                    'claimed_at' => date('Y-m-d H:i:s'),
                    'voucher_id' => $detail_voucher->id,
                );
                $insert_id = $this->promo_claim_model->insert($data);

                $data_voucher = array('is_used' => 'y');
                $update = $this->voucher_model->update($data_voucher, $detail_voucher->id);
                if ($insert_id && $update) {
                    $result = array(
                        'success' => true,
                        'message' => 'Promo berhasil di claim.',
                    );
                    $this->response($result, 200);
                } else {
                    $result = array(
                        'success' => false,
                        'message' => 'Terjadi kesalahan system.',
                    );
                    $this->response($result, 400);
                }
            } elseif ($detail_promo->is_global=='t') {
                // promo global
                $detail_promo_user = $this->promo_user_model->where(array('promo_id'=>$promo_id, 'user_id'=>$user_id, 'active'=>'y'))->get();
                if (!$detail_promo_user) {
                    $result = array(
                        'success' => false,
                        'message' => 'Anda tidak memiliki promo ini.',
                    );
                    $this->response($result, 400);
                }
                if ($detail_promo_user->is_claimed == 'y') {
                    $result = array(
                        'success' => false,
                        'message' => 'Anda sudah pernah melakukan claim terhadap promo ini.',
                    );
                    $this->response($result, 400);
                }
                $data = array(
                    'is_claimed' => 'y',
                    'claimed_at' => date('Y-m-d H:i:s'),
                    'voucher_id' => $detail_voucher->id,
                );
                $update = $this->promo_user_model->update($data, $detail_promo_user->id);

                $data_voucher = array('is_used' => 'y');
                $update_voucher = $this->voucher_model->update($data_voucher, $detail_voucher->id);
                if ($update && $update_voucher) {
                    $result = array(
                        'success' => true,
                        'message' => 'Promo berhasil di claim.',
                    );
                    $this->response($result, 200);
                } else {
                    $result = array(
                        'success' => false,
                        'message' => 'Terjadi kesalahan system.',
                    );
                    $this->response($result, 400);
                }
            }
        } else {
            $result = array(
                'success' => false,
                'errors' => $this->form_validation->error_array(),
            );
            $this->response($result, 400);
        }
    }

    public function reset_voucher_get()
    {
        $delete1 = $this->promo_claim_model->delete(array('is_claimed'=>'y'));

        $data = array('is_claimed' => null, 'claimed_at'=>null, 'voucher_id'=>null);
        $delete2 = $this->promo_user_model->update($data, array('created_at !='=>null));

        $data_voucher = array('is_used'=>'t');
        $delete3 = $this->voucher_model->update($data_voucher, array('is_used'=>'y'));

        $result = array(
            'success' => true,
            'message' => 'Berhasil direset.'
        );
        $this->response($result, 200);
    }

    

}
