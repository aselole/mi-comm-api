<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Profile extends REST_Controller{

    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
        $this->load->model(array('global_model', 'user_model'));
        $params['salt_prefix'] = $this->config->item('salt_prefix', 'ion_auth');
        $this->load->library('bcrypt',$params);
    }

    public function edit_photo_post()
    {
        $token = null;
        $headers = apache_request_headers();
        foreach ($headers as $header => $value) {
            $header = strtolower($header);
            if ($header=='x-api-key') {
                $token = $value;
            }
        }

        // convert token to user_id
        $user_id = $this->global_model->getUserIdBYToken($token);
        if (!$user_id) {
            $result = array(
                'success' => false,
                'message' => 'Token mismatch',
            );
            $this->response($result, 400);
        }

        $exist = $this->user_model->get($user_id);

        // $validation = array(
        //
		// );
		// $this->form_validation->set_rules($validation);

		// if ($this->form_validation->run()) {
            $date_now = date('Y-m-d');

            $data = array(
                'updated_at' => $date_now,
            );
//var_dump($_FILES);
            if ( !empty($_FILES['photo']['name']) ) {
                $email = $exist->email;
                $upload = $this->user_model->upload_pic_user($email);
                if ($upload['success']) {
                    $data['photo'] = $upload['data']['file_name'];
                    // if ($exist->photo != null && file_exists('./uploads/user/'.$exist->photo)) {
                    //     unlink('./uploads/user/'.$exist->photo);
                    // }
                } else {
                    $result = $upload;
                    $this->response($result, 400);
                }
            } else {
                $result = array(
                    'success' => false,
                    'message' => 'Foto tidak boleh kosong',
                );
                $this->response($result, 400);
            }

            $update = $this->user_model->update($data, $user_id);
            if ($update) {
                $new_data = $this->user_model->fields('photo')->get($user_id);
                $result = array(
                    'success' => true,
                    'message' => 'Foto berhasil diperbarui.',
                    'data' => $new_data
                );
            } else {
                $result = array(
                    'success' => true,
                    'message' => 'Terjadi kesalahan.',
                );
                $this->response($result, 400);
            }
        // } else {
        //     $result = array(
        //         'success' => false,
        //         'errors' => $this->form_validation->error_array(),
        //     );
        //     $this->response($result, 400);
        // }
        $this->response($result, 200);
    }

    public function edit_bio_post()
    {
        $token = null;
        $headers = apache_request_headers();
        foreach ($headers as $header => $value) {
            $header = strtolower($header);
            if ($header=='x-api-key') {
                $token = $value;
            }
        }

        // convert token to user_id
        $user_id = $this->global_model->getUserIdBYToken($token);
        if (!$user_id) {
            $result = array(
                'success' => false,
                'message' => 'Token mismatch',
            );
            $this->response($result, 400);
        }

        $exist = $this->user_model->get($user_id);

        $validation = array(
			array(
				"field" => "nama",
				"label" => "Nama",
				"rules" => "required"
			),
			array(
				"field" => "phone",
				"label" => "Telepon",
				"rules" => "required"
			),
            array(
				"field" => "address",
				"label" => "Alamat",
				"rules" => "required"
			),
		);
		$this->form_validation->set_rules($validation);

		if ($this->form_validation->run()) {
            $date_now = date('Y-m-d');
            $password      = trim($this->post('password'));

            $data = array(
                'first_name' => $this->post('nama'),
                'gender'     => $this->post('gender'),
                'phone'      => $this->post('phone'),
                'address'    => $this->post('address'),
                'updated_at' => $date_now,
            );

            $update = $this->user_model->update($data, $user_id);
            if ($update) {
                $new_data = $this->user_model->fields('first_name, gender, phone, address, updated_at')->get($user_id);
                $result = array(
                    'success' => true,
                    'message' => 'Biodata berhasil diperbarui.',
                    'data' => $new_data
                );
            } else {
                $result = array(
                    'success' => true,
                    'message' => 'Terjadi kesalahan.',
                );
                $this->response($result, 400);
            }
        } else {
            $result = array(
                'success' => false,
                'errors' => $this->form_validation->error_array(),
            );
            $this->response($result, 400);
        }
        $this->response($result, 200);
    }

    public function edit_password_post()
    {
        $token = null;
        $headers = apache_request_headers();
        foreach ($headers as $header => $value) {
            $header = strtolower($header);
            if ($header=='x-api-key') {
                $token = $value;
            }
        }

        // convert token to user_id
        $user_id = $this->global_model->getUserIdBYToken($token);
        if (!$user_id) {
            $result = array(
                'success' => false,
                'message' => 'Token mismatch',
            );
            $this->response($result, 400);
        }

        $exist = $this->user_model->get($user_id);

        $validation = array(
			array(
				"field" => "password",
				"label" => "Password",
				"rules" => "required"
			),
			array(
				"field" => "confirm_password",
				"label" => "Konfirmasi Password",
				"rules" => "required|matches[password]"
			),
		);
		$this->form_validation->set_rules($validation);

		if ($this->form_validation->run()) {
            $date_now = date('Y-m-d');
            $password      = trim($this->post('password'));
            $hash_password = $this->bcrypt->hash($password);
            $data = array(
                'password' => $hash_password,
                'updated_at' => $date_now,
            );

            $update = $this->user_model->update($data, $user_id);
            if ($update) {
                $new_data = $this->user_model->fields('updated_at')->get($user_id);
                $result = array(
                    'success' => true,
                    'message' => 'Password berhasil diperbarui.',
                    'data' => $new_data
                );
            } else {
                $result = array(
                    'success' => true,
                    'message' => 'Terjadi kesalahan.',
                );
                $this->response($result, 400);
            }
        } else {
            $result = array(
                'success' => false,
                'errors' => $this->form_validation->error_array(),
            );
            $this->response($result, 400);
        }
        $this->response($result, 200);
    }

}
