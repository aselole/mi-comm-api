<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Issue extends REST_Controller{

    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
        $this->load->model(array('global_model', 'issue_model', 'issue_comment_model', 'bad_comment_model', 'member_model', 'user_model'));
    }


    public function list_get()
    {
        $token = null;
        $headers = apache_request_headers();
        foreach ($headers as $header => $value) {
            $header = strtolower($header);
            if ($header=='x-api-key') {
                $token = $value;
            }
        }

        // convert token to user_id
        $user_id = $this->global_model->getUserIdBYToken($token);
        if (!$user_id) {
            $result = array(
                'success' => false,
                'message' => 'Token mismatch',
            );
            $this->response($result, 400);
        }

        $user = $this->user_model->fields('level_id')->get($user_id);
        if ($user->level_id == 2) {
            // member
            $member = $this
                        ->member_model
                        ->fields('community_id, user_id')
                        ->where(array('active' => 'y', 'user_id' => $user_id))
                        ->get();
            if (!$member) {
                $result = array(
                    'success' => true,
                    'message' => 'Tidak memiliki komunitas',
                );
            } else {
                $community_id = $member->community_id;
                $sql = "
                select
                    a.id,
                    a.community_id,
                    a.judul,
                    a.featured_img,
                    a.status,
                    a.author_id,
                    a.published_at,
                    (select count(b.issue_id)) as jml_komentar,
                    c.nama as community_nama
                from issues a
                left join issue_comments b on (b.issue_id=a.id)
                join communities c on (a.community_id=c.id)
                where
                    community_id=$community_id AND
                    status in ('open','closed') AND
                    active='y'
                group by a.id
                order by published_at desc
                ";
                $issues = $this->db->query($sql)->result();
                // $issues = $this
                //             ->issue_model
                //             ->fields('id, judul, featured_img, status, created_at, published_at')
                //             ->where(array('community_id'=>$community_id, 'active'=>'y'))
                //             ->order_by('published_at', 'desc')
                //             ->with_issue_comment('fields:*count*')
                //             ->get_all();
                $result['jml_communities'] = 1;
                $result['issues'] = $issues;
            }
        } elseif ($user->level_id == 3) {
            // trainer
            $member = $this
                        ->member_model
                        ->fields('community_id, user_id')
                        ->where(array('active' => 'y', 'user_id' => $user_id))
                        ->get_all();
            if (!$member) {
                $result = array(
                    'jml_communities' => 0,
                    'issues' => [],
                );
                $this->response($result, 200);
            } else {
                $ar_community_id = array();
                foreach ($member as $r) {
                    array_push($ar_community_id, $r->community_id);
                }
                $str_community_id = implode(',', $ar_community_id);
                $sql = "
                select
                    a.id,
                    a.community_id,
                    a.judul,
                    a.featured_img,
                    a.status,
                    a.author_id,
                    a.published_at,
                    (select count(b.issue_id)) as jml_komentar,
                    c.nama as community_nama,
                    c.member_jenis
                from issues a
                left join issue_comments b on (b.issue_id=a.id)
                join communities c on (a.community_id=c.id)
                where
                    community_id in ($str_community_id) AND
                    active='y'
                group by a.id
                order by published_at desc
                ";
                $issues = $this->db->query($sql)->result();
                // $issues = $this
                //             ->issue_model
                //             ->fields('id, community_id, judul, featured_img, status, created_at, published_at')
                //             ->where('community_id', $ar_community_id)
                //             ->where('active', 'y')
                //             ->order_by('published_at', 'desc')
                //             ->with_community('fields:nama')
                //             ->with_issue_comment('fields:*count*')
                //             ->get_all();
                $result['jml_communities'] = count($member);
                $result['issues'] = $issues;
            }
        }

        $this->response($result, 200);
    }

    public function detail_get()
    {
        $token = null;
        $headers = apache_request_headers();
        foreach ($headers as $header => $value) {
            $header = strtolower($header);
            if ($header=='x-api-key') {
                $token = $value;
            }
        }

        // convert token to user_id
        $user_id = $this->global_model->getUserIdBYToken($token);
        if (!$user_id) {
            $result = array(
                'success' => false,
                'message' => 'Token mismatch',
            );
            $this->response($result, 400);
        }

        $id = $this->get('id');
        if (!$id) {
            $result = array(
                'success' => false,
                'message' => 'Bad Request',
            );
            $this->response($result, 400);
        } else {
            $sql = "
            select
                a.id,
                a.community_id,
                a.judul,
                a.body,
                a.featured_img,
                a.status,
                a.published_at,
                (select count(b.issue_id)) as jml_komentar,
                c.nama as community_nama,
                c.member_jenis
            from issues a
            left join issue_comments b on (b.issue_id=a.id)
            join communities c on (a.community_id=c.id)
            where a.id=$id
            ";
            $issue = $this->db->query($sql)->row();
            $result['issue'] = $issue;

            // $issue = $this
            //         ->issue_model
            //         ->fields('id, community_id, judul, body, featured_img, status, published_at')
            //         ->with_author('fields:first_name|join:true')
            //         ->with_community('fields:nama|join:true')
            //         ->get($id);
            // $result['issue'] = $issue;

            $issue_comment_fix = array();
            $sql = "
            select
                a.id,
                a.comment,
                a.user_id,
                a.created_at,
                b.first_name as user_first_name,
                b.photo as user_photo
            from issue_comments a
            join users b on (a.user_id=b.id)
            where
                issue_id=$id AND
                parent_id=0
            order by a.created_at asc
            ";
            $issue_comment = $this->db->query($sql);
            if ($issue_comment->num_rows() > 0) {
                foreach ($issue_comment->result() as $r) {
                    $parent_id = $r->id;
                    $sql_child = "
                    select
                        a.id,
                        a.comment,
                        a.user_id,
                        a.parent_id,
                        a.created_at,
                        b.first_name as user_first_name,
                        b.photo as user_photo
                    from issue_comments a
                    join users b on (a.user_id=b.id)
                    where
                        issue_id=$id AND
                        parent_id=$parent_id
                    order by a.created_at asc
                    ";
                    $issue_comment_child = $this->db->query($sql_child);
                    if ($issue_comment_child->num_rows() > 0) {
                        $r->has_child = true;
                        $r->issue_comment_child = $issue_comment_child->result();
                    } else {
                        $r->has_child = false;
                    }
                    array_push($issue_comment_fix, $r);
                }
            }
            $result['issue_comments'] = $issue_comment_fix;

            // $issue_comments = $this
            //                     ->issue_comment_model
            //                     ->fields('id, comment, created_at')
            //                     ->where(array('issue_id'=>$id, 'parent_id'=>0))
            //                     ->order_by('created_at', 'asc')
            //                     ->with_user('fields:first_name')
            //                     ->with_issue_comment_child(
            //                         array(
            //                             'fields'=>'id, comment, created_at',
            //                             'order_inside'=>'created_at asc',
            //                             'with'=>array('relation'=>'user', 'fields'=>'first_name')
            //                         ))
            //                     ->get_all();
            // $result['issue_comments'] = $issue_comments;
        }
        $this->response($result, 200);
    }

    public function comment_part_get()
    {
        $parent_id = $this->get('parent_id');
        if (!$parent_id) {
            $result = array(
                'success' => false,
                'message' => 'Bad Request',
            );
            $this->response($result, 400);
        }
        $cek = $this->issue_comment_model->fields('issue_id, parent_id')->get($parent_id);
        if (!$cek || $cek->parent_id!=0) {
            $result = array(
                'success' => false,
                'message' => 'parent_id is not valid.',
            );
            $this->response($result, 400);
        }
        $issue_comment_child = $this
                    ->issue_comment_model
                    ->fields('id, comment, created_at')
                    ->where('parent_id', $parent_id)
                    ->with_user('fields:first_name, photo|join:true')
                    ->get_all();
        $result['issue_id'] = $cek->issue_id;
        $result['issue_comment_child'] = $issue_comment_child;
        $this->response($result, 200);
    }

    public function store_post()
    {
        $token = null;
        $headers = apache_request_headers();
        foreach ($headers as $header => $value) {
            $header = strtolower($header);
            if ($header=='x-api-key') {
                $token = $value;
            }
        }

        // convert token to user_id
        $user_id = $this->global_model->getUserIdBYToken($token);
        if (!$user_id) {
            $result = array(
                'success' => false,
                'message' => 'Token mismatch',
            );
            $this->response($result, 400);
        }

        $member = $this->member_model->fields('community_id')->where(array('active'=>'y', 'user_id'=>$user_id))->get();
        if (!$member) {
            $result = array(
                'success' => false,
                'message' => 'Anda tidak memiliki Community',
            );
            $this->response($result, 400);
        }
        $community_id = $member->community_id;

        $validation = array(
			array(
					"field" => "judul",
					"label" => "Judul",
					"rules" => "trim|required"
				),
			array(
					"field" => "body",
					"label" => "Body",
					"rules" => "trim|required"
				)
		);
		$this->form_validation->set_rules($validation);

		if ($this->form_validation->run()) {
            $date_now = date('Y-m-d');
            $data = array(
                'community_id' => $community_id,
                'judul'        => $this->post('judul'),
                'body'         => $this->post('body'),
                'author_id'    => $user_id,
                'published_at' => $date_now,
            );
            if ( !empty($_FILES['featured_img']['name']) ) {
                $upload = $this->issue_model->upload_pic_issue();
                if ($upload['success']) {
                    $data['featured_img'] = $upload['data']['file_name'];
                } else {
                    $result = $upload;
                    $this->response($result, 400);
                }
            }

            $insert_id = $this->issue_model->insert($data);
            if ($insert_id) {
                $result = array(
                    'success' => true,
                    'message' => 'Issue berhasil ditambahkan.',
                    'issue_id' => $insert_id,
                );
            } else {
                $result = array(
                    'success' => true,
                    'message' => 'Terjadi kesalahan.',
                );
                $this->response($result, 400);
            }
        } else {
            $result = array(
                'success' => false,
                'errors' => $this->form_validation->error_array(),
            );
            $this->response($result, 400);
        }
        $this->response($result, 200);
    }

    public function trainer_store_post()
    {
        $token = null;
        $headers = apache_request_headers();
        foreach ($headers as $header => $value) {
            $header = strtolower($header);
            if ($header=='x-api-key') {
                $token = $value;
            }
        }

        // convert token to user_id
        $user_id = $this->global_model->getUserIdBYToken($token);
        if (!$user_id) {
            $result = array(
                'success' => false,
                'message' => 'Token mismatch',
            );
            $this->response($result, 400);
        }
        $user = $this->user_model->fields('level_id')->get($user_id);
        if ($user->level_id != 3) {
            $result = array(
                'success' => false,
                'message' => 'Anda bukan Trainer',
            );
            $this->response($result, 400);
        }

        $validation = array(
            array(
				"field" => "community_id",
				"label" => "Community",
				"rules" => "required"
			),
			array(
				"field" => "judul",
				"label" => "Judul",
				"rules" => "trim|required"
			),
			array(
				"field" => "body",
				"label" => "Body",
				"rules" => "trim|required"
			),
            array(
				"field" => "published_at",
				"label" => "Tgl Publish",
				"rules" => "required"
			)
		);
		$this->form_validation->set_rules($validation);

		if ($this->form_validation->run()) {
            $community_id = $this->post('community_id');
            $member = $this->member_model->fields('community_id')->where(array('active'=>'y', 'user_id'=>$user_id, 'community_id'=>$community_id))->get();
            if (!$member) {
                $result = array(
                    'success' => false,
                    'message' => 'Anda tidak memiliki Community',
                );
                $this->response($result, 400);
            }
            $data = array(
                'community_id' => $community_id,
                'judul'        => $this->post('judul'),
                'body'         => $this->post('body'),
                'author_id'    => $user_id,
                'published_at' => $this->post('published_at'),
            );
            if ( !empty($_FILES['featured_img']['name']) ) {
                $upload = $this->issue_model->upload_pic_issue();
                if ($upload['success']) {
                    $data['featured_img'] = $upload['data']['file_name'];
                } else {
                    $result = $upload;
                    $this->response($result, 400);
                }
            }

            $insert_id = $this->issue_model->insert($data);
            if ($insert_id) {
                $result = array(
                    'success' => true,
                    'message' => 'Issue berhasil ditambahkan.',
                    'issue_id' => $insert_id,
                );
            } else {
                $result = array(
                    'success' => true,
                    'message' => 'Terjadi kesalahan.',
                );
                $this->response($result, 400);
            }
        } else {
            $result = array(
                'success' => false,
                'errors' => $this->form_validation->error_array(),
            );
            $this->response($result, 400);
        }
        $this->response($result, 200);
    }

    public function store_comment_post()
    {
        $token = null;
        $headers = apache_request_headers();
        foreach ($headers as $header => $value) {
            $header = strtolower($header);
            if ($header=='x-api-key') {
                $token = $value;
            }
        }

        // convert token to user_id
        $user_id = $this->global_model->getUserIdBYToken($token);
        if (!$user_id) {
            $result = array(
                'success' => false,
                'message' => 'Token mismatch',
            );
            $this->response($result, 400);
        }

        $validation = array(
            array(
					"field" => "issue_id",
					"label" => "issue_id",
					"rules" => "required"
				),
			array(
					"field" => "comment",
					"label" => "Komentar",
					"rules" => "trim|required"
				),
			array(
					"field" => "parent_id",
					"label" => "parent_id",
					"rules" => "required"
				)
		);
		$this->form_validation->set_rules($validation);

		if ($this->form_validation->run()) {
            $issue_id = $this->post('issue_id');
            $parent_id = $this->post('parent_id');

            if ($parent_id != 0) {
                $issue_comment = $this->issue_comment_model->get($parent_id);
                if ($issue_comment->issue_id != $issue_id) {
                    $result = array(
                        'success' => false,
                        'message' => 'issue_id is not valid.',
                    );
                    $this->response($result, 400);
                } else {
                    if ($issue_comment->parent_id != 0) {
                        $result = array(
                            'success' => false,
                            'message' => 'parent_id is not valid.',
                        );
                        $this->response($result, 400);
                    }
                }
            }

            $data = array(
                'issue_id'   => $issue_id,
                'user_id'    => $user_id,
                'comment'    => $this->post('comment'),
                'parent_id'  => $parent_id,
                'created_at' => date('Y-m-d H:i:s'),
            );

            $insert_id = $this->issue_comment_model->insert($data);
            if ($insert_id) {
                $result = array(
                    'success'          => true,
                    'message'          => 'Komentar berhasil ditambahkan.',
                    'issue_comment_id' => $insert_id
                );
            } else {
                $result = array(
                    'success' => true,
                    'message' => 'Terjadi kesalahan.',
                );
                $this->response($result, 400);
            }
        } else {
            $result = array(
                'success' => false,
                'errors' => $this->form_validation->error_array(),
            );
            $this->response($result, 400);
        }
        $this->response($result, 200);
    }

    public function laporkan_comment_post()
    {

        $token = null;
        $headers = apache_request_headers();
        foreach ($headers as $header => $value) {
            $header = strtolower($header);
            if ($header=='x-api-key') {
                $token = $value;
            }
        }

        // convert token to user_id
        $user_id = $this->global_model->getUserIdBYToken($token);
        if (!$user_id) {
            $result = array(
                'success' => false,
                'message' => 'Token mismatch',
            );
            $this->response($result, 400);
        }

        $validation = array(
            array(
					"field" => "issue_comment_id",
					"label" => "issue_comment_id",
					"rules" => "required"
				),
		);
		$this->form_validation->set_rules($validation);

		if ($this->form_validation->run()) {
            $issue_comment_id = $this->post('issue_comment_id');

            $filter = array(
                'issue_comment_id' => $this->post('issue_comment_id'),
                'pelapor_id'       => $user_id,
            );
            $cek = $this->bad_comment_model->where($filter)->get();
            if (!$cek) {
                // belum pernah
                $data = array(
                    'issue_comment_id' => $issue_comment_id,
                    'pelapor_id'       => $user_id,
                    'created_at'       => date('Y-m-d H:i:s'),
                );
                $insert = $this->bad_comment_model->insert($data);
                if (!$insert) {
                    $result = array(
                        'success' => false,
                        'message' => 'Terjadi kesalahan.',
                    );
                    $this->response($result, 400);
                }
                $result = array(
                    'success' => true,
                    'message' => 'Laporan Anda berhasil kami terima.',
                );
                $this->response($result, 200);
            } else {
                $result = array(
                    'success' => false,
                    'message' => 'Anda sudah pernah melaporkan komentar ini.',
                );
                $this->response($result, 400);
            }
        } else {
            $result = array(
                'success' => false,
                'errors' => $this->form_validation->error_array(),
            );
            $this->response($result, 400);
        }
        $this->response($result, 200);
    }

    public function delete_issue_delete($id)
    {
        $token = null;
        $headers = apache_request_headers();
        foreach ($headers as $header => $value) {
            $header = strtolower($header);
            if ($header=='x-api-key') {
                $token = $value;
            }
        }

        // convert token to user_id
        $user_id = $this->global_model->getUserIdBYToken($token);
        if (!$user_id) {
            $result = array(
                'success' => false,
                'message' => 'Token mismatch',
            );
            $this->response($result, 400);
        }

        $exist = $this->issue_model->get($id);
        if (!$exist) {
            $result = array(
                'success' => false,
                'message' => 'Data tidak ditemukan',
            );
            $this->response($result, 400);
        }

        $author_id = $this->issue_model->fields('author_id')->get($id)->author_id;
        if ($user_id != $author_id) {
            $result = array(
                'success' => false,
                'message' => 'Anda tidak boleh menghapus issue ini.',
            );
            $this->response($result, 400);
        }

        $delete_issue_comments = $this->db->query("delete from issue_comments where issue_id=$id");
        $delete_issue = $this->issue_model->delete($id);
        if ($delete_issue) {
            $result = array(
                'success' => true,
                'message' => 'Issue berhasil dihapus.',
            );
            $this->response($result, 200);
        } else {
            $result = array(
                'success' => false,
                'message' => 'Terjadi kesalahan.',
            );
            $this->response($result, 400);
        }
    }

    public function delete_issue_comment_delete($id)
    {
        $token = null;
        $headers = apache_request_headers();
        foreach ($headers as $header => $value) {
            $header = strtolower($header);
            if ($header=='x-api-key') {
                $token = $value;
            }
        }

        // convert token to user_id
        $user_id = $this->global_model->getUserIdBYToken($token);
        if (!$user_id) {
            $result = array(
                'success' => false,
                'message' => 'Token mismatch',
            );
            $this->response($result, 400);
        }

        $exist = $this->issue_comment_model->get($id);
        if (!$exist) {
            $result = array(
                'success' => false,
                'message' => 'Data tidak ditemukan',
            );
            $this->response($result, 400);
        }

        $comment_user_id = $this->issue_comment_model->fields('user_id')->get($id)->user_id;
        if ($user_id != $comment_user_id) {
            $result = array(
                'success' => false,
                'message' => 'Anda tidak boleh menghapus komentar ini.',
            );
            $this->response($result, 400);
        }

        $delete_issue_comment_child = $this->db->query("delete from issue_comments where parent_id = $id");
        $delete_issue_comment = $this->issue_comment_model->delete($id);
        if ($delete_issue_comment) {
            $result = array(
                'success' => true,
                'message' => 'Komentar berhasil dihapus.',
            );
            $this->response($result, 200);
        } else {
            $result = array(
                'success' => false,
                'message' => 'Terjadi kesalahan.',
            );
            $this->response($result, 400);
        }
    }

public function issue_comment($id)
    {
        $token = null;
        $headers = apache_request_headers();
        foreach ($headers as $header => $value) {
            $header = strtolower($header);
            if ($header=='x-api-key') {
                $token = $value;
            }
        }

        // convert token to user_id
        $user_id = $this->global_model->getUserIdBYToken($token);
        if (!$user_id) {
            $result = array(
                'success' => false,
                'message' => 'Token mismatch',
            );
            $this->response($result, 400);
        }

        $exist = $this->issue_comment_model->get($id);
        if (!$exist) {
            $result = array(
                'success' => false,
                'message' => 'Data tidak ditemukan',
            );
            $this->response($result, 400);
        }

        $comment_user_id = $this->issue_comment_model->fields('user_id')->get($id)->user_id;
        if ($user_id != $comment_user_id) {
            $result = array(
                'success' => false,
                'message' => 'Anda tidak boleh menghapus komentar ini.',
            );
            $this->response($result, 400);
        }

        $delete_issue_comment_child = $this->db->query("delete from issue_comments where parent_id = $id");
        $delete_issue_comment = $this->issue_comment_model->delete($id);
        if ($delete_issue_comment) {
            $result = array(
                'success' => true,
                'message' => 'Komentar berhasil dihapus.',
            );
            $this->response($result, 200);
        } else {
            $result = array(
                'success' => false,
                'message' => 'Terjadi kesalahan.',
            );
            $this->response($result, 400);
        }
    }

}
