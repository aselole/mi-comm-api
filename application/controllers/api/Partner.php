<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Partner extends REST_Controller{

    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
        $this->load->model(array('partner_model'));
    }

    public function index_get()
    {
        $id = $this->get('id');

        if (!$id) {
            $sql = "
            select
                id,
                nama,
                left(ket, 100) as short_ket,
                logo_partner as logo,
                lattitude,
                longitude
            from partners
            where active='y'
            ";
            $result = $this->db->query($sql)->result();
            // $result = $this->partner_model->fields('id, nama, logo_merchant, lattitude, longitude')->where(array('active'=>'y'))->get_all();
            $this->response($result);
        } else {
            $sql = "
            select
                id,
                nama,
                ket,
                alamat,
                telepon1,
                logo_partner as logo,
                lattitude,
                longitude
            from partners
            where
                id = $id and
                active='y'
            ";
            $exist = $this->db->query($sql);
            // $exist = $this->partner_model->fields($fields)->where(array('active'=>'y'))->get($id);
            if ($exist->num_rows() == 0) {
                $result = array(
                    'success' => false,
                    'message' => 'Data tidak ditemukan.',
                );
                $this->response($result);
            } else {
                $result['success'] = true;
                $result['partner'] = $exist->row();
                $this->response($result);
            }
        }
    }

}
