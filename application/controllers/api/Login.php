<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Login extends REST_Controller{

    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
        $this->table_name = 'users';
        $this->load->model(array('user_model'));
    }

    public function micomm_post()
    {
        $data = array (
            'username' => $this->post('username', TRUE),
            'password' => $this->post('password', TRUE),
            'token_firebase' => $this->post('token_firebase', TRUE)
        );

        $validation = array(
			array(
					"field" => "username",
					"label" => "Username",
					"rules" => "trim|required"
		    ),
			array(
					"field" => "password",
					"label" => "Password",
					"rules" => "trim|required"
			),
            array(
					"field" => "token_firebase",
					"label" => "Token Firebase",
					"rules" => "trim|required"
			)
		);
		$this->form_validation->set_rules($validation);

		if ($this->form_validation->run()) {
            $params['salt_prefix'] = $this->config->item('salt_prefix', 'ion_auth');
			$this->load->library('bcrypt',$params);

            $identity = 'username';
            $pos = strpos($data['username'], '@');
            if ($pos) {
                $identity = 'email';
            }
            $user_email = $data['username'];
            $sql = "
            SELECT
                a.id,
                a.password,
                a.token_firebase,
                a.member_jenis,
                a.first_name,
                a.place_birthday,
                a.date_birthday,
                a.gender,
                a.email,
                a.phone,
                a.address,
                a.photo,
                a.card_barcode,
                a.created_at,
                a.updated_at,
                a.level_id,
                a.product_id,
                b.nama as level_nama,
                c.nama as product_nama
            FROM ".$this->table_name." a
            LEFT JOIN levels b on (a.level_id=b.id)
            LEFT JOIN products c on (a.product_id=c.id)
            WHERE
                $identity = '$user_email'
                AND a.active = 1
                AND a.level_id != 1
            ";
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                $row = $query->row();
                $user_id = $row->id;
                $pass_db = $row->password;
                if ($this->bcrypt->verify($data['password'], $pass_db)) {
                    $query_key = $this->db->query("SELECT `key` FROM `keys` WHERE user_id = $user_id");
                    if ($query_key->num_rows() > 0) {
                        if ($row->level_id==2 && $row->member_jenis!='micomm') {
                            $result['success'] = false;
                            $result['message'] = 'Login Gagal. Anda bukan peserta Mi-Comm.';
                        } else {
                            $data = array(
                                'user_id'        => $row->id ? $row->id:                         '',
                                'first_name'     => $row->first_name ? $row->first_name:         '',
                                'place_birthday' => $row->place_birthday ? $row->place_birthday: '',
                                'date_birthday'  => $row->date_birthday ? $row->date_birthday:   '',
                                'gender'         => $row->gender ? $row->gender:                 '',
                                'email'          => $row->email ? $row->email:                   '',
                                'phone'          => $row->phone ? $row->phone:                   '',
                                'address'        => $row->address ? $row->address:               '',
                                'photo'          => $row->photo ? $row->photo:                   '',
                                'card_barcode'   => $row->card_barcode ? $row->card_barcode:     '',
                                'created_at'     => $row->created_at ? $row->created_at:         '',
                                'updated_at'     => $row->updated_at ? $row->updated_at:         '',
                                'level_id'       => $row->level_id ? $row->level_id:             '',
                                'level_nama'     => $row->level_nama ? $row->level_nama:         '',
                                'product_id'     => $row->product_id ? $row->product_id:         '',
                                'product_nama'   => $row->product_nama ? $row->product_nama:     '',
                            );
                            if ($row->level_id == 2) {
                                $data['member_jenis'] = $row->member_jenis;
                            }
                            $token_key = $query_key->row()->key;

                            // update token firebase
                            $token_firebase = $this->post('token_firebase');
                            $data_firebase = array('token_firebase' => $token_firebase);
                            $update_token = $this->user_model->update($data_firebase, $row->id);

                            $result = array(
                                'success' => true,
                                'message' => 'Berhasil login',
                                'token' => $token_key,
                                'data' => $data
                            );
                            $result['success'] = true;
                            $result['message'] = 'Berhasil Login.';
                            $result['token'] = $token_key;
                        }
                    } else {
                        $result['success'] = false;
                        $result['message'] = 'Anda tidak memiliki akses.';
                    }
                } else {
                    $result['success'] = false;
                    $result['message'] = 'Login Gagal. Username dan password Anda tidak dikenali.';
                }
            } else {
                $result['success'] = false;
                $result['message'] = 'Login Gagal. Username dan password Anda tidak dikenali.';
            }
            $this->response($result, 200);
        } else {
            $result['success'] = false;
            $result['errors'] = $this->form_validation->error_array();
            $this->response($result, 400);
        }
    }

    public function coach_post()
    {
        $data = array (
            'username' => $this->post('username', TRUE),
            'password' => $this->post('password', TRUE),
            'token_firebase' => $this->post('token_firebase', TRUE)
        );

        $validation = array(
			array(
					"field" => "username",
					"label" => "Username",
					"rules" => "trim|required"
		    ),
			// array(
			// 		"field" => "password",
			// 		"label" => "Password",
			// 		"rules" => "trim|required"
			// ),
            array(
					"field" => "token_firebase",
					"label" => "Token Firebase",
					"rules" => "trim|required"
			)
		);
		$this->form_validation->set_rules($validation);

		if ($this->form_validation->run()) {
            $params['salt_prefix'] = $this->config->item('salt_prefix', 'ion_auth');
			$this->load->library('bcrypt',$params);

            $identity = 'username';
            $pos = strpos($data['username'], '@');
            if ($pos) {
                $identity = 'email';
            }
            $user_email = $data['username'];
            $sql = "
            SELECT
                a.id,
                a.password,
                a.token_firebase,
                a.member_jenis,
                a.first_name,
                a.place_birthday,
                a.date_birthday,
                a.gender,
                a.email,
                a.phone,
                a.address,
                a.photo,
                a.card_barcode,
                a.created_at,
                a.updated_at,
                a.level_id,
                a.product_id,
                b.nama as level_nama,
                c.nama as product_nama
            FROM ".$this->table_name." a
            LEFT JOIN levels b on (a.level_id=b.id)
            LEFT JOIN products c on (a.product_id=c.id)
            WHERE
                $identity = '$user_email'
                AND a.active = 1
                AND a.level_id not in (1,3)
            ";
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                $row = $query->row();
                $user_id = $row->id;
                $pass_db = $row->password;
                // if ($this->bcrypt->verify($data['password'], $pass_db)) {
                    $query_key = $this->db->query("SELECT `key` FROM `keys` WHERE user_id = $user_id");
                    if ($query_key->num_rows() > 0) {
                        if ($row->level_id==2 && $row->member_jenis!='coach') {
                            $result['success'] = false;
                            $result['message'] = 'Login Gagal. Anda bukan peserta The Coach.';
                        } else {
                            $data = array(
                                'user_id'        => $row->id ? $row->id:                         '',
                                'first_name'     => $row->first_name ? $row->first_name:         '',
                                'place_birthday' => $row->place_birthday ? $row->place_birthday: '',
                                'date_birthday'  => $row->date_birthday ? $row->date_birthday:   '',
                                'gender'         => $row->gender ? $row->gender:                 '',
                                'email'          => $row->email ? $row->email:                   '',
                                'phone'          => $row->phone ? $row->phone:                   '',
                                'address'        => $row->address ? $row->address:               '',
                                'photo'          => $row->photo ? $row->photo:                   '',
                                'card_barcode'   => $row->card_barcode ? $row->card_barcode:     '',
                                'created_at'     => $row->created_at ? $row->created_at:         '',
                                'updated_at'     => $row->updated_at ? $row->updated_at:         '',
                                'level_id'       => $row->level_id ? $row->level_id:             '',
                                'level_nama'     => $row->level_nama ? $row->level_nama:         '',
                                'product_id'     => $row->product_id ? $row->product_id:         '',
                                'product_nama'   => $row->product_nama ? $row->product_nama:     '',
                            );
                            if ($row->level_id == 2) {
                                $data['member_jenis'] = $row->member_jenis;

                                // pasangan
                                $user_id = $row->id;
                                $pasangan = $this->db->query("select * from pasangans where utama_user_id=$user_id or pasangan_user_id=$user_id");
                                if ($pasangan->num_rows() > 0) {
                                    $pas = $pasangan->row();
                                    if ($pas->utama_user_id == $user_id) {
                                        $data['couple_user_id'] = $pas->pasangan_user_id;
                                        $data['couple_status'] = $pas->pasangan_status;
                                    } else {
                                        $data['couple_user_id'] = $pas->utama_user_id;
                                        $data['couple_status'] = $pas->utama_status;
                                    }
                                } else {
                                    $data['couple_user_id'] = 0;
                                    $data['couple_status'] = '';
                                }
                            }
                            $token_key = $query_key->row()->key;

                            // update token firebase
                            $token_firebase = $this->post('token_firebase');
                            $data_firebase = array('token_firebase' => $token_firebase);
                            $update_token = $this->user_model->update($data_firebase, $row->id);

                            $result = array(
                                'success' => true,
                                'message' => 'Berhasil login',
                                'token' => $token_key,
                                'data' => $data
                            );
                            $result['success'] = true;
                            $result['message'] = 'Berhasil Login.';
                            $result['token'] = $token_key;
                        }
                    } else {
                        $result['success'] = false;
                        $result['message'] = 'Anda tidak memiliki akses.';
                    }
                // } else {
                //     $result['success'] = false;
                //     $result['message'] = 'Login Gagal. Username dan password Anda tidak dikenali.';
                // }
            } else {
                $result['success'] = false;
                $result['message'] = 'Login Gagal. Username dan password Anda tidak dikenali.';
            }
            $this->response($result, 200);
        } else {
            $result['success'] = false;
            $result['errors'] = $this->form_validation->error_array();
            $this->response($result, 400);
        }
    }

    public function get_firebase_get()
    {
        $id = $this->get('id');
        $user = $this->user_model->fields('token_firebase')->get($id);
        $result['token_firebase'] = $user->token_firebase;
        $this->response($result, 200);
    }
}
