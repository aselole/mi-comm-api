<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Private_issue extends REST_Controller{

    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
        $this->load->model(array('global_model', 'private_issue_model', 'private_issue_comment_model', 'user_model'));
    }

    public function list_get()
    {
        $token = null;
        $headers = apache_request_headers();
        foreach ($headers as $header => $value) {
            $header = strtolower($header);
            if ($header=='x-api-key') {
                $token = $value;
            }
        }

        // convert token to user_id
        $user_id = $this->global_model->getUserIdBYToken($token);
        if (!$user_id) {
            $result = array(
                'success' => false,
                'message' => 'Token mismatch',
            );
            $this->response($result, 400);
        }

        $user = $this->user_model->fields('level_id')->get($user_id);
        if ($user->level_id == 2) {
            // member
            $sql = "
            select
                a.id,
                a.user_trainer_id,
                a.judul,
                a.status,
                a.created_at,
                b.first_name as trainer_first_name,
                b.photo as trainer_photo,
                (
                    select count(c.private_issue_id)
                    from private_issue_comments c
                    where
                        c.private_issue_id = a.id AND
                        c.comment is not null
                ) as jml_komentar,
                (
                    select count(c.private_issue_id)
                    from private_issue_comments c
                    where
                        c.user_id != $user_id AND
                        c.private_issue_id = a.id AND
                        c.read = 0 AND
                        c.comment is not null AND
                        c.is_root = ''
                ) as jml_komentar_unread,
                (
                    select c.created_at
                    from private_issue_comments c
                    where
                        c.private_issue_id = a.id AND
                        c.comment is not null
                    order by
                        c.created_at desc
                    limit 1
                ) as comment_update
                from private_issues a
                left join users b on (a.user_trainer_id=b.id)
                left join private_issue_comments c on (c.private_issue_id=a.id)
                where
                    user_member_id=$user_id AND
                    a.active='y'
                group by a.id
                order by comment_update desc
            ";
            $private_issues = $this->db->query($sql)->result();
            // $private_issues = $this
            //                     ->private_issue_model
            //                     ->fields('private_issues.id, user_trainer_id, judul, status, created_at')
            //                     ->where(array('active'=>'y', 'user_member_id'=>$user_id))
            //                     ->with_trainer('fields:first_name')
            //                     ->with_private_issue_comment('fields:*count*')
            //                     ->get_all();
        } elseif ($user->level_id == 3) {
            // trainer
            $sql = "
            select
                a.id,
                a.user_member_id,
                a.judul,
                a.status,
                a.created_at,
                b.first_name as member_first_name,
                b.member_jenis,
                b.photo as member_photo,
                (
                    select count(c.private_issue_id)
                    from private_issue_comments c
                    where
                        c.private_issue_id = a.id AND
                        c.comment is not null AND
                        c.is_root = ''
                ) as jml_komentar,
                (
                    select count(c.private_issue_id)
                    from private_issue_comments c
                    where
                        c.user_id != $user_id AND
                        c.private_issue_id = a.id AND
                        c.read = 0 AND
                        c.comment is not null AND
                        c.is_root = ''
                ) as jml_komentar_unread,
                (
                    select c.created_at
                    from private_issue_comments c
                    where
                        c.private_issue_id = a.id AND
                        c.comment is not null
                    order by
                        c.created_at desc
                    limit 1

                ) as comment_update
                from private_issues a
                left join users b on (a.user_member_id=b.id)
                left join private_issue_comments c on (c.private_issue_id=a.id)
                where
                    user_trainer_id=$user_id AND
                    a.active='y'
                group by a.id
                order by comment_update desc
            ";
            $private_issues = $this->db->query($sql)->result();
            // $private_issues = $this
            //                     ->private_issue_model
            //                     ->fields('id, user_member_id, judul, status, created_at')
            //                     ->where(array('active'=>'y', 'user_trainer_id'=>$user_id))
            //                     ->with_member('fields:first_name')
            //                     ->with_private_issue_comment('fields:*count*')
            //                     ->get_all();
        }
        $result = $private_issues;
        $this->response($result, 200);
    }

    public function detail_get()
    {
        $token = null;
        $headers = apache_request_headers();
        foreach ($headers as $header => $value) {
            $header = strtolower($header);
            if ($header=='x-api-key') {
                $token = $value;
            }
        }

        // convert token to user_id
        $user_id = $this->global_model->getUserIdBYToken($token);
        if (!$user_id) {
            $result = array(
                'success' => false,
                'message' => 'Token mismatch',
            );
            $this->response($result, 400);
        }

        $id = $this->get('id');
        if (!$id) {
            $result = array(
                'success' => false,
                'message' => 'Bad request',
            );
            $this->response($result, 400);
        }

        $sql = "
        select
            a.id,
            a.user_member_id,
            a.user_trainer_id,
            a.judul,
            a.body,
            a.status,
            a.created_at,
            b.first_name as member_first_name,
            b.photo as member_photo,
            b.member_jenis,
            c.first_name as trainer_first_name,
            c.photo as trainer_photo
        from private_issues a
        join users b on (a.user_member_id=b.id)
        join users c on (a.user_trainer_id=c.id)
        where a.id=$id
        ";
        $private_issue = $this->db->query($sql)->row();
        // $private_issue = $this
        //                     ->private_issue_model
        //                     ->fields('private_issues.id, user_member_id, user_trainer_id, judul, status, created_at')
        //                     ->where(array('active'=>'y'))
        //                     ->with_member('fields:first_name')
        //                     ->with_trainer('fields:first_name')
        //                     ->with_private_issue_comment('fields:*count*')
        //                     ->get($id);
        $result['private_issue'] = $private_issue;

        // jadikan semua read
        $data = array('read' => 1);
        $user = $this->user_model->fields('level_id')->get($user_id);
        $user_to = null;
        if ($user->level_id == 2) {
            $filter = array(
                'private_issue_id' => $id,
                'user_id'          => $private_issue->user_trainer_id,
            );
            $user_to = $private_issue->user_trainer_id;
        } elseif ($user->level_id == 3) {
            $filter = array(
                'private_issue_id' => $id,
                'user_id'          => $private_issue->user_member_id,
            );
            $user_to = $private_issue->user_member_id;
        }
        $this->private_issue_comment_model->update($data, $filter);

        $private_issue_comments = $this
                                    ->private_issue_comment_model
                                    ->fields('id, private_issue_id, user_id, comment, read, created_at')
                                    ->where('private_issue_id', $id)
                                    ->where('comment !=', '')
                                    ->order_by('created_at', 'asc')
                                    ->with_user('fields:first_name, photo|join:true')
                                    ->get_all();
        $result['private_issue_comments'] = $private_issue_comments;

        // kirim notifikasi sudah terbaca ke lawan bicara
        $token_firebase = $this->user_model->fields('token_firebase')->get($user_to)->token_firebase;
        $member_jenis = 'micomm';
        $data_firebase = array(
            'id'    => "$id",
            'jenis' => 'private_issue_read',
            'title' => 'terbaca',
            'body' => '',
        );
        $this->global_model->push_notif_fcm($token_firebase, 'terbaca', '', $data_firebase, $member_jenis, false);

        $this->response($result, 200);
    }

    public function store_post()
    {
        $token = null;
        $headers = apache_request_headers();
        foreach ($headers as $header => $value) {
            $header = strtolower($header);
            if ($header=='x-api-key') {
                $token = $value;
            }
        }
        // $headers = $_SERVER;
        // foreach ($headers as $header => $value) {
        //     $header = strtolower($header);
        //     if ($header=='http_x_api_key') {
        //         $token = $value;
        //     }
        // }


        // convert token to user_id
        $user_id = $this->global_model->getUserIdBYToken($token);
        if (!$user_id) {
            $result = array(
                'success' => false,
                'message' => 'Token mismatch',
            );
            $this->response($result, 400);
        }
        $user = $this->user_model->fields('level_id')->get($user_id);
        if (!$user || $user->level_id != 2) {
            $result = array(
                'success' => false,
                'message' => 'Anda tidak dapat membuat issue private.',
            );
            $this->response($result, 400);
        }

        $validation = array(
            array(
					"field" => "user_trainer_id",
					"label" => "user_trainer_id",
					"rules" => "required"
				),
			array(
					"field" => "judul",
					"label" => "Judul",
					"rules" => "trim|required"
				),
            array(
					"field" => "body",
					"label" => "Body",
					"rules" => "trim|required"
				),
		);
		$this->form_validation->set_rules($validation);

		if ($this->form_validation->run()) {
            $user_trainer_id = $this->post('user_trainer_id');
            $user_trainer = $this->user_model->fields('level_id')->where('active', 1)->get($user_trainer_id);
            if (!$user_trainer || $user_trainer->level_id != 3) {
                $result = array(
                    'success' => false,
                    'message' => 'user_trainer_id is invalid',
                );
                $this->response($result, 400);
            }

            $body = $this->post('body');
            $data = array(
                'user_member_id'  => $user_id,
                'user_trainer_id' => $user_trainer_id,
                'judul'           => $this->post('judul'),
                'body'            => $body,
            );

            $insert_id = $this->private_issue_model->insert($data);
            if ($insert_id) {
                $data_comment = array(
                    'private_issue_id' => $insert_id,
                    'user_id' => $user_id,
                    'comment' => $body,
                    'read' => 0,
                    'is_root' => 'y',
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $private_issue_comment_id = $this->private_issue_comment_model->insert($data_comment);

                $result = array(
                    'success' => true,
                    'message' => 'Issue Private berhasil dibuat.',
                    'id'      => $insert_id,
                );

                // firebase
                $member_jenis = 'micomm';
                $user = $this->user_model->fields('first_name, level_id')->get($user_id);
                $user_to = $this->private_issue_model->fields('user_trainer_id')->where('id', $insert_id)->get()->user_trainer_id;
                $token_firebase = $this->user_model->fields('token_firebase')->get($user_to)->token_firebase;
                $title          = "Pertanyaan dari: ".$user->first_name;
                $message        = $body;
                $data_firebase = array(
                    'id'    => "$insert_id",
                    'jenis' => 'private_issue',
                    'title' => "Pertanyaan dari: ".$user->first_name,
                    'body' => $body,
                );
                $this->global_model->push_notif_firebase($token_firebase, $title, $message, $data_firebase, $member_jenis);
                $this->global_model->push_notif_fcm($token_firebase, $title, $message, $data_firebase, $member_jenis, true);
            } else {
                $result = array(
                    'success' => true,
                    'message' => 'Terjadi kesalahan.',
                );
                $this->response($result, 400);
            }
        } else {
            $result = array(
                'success' => false,
                'errors' => $this->form_validation->error_array(),
            );
            $this->response($result, 400);
        }
        $this->response($result, 200);
    }

    public function store_comment_post()
    {
        $token = null;
        $headers = apache_request_headers();
        foreach ($headers as $header => $value) {
            $header = strtolower($header);
            if ($header=='x-api-key') {
                $token = $value;
            }
        }

        // convert token to user_id
        $user_id = $this->global_model->getUserIdBYToken($token);
        if (!$user_id) {
            $result = array(
                'success' => false,
                'message' => 'Token mismatch',
            );
            $this->response($result, 400);
        }

        $validation = array(
            array(
					"field" => "private_issue_id",
					"label" => "private_issue_id",
					"rules" => "required"
				),
			array(
					"field" => "comment",
					"label" => "Komentar",
					"rules" => "trim|required"
				),
		);
		$this->form_validation->set_rules($validation);

		if ($this->form_validation->run()) {
            $private_issue_id = $this->post('private_issue_id');

            $private_issue = $this->private_issue_model->get($private_issue_id);
            if (!$private_issue) {
                $result = array(
                    'success' => false,
                    'message' => 'private_issue_id is not valid.',
                );
                $this->response($result, 400);
            } else {
                $user = $this->user_model->fields('level_id')->get($user_id);
                if ($user->level_id == 2) {
                    // member
                    if ($private_issue->user_member_id != $user_id) {
                        $result = array(
                            'success' => false,
                            'message' => 'private_issue_id is not valid.',
                        );
                        $this->response($result, 400);
                    }
                } elseif ($user->level_id == 3) {
                    // trainer
                    if ($private_issue->user_trainer_id != $user_id) {
                        $result = array(
                            'success' => false,
                            'message' => 'private_issue_id is not valid.',
                        );
                        $this->response($result, 400);
                    }
                }

                $comment = $this->post('comment');
                $data = array(
                    'private_issue_id' => $private_issue_id,
                    'user_id'          => $user_id,
                    'comment'          => $comment,
                    'created_at'       => date('Y-m-d H:i:s'),
                );

                $insert_id = $this->private_issue_comment_model->insert($data);
                if ($insert_id) {
                    $result = array(
                        'success' => true,
                        'message' => 'Komentar berhasil ditambahkan.',
                    );

                    // firebase
                    $member_jenis = 'micomm';
                    $user = $this->user_model->fields('first_name, level_id')->get($user_id);
                    if ($user->level_id == 2) {
                        // member
                        $user_to = $this->private_issue_model->fields('user_trainer_id')->where(['id'=>$private_issue_id, 'user_member_id'=>$user_id])->get()->user_trainer_id;
                    } elseif ($user->level_id == 3) {
                        // trainer
                        $user_to = $this->private_issue_model->fields('user_member_id')->where(['id'=>$private_issue_id, 'user_trainer_id'=>$user_id])->get()->user_member_id;
                        $member_jenis = $this->user_model->fields('member_jenis')->where(['id'=>$user_to])->get()->member_jenis;
                    }
                    $token_firebase = $this->user_model->fields('token_firebase')->get($user_to)->token_firebase;
                    $title          = "Balasan dari: ".$user->first_name;
                    $message        = $comment;
                    $data_firebase = array(
                        'id'    => "$private_issue_id",
                        'jenis' => 'private_issue',
                        'title' => "Balasan dari: ".$user->first_name,
                        'body' => $comment,
                        'created_at' => date('Y-m-d H:i:s')
                    );
                    $this->global_model->push_notif_firebase($token_firebase, $title, $message, $data_firebase, $member_jenis);
                    $this->global_model->push_notif_fcm($token_firebase, $title, $message, $data_firebase, $member_jenis, true);
                } else {
                    $result = array(
                        'success' => true,
                        'message' => 'Terjadi kesalahan.',
                    );
                    $this->response($result, 400);
                }
            }
        } else {
            $result = array(
                'success' => false,
                'errors' => $this->form_validation->error_array(),
            );
            $this->response($result, 400);
        }
        $this->response($result, 200);
    }

    public function reset_private_issue_get()
    {
        $delete1 = $this->private_issue_comment_model->delete(array('id !='=>null));

        $delete1 = $this->private_issue_model->delete(array('id !='=>null));

        $result = array(
            'success' => true,
            'message' => 'Berhasil direset.'
        );
        $this->response($result, 200);
    }

}
