<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Voucher extends REST_Controller{

    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
        $this->load->model(array('voucher_model'));
    }

    public function list_get()
    {
        $promo_id = $this->get('promo_id');

        if (!$promo_id) {
            $result = array(
                'success' => false,
                'message' => 'Bad Request.',
            );
            $this->response($result, 400);
        } else {
            $fields = 'id, promo_id, kode, is_used';
            $exist = $this
                        ->voucher_model
                        ->fields($fields)
                        ->where(array('promo_id'=>$promo_id))
                        ->get_all();
            if (!$exist) {
                $result = array(
                    'success' => false,
                    'message' => 'Tidak ditemukan voucher.',
                );
                $this->response($result);
            } else {
                $this->response($exist);
            }
            $this->response($result, 200);
        }
    }
}
