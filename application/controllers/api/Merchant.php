<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Merchant extends REST_Controller{

    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
        $this->load->model(array('merchant_model'));
    }

    public function index_get()
    {
        $id = $this->get('id');

        if (!$id) {
            $sql = "
            select
                id,
                nama,
                left(ket, 100) as short_ket,
                logo_merchant,
                lattitude,
                longitude
            from merchants
            where active='y'
            ";
            $result = $this->db->query($sql)->result();
            // $result = $this->merchant_model->fields('id, nama, logo_merchant, lattitude, longitude')->where(array('active'=>'y'))->get_all();
            $this->response($result);
        } else {
            $fields = 'id, nama, alamat, ket, telepon1, logo_merchant, lattitude, longitude';
            $exist = $this->merchant_model->fields($fields)->where(array('active'=>'y'))->get($id);
            if (!$exist) {
                $result = array(
                    'success' => false,
                    'message' => 'Data tidak ditemukan.',
                );
                $this->response($result);
            } else {
                $this->response($exist);
            }
        }
    }

}
