<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Skill extends REST_Controller{

    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
        $this->load->model(array('skill_model'));
    }

    public function index_get()
    {
        $sql = "
        select
            id,
            nama,
            ket
        from skills
        ";
        $result = $this->db->query($sql)->result();
        $this->response($result);
    }

}
