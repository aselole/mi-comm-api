<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class User extends REST_Controller{

    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
        $this->load->model(array('user_model', 'skill_model'));
    }

    // public function index_get()
    // {
    //     $id = $this->get('id');
    //
    //     if ($id) {
    //         $result = $this->Users->get(array('id' => $id));
    //         if (!$result) {
    //             $result['message'] = 'Data not found.';
    //         }
    //     } else {
    //         $result = $this->Users->get_all();
    //     }
    //     $this->response($result);
    // }

    public function list_trainer_get()
    {
        $skill_id = $this->get('skill_id');
        if ($skill_id) {
            $list_trainer_id = $this->skill_model->get($skill_id)->list_trainer_id;
            $sql = "
            select
            id,
            username,
            email,
            first_name,
            photo,
            description
            from users
            where
                active = 1 and
                level_id = 3 and
                id in ($list_trainer_id)
            order by urut asc
            ";
        } else {
            $sql = "
            select
            id,
            username,
            email,
            first_name,
            photo,
            description
            from users
            where
                active = 1 and
                level_id = 3
            order by urut asc
            ";
        }
        $result = $this->db->query($sql)->result();
        $this->response($result, 200);
    }

    public function detail_trainer_get()
    {
        $id = $this->get('id');
        if (!$id) {
            $result = array(
                'success' => false,
                'message' => 'Bad request',
            );
            $this->response($result, 400);
        }

        $fields = '
        id,
        username,
        email,
        first_name,
        photo,
        description
        ';
        $trainer = $this->user_model->fields($fields)->where(array('active'=>1, 'level_id'=>3))->get($id);
        $result = $trainer;
        if (!$trainer) {
            $result = array(
                'success' => true,
                'message' => 'Data tidak ditemukan',
            );
        }
        $this->response($result, 200);
    }

    public function detail_pasangan_get()
    {
        $id = $this->get('couple_user_id');
        if ($id == null) {
            $result = array(
                'success' => false,
                'message' => 'Bad request',
            );
            $this->response($result, 400);
        }

        if ($id == 0) {
            $data = array(
                'user_id'        => 0,
                'first_name'     => '',
                'place_birthday' => '',
                'date_birthday'  => '',
                'gender'         => '',
                'email'          => '',
                'phone'          => '',
                'address'        => '',
                'photo'          => '',
                'card_barcode'   => '',
                'product_nama'   => '',
                'couple_status'  => '',
            );
            $result = $data;
            $this->response($result, 200);
        }

        $sql = "
        select * from pasangans where utama_user_id=$id or pasangan_user_id=$id
        ";
        $pasangan = $this->db->query($sql)->row();
        if ($pasangan->utama_user_id==$id) {
            $status = $pasangan->utama_status;
        } else {
            $status = $pasangan->pasangan_status;
        }
        $sql_detail = "
        select
        a.id,
        a.first_name,
        a.place_birthday,
        a.date_birthday,
        a.gender,
        a.email,
        a.phone,
        a.address,
        a.photo,
        a.card_barcode,
        a.product_id,
        c.nama as product_nama
        FROM users a
        LEFT JOIN products c on (a.product_id=c.id)
        WHERE
            a.id=$id
        ";
        $detail = $this->db->query($sql_detail)->row();
        $detail->couple_status = $status;
        $data = array(
            'user_id'        => $detail->id ? $detail->id:                         '',
            'first_name'     => $detail->first_name ? $detail->first_name:         '',
            'place_birthday' => $detail->place_birthday ? $detail->place_birthday: '',
            'date_birthday'  => $detail->date_birthday ? $detail->date_birthday:   '',
            'gender'         => $detail->gender ? $detail->gender:                 '',
            'email'          => $detail->email ? $detail->email:                   '',
            'phone'          => $detail->phone ? $detail->phone:                   '',
            'address'        => $detail->address ? $detail->address:               '',
            'photo'          => $detail->photo ? $detail->photo:                   '',
            'card_barcode'   => $detail->card_barcode ? $detail->card_barcode:     '',
            'product_nama'   => $detail->product_nama ? $detail->product_nama:     '',
            'couple_status'   => $detail->couple_status ? $detail->couple_status:     '',
        );
        $result = $data;
        if (!$detail) {
            $result = array(
                'success' => true,
                'message' => 'Data tidak ditemukan',
            );
        }
        $this->response($result, 200);
    }

}
