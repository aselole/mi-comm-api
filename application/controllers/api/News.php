<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class News extends REST_Controller{

    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
        $this->load->model(array('global_model','user_model','news_model'));
    }

    public function index_get()
    {
        $token = null;
        $headers = apache_request_headers();
        foreach ($headers as $header => $value) {
            $header = strtolower($header);
            if ($header=='x-api-key') {
                $token = $value;
            }
        }

        // convert token to user_id
        //var_dump($headers);
        $user_id = $this->global_model->getUserIdBYToken($token);
        if (!$user_id) {
            $result = array(
                'success' => false,
                'message' => 'Token mismatch',
            );
            $this->response($result, 400);
        }

        $user = $this->user_model->fields('level_id, member_jenis')->get($user_id);

        $id = $this->get('id');
        if (!$id) {
            if ($user->level_id == 3) {
                // trainer
                $sql = "
                select
                    a.id,
                    a.judul,
                    left(a.body, 100) as short_body,
                    a.published_at,
                    b.image as image_news
                from news a
                join image_news b on (b.news_id=a.id and b.is_featured='y')
                where 
                    a.active = 'y'
                order by a.published_at desc
                ";
                $result = $this->db->query($sql)->result();
                $this->response($result);
            }
            
            if ($user->level_id == 2 && $user->member_jenis=='micomm') {
                // member micomm
                $sql = "
                select
                    a.id,
                    a.judul,
                    left(a.body, 100) as short_body,
                    a.published_at,
                    b.image as image_news
                from news a
                join image_news b on (b.news_id=a.id and b.is_featured='y')
                where 
                    a.for_whom = 'micomm' and
                    a.active = 'y'
                order by a.published_at desc
                ";
                $result = $this->db->query($sql)->result();
                $this->response($result);
            } elseif ($user->level_id == 2 && $user->member_jenis=='coach') {
                // member the coach
                $sql = "
                select
                    a.id,
                    a.judul,
                    left(a.body, 100) as short_body,
                    a.published_at,
                    b.image as image_news
                from news a
                join image_news b on (b.news_id=a.id and b.is_featured='y')
                where 
                    a.for_whom = 'thecoach' and
                    a.active = 'y'
                order by a.published_at desc
                ";
                $result = $this->db->query($sql)->result();
                $this->response($result);
            }
        } else {
            $fields = 'id, judul, body, created_at';
            $exist = $this
                        ->news_model
                        ->fields($fields)
                        ->where(array('active'=>'y'))
                        ->with_image_news('fields:image, is_featured', 'order_inside:is_featured desc')
                        ->get($id);
            if (!$exist) {
                $result = array(
                    'success' => false,
                    'message' => 'Data tidak ditemukan.',
                );
                $this->response($result);
            } else {
                $judul = strtolower(str_replace(' ', '-', $exist->judul));
                $find  = array ('\'', '(', ')', '/' , '\/', '%', '$', '^', '*', '=', '!', '@','#', ':', ';', '"', '?','<', '>', ',', '.', '{', '}',
                        '[', ']', '|', '_', '&' );
                $url_p = str_replace($find, '', $judul);

                $event_url = $exist->id . '-' . $url_p;
                $exist->url = site_url('event/read/'.$event_url);

                $this->response($exist);
            }
        }
    }

}
