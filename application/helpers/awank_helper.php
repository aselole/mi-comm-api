<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

if (!function_exists('get_enum')) {
    function get_enum($table, $kolom)
    {
        $ci = &get_instance();
        $ci->load->database();

        $row   = $ci->db->query(" SHOW COLUMNS FROM $table LIKE '$kolom' ")->row()->Type;
        $regex = "/'(.*?)'/";
        preg_match_all($regex, $row, $enum_array);
        $enum_fields = $enum_array[1];
        return ($enum_fields);
    }
}

if (!function_exists('generateRandomString')) {
    function generateRandomString($length = 10) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }
}

if (!function_exists('logged_user')) {
    function logged_user($label='')
    {
        $ci = &get_instance();

        if ( ! empty($label) )
        {
            if ($ci->ion_auth->logged_in()) {
                return $ci->ion_auth->user()->row()->$label;
            }else{
                return $ci->ion_auth->logged_in();
            }
        }
        else
        {
            return $ci->ion_auth->logged_in();
        }

    }
}

if (!function_exists('get_admin_img')) {
    function get_admin_img()
    {
        $ci = &get_instance();

        $path   = 'uploads/user/';
        $gambar = $path . 'assets/layouts/layout4/img/avatar9.jpg';
        $user_id = $ci->session->userdata('user_id');
        if ($user_id) {
            $ci->load->model('user_model');
            $photo = $ci->user_model->fields('photo')->get($user_id)->photo;
            if ($photo) {
                $gambar = $path . $photo;
                if (file_exists($gambar)) {
                    return base_url($gambar);
                }
            }
        }

        return base_url('assets/layouts/layout4/img/avatar9.jpg');
    }
}


if (!function_exists('get_merchant_img')) {
    function get_merchant_img($image)
    {
        if (!empty($image)) {
            $path   = 'uploads/merchant/';
            $gambar = $path . $image;

            if (file_exists($gambar)) {
                return base_url($gambar);
            }
        }

        return base_url('assets/global/img/default/merchant.jpg');
    }
}

if (!function_exists('get_partner_img')) {
    function get_partner_img($image)
    {
        if (!empty($image)) {
            $path   = 'uploads/partner/';
            $gambar = $path . $image;

            if (file_exists($gambar)) {
                return base_url($gambar);
            }
        }

        return base_url('assets/global/img/default/merchant.jpg');
    }
}

if (!function_exists('get_news_img')) {
    function get_news_img($image)
    {
        if (!empty($image)) {
            $path   = 'uploads/news/';
            $gambar = $path . $image;

            if (file_exists($gambar)) {
                return base_url($gambar);
            }
        }

        return base_url('assets/global/img/default/def.jpg');
    }
}

if (!function_exists('get_promo_img')) {
    function get_promo_img($image)
    {
        if (!empty($image)) {
            $path   = 'uploads/promo/';
            $gambar = $path . $image;

            if (file_exists($gambar)) {
                return base_url($gambar);
            }
        }

        return base_url('assets/global/img/default/def.jpg');
    }
}

if (!function_exists('get_issue_img')) {
    function get_issue_img($image)
    {
        if (!empty($image)) {
            $path   = 'uploads/issue/';
            $gambar = $path . $image;

            if (file_exists($gambar)) {
                return base_url($gambar);
            }
        }

        return base_url('assets/global/img/default/def.jpg');
    }
}

if (!function_exists('get_user_img')) {
    function get_user_img($image)
    {
        if (!empty($image)) {
            $path   = 'uploads/user/';
            $gambar = $path . $image;

            if (file_exists($gambar)) {
                return base_url($gambar);
            }
        }

        return base_url('assets/global/img/default/team.jpg');
    }
}

if (!function_exists('count_unread_msg')) {
    function count_unread_msg()
    {
        $ci = &get_instance();
        $ci->load->model('private_issue_model');

        return $ci->private_issue_model->count_unread_msg();
    }
}

if (!function_exists('get_url')) {
    function get_url($id, $judul)
    {
        $judul2 = strtolower(str_replace(' ', '-', $judul));
        $find  = array ('\'', '(', ')', '/' , '\/', '%', '$', '^', '*', '=', '!', '@','#', ':', ';', '"', '?','<', '>', ',', '.', '{', '}',
        '[', ']', '|', '_', '&' );
        return $id . '-' .str_replace($find, '', $judul2);
    }
}
