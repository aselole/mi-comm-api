<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// $route['default_controller'] = 'admin/login';
$route['default_controller'] = 'home';
$route['404_override'] = 'page_404';
$route['translate_uri_dashes'] = FALSE;

$route['activation/activation_submit/(:any)'] = 'activation/activation_submit/$1';
$route['activation/(:any)/(:any)'] = 'activation/index/$1/$2';
