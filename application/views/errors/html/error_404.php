<!DOCTYPE html>
<!--
Theme: JANGO - Ultimate Multipurpose HTML Theme Built With Twitter Bootstrap 3.3.5
Version: 1.3.5
Author: Themehats
Site: http://www.themehats.com
Purchase: http://themeforest.net/item/jango-responsive-multipurpose-html5-template/11987314?ref=themehats
Contact: support@themehats.com
Follow: http://www.twitter.com/themehats
-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->


<!-- Mirrored from themehats.com/themes/jango/home-12.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 16 Feb 2016 11:34:49 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<!-- END THEME STYLES -->
	<meta charset="utf-8" />
	<title>Halaman tidak ada | MI-COMM</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700&amp;subset=all' rel='stylesheet' type='text/css'>
	<link href="<?php echo base_url()?>assets/front/assets/plugins/socicon/socicon.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>assets/front/assets/plugins/bootstrap-social/bootstrap-social.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>assets/front/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>assets/global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" />
	<link href="<?php echo base_url()?>assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/front/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>assets/front/assets/plugins/animate/animate.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>assets/front/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<script src="<?php echo base_url()?>assets/front/assets/plugins/jquery.min.js" type="text/javascript"></script>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN: BASE PLUGINS  -->
	<link href="<?php echo base_url()?>assets/front/assets/plugins/revo-slider/css/settings.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>assets/front/assets/plugins/revo-slider/css/layers.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>assets/front/assets/plugins/revo-slider/css/navigation.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>assets/front/assets/plugins/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>assets/front/assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>assets/front/assets/plugins/owl-carousel/owl.theme.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>assets/front/assets/plugins/owl-carousel/owl.transitions.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>assets/front/assets/plugins/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>assets/front/assets/plugins/slider-for-bootstrap/css/slider.css" rel="stylesheet" type="text/css" />
	<!-- END: BASE PLUGINS -->
	<!-- BEGIN THEME STYLES -->
	<link href="<?php echo base_url()?>assets/front/assets/base/css/plugins.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>assets/front/assets/base/css/components.css" id="style_components" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>assets/front/assets/base/css/themes/default.css" rel="stylesheet" id="style_theme" type="text/css" />
	<link href="<?php echo base_url()?>assets/front/assets/base/css/custom.css" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="favicon.ico" />
</head>


<body class="c-layout-header-fixed c-layout-header-mobile-fixed">
	<!-- BEGIN: LAYOUT/HEADERS/HEADER-1 -->
	<!-- BEGIN: HEADER -->
	<header class="c-layout-header c-layout-header-4 c-layout-header-default-mobile" data-minimize-offset="80">

		<div class="c-navbar">
			<div class="container-fluid">
				<!-- BEGIN: BRAND -->
				<div class="c-navbar-wrapper clearfix">
					<div class="c-brand c-pull-left">
						<a href="<?php echo base_url()?>" class="c-logo">
							<img src="<?php echo base_url()?>assets/front/assets/base/img/layout/logos/micomm.png" style="margin-top:-15px" alt="micomm" class="c-desktop-logo">
							<img src="<?php echo base_url()?>assets/front/assets/base/img/layout/logos/micomm.png" style="margin-top:-15px" alt="micomm" class="c-desktop-logo-inverse">
							<img src="<?php echo base_url()?>assets/front/assets/base/img/layout/logos/micomm.png" style="margin-top:-15px" alt="micomm" class="c-mobile-logo" width="40%">
						</a>
						<button class="c-hor-nav-toggler" type="button" data-target=".c-mega-menu">
							<span class="c-line"></span>
							<span class="c-line"></span>
							<span class="c-line"></span>
						</button>
					</div>
					<!-- BEGIN: MEGA MENU -->
					<!-- Dropdown menu toggle on mobile: c-toggler class can be applied to the link arrow or link itself depending on toggle mode -->
					<nav class="c-mega-menu c-pull-right c-mega-menu-dark c-mega-menu-dark-mobile c-fonts-uppercase c-fonts-bold">
						<ul class="nav navbar-nav c-theme-nav">
							<li class="">
								<a href="<?php echo base_url()?>" class="c-link dropdown-toggle">Home
								</a>
							</li>
							<li class="">
								<a href="<?php echo base_url()?>event" class="c-link dropdown-toggle">Event
								</a>
							</li>
							<li class="">
								<a href="<?php echo base_url()?>promo" class="c-link dropdown-toggle">Promo
								</a>
							</li>
							<li class="">
								<a href="<?php echo base_url()?>merchant" class="c-link dropdown-toggle">Merchant
								</a>
							</li>
							<li class="">
								<a href="<?php echo base_url()?>team" class="c-link dropdown-toggle">Team
								</a>
							</li>

						</ul>
					</nav>

					<!-- END: MEGA MENU -->
				</div>

			</header>
			<!-- END: HEADER -->
			<!-- BEGIN: PAGE CONTAINER -->
			<div class="c-layout-page">
				<div class="c-layout-breadcrumbs-1 c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
					<div class="container">
						<div class="c-page-title c-pull-left">
							<h3 class="c-font-uppercase c-font-sbold">Page 404</h3>
						</div>
					</div>
				</div>
				<!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->
				<!-- BEGIN: PAGE CONTENT -->
				<!-- BEGIN: BLOG LISTING -->
				<div class="c-content-box c-size-md">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="c-content-title-1">
									<h3 class="c-center c-font-uppercase c-font-bold">Halaman yang anda tuju tidak ada
									</h3>
									<div class="c-line-center c-theme-bg"></div>
								</div>

							</div>
						</div>
					</div>
					<!-- END: BLOG LISTING  -->
				</div>
				<!-- END: PAGE CONTAINER -->
				<!-- BEGIN: LAYOUT/FOOTERS/FOOTER-6 -->
				<!-- BEGIN: CONTENT/USER/FORGET-PASSWORD-FORM -->
				<div class="modal fade c-content-login-form" id="forget-password-form" role="dialog">
					<div class="modal-dialog">
						<div class="modal-content c-square">
							<div class="modal-header c-no-border">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<h3 class="c-font-24 c-font-sbold">Password Recovery</h3>
								<p>To recover your password please fill in your email address</p>
								<form>
									<div class="form-group">
										<label for="forget-email" class="hide">Email</label>
										<input type="email" class="form-control input-lg c-square" id="forget-email" placeholder="Email"> </div>
										<div class="form-group">
											<button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Submit</button>
											<a href="javascript:;" class="c-btn-forgot" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Back To Login</a>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<!-- END: CONTENT/USER/FORGET-PASSWORD-FORM -->
					<!-- BEGIN: CONTENT/USER/LOGIN-FORM -->
					<div class="modal fade c-content-login-form" id="login-form" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content c-square">
								<div class="modal-header c-no-border">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<h3 class="c-font-24 c-font-sbold">Good Night!</h3>
									<p>Let's make today a great day!</p>
									<span class="error-message c-font-red-2"></span>
									<form action="<?php echo base_url()?>auth/login_fo" id="form_login">
										<div class="form-group">
											<label for="login-email"  class="hide">Email</label>
											<input type="email" class="form-control input-lg c-square" id="login-email" name="identity" placeholder="Email"> </div>
											<div class="form-group">
												<label for="login-password" class="hide">Password</label>
												<input type="password" class="form-control input-lg c-square" id="login-password" name="password" placeholder="Password"> </div>

												<div class="form-group">
													<button type="" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Login</button>
													<a href="javascript:;" data-toggle="modal" data-target="#forget-password-form" data-dismiss="modal" class="c-btn-forgot">Forgot Your Password ?</a>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
							<!-- END: CONTENT/USER/LOGIN-FORM -->

							<a name="footer"></a>
							<footer class="c-layout-footer c-layout-footer-3 c-bg-dark ">
								<div class="c-prefooter ">
									<div class="container">
										<div class="row ">
											<div class="col-md-8">
												<div class="c-container c-first">
													<div class="c-content-title-1">
														<h3 class="c-font-uppercase c-font-bold c-font-white">MI
															<span class="c-theme-font">COMM</span>
														</h3>
														<div class="c-line-left"></div>
														<iframe  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3957.564623265982!2d112.7579463142372!3d-7.290273573678317!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7fbb48971fecb%3A0x12178e7a5c0bc3fc!2sMIC+TRANSFORMER!5e0!3m2!1sen!2sid!4v1490292681064" width="700" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
														<!-- <div id="map_micomm" style="height: 240px"></div> -->

													</div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="c-container c-last">
													<div class="c-content-title-1">
														<h3 class="c-font-uppercase c-font-bold c-font-white">Find us</h3>
														<div class="c-line-left"></div>
													</div>
													<ul class="c-socials">
														<li>
															<a href="#">
																<i class="icon-social-facebook"></i>
															</a>
														</li>
														<li>
															<a href="#">
																<i class="icon-social-twitter"></i>
															</a>
														</li>
														<li>
															<a href="#">
																<i class="icon-social-youtube"></i>
															</a>
														</li>
														<li>
															<a href="#">
																<i class="icon-social-tumblr"></i>
															</a>
														</li>
													</ul>
													<ul class="c-address">
														<li>
															<i class="icon-pointer c-theme-font"></i> Jl. Ngagel Madya No.17, Baratajaya, Surabaya</li>
															<li>
																<i class="icon-call-end c-theme-font"></i> 0821 1256 7388</li>
																<li>
																	<i class="icon-calendar c-theme-font"></i>  9:00 am – 05:00 pm</li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="c-postfooter">
												<div class="container">
													<div class="row no-space">
														<div class="col-md-6 col-sm-12 c-col">
															<p class="c-copyright c-font-grey">2017 &copy; MICOMM by <a style="color:white" href="http://mictransformer.com">MIC Transformer</a>.
																<span class="c-font-grey-3">All Rights Reserved.</span>
															</p>
														</div>
													</div>
												</div>
											</div>
										</footer>
										<!-- END: LAYOUT/FOOTERS/FOOTER-5 -->
										<!-- BEGIN: LAYOUT/FOOTERS/GO2TOP -->
										<div class="c-layout-go2top">
											<i class="icon-arrow-up"></i>
										</div>
										<!-- END: LAYOUT/FOOTERS/GO2TOP -->
										<!-- BEGIN: LAYOUT/BASE/BOTTOM -->

										<!-- js -->
										<!-- BEGIN: CORE PLUGINS -->
										<!--[if lt IE 9]>
										<script src="../assets/global/plugins/excanvas.min.js"></script>
										<![endif]-->
										<script src="<?php echo base_url()?>assets/front/assets/plugins/jquery.min.js" type="text/javascript"></script>
										<script src="<?php echo base_url()?>assets/front/assets/plugins/jquery-migrate.min.js" type="text/javascript"></script>
										<script src="<?php echo base_url()?>assets/front/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
										<script src="<?php echo base_url()?>assets/front/assets/plugins/jquery.easing.min.js" type="text/javascript"></script>
										<script src="<?php echo base_url()?>assets/front/assets/plugins/reveal-animate/wow.js" type="text/javascript"></script>
										<script src="<?php echo base_url()?>assets/front/assets/base/js/scripts/reveal-animate/reveal-animate.js" type="text/javascript"></script>
										<!-- END: CORE PLUGINS -->
										<!-- BEGIN: LAYOUT PLUGINS -->
										<script src="<?php echo base_url()?>assets/front/assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
										<script src="<?php echo base_url()?>assets/front/assets/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
										<script src="<?php echo base_url()?>assets/front/assets/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
										<script src="<?php echo base_url()?>assets/front/assets/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
										<script src="<?php echo base_url()?>assets/front/assets/plugins/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
										<script src="<?php echo base_url()?>assets/front/assets/plugins/slider-for-bootstrap/js/bootstrap-slider.js" type="text/javascript"></script>
										<!-- END: LAYOUT PLUGINS -->
										<!-- BEGIN: THEME SCRIPTS -->
										<script src="<?php echo base_url()?>assets/front/assets/base/js/components.js" type="text/javascript"></script>
										<script src="<?php echo base_url()?>assets/front/assets/base/js/components-shop.js" type="text/javascript"></script>
										<script src="<?php echo base_url()?>assets/front/assets/base/js/app.js" type="text/javascript"></script>
										<script src="<?php echo base_url()?>assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
										<script src="<?php echo base_url()?>assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
										<script>
											$(document).ready(function()
											{
												App.init(); // init core
												// initMaphome();
											});
										</script>



										<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5NVsAztA00PaPkn_A7AUdGi4pJ1X18s8&callback=initMap"></script>

										<!-- END: THEME SCRIPTS -->

										<!-- END: LAYOUT/BASE/BOTTOM -->

									</body>


									<!-- Mirrored from themehats.com/themes/jango/home-12.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 16 Feb 2016 11:35:34 GMT -->
									</html>
