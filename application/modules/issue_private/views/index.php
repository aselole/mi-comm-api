<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
    <div class="container">
        <div class="c-page-title c-pull-left">
            <h3 class="c-font-uppercase c-font-sbold">Pesan</h3>
        </div>
    </div>
</div>
<!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
<div class="container">
    <div class="c-layout-sidebar-menu c-theme ">
        <!-- BEGIN: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
        <div class="c-sidebar-menu-toggler">
            <h3 class="c-title c-font-uppercase c-font-bold">My Dashboard</h3>
            <a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
                <span class="c-line"></span>
                <span class="c-line"></span>
                <span class="c-line"></span>
            </a>
        </div>
        <ul class="c-sidebar-menu collapse " id="sidebar-menu-1">
            <li class="c-dropdown c-open">
                <img src="<?php echo get_user_img(logged_user('photo')) ?>" alt="" width="270px" height="270px">
            </li>
            <li class="c-dropdown c-open">
                <a href="javascript:;" class="c-toggler">Akun
                    <span class="c-arrow"></span>
                </a>
                <ul class="c-dropdown-menu">
                    <li class="">
                        <a href="<?php echo site_url('profile') ?>">Profil</a>
                    </li>
                    <li class="c-active">
                        <a href="<?php echo site_url('issue_private') ?>">Pesan
                            <?php if (count_unread_msg()): ?>
                                <span class="badge c-bg-blue"><?php echo count_unread_msg(); ?></span>
                            <?php endif; ?>
                        </a>
                    </li>
                    <?php if (logged_user('level_id') == 2): ?>
                    <li class="">
                        <a href="<?php echo site_url('transaksi') ?>">Transaksi
                        </a>
                    </li>
                    <?php endif; ?>
                    <?php if (FALSE): ?>
                    <li class="">
                        <a href="<?php echo site_url('issue_community') ?>">Issue</a>
                    </li>
                    <?php endif; ?>
                </ul>
            </li>
        </ul>
        <!-- END: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
    </div>
    <div id="modal_form" class="modal  modal-create" data-backdrop="static" data-keyboard="true">
    </div>
    <div class="c-layout-sidebar-content ">
        <!-- BEGIN: PAGE CONTENT -->
        <div class="c-content-title-1">
            <h3 class="c-font-uppercase c-font-bold">Pesan
                <!-- <button class="btn btn-xs btn-success btn-outline btn_create" >
                    <i class="fa fa-plus"></i> baru
                </button> -->
            </h3>
            <div class="c-line-left"></div>
        </div>
        <div class="c-shop-wishlist-1">
            <div class="row">
                <div class="col-md-10 col-sm-6 col-xs-12 c-margin-b-20">
                    <?php if ($this->session->flashdata('notif_type')): ?>
                    <div class="alert <?php echo $this->session->flashdata('notif_type') ?> alert-dismissible" role="alert">
                        <?php echo $this->session->flashdata('notif_msg'); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <?php endif; ?>
                    <?php if ($issue): ?>
                    <?php foreach ($issue as $i): ?>
                    <div class="row c-margin-b-10" >
                        <div class="c-content-product-2 c-bg-white" >
                            <div class="col-md-12" >
                                <div class="c-info-list" style="border: 0.5px solid #e7e7e7;padding:5px" >
                                    <h3 class="c-title c-font-bold c-font-22 c-font-dark">
                                    <?php $url = $i->id . '-' . strtolower(str_replace(' ', '-', $i->judul))?>
                                    <a class="c-theme-link" href="<?php echo site_url('issue_private/read/'.$url) ?>">
                                        <?php if (logged_user('level_id') == 2): ?>
                                            <?php echo $i->trainer_name ?>
                                        <?php else: ?>
                                            <?php echo $i->member_name ?>
                                        <?php endif; ?>
                                        <?php if ($i->un_read != 0): ?>
                                            <span class="badge c-bg-yellow"><?php echo $i->un_read ?></span>
                                        <?php endif; ?>
                                    </a>
                                    </h3>
                                    <p class="c-order-date c-font-14 c-font-thin c-theme-font"><?php echo date('d F Y H:i', strtotime($i->comment_update)) ?>
                                        <?php if ($i->status == 'open'): ?>
                                            <span class="label label-info"><?php echo $i->status ?></span>
                                        <?php else: ?>
                                            <span class="label label-default"><?php echo $i->status ?></span>
                                        <?php endif; ?>
                                    </p>
                                    <p class="c-desc c-font-16 c-font-thin"><b><?php echo $i->judul ?></b></p>
                                    <small>
                                        <a href="<?php echo site_url('issue_private/read/'.$url) ?>">balas</a> |
                                        <a href="" data-pesan-id="<?php echo $i->id ?>" data-judul="<?php echo $i->judul ?>"  class="btn_hapus font-kecil">hapus</a>
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                    <?php else: ?>
                        Kotak pesan kosong.
                    <?php endif; ?>
                </div>        <!-- END: PAGE CONTENT -->
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.btn_create').click(function(e) {
    e.preventDefault();
        var crsf = {<?php echo $csrf_name ?> : '<?php echo $csrf ?>'};
        $('.modal-create').load('<?php echo site_url('issue_private/create') ?>', crsf ,function() {
            $('.modal-create').modal('show');
        });
    });

    $('.btn_hapus').click(function(e) {
        e.preventDefault();
        var pesan_id = $(this).data('pesan-id');
        var judul = $(this).data('judul');

        swal(
            {
                title: "Hapus pesan ini?",
                text: judul,
                type: "warning",
                showCancelButton: true,
                cancelButtonClass: "btn-default",
                confirmButtonClass: "btn c-theme-btn c-btn-uppercase btn-md c-btn-sbold btn-block c-btn-square",
                confirmButtonText: "Hapus",
                closeOnConfirm: false
            },
            function(){
                $.post('<?php echo site_url('issue_private/hapus') ?>', {<?php echo $csrf_name ?>: '<?php echo $csrf ?>', pesan_id:pesan_id}, function(ress) {
                    if (ress.success) {
                        swal(
                            {
                                title: "Berhasil!",
                                text: ress.message,
                                type: "success",
                                confirmButtonClass: "btn-success",
                                confirmButtonText: "Success",
                                closeOnConfirm: true
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location.replace('<?php echo site_url("issue_private") ?>');
                                }
                            }
                        );

                    } else {
                        toastr.error(ress.message);
                    }
                });
            }
        );

    });




</script>
