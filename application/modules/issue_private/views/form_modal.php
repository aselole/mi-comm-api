
<div class="modal-dialog">
    <div class="modal-content c-square">
        <div class="modal-header">
            <button type="button" class="close" onclick="event.preventDefault(); closeModal();"></button>
            <h4 class="modal-title" id="exampleModalLabel">Pesan Baru</h4>
        </div>
        <div class="modal-body">
            <form action="<?php echo site_url('issue_private/store') ?>" class="form_ajax" method="post">
                <div class="form-body">
                    <?php echo form_hidden($csrf_name, $csrf); ?>

                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Ke:</label>
                        <?php $trainer = array ( '' => 'pilih' )+$trainer ?>
                        <?php echo form_dropdown('user_trainer_id', $trainer,'', "class='form-control  c-square'" ) ?>
                        <?php
                        // $ddajax = array(
                        //     'url' => site_url('form/dd_user'),
                        //     'name' =>'user_trainer_id',
                        //     'csrf' => $csrf,
                        //     'csrf_name' =>$csrf_name,
                        // );
                        // $this->load->view('global/v_dropdown_ajax', array('ddajax' => $ddajax ), FALSE); ?>

                        <div class="form-group">
                            <label for="message-text" class="control-label">Judul:</label>
                            <textarea class="form-control  c-square" id="message-text" name="judul"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Isi:</label>
                            <textarea class="form-control  c-square" id="message-text" name="body"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn c-theme-btn c-btn-square c-btn-bold c-btn-uppercase">Submit</button>
                    <button type="button" class="btn c-theme-btn c-btn-border-2x c-btn-square c-btn-bold c-btn-uppercase" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/custom/scripts/j_generalFormModal.js" type="text/javascript"></script>
