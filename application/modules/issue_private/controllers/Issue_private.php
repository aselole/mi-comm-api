<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Issue_private extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            redirect('home', 'refresh');
        }
        $this->load->model(array('private_issue_model', 'private_issue_comment_model', 'user_model', 'global_model'));
        $this->load->helper('text');
    }

    function index()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf']      = $this->security->get_csrf_hash();

        $data['title'] ='Pesan';
        $data['issue'] = $this->private_issue_model->get_data();

        $this->template->load('global/frontLayout/index', 'issue_private/index', $data);
    }

    public function read($url='')
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf']      = $this->security->get_csrf_hash();

        $urlArr   = explode('-', $url);
        $issue_id = $urlArr[0];
        unset($urlArr[0]);
        $label    = implode('-',$urlArr);

        $issue = $this->private_issue_model
                        ->with_trainer('fields:first_name')
                        ->get($issue_id);
        if (!$issue) {
            show_404();
        }elseif ( strtolower(str_replace(' ', '-', $issue->judul)) != $label  ) {
            show_404();
        }else{
            $this->private_issue_comment_model->update(array('read' => 1),array('private_issue_id' => $issue_id, 'user_id != ' => logged_user('id')));
        }

        $data['title']    ='Pesan';
        $data['issue']    = $issue;
        $data['comments'] = $this
                                ->private_issue_comment_model
                                ->fields('id, user_id, comment, read, created_at')
                                ->where(array('private_issue_id'=>$issue_id))
                                ->order_by('created_at', 'asc')
                                ->with_user('fields:first_name,photo,level_id|join:true')
                                ->get_all();

        $this->template->load('global/frontLayout/index', 'issue_private/read', $data);

    }

    public function create()
    {
        $this->load->model(array('user_model'));
        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf']      = $this->security->get_csrf_hash();

        if (logged_user('level_id') == 2) {
            $where = array('level_id' => 3);
        } else {
            $where = array('level_id' => 2);
        }

        $data['trainer'] = $this->user_model->as_dropdown('first_name')->get_all($where);
        $this->load->view('form_modal', $data);
    }

    public function store()
    {
        $config = array(
                        array(
                            'field'    => 'user_trainer_id',
                            'label'    => 'Trainer',
                            'rules'    => 'required',
                            'errors'   => array(
                                                'required' => '%s Pilih trainer tujuan.',
                                                ),
                        ),
                        array(
                            'field'    => 'judul',
                            'label'    => 'Judul',
                            'rules'    => 'required',
                            'errors'   => array(
                                                'required' => '%s tidak boleh kosong.',
                                                ),
                        ),
                        array(
                            'field'    => 'body',
                            'label'    => 'Isi',
                            'rules'    => 'required',
                            'errors'   => array(
                                                'required' => '%s tidak boleh kosong.',
                                                ),
                        ),
                    );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $result['errors'] = $this->form_validation->error_array();
            $this->output->set_status_header(400)
                        ->set_content_type('application/json')
                        ->set_output(json_encode($result));
        } else {
            $data = array(
                        'judul'           => $this->input->post('judul'),
                        'body'            => $this->input->post('body'),
                        'created_at'      => date('Y-m-d H:i:s'),
                        );
            if (logged_user('level_id') ==2) {
                $data['user_trainer_id'] = $this->input->post('user_trainer_id');
                $data['user_member_id']  = logged_user('id');
            } else {
                $data['user_trainer_id'] = logged_user('id');
                $data['user_member_id']  = $this->input->post('user_trainer_id');
            }

            if ($private_issue_id = $this->private_issue_model->insert($data)) {
                $data = array(
                            'private_issue_id' => $private_issue_id,
                            'user_id'          => logged_user('id'),
                            'comment'          => null,
                            'read'             => 0,
                            'created_at'       => date('Y-m-d H:i:s'),
                            );
                $insert = $this->private_issue_comment_model->insert($data);

                $result['success'] = true;
                $result['message'] = 'Pesan berhasil dikirim.';
                $result['url']     = site_url('issue_private');

                // firebase
                $user = $this->user_model->fields('first_name, level_id')->get(logged_user('id'));
                $user_to = $this->private_issue_model->fields('user_trainer_id')->where('user_member_id', logged_user('id'))->get()->user_trainer_id;

                $token_firebase = $this->user_model->fields('token_firebase')->get($user_to)->token_firebase;
                $title          = "Pertanyaan dari: ".$user->first_name;
                $message        = $this->input->post('body');
                $data_firebase = array(
                    'jenis' => 'private_issue',
                    'id'    => "$private_issue_id",
                );
                $this->global_model->push_notif_firebase($token_firebase, $title, $message, $data_firebase);
            } else {
                $result['success'] = false;
                $result['message'] = 'Terjadi kesalahan.';
            }
            $this->output->set_content_type('application/json') ->set_output(json_encode($result));
        }
    }

    public function store_comment()
    {
        $config = array(
                        array(
                            'field'    => 'comment',
                            'label'    => 'Komentar',
                            'rules'    => 'required',
                            'errors'   => array(
                                                'required' => '%s tidak boleh kosong.',
                                                ),
                        ),
                    );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $result['errors'] = $this->form_validation->error_array();
            $this->output->set_status_header(400)
                        ->set_content_type('application/json')
                        ->set_output(json_encode($result));
        } else {
            $private_issue_id = $this->input->post('private_issue_id');
            $data = array(
                        'private_issue_id' => $private_issue_id,
                        'user_id'          => logged_user('id'),
                        'comment'          => $this->input->post('comment'),
                        'read'             => 0,
                        'created_at'       => date('Y-m-d H:i:s'),
                        );
            $insert = $this->private_issue_comment_model->insert($data);
            $iss    = $this->private_issue_model->get($private_issue_id);
            $url    = $iss->id.'-'.strtolower(str_replace(' ', '-', $iss->judul));
            if ($insert) {
                $this->session->set_flashdata('notif_type', 'alert-info');
                $this->session->set_flashdata('notif_msg', 'Komentar berhasil ditambahkan.');

                $result['success'] = true;
                $result['message'] = 'Komentar berhasil ditambahkan.';
                $result['url']     = site_url('issue_private/read/'.$url);

                // firebase
                $member_jenis = 'micomm';
                $user = $this->user_model->fields('first_name, level_id')->get(logged_user('id'));
                if ($user->level_id == 2) {
                    // member
                    $user_to = $this->private_issue_model->fields('user_trainer_id')->where(['id'=>$private_issue_id, 'user_member_id'=>logged_user('id')])->get()->user_trainer_id;
                } elseif ($user->level_id == 3) {
                    // trainer
                    $user_to = $this->private_issue_model->fields('user_member_id')->where(['id'=>$private_issue_id, 'user_trainer_id'=>logged_user('id')])->get()->user_member_id;
                    
                    $member_jenis = $this->user_model->fields('member_jenis')->where(['id'=>$user_to])->get()->member_jenis;
                }
                $token_firebase = $this->user_model->fields('token_firebase')->get($user_to)->token_firebase;
                $title          = "Balasan dari: ".$user->first_name;
                $message        = $this->input->post('comment');
                $data_firebase = array(
                    'jenis' => 'private_issue',
                    'id'    => "$private_issue_id",
                );
                $this->global_model->push_notif_firebase($token_firebase, $title, $message, $data_firebase, $member_jenis);
            } else {
                $result['success'] = false;
                $result['message'] = 'Terjadi kesalahan.';
            }
            $this->output->set_content_type('application/json') ->set_output(json_encode($result));
        }
    }

    public function store_comment_fire()
    {
        $config = array(
                        array(
                            'field'    => 'comment',
                            'label'    => 'Komentar',
                            'rules'    => 'required',
                            'errors'   => array(
                                                'required' => '%s tidak boleh kosong.',
                                                ),
                        ),
                    );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $result['errors'] = $this->form_validation->error_array();
            $this->output->set_status_header(400)
                        ->set_content_type('application/json')
                        ->set_output(json_encode($result));
        } else {
            $private_issue_id = $this->input->post('private_issue_id');
            $user = $this->user_model->fields('id, first_name, photo, level_id')->get(logged_user('id'));
            $obj = array(
                        'private_issue_id' => $private_issue_id,
                        'comment'          => $this->input->post('comment'),
                        'read'             => 0,
                        'created_at'       => date('Y-m-d H:i:s'),
                        'user_id'          => $user->id,
                        'user_first_name'  => $user->first_name,
                        'user_photo'       => $user->photo,
                        'user_level_id'    => $user->level_id,
                        );
            // $store_to_firebase = $this->store_comment_to_firebase($data);
            $iss    = $this->private_issue_model->get($private_issue_id);
            $url    = $iss->id.'-'.strtolower(str_replace(' ', '-', $iss->judul));
            $result['success'] = true;
            $result['obj'] = $obj;
            $result['url'] = $url;

            $this->output->set_content_type('application/json') ->set_output(json_encode($result));
        }
    }

    public function store_comment_to_firebase($obj)
    {
        $data['obj'] = json_encode($obj);
        echo $this->load->view('store_comment_to_firebase.js', $data);
    }

    public function hapus()
    {
        $res['success'] = false;
        $res['message'] = 'Terjadi kesalahan!';
        $issue_private_id = $this->input->post('pesan_id');

        if ($this->private_issue_model->delete($issue_private_id)) {
            $this->private_issue_comment_model->delete( array('private_issue_id' => $issue_private_id));
            $res['success'] =true;
            $res['message'] = 'Pesan berhasil dihapus';
        }

        $this->output->set_content_type('application/json') ->set_output(json_encode($res));

    }
}
