<div class="c-layout-breadcrumbs-1 c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
    <div class="container">
        <div class="c-page-title c-pull-left">
            <h3 class="c-font-uppercase c-font-sbold">Berita</h3>
        </div>
        <ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
            <li>
                <a href="<?php echo site_url('') ?>">Home</a>
            </li>
            <li>/</li>
            <li>
                <a href="<?php echo site_url('event') ?>">Berita</a>
            </li>
            <li>/</li>
            <li class="c-state_active"><?php echo $title ?></li>
        </ul>
    </div>
</div>
<!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->
<!-- BEGIN: PAGE CONTENT -->
<!-- BEGIN: BLOG LISTING -->
<div class="c-content-box c-size-md">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="c-content-blog-post-1-view">
                    <div class="c-content-blog-post-1">
                        <?php if ($image_news): ?>
                            <div class="c-media">
                                <div class="c-content-media-2-slider" data-slider="owl" data-single-item="true" data-auto-play="4000">
                                    <div class="owl-carousel owl-theme c-theme owl-single" >
                                        <?php foreach ($image_news as $key => $value): ?>
                                            <div class="item" style="width:100%;height:100%">
                                                <a href="<?php echo get_news_img($value->image) ?>" data-lightbox="fancybox" data-fancybox-group="gallery">
                                                    <div class="c-content-media-2" style="background-image: url(<?php echo get_news_img($value->image) ?>); min-height:600px;"> </div>
                                                </a>
                                            </div>
                                        <?php endforeach ?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="c-title c-font-bold c-font-uppercase">
                            <a href="#"><?php echo $event->judul ?></a>
                        </div>
                        <div class="c-panel c-margin-b-30">
                            <div class="c-date">on
                                <span class="c-font-uppercase"><?php echo date('d-m-Y', strtotime($event->published_at)) ?></span>
                            </div>
                            <ul class="c-tags c-theme-ul-bg hide">
                                <li>ux</li>
                                <li>marketing</li>
                                <li>events</li>
                            </ul>
                            <div class="c-comments hide">
                                <a href="#">
                                    <i class="icon-speech"></i> 30 comments</a>
                            </div>
                        </div>
                        <div class="c-desc">
                            <?php echo $event->body ?>
                        </div>
                        <div class="c-comments hide">
                            <div class="c-content-title-1">
                                <h3 class="c-font-uppercase c-font-bold">Comments(30)</h3>
                                <div class="c-line-left"></div>
                            </div>
                            <div class="c-comment-list">
                                <div class="media">
                                    <div class="media-left">
                                        <a href="#">
                                            <img class="media-object" alt="" src="assets/base/img/content/team/team1.jpg"> </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">
                                            <a href="#" class="c-font-bold">Sean</a> on
                                            <span class="c-date">23 May 2015, 10:40AM</span>
                                        </h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <a href="#">
                                            <img class="media-object" alt="" src="assets/base/img/content/team/team3.jpg"> </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">
                                            <a href="#" class="c-font-bold">Strong Strong</a> on
                                            <span class="c-date">21 May 2015, 11:40AM</span>
                                        </h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="#">
                                                    <img class="media-object" alt="" src="assets/base/img/content/team/team4.jpg"> </a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">
                                                    <a href="#" class="c-font-bold">Emma Stone</a> on
                                                    <span class="c-date">30 May 2015, 9:40PM</span>
                                                </h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <a href="#">
                                            <img class="media-object" alt="" src="assets/base/img/content/team/team7.jpg"> </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">
                                            <a href="#" class="c-font-bold">Nick Nilson</a> on
                                            <span class="c-date">30 May 2015, 9:40PM</span>
                                        </h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. </div>
                                </div>
                            </div>
                            <div class="c-content-title-1">
                                <h3 class="c-font-uppercase c-font-bold">Leave A Comment</h3>
                                <div class="c-line-left"></div>
                            </div>
                            <form action="#">
                                <div class="form-group">
                                    <input type="text" placeholder="Your Name" class="form-control c-square"> </div>
                                <div class="form-group">
                                    <input type="text" placeholder="Your Email" class="form-control c-square"> </div>
                                <div class="form-group">
                                    <input type="text" placeholder="Your Website" class="form-control c-square"> </div>
                                <div class="form-group">
                                    <textarea rows="8" name="message" placeholder="Write comment here ..." class="form-control c-square"></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn c-theme-btn c-btn-uppercase btn-md c-btn-sbold btn-block c-btn-square">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <!-- BEGIN: CONTENT/BLOG/BLOG-SIDEBAR-1 -->
                <div class="c-content-tab-1 c-theme c-margin-t-30">
                    <div class="nav-justified">
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active">
                                <a href="#blog_recent_posts" data-toggle="tab">Berita Terbaru</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="blog_recent_posts">
                                <ul class="c-content-recent-posts-1">
                                    <?php foreach ($events->result() as $ev): ?>
                                        <li>
                                            <div class="c-post">
                                            <?php $event_url = $ev->id.'-'.strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $ev->judul))) ?>
                                                <a href="<?php echo site_url('event/read/'.$event_url) ?>" class="c-title"> <?php echo $ev->judul ?> </a>
                                                <div class="c-date"><?php echo date('d-m-Y' , strtotime($ev->published_at)) ?></div>
                                            </div>
                                        </li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
