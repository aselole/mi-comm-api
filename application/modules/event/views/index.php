<div class="c-layout-breadcrumbs-1 c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
    <div class="container">
        <div class="c-page-title c-pull-left">
            <h3 class="c-font-uppercase c-font-sbold">Berita</h3>
        </div>
    </div>
</div>
<!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->
<!-- BEGIN: PAGE CONTENT -->
<!-- BEGIN: BLOG LISTING -->
<div class="c-content-box c-size-md">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="c-content-blog-post-card-1-grid">
                    <?php if ($events): ?>
                        <div class="row isi">
                            <?php
                            $n_kolom = 3;
                            $col_md = 'col-md-4';
                            ?>
                            <?php
                            for ($i=1; $i <= $n_kolom; $i++) {
                                $hasil_mod = $i != $n_kolom ? $i : 0;
                                $no = 0;
                                ?>
                                <div class="<?php echo $col_md ?>">
                                    <?php foreach ($events->result() as $e): ?>
                                        <?php $no++ ?>
                                        <?php if ($no % $n_kolom == $hasil_mod): ?>
                                            <?php
                                            $url_p = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $e->judul)));
                                             ?>

                                            <?php $event_url = $e->id . '-' . $url_p ?>
                                            <div class="c-content-blog-post-card-1 c-option-2 c-bordered">
                                                <?php if ($e->image): ?>
                                                    <div class="c-media c-content-overlay">
                                                        <a href="<?php echo site_url('event/read/'.$event_url)?>">
                                                            <img class="c-overlay-object img-responsive img-flat" src="<?php echo get_news_img($e->image) ?>" alt="">
                                                        </a>
                                                    </div>
                                                <?php endif; ?>
                                                <div class="c-body">
                                                    <div class="c-title c-font-bold c-font-uppercase">
                                                        <a href="<?php echo site_url('event/read/'.$event_url)?>"><?php echo $e->judul ?></a>
                                                    </div>
                                                    <div class="c-author">on
                                                        <span class="c-font-uppercase"><?php echo date('d/m/Y', strtotime($e->published_at)) ?></span>
                                                    </div>
                                                    <div class="c-panel">
                                                        <div class="c-comments">
                                                            <!-- <i class="icon-speech"></i> 30 Komentar -->
                                                        </div>
                                                    </div>
                                                    <p>
                                                        <?php echo ellipsize($e->body, 100) ?>
                                                    </p>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="c-pagination">
                            <?php echo $this->pagination->create_links(); ?>
                        </div>
                    <?php else: ?>
                        belum ada event saat ini
                    <?php endif ?>

                    <?php // if (count($events) >= 6): ?>

                    <!-- <div id="loadMore-container" class="cbp-l-loadMore-button c-margin-t-60">
                        <a class="cbp-l-loadMore-link btn c-btn-square c-btn-border-2x c-btn-dark c-btn-bold c-btn-uppercase btn-load">
                            <span class="cbp-l-loadMore-defaultText">LOAD MORE</span>
                            <span class="cbp-l-loadMore-loadingText">LOADING...</span>
                            <span class="cbp-l-loadMore-noMoreLoading">NO MORE</span>
                        </a>
                    </div> -->
                    <?php // endif ?>

                </div>
            </div>
                <!-- END: CONTENT/BLOG/BLOG-SIDEBAR-1 -->
            </div>
        </div>
    </div>
</div>
<!-- END: BLOG LISTING  -->
<script type="text/javascript">

var siteUrl = '<?php site_url() ?>';
var baseUrl = '<?php base_url() ?>';
var start = 0;

$('.btn-load').click(function(event) {
    start=start+1;
    console.log(start);
    $.ajax({
        url: siteUrl + 'event/ajax_list',
        type: 'post',
        dataType: 'json',
        data: {start:start, <?php echo $csrf_name ?>:'<?php echo $csrf ?>'}
    })
    .done(function(res) {
        $('.cbp-l-loadMore-defaultText').addClass('hide');
        $('.cbp-l-loadMore-loadingText').addClass('show');
    })
    .done(function(res) {
        $.each(res.data, function(index, v) {
            var url = v.judul
                    .toLowerCase()
                    .replace(/ /g,'-')
                    .replace(/[^\w-]+/g,'');
            var url = v.id+'-'+url;
            $('.isi').append(
                '<div class="col-md-4">'+
                    '<div class="c-content-blog-post-card-1 c-option-2 c-bordered">'+
                        '<div class="c-media c-content-overlay">'+
                            '<a href="'+siteUrl+'event/read/'+url+'">'+
                            '<img class="c-overlay-object img-responsive img-flat" src="'+baseUrl+'uploads/news/'+v.image_news[0].image+'" alt="event"></a>'+
                        '</div>'+
                        '<div class="c-body">'+
                            '<div class="c-title c-font-bold c-font-uppercase">'+
                                '<a href="'+siteUrl+'event/read/'+url+'">'+v.judul+'</a>'+
                            '</div>'+
                            '<div class="c-author"> on '+
                                '<span class="c-font-uppercase"> '+v.published_at+'</span>'+
                            '</div>'+
                            '<div class="c-panel">'+
                                '<div class="c-comments">'+
                                        // '<i class="icon-speech"></i> 30 Komentar'+
                                '</div>'+
                            '</div>'+
                            '<p>'+ v.body.slice(1, 100) +'</p>'+
                        '</div>'+
                    '</div>'+
                '</div>'
            )
        });
    })
    .fail(function() {
        console.log("error");
    })
    .always(function(res) {
        $('.cbp-l-loadMore-loadingText').removeClass('show');
        $('.cbp-l-loadMore-defaultText').addClass('show');
        if (res.finish == true) {
            $('.cbp-l-loadMore-defaultText').removeClass('show');
            $('.cbp-l-loadMore-noMoreLoading').addClass('show');
            console.log("complete");
        }
    });
});

</script>
