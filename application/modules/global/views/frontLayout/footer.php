<div class="modal fade c-content-login-form" id="forget-password-form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header c-no-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3 class="c-font-24 c-font-sbold">Password Recovery</h3>
                <p>To recover your password please fill in your email address</p>
                <form>
                    <div class="form-group">
                        <label for="forget-email" class="hide">Email</label>
                        <input type="email" class="form-control input-lg c-square" id="forget-email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Submit</button>
                        <a href="javascript:;" class="c-btn-forgot" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Back To Login</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END: CONTENT/USER/FORGET-PASSWORD-FORM -->
<!-- BEGIN: CONTENT/USER/LOGIN-FORM -->
<div class="modal fade c-content-login-form" id="login-form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header c-no-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php
                $jam = date('H:i');
                if ($jam >= '4:00' && $jam <= '10:00') {
                    $selamat = 'Morning';
                } else if ($jam >= '10:00' && $jam <= '14:00') {
                    $selamat = 'Afternoon';
                } else if ($jam >= '14:00' && $jam <= '18:30') {
                    $selamat = 'Evening';
                } else if ($jam >= '18:30' && $jam <= '23:59') {
                    $selamat = 'Night';
                }else{
                    $selamat = 'Night';
                }

                ?>
                <h3 class="c-font-24 c-font-sbold">Good <?php echo @$selamat ?>!</h3>
                <p>Let's make today a great day!</p>
                <span class="error-message c-font-red-2"></span>
                <form action="<?php echo site_url('auth/login_fo') ?>" id="form_login">
                    <?php echo form_hidden('current_url', current_url()); ?>
                    <div class="form-group">
                        <label for="login-email"  class="hide">Email or username</label>
                        <input type="email" class="form-control input-lg c-square" id="login-email" name="identity" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="login-password" class="hide">Password</label>
                        <input type="password" class="form-control input-lg c-square" id="login-password" name="password" placeholder="Password">
                    </div>

                    <div class="form-group">
                        <button type="" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Login</button>
                        <a href="javascript:;" data-toggle="modal" data-target="#forget-password-form" data-dismiss="modal" class="c-btn-forgot">Forgot Your Password ?</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END: CONTENT/USER/LOGIN-FORM -->


<a name="footer"></a>
<footer class="c-layout-footer c-layout-footer-3 c-bg-dark ">
    <div class="c-prefooter ">
        <div class="container">
            <div class="row ">
                <div class="col-md-3">
                    <div class="c-container c-first">
                        <div class="c-content-title-1">
                            <h3 class="c-font-uppercase c-font-bold c-font-white">About
                                <!-- <span class="c-theme-font">Us</span> -->
                            </h3>
                            <div class="c-line-left"></div>
                            <p class="c-text" style="color:white"><b>Mi-Comm</b> adalah komunitas yang terbentuk dengan visi memberikan kontribusi yang nyata dalam membentuk perubahan sosial ke arah yang lebih baik. Dengan memberikan informasi, motivasi, solusi, fasilitas, dan juga manfaat lainnya. Kami yakin bahwa Mi-Comm akan mengembangkan kreatifitas, menjadikan benfit serta profit dalam kehidupan saat ini dan masa yang akan datang.</p>
                            <?php
                            // foreach ($data_setting->result() as $r) {
                            //     if ($r->kode=='about') {
                            //         echo '<p class="c-text">'.$r->isi.'</p>';
                            //     }
                            // }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="c-container c-first">
                        <div class="c-content-title-1">
                            <h3 class="c-font-uppercase c-font-bold c-font-white">Berita </h3>
                            <div class="c-line-left"></div>
                        </div>
                        <div class="c-blog">
                            <?php
                            $this->load->model('news_model');
                            $fberita = $this->news_model->where(['active'=>'y', 'for_whom'=>'micomm'])->limit(5)->order_by('published_at', 'desc')->get_all();
                            ?>
                            <?php foreach ($fberita as $fb): ?>
                                <div class="c-post">
                                    <?php
                                    $url_p = get_url($fb->id, $fb->judul);
                                    $fberita_url = 'event/read/' . $url_p ?>
                                    <a href="<?php echo site_url($fberita_url) ?>" class="c-font-regular">
                                        <p class="c-text"><?php echo ellipsize($fb->judul, 50) ?></p>
                                    </a>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="c-container c-first">
                        <div class="c-content-title-1">
                            <h3 class="c-font-uppercase c-font-bold c-font-white">Promo </h3>
                            <div class="c-line-left"></div>
                        </div>
                        <div class="c-blog">
                            <?php
                            $this->load->model('promo_model');
                            $date_now         = date('Y-m-d');
                            $batas_date_valid = date('Y-m-d', strtotime($date_now. ' - 7 days'));
                            $where = array('active'=> 'y', 'valid_until >= "'.$batas_date_valid.'"' => null, 'is_global' => 'y');
                            $fpromo = $this->promo_model->order_by('published_at', 'desc')->limit(5,0)->get_all($where);
                            ?>
                            <?php if ($fpromo): ?>
                                <?php foreach ($fpromo as $fp): ?>
                                    <div class="c-post">
                                        <a href="javascript:read_promo(<?php echo $fp->id; ?>)" class="c-font-regular">
                                            <p class="c-text"><?php echo ellipsize($fp->judul, 50) ?></p>
                                        </a>
                                    </div>
                                <?php endforeach ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="c-container c-last">
                        <div class="c-content-title-1">
                            <h3 class="c-font-uppercase c-font-bold c-font-white">Find
                                <span class="c-theme-font">US</span>
                            </h3>
                            <div class="c-line-left"></div>
                        </div>
                        <ul class="c-socials">
                            <li>
                                <a href="#">
                                    <i class="icon-social-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="icon-social-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                        <ul class="c-address" style="color:#fff">
                            <li>
                                <i class="icon-pointer c-theme-font"></i> Jl. Ngagel Madya No.17, Baratajaya, Surabaya
                            </li>
                            <li>
                                <i class="icon-call-end c-theme-font"></i> 0821 1256 7388
                            </li>
                            <li>
                                <i class="icon-calendar c-theme-font"></i>  9:00 am – 05:00 pm
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="c-postfooter">
        <div class="container">
            <div class="row no-space">
                <div class="col-md-6 col-sm-12 c-col">
                    <p class="c-copyright c-font-grey"><?php echo date('Y') ?> &copy; MICOMM by <a style="color:white" href="http://mictransformer.com">MIC Transformer</a>.
                        <span class="c-font-grey-3">All Rights Reserved.</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- END: LAYOUT/FOOTERS/FOOTER-5 -->
<!-- BEGIN: LAYOUT/FOOTERS/GO2TOP -->
<div class="c-layout-go2top">
    <i class="icon-arrow-up"></i>
</div>

<div id="modal_promo"> </div>
<div id="modal_merchant"></div>
<div id="modal_partner"></div>
<div id="modal_team"></div>

<!-- END: LAYOUT/FOOTERS/GO2TOP -->
<!-- BEGIN: LAYOUT/BASE/BOTTOM -->

<!-- js -->
<!-- BEGIN: CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url()?>assets/front/assets/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/front/assets/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/front/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/front/assets/plugins/jquery.easing.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/front/assets/plugins/reveal-animate/wow.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/front/assets/base/js/scripts/reveal-animate/reveal-animate.js" type="text/javascript"></script>
<!-- END: CORE PLUGINS -->
<!-- BEGIN: LAYOUT PLUGINS -->
<script src="<?php echo base_url()?>assets/front/assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/front/assets/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/front/assets/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/front/assets/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/front/assets/plugins/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/front/assets/plugins/slider-for-bootstrap/js/bootstrap-slider.js" type="text/javascript"></script>
<!-- END: LAYOUT PLUGINS -->
<!-- BEGIN: THEME SCRIPTS -->
<script src="<?php echo base_url()?>assets/front/assets/base/js/components.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/front/assets/base/js/components-shop.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/front/assets/base/js/app.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>


<script>
    $(document).ready(function()
    {
        App.init(); // init core
        // initMaphome();
    });

    var siteUrl = '<?php site_url()?>';

    function initMaphome() {

        var myLatLng = new google.maps.LatLng(-7.2902736, 112.7579463); //{lat: -7.2902736, lng: 112.7579463};

        var map = new google.maps.Map(document.getElementById('map_micomm'), {
            zoom: 13,
            center: myLatLng
        });

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'MIC TRANSFORMER'
        });

    }

    function read_promo(id) {
        var crsf = {<?php echo $csrf_name ?> : '<?php echo $csrf ?>'};
        $('#modal_promo').load(siteUrl + 'promo/read/' + id +'/t', crsf,function() {
        });
    }

    function read_merchant(merchant_id) {
        var crsf = {<?php echo $csrf_name ?> : '<?php echo $csrf ?>'};
        $('#modal_merchant').load(siteUrl + 'merchant/read/' + merchant_id, crsf,function() { });
    }

    function read_partner(partner_id) {
        var crsf = {<?php echo $csrf_name ?> : '<?php echo $csrf ?>'};
        $('#modal_partner').load(siteUrl + 'partner/read/' + partner_id, crsf,function() { });
    }

    function read_team(user_id) {
        var crsf = {<?php echo $csrf_name ?> : '<?php echo $csrf ?>'};
        $('#modal_team').load(siteUrl + 'team/read/' + user_id,crsf,function() { });
    }


    $('.c-btn-login').click(function(event) {
        console.log('masuk');
        event.preventDefault();
        var form = $('#form_login');
        var formUrl = form.attr('action');
        var formData = form.serialize()+'&<?php echo $csrf_name ?>=<?php echo $csrf ?>';
        $.ajax({
            url: formUrl,
            type: 'POST',
            dataType: 'json',
            data: formData,
            beforeSend: function (xhr) {
            },
            success: function (data) {
                console.log(data.url);
                if ( data.success )
                {
                    $('.error-message').html(data.message);
                    window.location.href = data.url;
                }
                else
                {
                    $('.error-message').html(data.message);
                }

            },
            error: function (e) {
            },
            complete: function (xhr) {
            }
        });

    });
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5NVsAztA00PaPkn_A7AUdGi4pJ1X18s8&callback=initMap"></script>

<!-- END: THEME SCRIPTS -->

<!-- END: LAYOUT/BASE/BOTTOM -->
<style >
    .img-c{
        width: 344.66px;
        height: 344.66px;
    }

    .img-flat{
        width: 344.66px;
        height: 255px;
    }

    .img-square{
        width: 344.66px;
        height: 344.66px;
    }

    .img-def{
        width: 400px;
        height: 300px;
    }
    .box {
        width:200px;height:300px;
        position:relative;
        border:1px solid #BBB;
        background:#eee;
        float:left;
        margin:20px
    }
    .ribbon {
        position: absolute;
        right: 0px; top: -5px;
        z-index: 1;
        overflow: hidden;
        width: 90px; height: 75px;
        text-align: right;
    }
    .ribbon span {
        font-size: 15px;
        color: #fff;
        text-transform: uppercase;
        text-align: left;
        padding-left: 5px;
        font-weight: bold; line-height: 30px;
        /*transform: rotate(45deg);*/
        width: 90px; display: block;
        background: #79A70A;
        /*background: linear-gradient(#9BC90D 0%, #79A70A 100%);*/
        box-shadow: 0 3px 10px -5px rgba(0, 0, 0, 1);
        position: absolute;
        top: 19px; right: -21px;
    }
    .ribbon span::before {
        content: '';
        position: absolute;
        left: 0px; top: 100%;
        z-index: -1;
        border-left: 3px solid #79A70A;
        border-right: 3px solid transparent;
        border-bottom: 3px solid transparent;
        border-top: 3px solid #79A70A;
    }
    .ribbon span::after {
        content: '';
        position: absolute;
        right: 0%; top: 100%;
        z-index: -1;
        border-right: 3px solid #79A70A;
        border-left: 3px solid transparent;
        border-bottom: 3px solid transparent;
        border-top: 3px solid #79A70A;
    }
    .red span {background: #F70505;}
    .red span::before {border-left-color: #8F0808; border-top-color: #8F0808;}
    .red span::after {border-right-color: #8F0808; border-top-color: #8F0808;}

    .blue span {background: #2989d8;}
    .blue span::before {border-left-color: #1e5799; border-top-color: #1e5799;}
    .blue span::after {border-right-color: #1e5799; border-top-color: #1e5799;}



</style>
