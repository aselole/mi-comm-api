<div class="c-navbar">
    <div class="container-fluid">
        <!-- BEGIN: BRAND -->
        <div class="c-navbar-wrapper clearfix">
            <div class="c-brand c-pull-left">
                <a href="<?php echo site_url()?>" class="c-logo">
                    <img src="<?php echo base_url()?>assets/front/assets/base/img/layout/logos/micomm.png" style="margin-top:-15px" alt="micomm" class="c-desktop-logo">
                    <img src="<?php echo base_url()?>assets/front/assets/base/img/layout/logos/micomm.png" style="margin-top:-15px" alt="micomm" class="c-desktop-logo-inverse">
                    <img src="<?php echo base_url()?>assets/front/assets/base/img/layout/logos/micomm.png" style="margin-top:-15px" alt="micomm" class="c-mobile-logo" width="40%">
                </a>
                <button class="c-hor-nav-toggler" type="button" data-target=".c-mega-menu">
                    <span class="c-line"></span>
                    <span class="c-line"></span>
                    <span class="c-line"></span>
                </button>
                <?php if ( logged_user() && logged_user('level_id') != 1): ?>
                    <button class="c-cart-toggler btn btn-pesan">
                        <i class="icon-envelope-open"></i>
                        <?php if (count_unread_msg()): ?>
                            <span class="c-cart-number c-theme-bg"><?php echo count_unread_msg(); ?></span>
                        <?php endif; ?>
                    </button>
                <?php endif; ?>
                <?php if ( logged_user() && logged_user('level_id') == 2): ?>
                    <button class="c-cart-toggler btn btn-histori">
                        <i class="icon-basket"></i>
                    </button>
                <?php endif; ?>
            </div>
            <!-- BEGIN: MEGA MENU -->
            <!-- Dropdown menu toggle on mobile: c-toggler class can be applied to the link arrow or link itself depending on toggle mode -->
            <nav class="c-mega-menu c-pull-right c-mega-menu-light c-mega-menu-dark-mobile c-fonts-uppercase c-fonts-bold">
                <?php $uri_1 = $this->uri->segment(1) ?>
                <ul class="nav navbar-nav c-theme-nav">
                    <li class="<?php if ($uri_1 == 'home' || $uri_1 == '') echo 'c-active' ?>">
                        <a href="<?php echo site_url()?>" class="c-link dropdown-toggle">Home
                        </a>
                    </li>
                    <li class="<?php if ($uri_1 == 'news') echo 'c-active' ?>">
                        <a href="<?php echo site_url('news')?>" class="c-link dropdown-toggle">Berita
                        </a>
                    </li>
                    <li class="<?php if ($uri_1 == 'promo') echo 'c-active' ?>">
                        <a href="<?php echo site_url('promo')?>" class="c-link dropdown-toggle">Promo
                        </a>
                    </li>
                    <li class="<?php if ($uri_1 == 'merchant') echo 'c-active' ?>">
                        <a href="<?php echo site_url('merchant')?>" class="c-link dropdown-toggle">Merchant
                        </a>
                    </li>
                    <?php if ( logged_user() ): ?>
                    <li class="<?php if ($uri_1 == 'issue') echo 'c-active' ?>">
                        <a href="<?php echo site_url('issue')?>" class="c-link dropdown-toggle c-toggler">Issue
                        </a>
                    </li>
                    <?php endif ?>
                    <li class="<?php if ($uri_1 == 'team') echo 'c-active' ?>">
                        <a href="<?php echo site_url('team')?>" class="c-link dropdown-toggle">Team
                        </a>
                    </li>

                    <?php if ( logged_user() ): ?>
                        <?php if ( logged_user('level_id') == 2): ?>
                            <li class="c-cart-toggler-wrapper">
                                <a href="<?php echo site_url('transaksi') ?>" class="c-btn-icon c-cart-toggler" title="transaksi">
                                    <i class="icon-basket"></i>
                                </a>
                            </li>
                        <?php endif; ?>

                        <?php if ( logged_user('level_id') != 1): ?>
                        <li class="c-cart-toggler-wrapper">
                            <a href="<?php echo site_url('issue_private') ?>" class="c-btn-icon c-cart-toggler" title="pesan">
                                <i class="icon-envelope-open c-cart-icon"></i>
                                <?php if (count_unread_msg()): ?>
                                    <span class="c-cart-number c-theme-bg"><?php echo count_unread_msg(); ?></span>
                                <?php endif; ?>
                            </a>
                        </li>
                        <?php endif; ?>

                        <li class="c-menu-type-classic">
                            <a href="javascript:;" class="c-toggler c-btn-border-opacity-04 c-btn btn-no-focus c-btn-header btn btn-sm c-btn-border-1x c-btn-dark c-btn-circle c-btn-uppercase c-btn-sbold"><i class="icon-user"></i> <?php echo logged_user('first_name') ?>
                                <span class="c-arrow c-toggler"></span>
                            </a>
                            <ul class="dropdown-menu c-menu-type-classic c-pull-left">
                                <?php if (logged_user('level_id') != 1): ?>
                                    <li class="">
                                        <a href="<?php echo site_url('profile') ?>">Profil
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <?php if (logged_user('level_id') == 1): ?>
                                    <li class="">
                                        <a href="<?php echo site_url('admin/dashboard') ?>">Back Office
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <li class="">
                                    <a href="<?php echo site_url('auth/logout/TRUE') ?>">Logout
                                    </a>
                                </li>
                            </ul>
                        </li>
                    <?php else: ?>
                        <li >
                            <a href="" data-toggle="modal" data-target="#login-form" class="c-btn-border-opacity-04 c-btn btn-no-focus c-btn-header btn btn-sm c-btn-border-1x c-btn-dark c-btn-circle c-btn-uppercase c-btn-sbold">
                                <i class="icon-user"></i> Log In</a>
                        </li>
                    <?php endif ?>
                </ul>
            </nav>

            <!-- END: MEGA MENU -->
        </div>
<script type="text/javascript">
    $('.btn-pesan').click(function(event) {
        window.location.href="<?php echo site_url('issue_private') ?>";
    });
    $('.btn-histori').click(function(event) {
        window.location.href="<?php echo site_url('transaksi') ?>";
    });
</script>
