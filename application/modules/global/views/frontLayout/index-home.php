<!DOCTYPE html>
<!--
Theme: JANGO - Ultimate Multipurpose HTML Theme Built With Twitter Bootstrap 3.3.5
Version: 1.3.5
Author: Themehats
Site: http://www.themehats.com
Purchase: http://themeforest.net/item/jango-responsive-multipurpose-html5-template/11987314?ref=themehats
Contact: support@themehats.com
Follow: http://www.twitter.com/themehats
-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->


<!-- Mirrored from themehats.com/themes/jango/home-12.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 16 Feb 2016 11:34:49 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
        <!-- END THEME STYLES -->
        <?php $this->load->view('head'); ?>
        <link rel="shortcut icon" href="favicon.ico" />
    </head>

    <?php $uri_1 = $this->uri->segment(1) ?>

    <!-- c-layout-header-fullscreen -->
    <body class="c-layout-header-fixed c-layout-header-mobile-fixed ">
        <!-- BEGIN: LAYOUT/HEADERS/HEADER-1 -->
        <!-- BEGIN: HEADER -->
        <header class="c-layout-header c-layout-header-4 c-bordered c-layout-header-default-mobile c-header-transparent-dark" data-minimize-offset="80">

            <?php $this->load->view('menu'); ?>

        </header>
        <!-- END: HEADER -->
        <!-- BEGIN: PAGE CONTAINER -->
        <div class="c-layout-page">
            <?php echo $content_layout ?>
        </div>
        <!-- END: PAGE CONTAINER -->
        <!-- BEGIN: LAYOUT/FOOTERS/FOOTER-6 -->
        <?php
        $sql = "select * from master_settings";
        $data['data_setting'] = $this->db->query($sql);
        ?>
        <?php $this->load->view('footer', $data); ?>
    </body>

        <script src="<?php echo base_url()?>assets/front/assets/plugins/revo-slider/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>assets/front/assets/plugins/revo-slider/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>assets/front/assets/plugins/revo-slider/js/extensions/revolution.extension.slideanims.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>assets/front/assets/plugins/revo-slider/js/extensions/revolution.extension.layeranimation.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>assets/front/assets/plugins/revo-slider/js/extensions/revolution.extension.navigation.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>assets/front/assets/plugins/revo-slider/js/extensions/revolution.extension.video.min.js" type="text/javascript"></script>

        <!-- END: THEME SCRIPTS -->
        <!-- BEGIN: PAGE SCRIPTS -->
        <script>
            $(document).ready(function()
            {
                var slider = $('.c-layout-revo-slider .tp-banner');
                var cont = $('.c-layout-revo-slider .tp-banner-container');
                var api = slider.show().revolution(
                {
                    sliderType: "standard",
                    sliderLayout: "fullscreen",
                    responsiveLevels: [2048, 1024, 778, 320],
                    gridwidth: [1240, 1024, 778, 320],
                    gridheight: [868, 768, 960, 720],
                    delay: 15000,
                    startwidth: 1170,
                    startheight: App.getViewPort().height,
                    navigationType: "hide",
                    navigationArrows: "solo",
                    touchenabled: "on",
                    navigation:
                    {
                        keyboardNavigation: "off",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation: "off",
                        onHoverStop: "on",
                        bullets:
                        {
                            style: "round",
                            enable: true,
                            hide_onmobile: false,
                            hide_onleave: true,
                            hide_delay: 200,
                            hide_delay_mobile: 1200,
                            hide_under: 0,
                            hide_over: 9999,
                            direction: "horizontal",
                            h_align: "center",
                            v_align: "bottom",
                            space: 5,
                            h_offset: 0,
                            v_offset: 60,
                        },
                    },
                    spinner: "spinner2",
                    shadow: 0,
                    disableProgressBar: "on",
                    hideThumbsOnMobile: "on",
                    hideNavDelayOnMobile: 1500,
                    hideBulletsOnMobile: "on",
                    hideArrowsOnMobile: "on",
                    hideThumbsUnderResolution: 0
                });
            }); //ready
        </script>
        <!-- END: PAGE SCRIPTS -->
        <!-- END: LAYOUT/BASE/BOTTOM -->
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-64667612-1', 'themehats.com');
    ga('send', 'pageview');
    </script>
<!-- Mirrored from themehats.com/themes/jango/home-12.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 16 Feb 2016 11:35:34 GMT -->
</html>
