<div class="c-layout-breadcrumbs-1 c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
    <div class="container">
        <div class="c-page-title c-pull-left">
            <h3 class="c-font-uppercase c-font-sbold">Team</h3>
        </div>
    </div>
</div>
<!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->
<!-- BEGIN: PAGE CONTENT -->
<!-- BEGIN: BLOG LISTING -->

<div class="c-content-box c-size-md">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-uppercase c-font-bold">Our Team
                    </h3>
                    <div class="c-line-center c-theme-bg"></div>
                </div>
                <div class="c-content-blog-post-card-1-grid">
                    <div class="row list-promo-private">
                        <?php if ($teams): ?>

                            <?php foreach ($teams as $t): ?>
                                <div class="col-md-4">
                                    <div class="c-content-blog-post-card-1 c-option-2 c-bordered">
                                        <div class="c-media c-content-overlay">
                                            <a href="javascript:read_team('<?php echo $t->id ?>')">
                                                <img class="c-overlay-object img-responsive img-c" src="<?php echo get_user_img($t->photo) ?>" alt="">
                                            </a>
                                        </div>
                                        <div class="c-body">
                                            <div class="c-title c-font-bold c-font-uppercase">
                                                <a href="javascript:read_team('<?php echo $t->id ?>')"><?php echo $t->first_name .' '. $t->last_name ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach;?>
                        <?php else: ?>
                            <p>Belum ada team</p>
                        <?php endif?>

                    </div>

                </div>
            </div>
            <!-- END: CONTENT/BLOG/BLOG-SIDEBAR-1 -->
        </div>
    </div>
</div>
<!-- END: BLOG LISTING  -->
