<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Team extends MX_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('user_model'));
    }

    function index()
    {
        $data['title'] = 'Team';
        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf']      = $this->security->get_csrf_hash();
        $data['teams']     = $this->user_model->get_all( array ('level_id' => 3 , 'active' => 1) );

        $this->template->load('global/frontLayout/index','team/index', $data);
    }

    public function read($id='')
    {
        $data['team'] = $this->user_model->get($id);
        $this->load->view('read_modal', $data);
    }



}
