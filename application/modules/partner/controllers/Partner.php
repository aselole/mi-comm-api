<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Partner extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('partner_model');
        //Codeigniter : Write Less Do More
    }

    public function index()
    {
        $data['title']       = 'Partner';

        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf']      = $this->security->get_csrf_hash();

        $data['partners']   = $this->partner_model->get_all(array('active' => 'y'));
        $loc                 = array();
        foreach ($data['partners'] as $v) {
            $loc[] = array("<b>$v->nama</b><br>$v->alamat", $v->latitude, $v->longitude);
        }
        $data['loc'] = json_encode($loc);

        $this->template->load('global/frontLayout/index', 'partner/index', $data);
    }

    public function read($partner_id)
    {
        $data['partner'] = $this->partner_model->get(array('id' => $partner_id, 'active' => 'y'));

        $this->load->view('read_modal', $data, false);
        // $this->template->load('global/frontLayout/index', 'promo/read', $data);
    }


}
