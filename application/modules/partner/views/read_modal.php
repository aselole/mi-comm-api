<div class="modal fade in mdl-partner" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" ><?php echo $partner->nama ?></h4>
            </div>
            <div class="modal-body">
                <img align="middle" src="<?php echo get_partner_img($partner->logo_partner) ?>"
                    style="max-width:100%;
                        display: block;
                        margin-left: auto;
                        margin-right: auto ">
                        <br><br>
                <p> <i class="icon-home"></i> <?php echo $partner->alamat ?></p>
                <p> <i class="icon-call-end"></i> <?php echo @$partner->telepon1 ?></p>
                <p> <?php echo $partner->ket ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn c-btn-dark c-btn-border-2x c-btn-square c-btn-bold c-btn-uppercase" data-dismiss="modal">Tutup</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script type="text/javascript">
    $('.mdl-partner').modal('show');
</script>
