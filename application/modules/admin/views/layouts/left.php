<?php
$ar_menu_user          = array('group', 'level', 'user', 'birthday', 'pasangan');
$ar_menu_master        = array('company', 'merchant', 'partner', 'master_email', 'setting_about');
$ar_menu_community     = array('community', 'member');
$ar_menu_promo         = array('promo', 'promo_user', 'voucher', 'promo_claim');
$ar_menu_news          = array('news');
$ar_menu_issue         = array('issue', 'bad_comment');
$ar_menu_private_issue = array('private_issue');
$ar_menu_slider_image  = array('image_slide');
?>
<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item start <?php echo $this->uri->segment(2)=='dashboard' ? 'active open' : '' ?>">
                <a href="<?php echo site_url().'admin/dashboard' ?>" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <?php if ($this->uri->segment(2)=='dashboard'): ?>
                        <span class="selected"></span>
                    <?php endif; ?>
                </a>
            </li>
            <li class="nav-item start <?php echo in_array($this->uri->segment(2), $ar_menu_master) ? 'active open' : '' ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-gears"></i>
                    <span class="title">Master</span>
                    <span class="selected"></span>
                    <span class="arrow <?php echo in_array($this->uri->segment(2), $ar_menu_master) ? 'open' : '' ?>"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?php echo $this->uri->segment(2)=='company' ? 'active' : '' ?>">
                        <a href="<?php echo site_url() ?>admin/company" class="nav-link <?php echo $this->uri->segment(2)=='company' ? 'active' : '' ?>">
                            <span class="title">Perusahaan</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item <?php echo $this->uri->segment(2)=='merchant' ? 'active' : '' ?>">
                        <a href="<?php echo site_url() ?>admin/merchant" class="nav-link <?php echo $this->uri->segment(2)=='merchant' ? 'active' : '' ?>">
                            <span class="title">Merchant</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item <?php echo $this->uri->segment(2)=='partner' ? 'active' : '' ?>">
                        <a href="<?php echo site_url() ?>admin/partner" class="nav-link <?php echo $this->uri->segment(2)=='partner' ? 'active' : '' ?>">
                            <span class="title">Partner</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item <?php echo $this->uri->segment(2)=='master_email' ? 'active' : '' ?>">
                        <a href="<?php echo site_url() ?>admin/master_email" class="nav-link <?php echo $this->uri->segment(2)=='master_email' ? 'active' : '' ?>">
                            <span class="title">Master Email</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="hide nav-item <?php echo $this->uri->segment(2)=='setting_about' ? 'active' : '' ?>">
                        <a href="<?php echo site_url() ?>admin/setting_about" class="nav-link <?php echo $this->uri->segment(2)=='setting_about' ? 'active' : '' ?>">
                            <span class="title">Setting About</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item start <?php echo in_array($this->uri->segment(2), $ar_menu_community) ? 'active open' : '' ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-heart-o"></i>
                    <span class="title">Community</span>
                    <span class="selected"></span>
                    <span class="arrow <?php echo in_array($this->uri->segment(2), $ar_menu_community) ? 'open' : '' ?>"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?php echo $this->uri->segment(2)=='community' ? 'active' : '' ?>">
                        <a href="<?php echo site_url() ?>admin/community" class="nav-link <?php echo $this->uri->segment(2)=='community' ? 'active' : '' ?>">
                            <span class="title">Community</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item <?php echo $this->uri->segment(2)=='member' ? 'active' : '' ?>">
                        <a href="<?php echo site_url() ?>admin/member" class="nav-link <?php echo $this->uri->segment(2)=='member' ? 'active' : '' ?>">
                            <span class="title">Member</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item start <?php echo in_array($this->uri->segment(2), $ar_menu_user) ? 'active open' : '' ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">User Manager</span>
                    <span class="selected"></span>
                    <span class="arrow <?php echo in_array($this->uri->segment(2), $ar_menu_user) ? 'open' : '' ?>"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item display-hide <?php echo $this->uri->segment(2)=='level' ? 'active' : '' ?>">
                        <a href="<?php echo site_url() ?>admin/level" class="nav-link <?php echo $this->uri->segment(2)=='level' ? 'active' : '' ?>">
                            <span class="title">Level</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item <?php echo $this->uri->segment(2)=='user' ? 'active' : '' ?>">
                        <a href="<?php echo site_url() ?>admin/user" class="nav-link <?php echo $this->uri->segment(2)=='user' ? 'active' : '' ?>">
                            <span class="title">User</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item <?php echo $this->uri->segment(2)=='birthday' ? 'active' : '' ?>">
                        <a href="<?php echo site_url() ?>admin/birthday" class="nav-link <?php echo $this->uri->segment(2)=='birthday' ? 'active' : '' ?>">
                            <span class="title">Next Birthday</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item <?php echo $this->uri->segment(2)=='pasangan' ? 'active' : '' ?>">
                        <a href="<?php echo site_url() ?>admin/pasangan" class="nav-link <?php echo $this->uri->segment(2)=='pasangan' ? 'active' : '' ?>">
                            <span class="title">Pasangan</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item start <?php echo in_array($this->uri->segment(2), $ar_menu_promo) ? 'active open' : '' ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-gears"></i>
                    <span class="title">Promo</span>
                    <span class="selected"></span>
                    <span class="arrow <?php echo in_array($this->uri->segment(2), $ar_menu_promo) ? 'open' : '' ?>"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?php echo $this->uri->segment(2)=='promo' ? 'active' : '' ?>">
                        <a href="<?php echo site_url() ?>admin/promo" class="nav-link <?php echo $this->uri->segment(2)=='promo' ? 'active' : '' ?>">
                            <span class="title">List Promo</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item <?php echo $this->uri->segment(2)=='promo_user' ? 'active' : '' ?>">
                        <a href="<?php echo site_url() ?>admin/promo_user" class="nav-link <?php echo $this->uri->segment(2)=='promo_user' ? 'active' : '' ?>">
                            <span class="title">Promo Private User</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item <?php echo $this->uri->segment(2)=='voucher' ? 'active' : '' ?>">
                        <a href="<?php echo site_url() ?>admin/voucher" class="nav-link <?php echo $this->uri->segment(2)=='voucher' ? 'active' : '' ?>">
                            <span class="title">Voucher</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item <?php echo $this->uri->segment(2)=='promo_claim' ? 'active' : '' ?>">
                        <a href="<?php echo site_url() ?>admin/promo_claim" class="nav-link <?php echo $this->uri->segment(2)=='promo_claim' ? 'active' : '' ?>">
                            <span class="title">Promo Global Claim</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item start <?php echo in_array($this->uri->segment(2), $ar_menu_news) ? 'active open' : '' ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-newspaper-o"></i>
                    <span class="title">News</span>
                    <span class="selected"></span>
                    <span class="arrow <?php echo in_array($this->uri->segment(2), $ar_menu_news) ? 'open' : '' ?>"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?php echo $this->uri->segment(2)=='news' ? 'active' : '' ?>">
                        <a href="<?php echo site_url() ?>admin/news" class="nav-link <?php echo $this->uri->segment(2)=='news' ? 'active' : '' ?>">
                            <span class="title">List News</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item start <?php echo in_array($this->uri->segment(2), $ar_menu_issue) ? 'active open' : '' ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-comments-o"></i>
                    <span class="title">Issue</span>
                    <span class="selected"></span>
                    <span class="arrow <?php echo in_array($this->uri->segment(2), $ar_menu_issue) ? 'open' : '' ?>"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?php echo $this->uri->segment(2)=='issue' ? 'active' : '' ?>">
                        <a href="<?php echo site_url() ?>admin/issue" class="nav-link <?php echo $this->uri->segment(2)=='issue' ? 'active' : '' ?>">
                            <span class="title">List Issue</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item <?php echo $this->uri->segment(2)=='bad_comment' ? 'active' : '' ?>">
                        <a href="<?php echo site_url() ?>admin/bad_comment" class="nav-link <?php echo $this->uri->segment(2)=='bad_comment' ? 'active' : '' ?>">
                            <span class="title">Komentar Buruk</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item start <?php echo in_array($this->uri->segment(2), $ar_menu_private_issue) ? 'active open' : '' ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-comments-o"></i>
                    <span class="title">Private Issue</span>
                    <span class="selected"></span>
                    <span class="arrow <?php echo in_array($this->uri->segment(2), $ar_menu_private_issue) ? 'open' : '' ?>"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?php echo $this->uri->segment(2)=='private_issue' ? 'active' : '' ?>">
                        <a href="<?php echo site_url() ?>admin/private_issue" class="nav-link <?php echo $this->uri->segment(2)=='private_issue' ? 'active' : '' ?>">
                            <span class="title">List Private Issue</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item start <?php echo in_array($this->uri->segment(2), $ar_menu_slider_image) ? 'active open' : '' ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-screen-desktop"></i>
                    <span class="title">Image Slide</span>
                    <span class="selected"></span>
                    <span class="arrow <?php echo in_array($this->uri->segment(2), $ar_menu_slider_image) ? 'open' : '' ?>"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?php echo $this->uri->segment(2)=='image_slide' ? 'active' : '' ?>">
                        <a href="<?php echo site_url() ?>admin/image_slide" class="nav-link <?php echo $this->uri->segment(2)=='image_slide' ? 'active' : '' ?>">
                            <span class="title">List Image</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
