
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" onclick="event.preventDefault(); closeModal();"></button>
			<h4 class="modal-title"><?php echo $judul ?></h4>
		</div>
		<div class="modal-body">
            <form class="form-horizontal form_ajax" method="<?php echo $method ?>" action="<?php echo $action ?>" enctype="multipart/form-data">
                <?php echo form_hidden($csrf_name, $csrf); ?>
    			<div class="form-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Jenis *</label>
                        <div class="col-md-8">
							<?php if ($mode=='create'): ?>
								<select class="form-control select2" name="member_jenis" style="width:100%">
									<option></option>
									<?php foreach ($member_jenis as $r): ?>
										<?php $terpilih = $r==@$dt->member_jenis ? 'selected' : '' ?>
										<?php echo '<option value="'.$r.'" '.$terpilih.'>'.$r.'</option>' ?>
									<?php endforeach; ?>
								</select>
							<?php else: ?>
								<span class="form-control-static"><?php echo @$dt->member_jenis ?></span>
							<?php endif; ?>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-md-3 control-label">Perusahaan *</label>
                        <div class="col-md-8">
							<?php if ($mode=='create'): ?>
								<select class="form-control select2" name="company_id" style="width:100%">
									<option></option>
									<?php foreach ($company as $r): ?>
										<?php $terpilih = $r->id==@$dt->company_id ? 'selected' : '' ?>
										<?php echo '<option value="'.$r->id.'" '.$terpilih.'>'.$r->jenis.'. '.$r->nama.' - '.$r->alamat.'</option>' ?>
									<?php endforeach; ?>
								</select>
							<?php else: ?>
								<span class="form-control-static"><?php echo @$dt->company->nama ?></span>
							<?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Nama *</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="nama" value="<?php echo @$dt->nama ?>" placeholder="Nama Komunitas">
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-md-3 control-label">Logo</label>
                        <div class="col-md-6">
							<?php if (@$dt->logo != null && file_exists('./uploads/community/'.@$dt->logo)): ?>
								<img style="width: 300px; height: 250px;" src="<?php echo base_url().'uploads/community/'.@$dt->logo ?>">
							<?php endif; ?>
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 300px; height: 250px;">

                                </div>
                                <div>
                                    <span class="btn red btn-outline btn-file">
                                        <span class="fileinput-new"> Pilih Foto </span>
                                        <span class="fileinput-exists"> Ganti </span>
                                        <input type="file" name="logo"> </span>
                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Hapus </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Keterangan *</label>
                        <div class="col-md-8">
                            <textarea class="form-control" name="ket" placeholder="Keterangan"><?php echo @$dt->ket ?></textarea>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-md-3 control-label">Alamat *</label>
                        <div class="col-md-8">
                            <textarea class="form-control" name="alamat" placeholder="Alamat"><?php echo @$dt->alamat ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Email</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="email" value="<?php echo @$dt->email ?>" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Telepon 1 *</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="telepon1" value="<?php echo @$dt->telepon1 ?>" placeholder="Telepon 1">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Telepon 2</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="telepon2" value="<?php echo @$dt->telepon2 ?>" placeholder="Telepon 2">
                        </div>
                    </div>
    			</div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn green">Submit</button>
                            <button type="button" class="btn default" onclick="event.preventDefault(); closeModal();">Cancel</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</div>
</div>
<script src="<?php echo base_url() ?>assets/custom/scripts/j_generalFormModal.js" type="text/javascript"></script>
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>

<script type="text/javascript">

// begin - select2 modal wajib
$.fn.modal.Constructor.prototype.enforceFocus = function() {};
// end - select2 modal wajib

$('.select2').select2({
    placeholder: 'Silahkan Pilih'
});
</script>
