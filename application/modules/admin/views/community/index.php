<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <div class="page-toolbar">
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">
                                <?php echo $judul ?>
                                <button type="button" class="btn btn-outline btn-sm blue" onclick="event.preventDefault(); btn_add();"><i class="fa fa-plus"></i> Tambah</button>
                            </span>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" id="mytable">
                            <thead>
                                <tr role="row" class="heading">
                                    <th width="2%">No </th>
                                    <th>Jenis</th>
                                    <th>Perusahaan</th>
                                    <th>Nama</th>
                                    <th>Ket</th>
                                    <th>Alamat</th>
                                    <th>Email</th>
                                    <th>Tlp1</th>
                                    <th>Tlp2</th>
                                    <th>Dibuat</th>
                                    <th width="2%">Aksi</th>
                                </tr>
                                <tr role="row" class="filter">
                                    <td></td>
                                    <td>
                                        <select class="form-control select2 form-filter input-sm" name="member_jenis" style="padding:0px;width:100%">
                                            <option value="">All</option>
                                            <?php foreach ($member_jenis as $r): ?>
                                                <?php echo '<option value="'.$r.'">'.$r.'</option>' ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control select2 form-filter input-sm" name="company_id" style="padding:0px;width:100%">
                                            <option value="">All</option>
                                            <?php foreach ($company as $r): ?>
                                                <?php echo '<option value="'.$r->id.'">'.$r->nama.'</option>' ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                    <td><input type="text" class="form-control form-filter input-sm" name="nama"></td>
                                    <td><input type="text" class="form-control form-filter input-sm" name="ket"></td>
                                    <td><input type="text" class="form-control form-filter input-sm" name="alamat"></td>
                                    <td><input type="text" class="form-control form-filter input-sm" name="email"></td>
                                    <td><input type="text" class="form-control form-filter input-sm" name="telepon1"></td>
                                    <td><input type="text" class="form-control form-filter input-sm" name="telepon2"></td>
                                    <td>
                                        <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                            <input type="text" class="form-control form-filter input-sm" id="created_at_start" readonly="" name="created_at_start" placeholder="From">
                                            <span class="input-group-btn">
                                                <button class="btn btn-sm default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                            <input type="text" class="form-control form-filter input-sm" id="created_at_end" readonly="" name="created_at_end" placeholder="To">
                                            <span class="input-group-btn">
                                                <button class="btn btn-sm default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="margin-bottom-5">
                                            <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                                <i class="fa fa-search"></i> Search
                                            </button>
                                        </div>
                                        <button class="btn btn-sm red btn-outline filter-cancel">
                                            <i class="fa fa-times"></i> Reset
                                        </button>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>

<div id="modal_form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<span id="site_url" data-site-url="<?php echo site_url() ?>"></span>

<script type="text/javascript">
$('.select2').select2();
$('.date-picker').datepicker({
    rtl: App.isRTL(),
    autoclose: true
});

$('#created_at_start').change(function(e) {
    var created_at_start = $('#created_at_start').val();
    var from_arr = created_at_start.split("/");
    var from = new Date(from_arr[2], from_arr[1]-1, from_arr[0]);

    var created_at_end = $('#created_at_end').val();
    var to_arr = created_at_end.split("/");
    var to = new Date(to_arr[2], to_arr[1]-1, to_arr[0]);

    if (from > to) {
        $('#created_at_end').val(created_at_start);
    }
});
$('#created_at_end').change(function(e) {
    var created_at_start = $('#created_at_start').val();
    var from_arr = created_at_start.split("/");
    var from = new Date(from_arr[2], from_arr[1]-1, from_arr[0]);

    var created_at_end = $('#created_at_end').val();
    var to_arr = created_at_end.split("/");
    var to = new Date(to_arr[2], to_arr[1]-1, to_arr[0]);

    if (to < from) {
        $('#created_at_start').val(created_at_end);
    }
});

var TableDatatablesAjax = function () {
    var grid = new Datatable();
    grid.init({
        src: $("#mytable"),
        dataTable: {
            "ajax": {
                "url": "<?php echo site_url('admin/community/getDatatable/') ?>", // ajax source
                data: function (d) {
                    d.<?php echo $csrf_name ?> = '<?php echo $csrf ?>';
                    d.member_jenis             = $('[name=member_jenis]').val();
                    d.company_id               = $('[name=company_id]').val();
                    d.nama                     = $('[name=nama]').val();
                    d.ket                      = $('[name=ket]').val();
                    d.alamat                   = $('[name=alamat]').val();
                    d.email                    = $('[name=email]').val();
                    d.tlp1                     = $('[name=tlp1]').val();
                    d.tlp2                     = $('[name=tlp2]').val();
                    d.created_at_start         = $('[name=created_at_start]').val();
                    d.created_at_end           = $('[name=created_at_end]').val();
                }
            },
            "order": [
                [1, "asc"]
            ]// set first column as a default sort by asc
        }
    });
}
jQuery(document).ready(function() {
   TableDatatablesAjax();
});

function btn_add() {
    $(".portlet").LoadingOverlay("show");
    $('#modal_form').load("<?php echo site_url('admin/community/create') ?>", function() {
        $(this).modal('show');
        $(".portlet").LoadingOverlay("hide");
    });
}

function btn_edit(id) {
    $(".portlet").LoadingOverlay("show");
    $('#modal_form').load("<?php echo site_url('admin/community/edit/') ?>"+id, function() {
        $(this).modal('show');
        $(".portlet").LoadingOverlay("hide");
    });
}

function btn_delete(id) {
    swal(
        {
            title: "Apakah Anda yakin?",
            text: "Semua data yang berkaitan dengan data community akan terhapus.",
            type: "warning",
            showCancelButton: true,
            cancelButtonClass: "btn-default",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, Delete it!",
            closeOnConfirm: true
        },
        function(){
            $.post('<?php echo site_url('admin/community/delete/') ?>'+id, {<?php echo $csrf_name ?>: '<?php echo $csrf ?>'}, function(res) {
                if (res.success) {
                    $('.filter-cancel').trigger('click');
                    toastr.success(res.message);
                } else {
                    toastr.error(res.message);
                }
            });
        }
    );
}

</script>
