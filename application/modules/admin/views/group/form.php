
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" onclick="event.preventDefault(); closeModal();"></button>
			<h4 class="modal-title"><?php echo $judul ?></h4>
		</div>
		<div class="modal-body">
            <form class="form-horizontal form_ajax" method="<?php echo $method ?>" action="<?php echo $action ?>">
                <?php echo form_hidden($csrf_name, $csrf); ?>
    			<div class="form-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Name *</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="name" value="<?php echo @$dt->name ?>" placeholder="Group Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Description *</label>
                        <div class="col-md-9">
                            <textarea class="form-control" name="description" placeholder="Description"><?php echo @$dt->description ?></textarea>
                        </div>
                    </div>
    			</div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn green">Submit</button>
                            <button type="button" class="btn default" onclick="event.preventDefault(); closeModal();">Cancel</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</div>
</div>
<script src="<?php echo base_url() ?>assets/custom/scripts/j_generalFormModal.js" type="text/javascript"></script>
