<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js" type="text/javascript"></script>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" onclick="event.preventDefault(); closeModal();"></button>
			<h4 class="modal-title"><?php echo $judul ?></h4>
		</div>
		<div class="modal-body">
            <form class="form-horizontal form_ajax" method="<?php echo $method ?>" action="<?php echo $action ?>">
                <?php echo form_hidden($csrf_name, $csrf); ?>
    			<div class="form-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Community *</label>
                        <div class="col-md-8">
                            <select class="form-control select2" name="community_id" style="width:100%" <?php echo $mode=='edit' ? 'disabled' : '' ?>>
                                <option></option>
                                <?php foreach ($communities as $r): ?>
                                    <?php $terpilih = $r->id==@$dt->community_id ? 'selected' : '' ?>
                                    <?php echo '<option value="'.$r->id.'" '.$terpilih.'>'.$r->nama.' - '.$r->ket.'</option>' ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <?php if ($mode=='create'): ?>
                        <div class="form-group">
                            <label class="col-md-3 control-label">User *</label>
                            <div class="col-md-8">
                                <select class="form-control select2" id="user_id" style="width:100%">
                                    <option></option>
                                    <?php foreach ($users as $r): ?>
                                        <?php if (!in_array($r->level_id, array(1,4))): ?>
                                            <?php echo '<option value="'.$r->id.'">'.$r->first_name.' - '.$r->email.' ('.$r->level_nama.')</option>' ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                                <div class="margin-top-10">
                                    <a href="javascript:;" class="btn red" id="object_tagsinput_add">Add tag</a>
                                </div>
                                <div class="margin-top-10">
                                    <input type="text" class="form-control input-large" id="ar_user_id" name="ar_user_id">
                                </div>
                            </div>
                        </div>
                    <?php elseif ($mode=='edit'): ?>
                        <div class="form-group">
                            <label class="col-md-3 control-label">User *</label>
                            <div class="col-md-8">
                                <select class="form-control select2" id="user_id" style="width:100%" disabled>
                                    <option></option>
                                    <?php foreach ($users as $r): ?>
                                        <?php if (!in_array($r->level_id, array(1,4))): ?>
                                            <?php $terpilih = $r->id==@$dt->user_id ? 'selected' : '' ?>
                                            <?php echo '<option value="'.$r->id.'" '.$terpilih.'>'.$r->first_name.' - '.$r->email.' ('.$r->level_nama.')</option>' ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Tgl Bergabung *</label>
                        <div class="col-md-8">
                            <input class="form-control datepicker" type="text" name="tgl_bergabung" value="<?php echo @$dt->tgl_bergabung ? date('d F Y', strtotime(@$dt->tgl_bergabung)) : '' ?>" data-date-format="dd MM yyyy">
                        </div>
                    </div>
                    <?php if ($mode=='edit'): ?>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Aktif *</label>
                            <div class="col-md-8">
                                <select class="select2" name="active" style="width:20%">
                                    <option></option>
                                    <option value="y" <?php echo @$dt->active=='y' ? 'selected' : '' ?>>Y</option>
                                    <option value="t" <?php echo @$dt->active=='t' ? 'selected' : '' ?>>T</option>
                                </select>
                            </div>
                        </div>
                    <?php endif; ?>
    			</div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn green">Submit</button>
                            <button type="button" class="btn default" onclick="event.preventDefault(); closeModal();">Cancel</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</div>
</div>
<script src="<?php echo base_url() ?>assets/custom/scripts/j_generalFormModal.js" type="text/javascript"></script>

<script type="text/javascript">

// begin - select2 modal wajib
$.fn.modal.Constructor.prototype.enforceFocus = function() {};
// end - select2 modal wajib

$('.select2').select2({
    placeholder: 'Silahkan Pilih'
});

var elt = $('#ar_user_id');

elt.tagsinput({
  itemValue: 'value',
  itemText: 'text',
});

$('#object_tagsinput_add').on('click', function(){
    elt.tagsinput('add', {
        "value": $('#user_id').val(),
        "text": $('#user_id').find(':selected').html(),
    });
    $('#user_id').val('').change();
});

$('.datepicker').datepicker({
    rtl: App.isRTL(),
    orientation: "left",
    autoclose: true
});
</script>
