<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <div class="page-toolbar">
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">
                                <?php echo $judul ?>
                                <!-- <button type="button" class="btn btn-outline btn-sm blue" onclick="event.preventDefault(); btn_add();"><i class="fa fa-plus"></i> Tambah</button> -->
                            </span>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" id="mytable">
                            <thead>
                                <tr role="row" class="heading">
                                    <th width="2%">No </th>
                                    <th>Jenis</th>
                                    <th>Community</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Level</th>
                                    <th>Gabung</th>
                                    <th>Aktif</th>
                                    <th width="2%">Aksi</th>
                                </tr>
                                <tr role="row" class="filter">
                                    <td></td>
                                    <td>
                                        <select class="form-control select2 form-filter input-sm" name="member_jenis" style="padding:0px;width:100%">
                                            <option value="">All</option>
                                            <?php foreach ($member_jenis as $r): ?>
                                                <?php echo '<option value="'.$r.'">'.$r.'</option>' ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control select2 form-filter input-sm" name="community_id" style="padding:0px;width:100%">
                                            <option value="">All</option>
                                            <?php foreach ($communities as $r): ?>
                                                <?php echo '<option value="'.$r->id.'">'.$r->nama.'</option>' ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                    <td><input type="text" class="form-control form-filter input-sm" name="first_name"></td>
                                    <td><input type="text" class="form-control form-filter input-sm" name="email"></td>
                                    <td>
                                        <select class="form-control form-filter input-sm" name="level_id" style="padding:0px;width:100%">
                                            <option value="">All</option>
                                            <?php foreach ($levels as $r): ?>
                                                <?php echo '<option value="'.$r->id.'">'.$r->nama.'</option>' ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                    <td>
                                        <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                            <input type="text" class="form-control form-filter input-sm" id="tgl_bergabung_start" readonly="" name="tgl_bergabung_start" placeholder="From">
                                            <span class="input-group-btn">
                                                <button class="btn btn-sm default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                            <input type="text" class="form-control form-filter input-sm" id="tgl_bergabung_end" readonly="" name="tgl_bergabung_end" placeholder="To">
                                            <span class="input-group-btn">
                                                <button class="btn btn-sm default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <select class="form-control form-filter input-sm" name="active" style="padding:0px;width:100%">
                                            <option value="">All</option>
                                            <option value="y">Y</option>
                                            <option value="t">T</option>
                                        </select>
                                    </td>
                                    <td>
                                        <div class="margin-bottom-5">
                                            <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                                <i class="fa fa-search"></i> Search
                                            </button>
                                        </div>
                                        <button class="btn btn-sm red btn-outline filter-cancel">
                                            <i class="fa fa-times"></i> Reset
                                        </button>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>

<div id="modal_form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<span id="site_url" data-site-url="<?php echo site_url() ?>"></span>

<script type="text/javascript">
$('.select2').select2();
$('.date-picker').datepicker({
    rtl: App.isRTL(),
    autoclose: true
});

$('#tgl_bergabung_start').change(function(e) {
    var tgl_bergabung_start = $('#tgl_bergabung_start').val();
    var from_arr = tgl_bergabung_start.split("/");
    var from = new Date(from_arr[2], from_arr[1]-1, from_arr[0]);

    var tgl_bergabung_end = $('#tgl_bergabung_end').val();
    var to_arr = tgl_bergabung_end.split("/");
    var to = new Date(to_arr[2], to_arr[1]-1, to_arr[0]);

    if (from > to) {
        $('#tgl_bergabung_end').val(tgl_bergabung_start);
    }
});
$('#tgl_bergabung_end').change(function(e) {
    var tgl_bergabung_start = $('#tgl_bergabung_start').val();
    var from_arr = tgl_bergabung_start.split("/");
    var from = new Date(from_arr[2], from_arr[1]-1, from_arr[0]);

    var tgl_bergabung_end = $('#tgl_bergabung_end').val();
    var to_arr = tgl_bergabung_end.split("/");
    var to = new Date(to_arr[2], to_arr[1]-1, to_arr[0]);

    if (to < from) {
        $('#tgl_bergabung_start').val(tgl_bergabung_end);
    }
});

var TableDatatablesAjax = function () {
    var grid = new Datatable();
    grid.init({
        src: $("#mytable"),
        dataTable: {
            "ajax": {
                "url": "<?php echo site_url('admin/member/getDatatable/') ?>", // ajax source
                data: function (d) {
                    d.<?php echo $csrf_name ?> = '<?php echo $csrf ?>';
                    d.member_jenis             = $('[name=member_jenis]').val();
                    d.community_id             = $('[name=community_id]').val();
                    d.first_name               = $('[name=first_name]').val();
                    d.email                    = $('[name=email]').val();
                    d.level_id                 = $('[name=level_id]').val();
                    d.tgl_bergabung_start      = $('[name=tgl_bergabung_start]').val();
                    d.tgl_bergabung_end        = $('[name=tgl_bergabung_end]').val();
                    d.active                   = $('[name=active]').val();
                }
            },
            "order": [
                [1, "asc"]
            ]// set first column as a default sort by asc
        }
    });
}
jQuery(document).ready(function() {
   TableDatatablesAjax();
});

function btn_add() {
    $(".portlet").LoadingOverlay("show");
    $('#modal_form').load("<?php echo site_url('admin/member/create') ?>", function() {
        $(this).modal('show');
        $(".portlet").LoadingOverlay("hide");
    });
}

function btn_edit(id) {
    $(".portlet").LoadingOverlay("show");
    $('#modal_form').load("<?php echo site_url('admin/member/edit/') ?>"+id, function() {
        $(this).modal('show');
        $(".portlet").LoadingOverlay("hide");
    });
}

function btn_delete(id) {
    swal(
        {
            title: "Apakah Anda yakin?",
            text: "Semua data yang berkaitan dengan member akan terhapus.",
            type: "warning",
            showCancelButton: true,
            cancelButtonClass: "btn-default",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, Delete it!",
            closeOnConfirm: true
        },
        function(){
            $.post('<?php echo site_url('admin/member/delete/') ?>'+id, {<?php echo $csrf_name ?>: '<?php echo $csrf ?>'}, function(res) {
                if (res.success) {
                    $('.filter-cancel').trigger('click');
                    toastr.success(res.message);
                } else {
                    toastr.error(res.message);
                }
            });
        }
    );
}

</script>
