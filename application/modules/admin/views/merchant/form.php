<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/global/plugins/jquery-latitude-longitude-picker-gmaps/css/jquery-gmaps-latlon-picker.css"/>

<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAenTO4nxd6GrFmnHbEhxfaulJLHI9y5dM"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/jquery-latitude-longitude-picker-gmaps/js/jquery-gmaps-latlon-picker.js"></script>

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <div class="page-toolbar">
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">
                                <?php echo $judul ?>
                            </span>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <form class="form-horizontal form_ajax" method="<?php echo $method ?>" action="<?php echo $action ?>" enctype="multipart/form-data">
                            <?php echo form_hidden($csrf_name, $csrf); ?>
                			<div class="form-body">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Nama *</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="nama" value="<?php echo @$dt->nama ?>" placeholder="Nama Merchant">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Keterangan *</label>
                                        <div class="col-md-8">
                                            <textarea name="ket" id="summernote_1" rows="8" class="form-control"><?php echo @$dt->ket ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Alamat *</label>
                                        <div class="col-md-6">
                                            <textarea class="form-control" name="alamat" placeholder="Alamat"><?php echo @$dt->alamat ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Telepon 1 *</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="telepon1" value="<?php echo @$dt->telepon1 ?>" placeholder="Telepon 1">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Telepon 2</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="telepon2" value="<?php echo @$dt->telepon2 ?>" placeholder="Telepon 2">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Email</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="email" value="<?php echo @$dt->email ?>" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Peta</label>
                                        <div class="col-md-9">
                                            <fieldset class="gllpLatlonPicker">
                                                <input type="text" class="gllpSearchField">
                                                <input type="button" class="gllpSearchButton" value="search">
                                                <br/><br/>
                                                <div class="gllpMap">Google Maps</div>
                                                <br/>
                                                Latitude / Longitude :
                                                <br/>
                                                <input type="text" name="lattitude" class="gllpLatitude" value="<?php echo @$dt->lattitude ? @$dt->lattitude : '-7.2574719' ?>"/>
                                                <input type="text" name="longitude" class="gllpLongitude" value="<?php echo @$dt->longitude ? @$dt->longitude : '112.75208829999997' ?>"/>
                                                <input type="hidden" class="gllpZoom" value="18"/>
                                                <input type="button" class="gllpUpdateButton" value="update map">
                                                <br/>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Aktif *</label>
                                        <div class="col-md-6">
                                            <div class="mt-radio-list">
                                                <?php if ($mode=='add'): ?>
                                                    <label class="mt-radio mt-radio-outline">
                                                        <input type="radio" name="active" id="optionsRadios22" value="y" checked> Ya
                                                        <span></span>
                                                    </label>
                                                <?php elseif ($mode=='edit'): ?>
                                                    <label class="mt-radio mt-radio-outline">
                                                        <input type="radio" name="active" id="optionsRadios22" value="y" <?php echo @$dt->active=='y' ? 'checked' : '' ?>> Ya
                                                        <span></span>
                                                    </label>
                                                <?php endif; ?>
                                                <label class="mt-radio mt-radio-outline">
                                                    <input type="radio" name="active" id="optionsRadios23" value="t" <?php echo @$dt->active=='t' ? 'checked' : '' ?>> Tidak
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Nama Company *</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="nama_company" value="<?php echo @$dt->nama_company ?>" placeholder="Nama Company">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Jenis Company</label>
                                        <div class="col-md-6">
                                            <select class="form-group select2" name="jenis_company" style="width:100%">
                                                <option></option>
                                                <?php foreach ($jenis_company as $r): ?>
                                                    <?php $terpilih = $r==@$jenis_company ? 'selected' : '' ?>
                                                    <?php echo '<option value="'.$r.'" '.$terpilih.'>'.$r.'</option>' ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Alamat</label>
                                        <div class="col-md-6">
                                            <textarea class="form-control" name="alamat_company" placeholder="Alamat"><?php echo @$dt->alamat_company ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Telepon 1</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="telepon1_company" value="<?php echo @$dt->telepon1_company ?>" placeholder="Telepon 1">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Telepon 2</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="telepon2_company" value="<?php echo @$dt->telepon2_company ?>" placeholder="Telepon 2">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Email</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="email_company" value="<?php echo @$dt->email_company ?>" placeholder="Email">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group ">
                                        <div class="col-md-12">
                                            <?php if (@$dt->logo_merchant != null && file_exists('./uploads/merchant/'.@$dt->logo_merchant)): ?>
                                                <img style="width: 200px; height: 150px;" src="<?php echo base_url().'uploads/merchant/'.@$dt->logo_merchant ?>">
                                                <br>
                                                <br>
                                            <?php endif; ?>
                                            <span class="help-block">Best View 250 x 250 pixel</span>
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                                </div>
                                                <div>
                                                    <span class="btn red btn-outline btn-file">
                                                        <span class="fileinput-new"> Pilih Logo </span>
                                                        <span class="fileinput-exists"> Ganti </span>
                                                        <input type="file" name="logo_merchant"> </span>
                                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Hapus </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                			</div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Submit</button>
                                        <a href="<?php echo site_url('admin/merchant') ?>"><button type="button" class="btn default">Batal</button></a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>

<div id="modal_form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<span id="site_url" data-site-url="<?php echo site_url() ?>"></span>
<script src="<?php echo base_url() ?>assets/custom/scripts/j_generalForm.js" type="text/javascript"></script>
<script type="text/javascript">
$('#summernote_1').summernote({
    height: 300,
    toolbar: [
            [ 'style', [ 'style' ] ],
            [ 'font', [ 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear'] ],
            [ 'fontname', [ 'fontname' ] ],
            [ 'fontsize', [ 'fontsize' ] ],
            [ 'color', [ 'color' ] ],
            [ 'para', [ 'ol', 'ul', 'paragraph', 'height' ] ],
            [ 'table', [ 'table' ] ],
            [ 'insert', [ 'link'] ],
            [ 'view', [ 'undo', 'redo', 'fullscreen', 'codeview', 'help' ] ]
        ]
});
</script>
