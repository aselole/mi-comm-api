<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <div class="page-toolbar">
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">
                                <?php echo $judul ?>
                            </span>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <form class="form-horizontal form_ajax" method="<?php echo $method ?>" action="<?php echo $action ?>" enctype="multipart/form-data">
                            <?php echo form_hidden($csrf_name, $csrf); ?>
                            <input type="hidden" name="level_id" value="2">
                			<div class="form-body">
                                <div class="col-md-8">
                                    <?php if ($mode=='add'): ?>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Jenis *</label>
                                            <div class="col-md-6">
                                                <select class="form-control select2" name="member_jenis" id="member_jenis">
                                                    <option></option>
                                                    <?php foreach ($member_jenis as $r): ?>
                                                        <?php $terpilih = $r=='micomm' ? 'selected' : '' ?>
                                                        <?php echo '<option value="'.$r.'" '.$terpilih.'>'.$r.'</option>' ?>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <?php echo form_hidden('member_jenis', $dt->member_jenis); ?>
                                        <?php if (@$dt->member_jenis): ?>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Jenis</label>
                                                <div class="col-md-6">
                                                    <span class="form-control-static"><?php echo @$dt->member_jenis ?></span>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <hr>
                                    <?php if ($mode=='add'): ?>
                                        <div class="select2member">

                                        </div>
                                    <?php elseif ($mode=='edit'): ?>
                                        <input type="hidden" name="community_id" value="<?php echo @$dt->community_id ?>">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Community *</label>
                                            <div class="col-md-6 ">
                                                <span class="form-control-static"><?php echo @$community_nama ?></span>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Nama *</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="first_name" value="<?php echo @$dt->first_name ?>" placeholder="Nama">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Kota Lahir</label>
                                        <div class="col-md-6">
                                            <select class="form-control select2" name="place_birthday">
                                                <option></option>
                                                <?php foreach ($kokab->result() as $r): ?>
                                                    <option value="<?php echo $r->kokab_nama ?>" <?php echo @$dt->place_birthday==$r->kokab_nama ? 'selected' : '' ?>><?php echo $r->kokab_nama.' - '.$r->provinsi_nama ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Tgl Lahir</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control datepicker" name="date_birthday" data-date-format="dd MM yyyy" value="<?php echo (@$dt->date_birthday) ? date('d F Y', strtotime(@$dt->date_birthday)) : '' ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Jenis Kelamin *</label>
                                        <div class="col-md-6">
                                            <div class="mt-radio-list">
                                                <?php if ($mode=='add'): ?>
                                                    <label class="mt-radio mt-radio-outline">
                                                        <input type="radio" name="gender" id="optionsRadios22" value="L" checked> Laki-Laki
                                                        <span></span>
                                                    </label>
                                                <?php elseif ($mode=='edit'): ?>
                                                    <label class="mt-radio mt-radio-outline">
                                                        <input type="radio" name="gender" id="optionsRadios22" value="L" <?php echo @$dt->gender=='L' ? 'checked' : '' ?>> Laki-Laki
                                                        <span></span>
                                                    </label>
                                                <?php endif; ?>
                                                <label class="mt-radio mt-radio-outline">
                                                    <input type="radio" name="gender" id="optionsRadios23" value="P" <?php echo @$dt->gender=='P' ? 'checked' : '' ?>> Perempuan
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Telepon</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="phone" value="<?php echo @$dt->phone ?>" placeholder="Telepon">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Alamat</label>
                                        <div class="col-md-6">
                                            <textarea class="form-control" name="address" placeholder="Alamat"><?php echo @$dt->address ?></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group product_id">
                                        <label class="col-md-3 control-label">Product</label>
                                        <div class="col-md-6">
                                            <select class="form-control select2" name="product_id" style="width:100%">
                                                <option></option>
                                                <?php $i = 0 ?>
                                                <?php foreach ($product as $r): ?>
                                                    <?php
                                                    $i++;
                                                    if ($mode=='add') {
                                                        $terpilih = $i==1 ? 'selected' : '';
                                                    } elseif ($mode=='edit') {
                                                        $terpilih = $r->id==@$dt->product_id ? 'selected' : '';
                                                    }
                                                    ?>
                                                    <?php echo '<option value="'.$r->id.'" '.$terpilih.'>'.$r->nama.'</option>' ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">ID Barcode *</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="card_barcode" value="<?php echo @$dt->card_barcode ?>" placeholder="Barcode" <?php echo $mode=='edit' ? 'readonly' : '' ?>>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Email *</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="email" value="<?php echo @$dt->email ?>" placeholder="Email">
                                            <span class="help-block">Email ini juga sebagai Username</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Password <?php echo $mode=='add' ? '*' : '' ?></label>
                                        <div class="col-md-6">
                                            <input type="password" class="form-control" name="password" placeholder="Password">
                                            <?php if ($mode=='edit'): ?>
                                                <span class="help-block"> Biarkan kosong jika tidak ingin diubah. </span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group ">
                                        <div class="col-md-12">
                                            <?php if (@$dt->photo != null && file_exists('./uploads/user/'.@$dt->photo)): ?>
                                                <img style="width: 200px; height: 150px;" src="<?php echo base_url().'uploads/user/'.@$dt->photo ?>">
                                                <br>
                                                <br>
                                            <?php endif; ?>
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                                </div>
                                                <div>
                                                    <span class="btn red btn-outline btn-file">
                                                        <span class="fileinput-new"> Pilih Foto </span>
                                                        <span class="fileinput-exists"> Ganti </span>
                                                        <input type="file" name="photo"> </span>
                                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Hapus </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                			</div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Submit</button>
                                        <a href="<?php echo site_url('admin/user') ?>"><button type="button" class="btn default">Batal</button></a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>

<div id="modal_form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<span id="site_url" data-site-url="<?php echo site_url() ?>"></span>
<script src="<?php echo base_url() ?>assets/custom/scripts/j_generalForm.js" type="text/javascript"></script>
<script type="text/javascript">
$.fn.select2.defaults.set( "theme", "bootstrap" );
$('.select2, .select2-multiple').select2({
    placeholder: 'Pilih',
});

$('.datepicker').datepicker({
    rtl: App.isRTL(),
    autoclose: true
});

function get_community() {
    var member_jenis = $('#member_jenis').val();
    $('.select2member').load("<?php echo site_url('admin/user/get_community_member/') ?>"+member_jenis, function() {

    });
}

var mode = <?php echo json_encode($mode) ?>;
if (mode=='add') {
    get_community();
}
$('#member_jenis').change(function(e) {
    e.preventDefault();
    get_community();
});

</script>
