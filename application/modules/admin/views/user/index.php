<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <div class="page-toolbar">
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">
                                <?php echo $judul ?>
                                <a href="<?php echo site_url('admin/user/create_member') ?>">
                                    <button type="button" class="btn btn-outline btn-sm blue"><i class="fa fa-plus"></i> Tambah Member</button>
                                </a>
                                <a href="<?php echo site_url('admin/user/create_trainer') ?>">
                                    <button type="button" class="btn btn-outline btn-sm blue"><i class="fa fa-plus"></i> Tambah Trainer</button>
                                </a>
                            </span>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <?php if ($this->session->flashdata('notif_type')): ?>
                            <div class="alert <?php echo $this->session->flashdata('notif_type') ?> alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <?php echo $this->session->flashdata('notif_msg'); ?>
                            </div>
                        <?php endif; ?>
                        <div class="table-container">
                            <div class="table-actions-wrapper">
                                <span> </span>
                                <select class="table-group-action-input form-control input-inline input-small input-sm">
                                    <option value="">Pilih</option>
                                    <option value="aktifkan_checked">Aktifkan</option>
                                </select>
                                <button class="btn btn-sm green table-group-action-submit">
                                    <i class="fa fa-check"></i> Submit
                                </button>
                            </div>
                            <table class="table table-striped table-bordered table-hover dt-responsive" id="mytable">
                                <thead>
                                    <tr role="row" class="heading">
                                        <th width="2%">
                                            <input type="checkbox" class="group-checkable">
                                        </th>
                                        <th width="8%">Level</th>
                                        <th>Jenis</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Active</th>
                                        <th>Last Login</th>
                                        <th>Action</th>
                                    </tr>
                                    <tr role="row" class="filter">
                                        <td></td>
                                        <td>
                                            <select class="form-control form-filter input-sm" name="level_id" style="padding:0px;width:100%">
                                                <option value="">All</option>
                                                <?php foreach ($levels as $r): ?>
                                                    <?php echo '<option value="'.$r->id.'">'.$r->nama.'</option>' ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="form-control form-filter input-sm" name="member_jenis" style="padding:0px;width:100%">
                                                <option value="">All</option>
                                                <?php foreach ($member_jenis as $r): ?>
                                                    <?php echo '<option value="'.$r.'">'.$r.'</option>' ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="first_name"></td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="email"></td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="phone"></td>
                                        <td>
                                            <select class="form-control form-filter input-sm" name="active" style="padding:0px;width:100%">
                                                <option value="">All</option>
                                                <option value="1">1</option>
                                                <option value="0">0</option>
                                            </select>
                                        </td>
                                        <td>
                                            <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                                <input type="text" class="form-control form-filter input-sm" id="last_login_start" readonly="" name="last_login_start" placeholder="From">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-sm default" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                            </div>
                                            <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                                <input type="text" class="form-control form-filter input-sm" id="last_login_end" readonly="" name="last_login_end" placeholder="To">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-sm default" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="margin-bottom-5">
                                                <button class="btn btn-xs green btn-outline filter-submit margin-bottom">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                            <button class="btn btn-xs red btn-outline filter-cancel">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>

<div id="modal_form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<span id="site_url" data-site-url="<?php echo site_url() ?>"></span>
<!-- <div id="modal_view_kunjungan" class="modal fade bs-modal-lg" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div> -->
<script type="text/javascript">

$('.date-picker').datepicker({
    rtl: App.isRTL(),
    autoclose: true
});

$('#last_login_start').change(function(e) {
    var last_login_start = $('#last_login_start').val();
    var from_arr = last_login_start.split("/");
    var from = new Date(from_arr[2], from_arr[1]-1, from_arr[0]);

    var last_login_end = $('#last_login_end').val();
    var to_arr = last_login_end.split("/");
    var to = new Date(to_arr[2], to_arr[1]-1, to_arr[0]);

    if (from > to) {
        $('#last_login_end').val(last_login_start);
    }
});

$('#last_login_end').change(function(e) {
    var last_login_start = $('#last_login_start').val();
    var from_arr = last_login_start.split("/");
    var from = new Date(from_arr[2], from_arr[1]-1, from_arr[0]);

    var last_login_end = $('#last_login_end').val();
    var to_arr = last_login_end.split("/");
    var to = new Date(to_arr[2], to_arr[1]-1, to_arr[0]);

    if (to < from) {
        $('#last_login_start').val(last_login_end);
    }
});


var TableDatatablesAjax = function () {
    var grid = new Datatable();
    grid.setDefaultParam("<?php echo $csrf_name ?>", '<?php echo $csrf ?>');
    grid.setAjaxParam('level_id'                , $('[name =level_id]').val());
    grid.setAjaxParam('member_jenis'              , $('[name =member_jenis]').val());
    grid.setAjaxParam('first_name'                    , $('[name =first_name]').val());
    grid.setAjaxParam('email'        , $('[name =email]').val());
    grid.setAjaxParam('phone'          , $('[name =phone]').val());
    grid.setAjaxParam('active'                   , $('[name =active]').val());
    grid.setAjaxParam('last_login_start'         , $('[name =last_login_start]').val());
    grid.setAjaxParam('last_login_end'           , $('[name =last_login_end]').val());
    grid.init({
        src: $("#mytable"),
        dataTable: {
            "ajax": {
                "url": "<?php echo site_url('admin/user/getDatatable/') ?>" // ajax source
            },
            "order": [
                [6, "asc"]
            ]// set first column as a default sort by asc
        }
    });
    grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
        e.preventDefault();

        var action = $(".table-group-action-input", grid.getTableWrapper());
        if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
            swal(
                {
                    title: "Apakah Anda yakin?",
                    text: "Semua data user yang terpilih akan diaktifkan.",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonClass: "btn-default",
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Ya, Aktifkan!",
                    closeOnConfirm: true
                },
                function(){
                    grid.setAjaxParam("customActionType", "group_action");
                    grid.setAjaxParam("customActionName", action.val());
                    grid.setAjaxParam("id", grid.getSelectedRows());
                    grid.getDataTable().ajax.reload();
                    grid.clearAjaxParams();
                    grid.getDataTable().ajax.reload();
                }
            );
        } else if (action.val() == "") {
            App.alert({
                type: 'danger',
                icon: 'warning',
                message: 'Please select an action',
                container: grid.getTableWrapper(),
                place: 'prepend'
            });
        } else if (grid.getSelectedRowsCount() === 0) {
            App.alert({
                type: 'danger',
                icon: 'warning',
                message: 'No record selected',
                container: grid.getTableWrapper(),
                place: 'prepend'
            });
        }
    });
}
jQuery(document).ready(function() {
   TableDatatablesAjax();
});

function btn_view(id) {
    $(".portlet").LoadingOverlay("show");
	$("#modal_form").load('<?php echo site_url(); ?>/data-input/read/'+id,function() {
		$(this).modal("show");
        $(".portlet").LoadingOverlay("hide");
	});
}

function btn_delete(id) {
    swal(
        {
            title: "Apakah Anda yakin?",
            text: "Semua data yang berkaitan dengan data user akan terhapus.",
            type: "warning",
            showCancelButton: true,
            cancelButtonClass: "btn-default",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, Delete it!",
            closeOnConfirm: true
        },
        function(){
            $.post('<?php echo site_url('admin/user/delete/') ?>'+id, {'<?php echo $csrf_name ?>':'<?php echo $csrf ?>'}, function(res) {
                if (res.success) {
                    $('.filter-cancel').trigger('click');
                    toastr.success(res.message);
                } else {
                    toastr.error(res.message);
                }
            });
        }
    );
}

function btn_aktifkan(id, aksi) {
    if (aksi==1) {
        var text = 'User ini akan diaktifkan.';
        var icon = 'info';
    } else {
        var text = 'User ini akan dinon-aktifkan.';
        var icon = 'warning';
    }
    swal(
        {
            title: "Apakah Anda yakin?",
            text: text,
            type: icon,
            showCancelButton: true,
            cancelButtonClass: "btn-default",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ya, lakukan!",
            closeOnConfirm: true
        },
        function(){
            $.post('<?php echo site_url('admin/user/aktifkan') ?>', {'<?php echo $csrf_name ?>':'<?php echo $csrf ?>', id:id, aksi:aksi}, function(res) {
                if (res.success) {
                    $('.filter-cancel').trigger('click');
                    toastr.success(res.message);
                } else {
                    $('.filter-cancel').trigger('click');
                    toastr.error(res.message);
                }
            });
        }
    );
}

function btn_send_activation(id) {
    $(".portlet").LoadingOverlay("show");
    $.post('<?php echo site_url('admin/user/send_activation') ?>', {'<?php echo $csrf_name ?>':'<?php echo $csrf ?>',id:id}, function(res) {
        $(".portlet").LoadingOverlay("hide");
        if (res.success) {
            $('.filter-cancel').trigger('click');
            toastr.success(res.message);
        } else {
            toastr.error(res.message);
        }
    });
}

</script>
