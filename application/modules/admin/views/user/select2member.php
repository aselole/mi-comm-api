<div class="form-group">
    <label class="col-md-3 control-label">Community *</label>
    <div class="col-md-6 ">
        <select class="form-control select2 " name="community_id" style="width:100%" <?php echo $mode=='edit' ? 'disabled' : '' ?>>
            <option></option>
            <?php foreach ($communities as $r): ?>
                <?php echo '<option value="'.$r->id.'">'.$r->nama.' - '.$r->ket.'</option>' ?>
            <?php endforeach; ?>
        </select>
    </div>
</div>

<script type="text/javascript">
$('.select2').select2({
    placeholder: 'Silahkan Pilih'
});
</script>
