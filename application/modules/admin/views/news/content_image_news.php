<div class="panel-group accordion" id="accordion1">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_2" aria-expanded="false"> Gambar </a>
            </h4>
        </div>
        <div id="collapse_2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
                <?php if ($image_news): ?>
                    <?php foreach ($image_news as $img): ?>
                        <?php if ($img->image != null && file_exists('./uploads/news/'.@$img->image)): ?>
                            <img style="width: 300px; height: 250px;" src="<?php echo base_url().'uploads/news/'.$img->image ?>">
                            <br>
                            Cover : (<?php echo $img->is_featured ?>)
                            <?php if ($img->is_featured!='y'): ?>
                                <button type="button" class="btn btn-success btn-outline" onclick="event.preventDefault(); btn_edit_image_news(<?php echo $img->id ?>, 'y');">make cover</button>
                            <?php endif; ?>
                            <button type="button" class="btn btn-danger btn-outline" onclick="event.preventDefault(); btn_delete_image_news(<?php echo $img->id ?>);">delete</button>
                            <br>
                            <br>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
function btn_delete_image_news(id) {
    $.post('<?php echo site_url('admin/news/delete_image') ?>', {'<?php echo $csrf_name ?>':'<?php echo $csrf ?>', id:id}, function(res) {
        if (res.success) {
            toastr.success(res.message);
            reload_content_img();
        } else {
            toastr.error(res.message);
        }
    });
}
function btn_edit_image_news(id, is_featured) {
    $.post('<?php echo site_url('admin/news/edit_image') ?>', {'<?php echo $csrf_name ?>':'<?php echo $csrf ?>', id:id, is_featured:is_featured}, function(res) {
        if (res.success) {
            toastr.success(res.message);
            reload_content_img();
        } else {
            toastr.error(res.message);
        }
    });
}
</script>
