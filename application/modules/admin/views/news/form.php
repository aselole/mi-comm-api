<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <div class="page-toolbar">
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">
                                <?php echo $judul ?>

                            </span>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <form class="form-horizontal form_img" method="<?php echo $method ?>" action="<?php echo site_url('admin/news/upload_img') ?>" enctype="multipart/form-data">
                            <?php echo form_hidden($csrf_name, $csrf); ?>
                            <input type="hidden" id="news_id" value="0">
                            <div class="form-body">
                                <div class="col-md-12">
                                    <div class="form-group ">
                                        <label class="col-md-2 control-label">Gambar</label>
                                        <div class="col-md-8">
                                            <span class="help-block">Best View 345 x 255 pixel</span>
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 300px; height: 250px;">
                                                </div>
                                                <div>
                                                    <span class="btn red btn-outline btn-file">
                                                        <span class="fileinput-new"> Pilih Gambar </span>
                                                        <span class="fileinput-exists"> Ganti </span>
                                                        <input type="file" id="featured_img" name="featured_img"> </span>
                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Hapus </a>
                                                    </span>
                                                </div>
                                                <br>
                                                <button type="submit" class="btn green">Upload</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-8">
                                            <div class="content_img">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <form class="form-horizontal form_ajax" method="<?php echo $method ?>" action="<?php echo $action ?>">
                            <?php echo form_hidden($csrf_name, $csrf); ?>
                            <input type="hidden" id="id" value="0">
                			<div class="form-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">For *</label>
                                        <div class="col-md-6">
                                            <select name="for_whom" class="form-control select2" >
                                                <option></option>
                                                <?php foreach ($ar_for_whom as $x): ?>
                                                    <option value="<?php echo $x ?>" <?php echo @$dt->for_whom==$x ? 'selected' : '' ?>><?php echo strtoupper($x) ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Judul *</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="judul" value="<?php echo @$dt->judul ?>" placeholder="Judul">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Body *</label>
                                        <div class="col-md-8">
                                            <textarea name="body" id="summernote_1" rows="8" class="form-control"><?php echo @$dt->body ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Tgl Publish *</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control datepicker" name="published_at" data-date-format="dd MM yyyy" value="<?php echo (@$dt->published_at) ? date('d F Y', strtotime(@$dt->published_at)) : '' ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Aktif *</label>
                                        <div class="col-md-3">
                                            <select class="form-control select2" name="active">
                                                <option></option>
                                                <option value="y" <?php echo @$dt->active=='y' ? 'selected' : '' ?>>Ya</option>
                                                <option value="t" <?php echo @$dt->active=='t' ? 'selected' : '' ?>>Tidak</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                			</div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Submit</button>
                                        <a href="<?php echo site_url('admin/news') ?>"><button type="button" class="btn default">Kembali</button></a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>

<div id="modal_form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<span id="site_url" data-site-url="<?php echo site_url() ?>"></span>
<script src="<?php echo base_url() ?>assets/custom/scripts/j_formNews.js" type="text/javascript"></script>

<script type="text/javascript">
$.fn.select2.defaults.set( "theme", "bootstrap" );
$('.select2').select2({
    placeholder: 'Silahkan Pilih'
});
$('.datepicker').datepicker({
    rtl: App.isRTL(),
    autoclose: true
});

$('#summernote_1').summernote({
    height: 300,
    toolbar: [
            [ 'style', [ 'style' ] ],
            [ 'font', [ 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear'] ],
            [ 'fontname', [ 'fontname' ] ],
            [ 'fontsize', [ 'fontsize' ] ],
            [ 'color', [ 'color' ] ],
            [ 'para', [ 'ol', 'ul', 'paragraph', 'height' ] ],
            [ 'table', [ 'table' ] ],
            [ 'insert', [ 'link'] ],
            [ 'view', [ 'undo', 'redo', 'fullscreen', 'codeview', 'help' ] ]
        ]
});

var submitUpload = function(formObj,options={}) {
    var btnObj = formObj.find('button[type=submit]');
    if(formObj.attr('enctype')=="multipart/form-data"){
        var formData = new FormData(formObj[0]);
        options['cache'] = false;
        options['contentType'] = false;
        options['processData'] = false;
    }else{
        var formData = formObj.serialize();
    }

    var news_id = $('#news_id').val();
    formData.append("news_id", news_id);

    $(".help-block-error" , formObj).remove();
    $(".form-group" , formObj).removeClass('has-error');
    // default settings
    options = $.extend(true, {
        url: formObj.attr('action'),
        dataType: "json",
        data: formData,
        type: formObj.attr('method'),

        beforeSend: function (e) {
            btnObj.button('loading');
        },
        error: function (e) {
            console.log(e);
            if (e.status == 400){
                toastr.error(e.responseJSON.message);
                form_set_errors(e.responseJSON.errors,formObj);
            }else{
                toastr.error('Maaf, telah terjadi kesalahan.');
            }
        },
        success: function(response) {
            // console.log(response);
            if (response.success) {
                $('input[name=featured_img]').val('');
                $('.fileinput-exists').trigger('click');
                toastr.success(response.message);
                $('#news_id').val(response.news_id);
                $('#id').val(response.news_id);
                reload_content_img();
            } else {
                toastr.error(response.message);
                $('input[name=file]').val('');
            }
        },
        complete:function (e) {
            btnObj.button('reset');
        }
    }, options);

    $.ajax(
        options
    );
}

$('.form_img').submit(function(e) {
    e.preventDefault();
    submitUpload($(this));
});

function reload_content_img() {
    var news_id = $('#news_id').val();
    $('.content_img').load('<?php echo site_url('admin/news/get_image_news/') ?>'+news_id, function() {

    });
}

var mode = <?php echo json_encode($mode) ?>;
if (mode=='edit') {
    var news_id = <?php echo json_encode(@$dt->id) ?>;
    $('#news_id').val(news_id);
    $('#id').val(news_id);

    reload_content_img();
}
</script>
