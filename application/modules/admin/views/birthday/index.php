<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <div class="page-toolbar">
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">
                                <?php echo $judul ?>
                            </span>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" id="mytable">
                            <thead>
                                <tr role="row" class="heading">
                                    <th width="2%">No </th>
                                    <th>Nama</th>
                                    <th>Community</th>
                                    <th>Perusahaan</th>
                                    <th>Next Birthday</th>
                                    <th>Kurang</th>
                                    <th>Yang Ke</th>
                                    <th width="2%">Aksi</th>
                                </tr>
                                <tr role="row" class="filter">
                                    <td></td>
                                    <td><input type="text" class="form-control form-filter input-sm" name="first_name"></td>
                                    <td><input type="text" class="form-control form-filter input-sm" name="community_nama"></td>
                                    <td><input type="text" class="form-control form-filter input-sm" name="company_nama"></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <!-- <td>
                                        <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                            <input type="text" class="form-control form-filter input-sm" id="next_birthday_start" readonly="" name="next_birthday_start" placeholder="From">
                                            <span class="input-group-btn">
                                                <button class="btn btn-sm default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                            <input type="text" class="form-control form-filter input-sm" id="next_birthday_end" readonly="" name="next_birthday_end" placeholder="To">
                                            <span class="input-group-btn">
                                                <button class="btn btn-sm default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </td>
                                    <td><input type="text" class="form-control form-filter input-sm" name="days"></td>
                                    <td><input type="text" class="form-control form-filter input-sm" name="yang_ke"></td> -->
                                    <td>
                                        <div class="margin-bottom-5">
                                            <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                                <i class="fa fa-search"></i> Search
                                            </button>
                                        </div>
                                        <button class="btn btn-sm red btn-outline filter-cancel">
                                            <i class="fa fa-times"></i> Reset
                                        </button>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>

<div id="modal_form" class="modal fade bs-modal-lg" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<span id="site_url" data-site-url="<?php echo site_url() ?>"></span>

<script type="text/javascript">
$('.date-picker').datepicker({
    rtl: App.isRTL(),
    autoclose: true
});

$('#next_birthday_start').change(function(e) {
    var next_birthday_start = $('#next_birthday_start').val();
    var from_arr = next_birthday_start.split("/");
    var from = new Date(from_arr[2], from_arr[1]-1, from_arr[0]);

    var next_birthday_end = $('#next_birthday_end').val();
    var to_arr = next_birthday_end.split("/");
    var to = new Date(to_arr[2], to_arr[1]-1, to_arr[0]);

    if (from > to) {
        $('#next_birthday_end').val(next_birthday_start);
    }
});
$('#next_birthday_end').change(function(e) {
    var next_birthday_start = $('#next_birthday_start').val();
    var from_arr = next_birthday_start.split("/");
    var from = new Date(from_arr[2], from_arr[1]-1, from_arr[0]);

    var next_birthday_end = $('#next_birthday_end').val();
    var to_arr = next_birthday_end.split("/");
    var to = new Date(to_arr[2], to_arr[1]-1, to_arr[0]);

    if (to < from) {
        $('#next_birthday_start').val(next_birthday_end);
    }
});

var TableDatatablesAjax = function () {
    var grid = new Datatable();
    grid.init({
        src: $("#mytable"),
        dataTable: {
            "processing": true,
            "ajax": {
                "url": "<?php echo site_url('admin/birthday/ajax_list/') ?>", // ajax source
                data: function (d) {
                    d.<?php echo $csrf_name ?>       = '<?php echo $csrf ?>';
                    d.first_name                 = $('[name=first_name]').val();
                    d.community_nama                 = $('[name=community_nama]').val();
                    d.company_nama                 = $('[name=company_nama]').val();
                    // d.next_birthday_start = $('[name=next_birthday_start]').val();
                    // d.next_birthday_end   = $('[name=next_birthday_end]').val();
                    // d.days                = $('[name=days]').val();
                    // d.yang_ke                = $('[name=yang_ke]').val();
                }
            },
            "order": [
                [1, "asc"]
            ],// set first column as a default sort by asc

        }
    });
}
jQuery(document).ready(function() {
   TableDatatablesAjax();
});

function btn_edit(id) {
    $('#modal_form').load('<?php echo site_url() ?>admin/birthday/edit_gift/'+id, function() {
        $(this).modal('show');
    });
}


</script>
