<link href="<?php echo base_url() ?>assets/pages/css/blog.min.css" rel="stylesheet" type="text/css" />
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <div class="page-toolbar">
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="blog-page blog-content-2">
            <a href="<?php echo site_url('admin/private_issue') ?>">
                <button type="button" class="btn btn-outline dark"><i class="fa fa-arrow-left"></i> Kembali</button>
            </a>
            <button type="button" class="btn btn-outline green" onclick="event.preventDefault(); btn_edit(<?php echo $dt->id ?>);"><i class="fa fa-edit"></i> Edit</button>
            <img src="<?php echo base_url('assets/global/img/loading-spinner-blue.gif') ?>" class="img-loading display-hide">
            <div class="row">
                <div class="col-lg-9">
                    <div class="blog-single-content bordered blog-container">
                        <div class="blog-single-head">
                            <h1 class="blog-single-head-title"><?php echo $dt->judul ?> </h1>
                            <div class="blog-single-head-date">
                                <i class="icon-calendar font-blue"></i>
                                <a href="javascript:;"><?php echo date('d F Y, H:i', strtotime($dt->created_at)) ?></a>
                                <br>
                                <i class="fa fa-user font-blue"></i>
                                <a href="javascript:;"><?php echo $dt->member_first_name ?></a>
                                <br>
                                <i class="fa fa-user-md font-blue"></i>
                                <a href="javascript:;"><?php echo $dt->trainer_first_name ?></a>
                                <br>
                                <a href="javascript:;">Status: <?php echo $dt->status ?></a>
                            </div>
                        </div>
                        <br>
                        <div class="blog-single-desc">
                            <p>
                                <?php echo $dt->body ?>
                            </p>
                        </div>
                        <div class="blog-comments">
                            <?php $jml_komen = $comments ? count($comments) : 0 ?>
                            <h3 class="sbold blog-comments-title">Komentar (<?php echo $jml_komen ?>)</h3>
                            <div class="c-comment-list">
                                <?php if ($comments): ?>
                                    <div class="scroller" style="height: 525px;" data-always-visible="1" data-rail-visible1="1">
                                        <ul class="chats">
                                            <?php foreach ($comments as $r): ?>
                                                <?php
                                                $photo = base_url('assets/pages/media/profile/avatar.png');
                                                if ($r->user_photo && file_exists('./uploads/user/'.$r->user_photo)) {
                                                    $photo = base_url('uploads/user/'.$r->user_photo);
                                                }

                                                $read = '<span class="label label-sm label-warning">unread</span>';
                                                if ($r->read == 1) {
                                                    $read = '<span class="label label-sm label-success">read</span>';
                                                }
                                                ?>
                                                <li class="<?php echo $r->user_level_id==2 ? 'in' : 'out' ?>">
                                                    <img class="avatar" alt="" src="<?php echo $photo ?>" />
                                                    <div class="message">
                                                        <span class="arrow"> </span>
                                                        <a href="javascript:;" class="name"> <?php echo $r->user_first_name ?> </a>
                                                        <span class="datetime"> at <?php echo date('d/m/Y H:i', strtotime($r->created_at)) ?> <?php echo $read ?></span>
                                                        <span class="body"> <?php echo $r->comment ?> </span>
                                                    </div>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>

<div id="modal_form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<span id="site_url" data-site-url="<?php echo site_url() ?>"></span>

<script type="text/javascript">
function btn_edit(id) {
    $('.img-loading').show();
    $('#modal_form').load('<?php echo site_url('admin/private_issue/edit/') ?>'+id, function() {
        $(this).modal('show');
        $('.img-loading').hide();
    });
}

$('.btn_delete').click(function(e) {
    e.preventDefault();
    var comment_id = $(this).data('comment-id');
    var parent = $(this).data('parent');
    var msg = 'Komentar ini akan dihapus.';
    if (parent=='y') {
        msg = 'Komentar ini dan balasannya akan terhapus.';
    }
    swal(
        {
            title: "Apakah Anda yakin?",
            text: msg,
            type: "warning",
            showCancelButton: true,
            cancelButtonClass: "btn-default",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, Delete it!",
            closeOnConfirm: true
        },
        function(){
            $.post('<?php echo site_url('admin/issue/delete_comment') ?>', {<?php echo $csrf_name ?>: '<?php echo $csrf ?>', comment_id:comment_id}, function(res) {
                if (res.success) {
                    $('.filter-cancel').trigger('click');
                    toastr.success(res.message);
                    location.reload();
                } else {
                    toastr.error(res.message);
                }
            });
        }
    );
});

</script>
