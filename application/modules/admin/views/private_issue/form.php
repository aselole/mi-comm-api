<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" onclick="event.preventDefault(); closeModal();"></button>
			<h4 class="modal-title"><?php echo $judul ?></h4>
		</div>
		<div class="modal-body">
            <form class="form-horizontal form_ajax" method="<?php echo $method ?>" action="<?php echo $action ?>">
                <?php echo form_hidden($csrf_name, $csrf); ?>
    			<div class="form-body">
                    <div class="form-group">
						<label class="col-md-3 control-label">Status *</label>
                        <div class="col-md-4">
							<select class="form-control select2" name="status">
								<option></option>
								<?php foreach ($status as $r): ?>
									<?php $terpilih = $r==@$dt->status ? 'selected' : '' ?>
									<?php echo '<option value="'.$r.'" '.$terpilih.'>'.$r.'</option>' ?>
								<?php endforeach; ?>
							</select>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-md-3 control-label">Aktif *</label>
                        <div class="col-md-4">
							<select class="form-control select2" name="active">
								<option></option>
								<option value="y" <?php echo @$dt->active=='y' ? 'selected' : '' ?>>Y</option>
								<option value="t" <?php echo @$dt->active=='t' ? 'selected' : '' ?>>T</option>
							</select>
                        </div>
                    </div>
    			</div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn green">Submit</button>
                            <button type="button" class="btn default" onclick="event.preventDefault(); closeModal();">Cancel</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</div>
</div>
<script src="<?php echo base_url() ?>assets/custom/scripts/j_generalFormModal.js" type="text/javascript"></script>
<script type="text/javascript">
// begin - select2 modal wajib
$.fn.modal.Constructor.prototype.enforceFocus = function() {};
// end - select2 modal wajib

$('.select2').select2({
    placeholder: 'Silahkan Pilih'
});
</script>
