<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <div class="page-toolbar">
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">
                                <?php echo $judul ?>
                            </span>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" id="mytable">
                            <thead>
                                <tr role="row" class="heading">
                                    <th width="2%">No </th>
                                    <th>Community</th>
                                    <th>Issue</th>
                                    <th>Komentar</th>
                                    <th>Tgl Komen</th>
                                    <th>Pelapor</th>
                                    <th width="2%">Aksi</th>
                                </tr>
                                <tr role="row" class="filter">
                                    <td></td>
                                    <td><input type="text" class="form-control form-filter input-sm" name="community_nama"></td>
                                    <td><input type="text" class="form-control form-filter input-sm" name="issue_judul"></td>
                                    <td><input type="text" class="form-control form-filter input-sm" name="comment"></td>
                                    <td>
                                        <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                            <input type="text" class="form-control form-filter input-sm" id="issue_comment_created_at_start" readonly="" name="issue_comment_created_at_start" placeholder="From">
                                            <span class="input-group-btn">
                                                <button class="btn btn-sm default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                            <input type="text" class="form-control form-filter input-sm" id="issue_comment_created_at_end" readonly="" name="issue_comment_created_at_end" placeholder="To">
                                            <span class="input-group-btn">
                                                <button class="btn btn-sm default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </td>
                                    <td><input type="text" class="form-control form-filter input-sm" name="user_first_name"></td>
                                    <td>
                                        <div class="margin-bottom-5">
                                            <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                                <i class="fa fa-search"></i> Search
                                            </button>
                                        </div>
                                        <button class="btn btn-sm red btn-outline filter-cancel">
                                            <i class="fa fa-times"></i> Reset
                                        </button>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>

<div id="modal_form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<span id="site_url" data-site-url="<?php echo site_url() ?>"></span>

<script type="text/javascript">
$('.date-picker').datepicker({
    rtl: App.isRTL(),
    autoclose: true
});

$('#issue_comment_created_at_start').change(function(e) {
    var issue_comment_created_at_start = $('#issue_comment_created_at_start').val();
    var from_arr = issue_comment_created_at_start.split("/");
    var from = new Date(from_arr[2], from_arr[1]-1, from_arr[0]);

    var issue_comment_created_at_end = $('#issue_comment_created_at_end').val();
    var to_arr = issue_comment_created_at_end.split("/");
    var to = new Date(to_arr[2], to_arr[1]-1, to_arr[0]);

    if (from > to) {
        $('#issue_comment_created_at_end').val(issue_comment_created_at_start);
    }
});
$('#issue_comment_created_at_end').change(function(e) {
    var issue_comment_created_at_start = $('#issue_comment_created_at_start').val();
    var from_arr = issue_comment_created_at_start.split("/");
    var from = new Date(from_arr[2], from_arr[1]-1, from_arr[0]);

    var issue_comment_created_at_end = $('#issue_comment_created_at_end').val();
    var to_arr = issue_comment_created_at_end.split("/");
    var to = new Date(to_arr[2], to_arr[1]-1, to_arr[0]);

    if (to < from) {
        $('#issue_comment_created_at_start').val(issue_comment_created_at_end);
    }
});

var TableDatatablesAjax = function () {
    var grid = new Datatable();
    grid.init({
        src: $("#mytable"),
        dataTable: {
            "processing": true,
            "ajax": {
                "url": "<?php echo site_url('admin/bad_comment/ajax_list/') ?>", // ajax source
                data: function (d) {
                    d.<?php echo $csrf_name ?>       = '<?php echo $csrf ?>';
                    d.community_nama                 = $('[name=community_nama]').val();
                    d.issue_judul                    = $('[name=issue_judul]').val();
                    d.comment                        = $('[name=comment]').val();
                    d.issue_comment_created_at_start = $('[name=issue_comment_created_at_start]').val();
                    d.issue_comment_created_at_end   = $('[name=issue_comment_created_at_end]').val();
                    d.user_first_name                = $('[name=user_first_name]').val();
                }
            },
            "order": [
                [1, "asc"]
            ],// set first column as a default sort by asc

        }
    });
}
jQuery(document).ready(function() {
   TableDatatablesAjax();
});

function btn_delete(id) {
    swal(
        {
            title: "Apakah Anda yakin?",
            text: "Komentar ini akan terhapus.",
            type: "warning",
            showCancelButton: true,
            cancelButtonClass: "btn-default",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, Delete it!",
            closeOnConfirm: true
        },
        function(){
            $.post('<?php echo site_url('admin/bad_comment/delete/') ?>'+id, {<?php echo $csrf_name ?>: '<?php echo $csrf ?>'}, function(res) {
                if (res.success) {
                    $('.filter-cancel').trigger('click');
                    toastr.success(res.message);
                } else {
                    toastr.error(res.message);
                }
            });
        }
    );
}

function btn_abaikan(id) {
    swal(
        {
            title: "Abaikan Laporan Komentar Buruk?",
            text: "Laporan tentang komentar ini akan diabaikan.",
            type: "info",
            showCancelButton: true,
            cancelButtonClass: "btn-default",
            confirmButtonClass: "btn-info",
            confirmButtonText: "Abaikan laporan ini!",
            closeOnConfirm: true
        },
        function(){
            $.post('<?php echo site_url('admin/bad_comment/abaikan/') ?>'+id, {<?php echo $csrf_name ?>: '<?php echo $csrf ?>'}, function(res) {
                if (res.success) {
                    $('.filter-cancel').trigger('click');
                    toastr.success(res.message);
                } else {
                    toastr.error(res.message);
                }
            });
        }
    );
}

</script>
