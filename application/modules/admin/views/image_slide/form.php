<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <div class="page-toolbar">
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">
                                <?php echo $judul ?>
                            </span>
                        </div>
                        <div class="tools">
                        </div>
                    </div>
                    <div class="portlet-body">
                        <form class="form-horizontal form_ajax" method="<?php echo $method ?>" action="<?php echo $action ?>" enctype="multipart/form-data">
                            <?php echo form_hidden($csrf_name, $csrf); ?>
                            <div class="form-body">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">file *</label>
                                        <div class="col-md-6">
                                            <span class="help-block">Best View 1350 x 370 pixel</span>
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                                    <?php if (@$dt->file != null && file_exists('./uploads/image_slide/'.@$dt->file)): ?>
                                                        <img style="width: 200px; height: 150px;" src="<?php echo base_url().'uploads/image_slide/'.@$dt->file ?>">
                                                        <br>
                                                        <br>
                                                    <?php endif; ?>
                                                </div>
                                                <div>
                                                    <span class="btn red btn-outline btn-file">
                                                        <span class="fileinput-new"> Pilih Gambar </span>
                                                        <span class="fileinput-exists"> Ganti </span>
                                                        <input type="file" name="file"> </span>
                                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Hapus </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Order *</label>
                                        <div class="col-md-6">
                                            <input type="number" class="form-control" name="order" placeholder="order" value="<?php echo @$dt->order ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Publish *</label>
                                        <div class="col-md-6">
                                            <div class="mt-radio-list">
                                                <?php if ($mode=='add'): ?>
                                                <label class="mt-radio mt-radio-outline">
                                                    <input type="radio" name="publish" id="optionsRadios22" value="y" checked> Ya
                                                    <span></span>
                                                </label>
                                                <?php elseif ($mode=='edit'): ?>
                                                <label class="mt-radio mt-radio-outline">
                                                    <input type="radio" name="publish" id="optionsRadios22" value="y" <?php echo @$dt->publish=='y' ? 'checked' : '' ?>> Ya
                                                    <span></span>
                                                </label>
                                                <?php endif; ?>
                                                <label class="mt-radio mt-radio-outline">
                                                    <input type="radio" name="publish" id="optionsRadios23" value="t" <?php echo @$dt->publish=='t' ? 'checked' : '' ?>> Tidak
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Submit</button>
                                        <a href="<?php echo site_url('admin/image_slide') ?>"><button type="button" class="btn default">Batal</button></a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<div id="modal_form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<span id="site_url" data-site-url="<?php echo site_url() ?>"></span>
<script src="<?php echo base_url() ?>assets/custom/scripts/j_generalForm.js" type="text/javascript"></script>
