<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <div class="page-toolbar">
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">
                                <?php echo $judul ?>
                                <a href="<?php echo site_url('admin/image_slide/create') ?>">
                                    <button type="button" class="btn btn-outline btn-sm blue"><i class="fa fa-plus"></i> Tambah</button>
                                </a>
                            </span>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <?php if ($this->session->flashdata('notif_type')): ?>
                            <div class="alert <?php echo $this->session->flashdata('notif_type') ?> alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <?php echo $this->session->flashdata('notif_msg'); ?>
                            </div>
                        <?php endif; ?>
                        <table class="table table-striped table-bordered table-hover dt-responsive" id="mytable">
                            <thead>
                                <tr role="row" class="heading">
                                    <th width="2%">No </th>
                                    <th>File</th>
                                    <th>Order</th>
                                    <th>Publish</th>
                                    <th width="10%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>

<div id="modal_form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<span id="site_url" data-site-url="<?php echo site_url() ?>"></span>
<!-- <div id="modal_view_kunjungan" class="modal fade bs-modal-lg" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div> -->
<script type="text/javascript">

// var TableDatatablesAjax = function () {
    var grid = new Datatable();
    grid.init({
        src: $("#mytable"),
        dataTable: {
            "ajax": {
                "url": "<?php echo site_url('admin/image_slide/getDatatable/') ?>", // ajax source
                data: function (d) {
                    d.<?php echo $csrf_name ?> = '<?php echo $csrf ?>';
                }
            },
            "order": [
                [1, "asc"]
            ]// set first column as a default sort by asc
        }
    });
// }
// jQuery(document).ready(function() {
//    TableDatatablesAjax();
// });

function btn_view(id) {
    $(".portlet").LoadingOverlay("show");
	$("#modal_form").load('<?php echo site_url(); ?>/data-input/read/'+id,function() {
		$(this).modal("show");
        $(".portlet").LoadingOverlay("hide");
	});
}

function btn_delete(id) {
    swal(
        {
            title: "Apakah Anda yakin?",
            text: "Data ini akan dihapus.",
            type: "warning",
            showCancelButton: true,
            cancelButtonClass: "btn-default",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, Delete it!",
            closeOnConfirm: true
        },
        function(){
            $.post('<?php echo site_url('admin/image_slide/delete/') ?>'+id, {<?php echo $csrf_name ?> : '<?php echo $csrf ?>'}, function(res) {
                    grid.getDataTable().ajax.reload();
                if (res.success) {
                    $('.filter-cancel').trigger('click');
                    toastr.success(res.message);
                } else {
                    toastr.error(res.message);
                }
            });
        }
    );
}

</script>
