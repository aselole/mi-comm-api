<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <div class="page-toolbar">
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">
                                <?php echo $judul ?>
                            </span>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <form class="form-horizontal form_ajax" method="<?php echo $method ?>" action="<?php echo $action ?>" enctype="multipart/form-data">
                            <?php echo form_hidden($csrf_name, $csrf); ?>
                			<div class="form-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Community *</label>
                                        <div class="col-md-6">
                                            <select class="form-control select2" name="community_id" <?php echo $mode=='edit' ? 'disabled' : '' ?>>
                                                <option></option>
                                                <?php foreach ($communities as $r): ?>
                                                    <?php $terpilih = $r->id==@$dt->community_id ? 'selected' : '' ?>
                                                    <?php echo '<option value="'.$r->id.'" '.$terpilih.'>'.$r->nama.' - '.$r->ket.'</option>' ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Judul *</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="judul" value="<?php echo @$dt->judul ?>" placeholder="Judul">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Body *</label>
                                        <div class="col-md-6">
                                            <textarea name="body" rows="8" class="form-control"><?php echo @$dt->body ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label class="col-md-3 control-label">Gambar</label>
                                        <div class="col-md-8">
                                            <?php if (@$dt->featured_img != null && file_exists('./uploads/issue/'.@$dt->featured_img)): ?>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <img style="width: 300px; height: 250px;" src="<?php echo base_url().'uploads/issue/'.@$dt->featured_img ?>">
                                                    </div>
                                                    <div class="col-md-offset-2 col-md-4">
                                                        <span class="help-block">Best View 345 x 300 pixel</span>
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 300px; height: 250px;">
                                                            </div>
                                                            <div>
                                                                <span class="btn red btn-outline btn-file">
                                                                    <span class="fileinput-new"> Pilih Gambar </span>
                                                                    <span class="fileinput-exists"> Ganti </span>
                                                                    <input type="file" name="featured_img"> </span>
                                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Hapus </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php else: ?>
                                                <span class="help-block">Best View 345 x 300 pixel</span>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 300px; height: 250px;">
                                                    </div>
                                                    <div>
                                                        <span class="btn red btn-outline btn-file">
                                                            <span class="fileinput-new"> Pilih Gambar </span>
                                                            <span class="fileinput-exists"> Ganti </span>
                                                            <input type="file" name="featured_img"> </span>
                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Hapus </a>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Tgl Publish *</label>
                                        <div class="col-md-3">
                                            <input class="form-control datepicker" type="text" name="published_at" value="<?php echo @$dt->published_at ? date('d F Y', strtotime(@$dt->published_at)) : '' ?>" data-date-format="dd MM yyyy">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Status *</label>
                                        <div class="col-md-3">
                                            <select class="form-control select2" name="status">
                                                <option></option>
                                                <?php foreach ($status as $stat): ?>
                                                    <?php $terpilih = $stat==@$dt->status ? 'selected' : '' ?>
                                                    <?php echo '<option value="'.$stat.'" '.$terpilih.'>'.$stat.'</option>' ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Aktif *</label>
                                        <div class="col-md-3">
                                            <select class="form-control select2" name="active">
                                                <option></option>
                                                <option value="y" <?php echo @$dt->active=='y' ? 'selected' : '' ?>>Ya</option>
                                                <option value="t" <?php echo @$dt->active=='t' ? 'selected' : '' ?>>Tidak</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                			</div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Submit</button>
                                        <a href="<?php echo site_url('admin/issue') ?>"><button type="button" class="btn default">Batal</button></a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>

<div id="modal_form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<span id="site_url" data-site-url="<?php echo site_url() ?>"></span>
<script src="<?php echo base_url() ?>assets/custom/scripts/j_generalForm.js" type="text/javascript"></script>

<script type="text/javascript">
$.fn.select2.defaults.set( "theme", "bootstrap" );
$('.select2').select2({
    placeholder: 'Silahkan Pilih'
});

$('.datepicker').datepicker({
    rtl: App.isRTL(),
    orientation: "left",
    autoclose: true
});
</script>
