
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" onclick="event.preventDefault(); closeModal();"></button>
			<h4 class="modal-title"><?php echo $judul ?></h4>
		</div>
		<div class="modal-body">
            <form class="form-horizontal form_ajax" method="<?php echo $method ?>" action="<?php echo $action ?>">
                <?php echo form_hidden($csrf_name, $csrf); ?>
                <input type="hidden" name="issue_id" value="<?php echo $issue_id ?>">
                <input type="hidden" name="parent_id" value="<?php echo $parent_id ?>">
    			<div class="form-body">
                    <div class="form-group">
                        <div class="col-md-12">
                            <textarea class="form-control" name="comment" placeholder="Tulis Komentar di sini ..."></textarea>
                        </div>
                    </div>
    			</div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <button type="button" class="btn default" onclick="event.preventDefault(); closeModal();">Cancel</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</div>
</div>
<script src="<?php echo base_url() ?>assets/custom/scripts/j_generalFormModal.js" type="text/javascript"></script>
