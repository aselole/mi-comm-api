<link href="<?php echo base_url() ?>assets/pages/css/blog.min.css" rel="stylesheet" type="text/css" />
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <div class="page-toolbar">
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="blog-page blog-content-2">
            <a href="<?php echo site_url('admin/issue') ?>">
                <button type="button" class="btn btn-outline dark"><i class="fa fa-arrow-left"></i> Kembali</button>
            </a>
            <div class="row">
                <div class="col-lg-9">
                    <div class="blog-single-content bordered blog-container">
                        <div class="blog-single-head">
                            <h1 class="blog-single-head-title"><?php echo $dt->judul ?> </h1>
                            <div class="blog-single-head-date">
                                <i class="icon-calendar font-blue"></i>
                                <a href="javascript:;"><?php echo date('d F Y', strtotime($dt->published_at)) ?></a>
                                <br>
                                <i class="fa fa-user font-blue"></i>
                                <a href="javascript:;"><?php echo $dt->author->first_name ?></a>
                                <br>
                                <a href="javascript:;">Status: <?php echo $dt->status ?></a>
                            </div>
                        </div>
                        <div class="blog-single-img">
                            <?php if ($dt->featured_img && file_exists('./uploads/issue/'.$dt->featured_img)): ?>
                                <img src="<?php echo base_url('uploads/issue/'.$dt->featured_img) ?>" />
                            <?php endif; ?>
                        </div>
                        <div class="blog-single-desc">
                            <p>
                                <?php echo $dt->body ?>
                            </p>
                        </div>
                        <div class="blog-comments">
                            <?php $jml_komen = isset($dt->issue_comment[0]->counted_rows) ? $dt->issue_comment[0]->counted_rows : 0 ?>
                            <h3 class="sbold blog-comments-title">Komentar (<?php echo $jml_komen ?>)</h3>
                            <div class="c-comment-list">
                                <?php if ($comments): ?>
                                    <?php foreach ($comments as $r): ?>
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="#">
                                                    <?php if ($r->user->photo && file_exists('./uploads/user/'.$r->user->photo)): ?>
                                                        <img class="media-object" alt="" src="<?php echo base_url('uploads/user/'.$r->user->photo) ?>">
                                                    <?php else: ?>
                                                        <img class="media-object" alt="" src="<?php echo base_url('assets/pages/media/profile/avatar.png') ?>">
                                                    <?php endif; ?>
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">
                                                    <a href="#"><?php echo $r->user->first_name ?></a> on
                                                    <span class="c-date"><?php echo date('d F Y, H:i', strtotime($r->created_at)) ?></span>
                                                    <a href="#" data-issue-id="<?php echo $dt->id ?>" data-parent-id="<?php echo $r->id ?>" class="btn_reply"><i class="fa fa-reply"></i></a>
                                                    <a href="#" data-comment-id="<?php echo $r->id ?>" data-parent='y' class="btn_delete"><i class="fa fa-times"></i></a>
                                                </h4>
                                                <?php echo $r->comment ?>
                                                <?php if (isset($r->issue_comment_child)): ?>
                                                    <?php foreach ($r->issue_comment_child as $rr): ?>
                                                        <div class="media">
                                                            <div class="media-left">
                                                                <a href="#">
                                                                    <?php if ($rr->user->photo && file_exists('./uploads/user/'.$rr->user->photo)): ?>
                                                                        <img class="media-object" alt="" src="<?php echo base_url('uploads/user/'.$rr->user->photo) ?>">
                                                                    <?php else: ?>
                                                                        <img class="media-object" alt="" src="<?php echo base_url('assets/pages/media/profile/avatar.png') ?>">
                                                                    <?php endif; ?>
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <h4 class="media-heading">
                                                                    <a href="#"><?php echo $rr->user->first_name ?></a> on
                                                                    <span class="c-date"><?php echo date('d F Y, H:i', strtotime($rr->created_at)) ?></span>
                                                                    <a href="#" data-comment-id="<?php echo $rr->id ?>" data-parent='t' class="btn_delete"><i class="fa fa-times"></i></a>
                                                                </h4>
                                                                <?php echo $rr->comment ?>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                            <h3 class="sbold blog-comments-title">Tinggalkan Komentar</h3>
                            <form method="post" class="form_ajax" action="<?php echo site_url('admin/issue/store_comment') ?>">
                                <?php echo form_hidden($csrf_name, $csrf) ?>
                                <input type="hidden" name="issue_id" value="<?php echo $dt->id ?>">
                                <input type="hidden" name="parent_id" value="0">
                                <div class="form-group">
                                    <textarea rows="8" name="comment" placeholder="Tulis komentar disini ..." class="form-control c-square"></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn blue uppercase btn-md sbold btn-block">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>

<div id="modal_form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<span id="site_url" data-site-url="<?php echo site_url() ?>"></span>
<script src="<?php echo base_url() ?>assets/custom/scripts/j_generalForm.js" type="text/javascript"></script>

<script type="text/javascript">
$('.btn_reply').click(function(e) {
    e.preventDefault();
    var issue_id = $(this).data('issue-id');
    var parent_id = $(this).data('parent-id');
    $('#modal_form').load('<?php echo site_url() ?>'+'admin/issue/form_modal_comment/'+issue_id+'/'+parent_id, function() {
        $('#modal_form').modal('show');
    });
});

$('.btn_delete').click(function(e) {
    e.preventDefault();
    var comment_id = $(this).data('comment-id');
    var parent = $(this).data('parent');
    var msg = 'Komentar ini akan dihapus.';
    if (parent=='y') {
        msg = 'Komentar ini dan balasannya akan terhapus.';
    }
    swal(
        {
            title: "Apakah Anda yakin?",
            text: msg,
            type: "warning",
            showCancelButton: true,
            cancelButtonClass: "btn-default",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, Delete it!",
            closeOnConfirm: true
        },
        function(){
            $.post('<?php echo site_url('admin/issue/delete_comment') ?>', {<?php echo $csrf_name ?>: '<?php echo $csrf ?>', comment_id:comment_id}, function(res) {
                if (res.success) {
                    $('.filter-cancel').trigger('click');
                    toastr.success(res.message);
                    location.reload();
                } else {
                    toastr.error(res.message);
                }
            });
        }
    );
});

</script>
