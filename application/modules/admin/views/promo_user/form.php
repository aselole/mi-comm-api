<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js" type="text/javascript"></script>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <div class="page-toolbar">
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">
                                <?php echo $judul ?>
                            </span>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <form class="form-horizontal form_ajax" method="<?php echo $method ?>" action="<?php echo $action ?>" enctype="multipart/form-data">
                            <?php echo form_hidden($csrf_name, $csrf); ?>
                			<div class="form-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Promo *</label>
                                        <div class="col-md-6">
                                            <?php if ($mode=='create'): ?>
                                                <select class="form-control select2" name="promo_id" id="promo_id">
                                                    <option></option>
                                                    <?php foreach ($promo as $r): ?>
                                                        <?php $terpilih = $r->id == @$dt->promo_id ? 'selected' : '' ?>
                                                        <?php echo '<option value="'.$r->id.'" '.$terpilih.'>'.$r->judul.'</option>' ?>
                                                    <?php endforeach; ?>
                                                </select>
                                            <?php elseif ($mode=='edit'): ?>
                                                <select class="form-control select2" name="promo_id" id="promo_id" disabled>
                                                    <option></option>
                                                    <?php echo '<option value="'.$promo->id.'" selected>'.$promo->judul.'</option>' ?>
                                                </select>
                                            <?php endif; ?>
                                            <div class="detail_promo margin-top-10">

                                            </div>
                                        </div>
                                    </div>
                                    <?php if ($mode=='create'): ?>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">User Member *</label>
                                            <div class="col-md-6">
                                                <select class="form-control select2" name="user_id" id="user_id">
                                                    <option></option>
                                                    <?php foreach ($user as $r): ?>
                                                        <?php echo '<option value="'.$r->id.'">'.$r->first_name.' ('.$r->email.')</option>' ?>
                                                    <?php endforeach; ?>
                                                </select>
                                                <div class="margin-top-10">
                                                    <a href="javascript:;" class="btn red" id="object_tagsinput_add">Add tag</a>
                                                </div>
                                                <div class="margin-top-10">
                                                    <input type="text" class="form-control input-large" id="ar_user_id" name="ar_user_id">
                                                </div>
                                            </div>
                                        </div>
                                    <?php elseif ($mode=='edit'): ?>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">User Member *</label>
                                            <div class="col-md-6">
                                                <select class="form-control select2" name="user_id" id="user_id" disabled>
                                                    <option></option>
                                                    <?php echo '<option value="'.$user->id.'" selected>'.$user->first_name.' ('.$user->email.')</option>' ?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Aktif *</label>
                                        <div class="col-md-3">
                                            <select class="form-control select2" name="active">
                                                <option></option>
                                                <option value="y" <?php echo @$dt->active=='y' ? 'selected' : '' ?>>Ya</option>
                                                <option value="t" <?php echo @$dt->active=='t' ? 'selected' : '' ?>>Tidak</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                			</div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Submit</button>
                                        <a href="<?php echo site_url('admin/promo_user') ?>"><button type="button" class="btn default">Batal</button></a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>

<div id="modal_form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<span id="site_url" data-site-url="<?php echo site_url() ?>"></span>
<script src="<?php echo base_url() ?>assets/custom/scripts/j_generalForm.js" type="text/javascript"></script>

<script type="text/javascript">
var mode = <?php echo json_encode($mode) ?>;
$.fn.select2.defaults.set( "theme", "bootstrap" );
$('.select2').select2({
    placeholder: 'Silahkan Pilih'
});

var elt = $('#ar_user_id');

elt.tagsinput({
  itemValue: 'value',
  itemText: 'text',
});

$('#object_tagsinput_add').on('click', function(){
    elt.tagsinput('add', {
        "value": $('#user_id').val(),
        "text": $('#user_id').find(':selected').html(),
    });
    $('#user_id').val('').change();
});

$('#promo_id').change(function(e) {
    e.preventDefault();
    var promo_id = $(this).val();
    $('.detail_promo').load('<?php echo site_url('admin/promo_user/detail/') ?>'+promo_id);
});

if (mode=='edit') {
    $('#promo_id').trigger('change');
}
</script>
