<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <div class="page-toolbar">
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">
                                <?php echo $judul ?>
                                <a href="<?php echo site_url('admin/promo_user/create') ?>">
                                    <button type="button" class="btn btn-outline btn-sm blue"><i class="fa fa-plus"></i> Tambah</button>
                                </a>
                            </span>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <?php if ($this->session->flashdata('notif_type')): ?>
                            <div class="alert <?php echo $this->session->flashdata('notif_type') ?> alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <?php echo $this->session->flashdata('notif_msg'); ?>
                            </div>
                        <?php endif; ?>
                        <table class="table table-striped table-bordered table-hover dt-responsive" id="mytable">
                            <thead>
                                <tr role="row" class="heading">
                                    <th width="2%">No </th>
                                    <th width="30%">Promo</th>
                                    <th width="30%">User</th>
                                    <th width="20%">Dibuat</th>
                                    <th>Claim</th>
                                    <th width="20%">Tgl Claim</th>
                                    <th>Aktif</th>
                                    <th>Aksi</th>
                                </tr>
                                <tr role="row" class="filter">
                                    <td></td>
                                    <td><input type="text" class="form-control form-filter input-sm" name="promo_judul"></td>
                                    <td><input type="text" class="form-control form-filter input-sm" name="user_first_name"></td>
                                    <td>
                                        <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                            <input type="text" class="form-control form-filter input-sm" id="created_at_start" readonly="" name="created_at_start" placeholder="From">
                                            <span class="input-group-btn">
                                                <button class="btn btn-sm default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                            <input type="text" class="form-control form-filter input-sm" id="created_at_end" readonly="" name="created_at_end" placeholder="To">
                                            <span class="input-group-btn">
                                                <button class="btn btn-sm default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <select class="form-control form-filter input-sm" name="is_claimed" style="padding:0px;width:100%">
                                            <option value="all">All</option>
                                            <option value="y">y</option>
                                            <option value="">t</option>
                                        </select>
                                    </td>
                                    <td>
                                        <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                            <input type="text" class="form-control form-filter input-sm" id="claimed_at_start" readonly="" name="claimed_at_start" placeholder="From">
                                            <span class="input-group-btn">
                                                <button class="btn btn-sm default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                            <input type="text" class="form-control form-filter input-sm" id="claimed_at_end" readonly="" name="claimed_at_end" placeholder="To">
                                            <span class="input-group-btn">
                                                <button class="btn btn-sm default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <select class="form-control form-filter input-sm" name="active" style="padding:0px;width:100%">
                                            <option value="">All</option>
                                            <option value="y">y</option>
                                            <option value="t">t</option>
                                        </select>
                                    </td>
                                    <td>
                                        <div class="margin-bottom-5">
                                            <button class="btn btn-xs green btn-outline filter-submit margin-bottom">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                        <button class="btn btn-xs red btn-outline filter-cancel">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>

<div id="modal_form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<span id="site_url" data-site-url="<?php echo site_url() ?>"></span>
<!-- <div id="modal_view_kunjungan" class="modal fade bs-modal-lg" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div> -->
<script type="text/javascript">

$('.date-picker').datepicker({
    rtl: App.isRTL(),
    autoclose: true
});

$('#created_at_start').change(function(e) {
    var created_at_start = $('#created_at_start').val();
    var from_arr = created_at_start.split("/");
    var from = new Date(from_arr[2], from_arr[1]-1, from_arr[0]);

    var created_at_end = $('#created_at_end').val();
    var to_arr = created_at_end.split("/");
    var to = new Date(to_arr[2], to_arr[1]-1, to_arr[0]);

    if (from > to) {
        $('#created_at_end').val(created_at_start);
    }
});
$('#created_at_end').change(function(e) {
    var created_at_start = $('#created_at_start').val();
    var from_arr = created_at_start.split("/");
    var from = new Date(from_arr[2], from_arr[1]-1, from_arr[0]);

    var created_at_end = $('#created_at_end').val();
    var to_arr = created_at_end.split("/");
    var to = new Date(to_arr[2], to_arr[1]-1, to_arr[0]);

    if (to < from) {
        $('#created_at_start').val(created_at_end);
    }
});

$('#claimed_at_start').change(function(e) {
    var claimed_at_start = $('#claimed_at_start').val();
    var from_arr = claimed_at_start.split("/");
    var from = new Date(from_arr[2], from_arr[1]-1, from_arr[0]);

    var claimed_at_end = $('#claimed_at_end').val();
    var to_arr = claimed_at_end.split("/");
    var to = new Date(to_arr[2], to_arr[1]-1, to_arr[0]);

    if (from > to) {
        $('#claimed_at_end').val(claimed_at_start);
    }
});
$('#claimed_at_end').change(function(e) {
    var claimed_at_start = $('#claimed_at_start').val();
    var from_arr = claimed_at_start.split("/");
    var from = new Date(from_arr[2], from_arr[1]-1, from_arr[0]);

    var claimed_at_end = $('#claimed_at_end').val();
    var to_arr = claimed_at_end.split("/");
    var to = new Date(to_arr[2], to_arr[1]-1, to_arr[0]);

    if (to < from) {
        $('#claimed_at_start').val(claimed_at_end);
    }
});

var TableDatatablesAjax = function () {
    var grid = new Datatable();
    grid.init({
        src: $("#mytable"),
        dataTable: {
            "ajax": {
                "url": "<?php echo site_url('admin/promo_user/getDatatable/') ?>", // ajax source
                data: function (d) {
                    d.<?php echo $csrf_name ?> = '<?php echo $csrf ?>';
                    d.promo_judul              = $('[name=promo_judul]').val();
                    d.user_first_name          = $('[name=user_first_name]').val();
                    d.created_at_start         = $('[name=created_at_start]').val();
                    d.created_at_end           = $('[name=created_at_end]').val();
                    d.is_claimed               = $('[name=is_claimed]').val();
                    d.claimed_at_start         = $('[name=claimed_at_start]').val();
                    d.claimed_at_end           = $('[name=claimed_at_end]').val();
                    d.active                   = $('[name=active]').val();
                }
            },
            "order": [
                [1, "asc"]
            ]// set first column as a default sort by asc
        }
    });
}
jQuery(document).ready(function() {
   TableDatatablesAjax();
});

function btn_view(id) {
    $(".portlet").LoadingOverlay("show");
	$("#modal_form").load('<?php echo site_url('admin/promo/read/'); ?>'+id,function() {
		$(this).modal("show");
        $(".portlet").LoadingOverlay("hide");
	});
}

function btn_delete(id) {
    swal(
        {
            title: "Apakah Anda yakin?",
            text: "Data Promo User akan terhapus.",
            type: "warning",
            showCancelButton: true,
            cancelButtonClass: "btn-default",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ya, Hapus!",
            closeOnConfirm: true
        },
        function(){
            $.post('<?php echo site_url('admin/promo_user/delete/') ?>'+id, {<?php echo $csrf_name ?>: '<?php echo $csrf ?>'}, function(res) {
                if (res.success) {
                    $('.filter-cancel').trigger('click');
                    toastr.success(res.message);
                } else {
                    toastr.error(res.message);
                }
            });
        }
    );
}

</script>
