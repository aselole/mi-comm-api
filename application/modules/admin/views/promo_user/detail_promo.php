<table class="table table-bordered table-striped">
    <tbody>
        <tr>
            <td style="width:15%"> Merchant </td>
            <td style="width:35%">
                <span class="text-muted"> <?php echo $promo->merchant_nama ?> - <?php echo $promo->alamat ?></span>
            </td>
        </tr>
        <tr>
            <td style="width:15%"> Judul </td>
            <td style="width:35%">
                <span class="text-muted"> <?php echo $promo->judul ?> </span>
            </td>
        </tr>
        <tr>
            <td style="width:15%"> Body </td>
            <td style="width:35%">
                <span class="text-muted"> <?php echo $promo->body ?> </span>
            </td>
        </tr>
        <tr>
            <td style="width:15%"> Valid Sampai </td>
            <td style="width:35%">
                <span class="text-muted"> <?php echo date('d F Y', strtotime($promo->valid_until)) ?> </span>
            </td>
        </tr>
        <tr>
            <td style="width:15%"> Aktif </td>
            <td style="width:35%">
                <span class="text-muted"> <?php echo $promo->active ?> </span>
            </td>
        </tr>
        <tr>
            <td style="width:15%"> Image </td>
            <td style="width:35%">
                <span class="text-muted">
                    <?php if ($promo->featured_img && file_exists('./uploads/promo/'.$promo->featured_img)): ?>
                        <img src="<?php echo base_url('uploads/promo/'.$promo->featured_img) ?>" alt="">
                    <?php endif; ?>
                </span>
            </td>
        </tr>
    </tbody>
</table>
