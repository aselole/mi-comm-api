
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" onclick="event.preventDefault(); closeModal();"></button>
			<h4 class="modal-title"><?php echo $judul ?></h4>
		</div>
		<div class="modal-body">
            <form class="form-horizontal">
    			<div class="form-body">
                    <?php
                    $featured_img = base_url('assets/pages/img/page_general_search/06.jpg');
                    if (@$dt->featured_img!=null && file_exists('./uploads/promo/'.@$dt->featured_img)) {
                        $featured_img = base_url('uploads/promo/'.@$dt->featured_img);
                    }
                    ?>
                    <center><img src="<?php echo $featured_img ?>" width="300px" height="250px" alt=""></center>
                    <h2><b><?php echo @$dt->judul ?></b></h2>
					<p> <b><i class="icon-calendar"></i> Berlaku sampai <?php echo date('d/m/Y', strtotime(@$dt->valid_until)) ?></b></p>
                    <p><?php echo @$dt->body ?></p>
    			</div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="button" class="btn default" onclick="event.preventDefault(); closeModal();">Tutup</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</div>
</div>
<script type="text/javascript">
function closeModal() {
    $('#modal_form').modal('hide');
}
</script>
