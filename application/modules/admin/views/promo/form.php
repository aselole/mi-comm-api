<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <div class="page-toolbar">
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">
                                <?php echo $judul ?>
                            </span>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <form class="form-horizontal form_ajax" method="<?php echo $method ?>" action="<?php echo $action ?>" enctype="multipart/form-data">
                            <?php echo form_hidden($csrf_name, $csrf); ?>
                			<div class="form-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Global *</label>
                                        <div class="col-md-6">
                                            <div class="mt-radio-list">
                                                <?php if ($mode=='add'): ?>
                                                    <label class="mt-radio mt-radio-outline">
                                                        <input type="radio" name="is_global" id="optionsRadios22" value="y" checked> Global
                                                        <span></span>
                                                    </label>
                                                    <label class="mt-radio mt-radio-outline">
                                                        <input type="radio" name="is_global" id="optionsRadios23" value="t"> Private
                                                        <span></span>
                                                    </label>
                                                <?php elseif ($mode=='edit'): ?>
                                                    <label class="mt-radio mt-radio-outline">
                                                        <input type="radio" name="is_global" id="optionsRadios22" value="y" <?php echo @$dt->is_global=='y' ? 'checked' : '' ?> disabled> Global
                                                        <span></span>
                                                    </label>
                                                    <label class="mt-radio mt-radio-outline">
                                                        <input type="radio" name="is_global" id="optionsRadios23" value="t" <?php echo @$dt->is_global=='t' ? 'checked' : '' ?> disabled> Private
                                                        <span></span>
                                                    </label>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Merchant *</label>
                                        <div class="col-md-6">
                                            <select class="form-control select2" name="merchant_id">
                                                <option></option>
                                                <?php foreach ($merchants as $r): ?>
                                                    <?php $terpilih = $r->id==@$dt->merchant_id ? 'selected' : '' ?>
                                                    <?php echo '<option value="'.$r->id.'" '.$terpilih.'>'.$r->nama.' - '.$r->alamat.'</option>' ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Judul *</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="judul" value="<?php echo @$dt->judul ?>" placeholder="Judul">
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label class="col-md-3 control-label">Foto</label>
                                        <div class="col-md-6">
                                            <?php if (@$dt->featured_img != null && file_exists('./uploads/promo/'.@$dt->featured_img)): ?>
                                                <img style="width: 300px; height: 250px;" src="<?php echo base_url().'uploads/promo/'.@$dt->featured_img ?>">
                                                <br>
                                                <br>
                                            <?php endif; ?>
                                            <span class="help-block">Best View 345 x 345 pixel</span>
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 300px; height: 250px;">
                                                </div>
                                                <div>
                                                    <span class="btn red btn-outline btn-file">
                                                        <span class="fileinput-new"> Pilih Foto </span>
                                                        <span class="fileinput-exists"> Ganti </span>
                                                        <input type="file" name="featured_img"> </span>
                                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Hapus </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Body *</label>
                                        <div class="col-md-8">
                                            <textarea name="body" id="summernote_1" rows="8" class="form-control"><?php echo @$dt->body ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Tgl Publish *</label>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control datepicker" name="published_at" data-date-format="dd MM yyyy" value="<?php echo (@$dt->published_at) ? date('d F Y', strtotime(@$dt->published_at)) : '' ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Valid Sampai *</label>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control datepicker" name="valid_until" data-date-format="dd MM yyyy" value="<?php echo (@$dt->valid_until) ? date('d F Y', strtotime(@$dt->valid_until)) : '' ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Aktif *</label>
                                        <div class="col-md-3">
                                            <select class="form-control select2" name="active">
                                                <option></option>
                                                <option value="y" <?php echo @$dt->active=='y' ? 'selected' : '' ?>>Ya</option>
                                                <option value="t" <?php echo @$dt->active=='t' ? 'selected' : '' ?>>Tidak</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                			</div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Submit</button>
                                        <a href="<?php echo site_url('admin/promo') ?>"><button type="button" class="btn default">Batal</button></a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>

<div id="modal_form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<span id="site_url" data-site-url="<?php echo site_url() ?>"></span>
<script src="<?php echo base_url() ?>assets/custom/scripts/j_generalForm.js" type="text/javascript"></script>

<script type="text/javascript">
$.fn.select2.defaults.set( "theme", "bootstrap" );
$('.select2').select2({
    placeholder: 'Silahkan Pilih'
});
$('.datepicker').datepicker({
    rtl: App.isRTL(),
    autoclose: true
});

$('#summernote_1').summernote({
    height: 300,
    toolbar: [
            [ 'style', [ 'style' ] ],
            [ 'font', [ 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear'] ],
            [ 'fontname', [ 'fontname' ] ],
            [ 'fontsize', [ 'fontsize' ] ],
            [ 'color', [ 'color' ] ],
            [ 'para', [ 'ol', 'ul', 'paragraph', 'height' ] ],
            [ 'table', [ 'table' ] ],
            [ 'insert', [ 'link'] ],
            [ 'view', [ 'undo', 'redo', 'fullscreen', 'codeview', 'help' ] ]
        ]
});
</script>
