<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <div class="page-toolbar">
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">
                                <?php echo $judul ?>
                                <a href="<?php echo site_url('admin/promo/create') ?>">
                                    <button type="button" class="btn btn-outline btn-sm blue"><i class="fa fa-plus"></i> Tambah</button>
                                </a>
                            </span>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <?php if ($this->session->flashdata('notif_type')): ?>
                            <div class="alert <?php echo $this->session->flashdata('notif_type') ?> alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <?php echo $this->session->flashdata('notif_msg'); ?>
                            </div>
                        <?php endif; ?>
                        <div class="table-container">
                            <div class="table-actions-wrapper">
                                <span> </span>
                                <select class="table-group-action-input form-control input-inline input-small input-sm">
                                    <option value="">Pilih</option>
                                    <option value="delete_checked">Hapus</option>
                                </select>
                                <button class="btn btn-sm green table-group-action-submit">
                                    <i class="fa fa-check"></i> Submit</button>
                                </div>
                                <table class="table table-striped table-bordered table-hover table-checkable" id="mytable">
                                    <thead>
                                        <tr role="row" class="heading">
                                            <th width="2%">
                                                <input type="checkbox" class="group-checkable">
                                            </th>
                                            <th width="2%">No </th>
                                            <th width="2%">Global</th>
                                            <th width="2%">Merchant</th>
                                            <th>Judul</th>
                                            <th width="20%">Publish</th>
                                            <th width="20%">Valid</th>
                                            <th width="2%">Aktif</th>
                                            <th>Aksi</th>
                                        </tr>
                                        <tr role="row" class="filter">
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <select class="form-control form-filter input-sm" name="is_global" style="padding:0px;width:100%">
                                                    <option value="">All</option>
                                                    <option value="y">y</option>
                                                    <option value="t">t</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select class="form-control form-filter input-sm" name="merchant_id" style="padding:0px;width:100%">
                                                    <option value="">All</option>
                                                    <?php foreach ($merchants as $r): ?>
                                                        <?php echo '<option value="'.$r->id.'">'.$r->nama.'</option>' ?>
                                                    <?php endforeach; ?>
                                                </select>
                                            </td>
                                            <td><input type="text" class="form-control form-filter input-sm" name="judul"></td>
                                            <td>
                                                <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                                    <input type="text" class="form-control form-filter input-sm" id="published_at_start" readonly="" name="published_at_start" placeholder="From">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-sm default" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                                <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                                    <input type="text" class="form-control form-filter input-sm" id="published_at_end" readonly="" name="published_at_end" placeholder="To">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-sm default" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                                    <input type="text" class="form-control form-filter input-sm" id="valid_until_start" readonly="" name="valid_until_start" placeholder="From">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-sm default" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                                <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                                    <input type="text" class="form-control form-filter input-sm" id="valid_until_end" readonly="" name="valid_until_end" placeholder="To">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-sm default" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </td>
                                            <td>
                                                <select class="form-control form-filter input-sm" name="active" style="padding:0px;width:100%">
                                                    <option value="">All</option>
                                                    <option value="y">y</option>
                                                    <option value="t">t</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="margin-bottom-5">
                                                    <button class="btn btn-xs green btn-outline filter-submit margin-bottom">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                                <button class="btn btn-xs red btn-outline filter-cancel">
                                                    <i class="fa fa-times"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>

<div id="modal_form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<span id="site_url" data-site-url="<?php echo site_url() ?>"></span>
<!-- <div id="modal_view_kunjungan" class="modal fade bs-modal-lg" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div> -->
<script type="text/javascript">

$('.date-picker').datepicker({
    rtl: App.isRTL(),
    autoclose: true
});

$('#published_at_start').change(function(e) {
    var published_at_start = $('#published_at_start').val();
    var from_arr = published_at_start.split("/");
    var from = new Date(from_arr[2], from_arr[1]-1, from_arr[0]);

    var published_at_end = $('#published_at_end').val();
    var to_arr = published_at_end.split("/");
    var to = new Date(to_arr[2], to_arr[1]-1, to_arr[0]);

    if (from > to) {
        $('#published_at_end').val(published_at_start);
    }
});
$('#published_at_end').change(function(e) {
    var published_at_start = $('#published_at_start').val();
    var from_arr = published_at_start.split("/");
    var from = new Date(from_arr[2], from_arr[1]-1, from_arr[0]);

    var published_at_end = $('#published_at_end').val();
    var to_arr = published_at_end.split("/");
    var to = new Date(to_arr[2], to_arr[1]-1, to_arr[0]);

    if (to < from) {
        $('#published_at_start').val(published_at_end);
    }
});

$('#valid_until_start').change(function(e) {
    var valid_until_start = $('#valid_until_start').val();
    var from_arr = valid_until_start.split("/");
    var from = new Date(from_arr[2], from_arr[1]-1, from_arr[0]);

    var valid_until_end = $('#valid_until_end').val();
    var to_arr = valid_until_end.split("/");
    var to = new Date(to_arr[2], to_arr[1]-1, to_arr[0]);

    if (from > to) {
        $('#valid_until_end').val(valid_until_start);
    }
});
$('#valid_until_end').change(function(e) {
    var valid_until_start = $('#valid_until_start').val();
    var from_arr = valid_until_start.split("/");
    var from = new Date(from_arr[2], from_arr[1]-1, from_arr[0]);

    var valid_until_end = $('#valid_until_end').val();
    var to_arr = valid_until_end.split("/");
    var to = new Date(to_arr[2], to_arr[1]-1, to_arr[0]);

    if (to < from) {
        $('#valid_until_start').val(valid_until_end);
    }
});


var TableDatatablesAjax = function () {
    var grid = new Datatable();
    grid.setDefaultParam("<?php echo $csrf_name ?>", '<?php echo $csrf ?>');
    grid.setAjaxParam('is_global'                , $('[name =is_global]').val());
    grid.setAjaxParam('merchant_id'              , $('[name =merchant_id]').val());
    grid.setAjaxParam('judul'                    , $('[name =judul]').val());
    grid.setAjaxParam('valid_until_start'        , $('[name =valid_until_start]').val());
    grid.setAjaxParam('valid_until_end'          , $('[name =valid_until_end]').val());
    grid.setAjaxParam('active'                   , $('[name =active]').val());
    grid.setAjaxParam('published_at_start'         , $('[name =published_at_start]').val());
    grid.setAjaxParam('published_at_end'           , $('[name =published_at_end]').val());
    //             }
    grid.init({
        src: $("#mytable"),
        dataTable: {
            "ajax": {
                "url": "<?php echo site_url('admin/promo/getDatatable/') ?>", // ajax source
                // data: function (d) {
                //     d.<?php //echo $csrf_name ?> = '<?php //echo $csrf ?>';
                //     d.is_global                = $('[name=is_global]').val();
                //     d.merchant_id              = $('[name=merchant_id]').val();
                //     d.judul                    = $('[name=judul]').val();
                //     d.valid_until_start        = $('[name=valid_until_start]').val();
                //     d.valid_until_end          = $('[name=valid_until_end]').val();
                //     d.active                   = $('[name=active]').val();
                //     d.created_at_start         = $('[name=created_at_start]').val();
                //     d.created_at_end           = $('[name=created_at_end]').val();
                //     d.created_at_end           = $('[name=created_at_end]').val();
                //     d.created_at_end           = $('[name=created_at_end]').val();
                // }
            },
            "order": [
                [1, "asc"]
            ]// set first column as a default sort by asc
        }
    });
    grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
        e.preventDefault();

        var action = $(".table-group-action-input", grid.getTableWrapper());
        if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
            swal(
                {
                    title: "Apakah Anda yakin?",
                    text: "Semua data yang berkaitan dengan promo akan terhapus.",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonClass: "btn-default",
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Ya, Hapus!",
                    closeOnConfirm: true
                },
                function(){
                    grid.setAjaxParam("customActionType", "group_action");
                    grid.setAjaxParam("customActionName", action.val());
                    grid.setAjaxParam("id", grid.getSelectedRows());
                    grid.getDataTable().ajax.reload();
                    grid.clearAjaxParams();
                }
            );
        } else if (action.val() == "") {
            App.alert({
                type: 'danger',
                icon: 'warning',
                message: 'Please select an action',
                container: grid.getTableWrapper(),
                place: 'prepend'
            });
        } else if (grid.getSelectedRowsCount() === 0) {
            App.alert({
                type: 'danger',
                icon: 'warning',
                message: 'No record selected',
                container: grid.getTableWrapper(),
                place: 'prepend'
            });
        }
    });

}
jQuery(document).ready(function() {
   TableDatatablesAjax();
});

function btn_view(id) {
    $(".portlet").LoadingOverlay("show");
	$("#modal_form").load('<?php echo site_url('admin/promo/read/'); ?>'+id,function() {
		$(this).modal("show");
        $(".portlet").LoadingOverlay("hide");
	});
}

function btn_delete(id) {
    swal(
        {
            title: "Apakah Anda yakin?",
            text: "Semua data yang berkaitan dengan promo akan terhapus.",
            type: "warning",
            showCancelButton: true,
            cancelButtonClass: "btn-default",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ya, Hapus!",
            closeOnConfirm: true
        },
        function(){
            $.post('<?php echo site_url('admin/promo/delete/') ?>'+id, {<?php echo $csrf_name ?>: '<?php echo $csrf ?>'}, function(res) {
                if (res.success) {
                    $('.filter-cancel').trigger('click');
                    toastr.success(res.message);
                } else {
                    toastr.error(res.message);
                }
            });
        }
    );
}

</script>
