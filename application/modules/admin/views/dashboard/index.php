<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Admin Dashboard 
                </h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <div class="page-toolbar hide">
                <div id="dashboard-report-range" data-display-range="0" class="pull-right tooltips btn btn-fit-height green" data-placement="left" data-original-title="Change dashboard date range">
                    <i class="icon-calendar"></i>&nbsp;
                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                    <i class="fa fa-angle-down"></i>
                </div>
                <!-- BEGIN THEME PANEL -->
                <div class="btn-group btn-theme-panel">
                    <a href="javascript:;" class="btn dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-settings"></i>
                    </a>
                    <div class="dropdown-menu theme-panel pull-right dropdown-custom hold-on-click">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <h3>HEADER</h3>
                                <ul class="theme-colors">
                                    <li class="theme-color theme-color-default active" data-theme="default">
                                        <span class="theme-color-view"></span>
                                        <span class="theme-color-name">Dark Header</span>
                                    </li>
                                    <li class="theme-color theme-color-light " data-theme="light">
                                        <span class="theme-color-view"></span>
                                        <span class="theme-color-name">Light Header</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END THEME PANEL -->
            </div>
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Dashboard</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-green-sharp">
                                <span data-counter="counterup" data-value="<?php echo $jumlah_komunitas ?>">0</span>
                            </h3>
                            <small>Jumlah Komunitas</small>
                        </div>
                        <div class="icon">
                            <i class="icon-pie-chart"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-red-haze">
                                <span data-counter="counterup" data-value="<?php echo $jumlah_member ?>">0</span>
                            </h3>
                            <small>Jumlah Member</small>
                        </div>
                        <div class="icon">
                            <i class="icon-user"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-blue-sharp">
                                <span data-counter="counterup" data-value="<?php echo $jumlah_komentar_buruk ?>"></span>
                            </h3>
                            <small>Jumlah Komentar Buruk</small>
                        </div>
                        <div class="icon">
                            <i class="icon-dislike"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-xs-12 col-sm-12">
                <div class="portlet light tasks-widget bordered">
                    <div class="portlet-title tabbable-line">
                        <div class="caption">
                            <i class="icon-bubbles font-dark hide"></i>
                            <span class="caption-subject font-dark bold uppercase">Promo</span>
                        </div>
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#portlet_comments_1" data-toggle="tab"> Private </a>
                            </li>
                            <li>
                                <a href="#portlet_comments_2" data-toggle="tab"> Global </a>
                            </li>
                        </ul>
                    </div>
                    <div class="portlet-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="portlet_comments_1">
                                <!-- BEGIN: Comments -->
                                <div class="mt-comments">
                                    <div class="scroller" style="height: 312px;" data-always-visible="1" data-rail-visible1="1">
                                        <?php if (!empty($promo_khusus)): ?>
                                            <?php foreach ($promo_khusus as $pp): ?>
                                                <div class="mt-comment">
                                                    <div class="mt-comment-body">
                                                        <div class="mt-comment-info">
                                                            <span class="mt-comment-author"><?php echo $pp->judul ?></span>
                                                            <span class="mt-comment-date"><?php echo date('d-m-Y', strtotime($pp->valid_until)) ?></span>
                                                        </div>
                                                        <div class="mt-comment-text"> <?php echo ellipsize($pp->body,100) ?> </div>
                                                    </div>
                                                </div>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </div>
                                </div>
                                <!-- END: Comments -->
                            </div>
                            <div class="tab-pane" id="portlet_comments_2">
                                <!-- BEGIN: Comments -->
                                <div class="mt-comments">
                                    <div class="scroller" style="height: 312px;" data-always-visible="1" data-rail-visible1="1">
                                        <?php if (!empty($promo_global)): ?>
                                            <?php foreach ($promo_global as $pg): ?>
                                                <div class="mt-comment">
                                                    <div class="mt-comment-body">
                                                        <div class="mt-comment-info">
                                                            <span class="mt-comment-author"><?php echo $pg->judul ?></span>
                                                            <span class="mt-comment-date"><?php echo date('d-m-Y', strtotime($pg->valid_until)) ?></span>
                                                        </div>
                                                        <div class="mt-comment-text"> <?php echo ellipsize($pg->body,125) ?> </div>
                                                    </div>
                                                </div>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </div>
                                </div>
                                <!-- END: Comments -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
            <div class="col-lg-6 col-xs-12 col-sm-12">
                <div class="portlet light tasks-widget bordered">
                    <div class="portlet-title tabbable-line">
                        <div class="caption">
                            <i class="icon-bubbles font-dark hide"></i>
                            <span class="caption-subject font-dark bold uppercase">Promo Klaim</span>
                        </div>
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#portlet_comments_12" data-toggle="tab"> Private </a>
                            </li>
                            <li>
                                <a href="#portlet_comments_22" data-toggle="tab"> Global </a>
                            </li>
                        </ul>
                    </div>
                    <div class="portlet-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="portlet_comments_12">
                                <!-- BEGIN: Comments -->
                                <div class="mt-comments">
                                    <div class="scroller" style="height: 312px;" data-always-visible="1" data-rail-visible1="1">
                                        <?php if ($promo_user): ?>
                                            <?php foreach ($promo_user as $pu): ?>
                                                <div class="mt-comment">
                                                    <div class="mt-comment-body">
                                                        <div class="mt-comment-info">
                                                            <span class="mt-comment-author"><b><?php echo $pu->merchant_nama ?></b> <?php echo $pu->judul ?></span>
                                                            <span class="mt-comment-date"><?php echo date('d-m-Y', strtotime($pu->claimed_at)) ?></span>
                                                        </div>
                                                        <div class="mt-comment-text"> <small>claimed by </small><b><?php echo $pu->first_name ?></b> </div>
                                                        <div class="mt-comment-details">
                                                            <ul class="mt-comment-actions">
                                                                </li>
                                                                    <!-- <a href="#">View</a> -->
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </div>
                                </div>
                                <!-- END: Comments -->
                            </div>
                            <div class="tab-pane" id="portlet_comments_22">
                                <!-- BEGIN: Comments -->
                                <div class="mt-comments">
                                    <div class="scroller" style="height: 312px;" data-always-visible="1" data-rail-visible1="1">
                                        <?php if ($promo_claim): ?>
                                            <?php foreach ($promo_claim as $pc): ?>
                                                <div class="mt-comment">
                                                    <div class="mt-comment-body">
                                                        <div class="mt-comment-info">
                                                            <span class="mt-comment-author"><b><?php echo $pu->merchant_nama ?></b> <?php echo $pu->judul ?></span>
                                                            <span class="mt-comment-date"><?php echo date('d-m-Y', strtotime($pu->claimed_at)) ?></span>
                                                        </div>
                                                        <div class="mt-comment-text"> <small>claimed by </small><b><?php echo $pu->first_name ?></b> </div>
                                                        <div class="mt-comment-details">
                                                            <ul class="mt-comment-actions">
                                                                </li>
                                                                    <!-- <a href="#">View</a> -->
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </div>
                                </div>
                                <!-- END: Comments -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>             
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
