<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <div class="page-toolbar">
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">
                                <?php echo $judul ?>
                            </span>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <form class="form-horizontal form_ajax" method="<?php echo $method ?>" action="<?php echo $action ?>">
                            <?php echo form_hidden($csrf_name, $csrf); ?>
                            <input type="hidden" name="kode" value="<?php echo $dt->kode ?>">
                			<div class="form-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Body *</label>
                                        <div class="col-md-10">
                                            <textarea name="isi" id="summernote_1" rows="8" class="form-control"><?php echo @$dt->isi ?></textarea>
                                        </div>
                                    </div>
                                </div>
                			</div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Update</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>

<div id="modal_form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<span id="site_url" data-site-url="<?php echo site_url() ?>"></span>
<script src="<?php echo base_url() ?>assets/custom/scripts/j_formNews.js" type="text/javascript"></script>

<script type="text/javascript">
$.fn.select2.defaults.set( "theme", "bootstrap" );

$('#summernote_1').summernote({
    // height: 300,
    // toolbar: [
    //         [ 'style', [ 'style' ] ],
    //         [ 'font', [ 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear'] ],
    //         [ 'fontname', [ 'fontname' ] ],
    //         [ 'fontsize', [ 'fontsize' ] ],
    //         [ 'color', [ 'color' ] ],
    //         [ 'para', [ 'ol', 'ul', 'paragraph', 'height' ] ],
    //         [ 'table', [ 'table' ] ],
    //         [ 'insert', [ 'link'] ],
            // [ 'view', [ 'undo', 'redo', 'fullscreen', 'codeview', 'help' ] ]
        // ]
});

</script>
