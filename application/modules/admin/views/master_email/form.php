<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <div class="page-toolbar">
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">
                                <?php echo $judul ?>
                            </span>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <form class="form-horizontal form_ajax" method="<?php echo $method ?>" action="<?php echo $action ?>">
                            <?php echo form_hidden($csrf_name, $csrf); ?>
                            <input type="hidden" id="id" value="<?php echo $dt->id ?>">
                			<div class="form-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?php if (@$dt->kode=='email_aktifasi'): ?>
                                            <div class="panel-group accordion" id="accordion1">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_1"> Daftar Kamus </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse_1" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li>
                                                                    <p>
                                                                        <b>%nama%</b> : Nama User Mi-comm
                                                                    </p>
                                                                </li>
                                                                <li>
                                                                    <p>
                                                                        <b>%link_aktifasi%</b> : Link Aktifasi
                                                                    </p>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Nama</label>
                                        <div class="col-md-6">
                                            <p class="form-control-static"> <b><?php echo $dt->nama ?></b> </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Body *</label>
                                        <div class="col-md-10">
                                            <textarea name="isi" id="summernote_1" rows="8" class="form-control"><?php echo @$dt->isi ?></textarea>
                                        </div>
                                    </div>
                                </div>
                			</div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Submit</button>
                                        <a href="<?php echo site_url('admin/master_email') ?>"><button type="button" class="btn default">Kembali</button></a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>

<div id="modal_form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<span id="site_url" data-site-url="<?php echo site_url() ?>"></span>
<script src="<?php echo base_url() ?>assets/custom/scripts/j_formNews.js" type="text/javascript"></script>

<script type="text/javascript">
$.fn.select2.defaults.set( "theme", "bootstrap" );
$('.select2').select2({
    placeholder: 'Silahkan Pilih'
});
$('.datepicker').datepicker({
    rtl: App.isRTL(),
    autoclose: true
});

$('#summernote_1').summernote({
    // height: 300,
    // toolbar: [
    //         [ 'style', [ 'style' ] ],
    //         [ 'font', [ 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear'] ],
    //         [ 'fontname', [ 'fontname' ] ],
    //         [ 'fontsize', [ 'fontsize' ] ],
    //         [ 'color', [ 'color' ] ],
    //         [ 'para', [ 'ol', 'ul', 'paragraph', 'height' ] ],
    //         [ 'table', [ 'table' ] ],
    //         [ 'insert', [ 'link'] ],
            // [ 'view', [ 'undo', 'redo', 'fullscreen', 'codeview', 'help' ] ]
        // ]
});

</script>
