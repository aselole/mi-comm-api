<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <div class="page-toolbar">
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">
                                <?php echo $judul ?>
                            </span>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="panel-group accordion" id="accordion1">
                            <?php foreach ($master_emails as $r): ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="<?php echo '#collapse_'.$r->id ?>"> <?php echo $r->nama ?> </a>
                                        </h4>
                                    </div>
                                    <div id="<?php echo 'collapse_'.$r->id ?>" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <a href="<?php echo site_url('admin/master_email/edit/').$r->id ?>">
                                                <button class="btn btn-success"><i class=" fa fa-pencil"></i> Edit</button>
                                            </a>
                                            <?php echo $r->isi ?>
                                            <!-- <textarea name="name" rows="8" cols="80"></textarea> -->
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>

<div id="modal_form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<span id="site_url" data-site-url="<?php echo site_url() ?>"></span>
<!-- <div id="modal_view_kunjungan" class="modal fade bs-modal-lg" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div> -->
<script type="text/javascript">

function btn_edit(id) {
    $(".portlet").LoadingOverlay("show");
    $('#modal_form').load("<?php echo site_url('admin/group/edit/') ?>"+id, function() {
        $(this).modal('show');
        $(".portlet").LoadingOverlay("hide");
    });
}

function btn_view(id) {
    $(".portlet").LoadingOverlay("show");
	$("#modal_form").load('<?php echo site_url(); ?>/data-input/read/'+id,function() {
		$(this).modal("show");
        $(".portlet").LoadingOverlay("hide");
	});
}

</script>
