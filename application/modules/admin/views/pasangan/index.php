<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <div class="page-toolbar">
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">
                                <?php echo $judul ?>
                                <a href="<?php echo site_url('admin/pasangan/create') ?>">
                                    <button type="button" class="btn btn-outline btn-sm blue"><i class="fa fa-plus"></i> Tambah</button>
                                </a>
                            </span>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <?php if ($this->session->flashdata('notif_type')): ?>
                            <div class="alert <?php echo $this->session->flashdata('notif_type') ?> alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <?php echo $this->session->flashdata('notif_msg'); ?>
                            </div>
                        <?php endif; ?>
                        <table class="table table-striped table-bordered table-hover dt-responsive" id="mytable">
                            <thead>
                                <tr role="row" class="heading">
                                    <th width="2%">No </th>
                                    <th width="10%">Company</th>
                                    <th width="10%">Community</th>
                                    <th width="8%">Utama</th>
                                    <th>Status Utama</th>
                                    <th>Pasangan</th>
                                    <th>Status Pasangan</th>
                                    <th>Action</th>
                                </tr>
                                <tr role="row" class="filter">
                                    <td></td>
                                    <td>
                                        <select class="form-control form-filter input-sm select2" name="company_id" style="padding:0px;width:100%">
                                            <option value="">All</option>
                                            <?php foreach ($company as $r): ?>
                                                <?php echo '<option value="'.$r->id.'">'.$r->nama.'</option>' ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control form-filter input-sm select2" name="community_id" style="padding:0px;width:100%">
                                            <option value="">All</option>
                                            <?php foreach ($community as $r): ?>
                                                <?php echo '<option value="'.$r->id.'">'.$r->nama.'</option>' ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                    <td><input type="text" class="form-control form-filter input-sm" name="utama_nama"></td>
                                    <td>
                                        <select class="form-control form-filter input-sm" name="utama_status" style="padding:0px;width:100%">
                                            <option value="">All</option>
                                            <?php foreach ($status as $r): ?>
                                                <?php echo '<option value="'.$r.'">'.$r.'</option>' ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                    <td><input type="text" class="form-control form-filter input-sm" name="pasangan_nama"></td>
                                    <td>
                                        <select class="form-control form-filter input-sm" name="pasangan_status" style="padding:0px;width:100%">
                                            <option value="">All</option>
                                            <?php foreach ($status as $r): ?>
                                                <?php echo '<option value="'.$r.'">'.$r.'</option>' ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                    <td>
                                        <div class="margin-bottom-5">
                                            <button class="btn btn-xs green btn-outline filter-submit margin-bottom">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                        <button class="btn btn-xs red btn-outline filter-cancel">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>

<div id="modal_form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<span id="site_url" data-site-url="<?php echo site_url() ?>"></span>
<!-- <div id="modal_view_kunjungan" class="modal fade bs-modal-lg" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div> -->
<script type="text/javascript">
$('.select2').select2({
    allowClear: true
});
$('.date-picker').datepicker({
    rtl: App.isRTL(),
    autoclose: true
});

var TableDatatablesAjax = function () {
    var grid = new Datatable();
    grid.init({
        src: $("#mytable"),
        dataTable: {
            "ajax": {
                "url": "<?php echo site_url('admin/pasangan/ajax_list/') ?>", // ajax source
                data: function (d) {
                    d.<?php echo $csrf_name ?> = '<?php echo $csrf ?>';
                    d.company_id                 = $('[name=company_id]').val();
                    d.community_id                 = $('[name=community_id]').val();
                    d.utama_nama                 = $('[name=utama_nama]').val();
                    d.utama_status               = $('[name=utama_status]').val();
                    d.pasangan_nama                 = $('[name=pasangan_nama]').val();
                    d.pasangan_status                    = $('[name=pasangan_status]').val();
                }
            },
            "order": [
                [1, "asc"]
            ]// set first column as a default sort by asc
        }
    });
}
jQuery(document).ready(function() {
   TableDatatablesAjax();
});

function btn_view(id) {
    $(".portlet").LoadingOverlay("show");
	$("#modal_form").load('<?php echo site_url(); ?>/data-input/read/'+id,function() {
		$(this).modal("show");
        $(".portlet").LoadingOverlay("hide");
	});
}

function btn_delete(id) {
    swal(
        {
            title: "Apakah Anda yakin?",
            text: "Semua data yang berkaitan dengan data user akan terhapus.",
            type: "warning",
            showCancelButton: true,
            cancelButtonClass: "btn-default",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, Delete it!",
            closeOnConfirm: true
        },
        function(){
            $.post('<?php echo site_url('admin/user/delete/') ?>'+id, {'<?php echo $csrf_name ?>':'<?php echo $csrf ?>'}, function(res) {
                if (res.success) {
                    $('.filter-cancel').trigger('click');
                    toastr.success(res.message);
                } else {
                    toastr.error(res.message);
                }
            });
        }
    );
}

function btn_send_activation(id) {
    $(".portlet").LoadingOverlay("show");
    $.post('<?php echo site_url('admin/user/send_activation') ?>', {'<?php echo $csrf_name ?>':'<?php echo $csrf ?>',id:id}, function(res) {
        $(".portlet").LoadingOverlay("hide");
        if (res.success) {
            $('.filter-cancel').trigger('click');
            toastr.success(res.message);
        } else {
            toastr.error(res.message);
        }
    });
}

</script>
