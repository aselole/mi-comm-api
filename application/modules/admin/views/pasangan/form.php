<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <div class="page-toolbar">
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">
                                <?php echo $judul ?>
                            </span>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <form class="form-horizontal form_ajax" method="<?php echo $method ?>" action="<?php echo $action ?>" >
                            <?php echo form_hidden($csrf_name, $csrf); ?>
                			<div class="form-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Community *</label>
                                        <div class="col-md-6">
                                            <?php if ($mode=='create'): ?>
                                                <select class="form-control select2" name="community_id" id="community_id">
                                                    <option></option>
                                                    <?php foreach ($community as $r): ?>
                                                        <?php echo '<option value="'.$r->id.'">'.$r->company_nama.' - '.$r->nama.'</option>' ?>
                                                    <?php endforeach; ?>
                                                </select>
                                            <?php elseif ($mode=='edit'): ?>
                                                <input type="text" class="form-control" value="<?php echo $community->company_nama.' - '.$community->community_nama ?>" readonly>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="form-select-user">

                                    </div>
                                </div>
                			</div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Submit</button>
                                        <a href="<?php echo site_url('admin/pasangan') ?>"><button type="button" class="btn default">Batal</button></a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>

<div id="modal_form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<span id="site_url" data-site-url="<?php echo site_url() ?>"></span>
<script src="<?php echo base_url() ?>assets/custom/scripts/j_formPasangan.js" type="text/javascript"></script>

<script type="text/javascript">
var mode = <?php echo json_encode($mode) ?>;
$.fn.select2.defaults.set( "theme", "bootstrap" );
$('.select2').select2({
    placeholder: 'Silahkan Pilih'
});

$('#community_id').change(function(e) {
    e.preventDefault();
    var community_id = $(this).val();
    $('.form-select-user').load('<?php echo site_url('admin/pasangan/get_form_user/') ?>'+community_id, function() {

    });
});

if (mode=='edit') {
    var pasangan_id = <?php echo json_encode(@$dt->id) ?>;
    var community_id = <?php echo json_encode(@$community->community_id) ?>;
    $('.form-select-user').load('<?php echo site_url('admin/pasangan/get_form_user/') ?>'+community_id+'/'+pasangan_id, function() {

    });
}
</script>
