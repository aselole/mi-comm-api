<div class="form-group">
    <label class="col-md-3 control-label">Utama *</label>
    <div class="col-md-6">
        <select class="form-control select2" name="utama_user_id" id="utama_user_id">
            <option></option>
            <?php foreach ($user as $r): ?>
                <?php $terpilih = $r->id==@$pasangan->utama_user_id ? 'selected' : '' ?>
                <?php echo '<option value="'.$r->id.'" '.$terpilih.'>'.$r->first_name.'</option>' ?>
            <?php endforeach; ?>
        </select>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 control-label">Status Utama *</label>
    <div class="col-md-6">
        <select class="form-control select2" name="utama_status" id="utama_status">
            <option></option>
            <?php foreach ($status as $r): ?>
                <?php $terpilih = $r==@$pasangan->utama_status ? 'selected' : '' ?>
                <?php echo '<option value="'.$r.'" '.$terpilih.'>'.$r.'</option>' ?>
            <?php endforeach; ?>
        </select>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 control-label">Pasangan *</label>
    <div class="col-md-6">
        <select class="form-control select2" name="pasangan_user_id" id="pasangan_user_id">
            <option></option>
            <?php foreach ($user as $r): ?>
                <?php $terpilih = $r->id==@$pasangan->pasangan_user_id ? 'selected' : '' ?>
                <?php echo '<option value="'.$r->id.'" '.$terpilih.'>'.$r->first_name.'</option>' ?>
            <?php endforeach; ?>
        </select>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 control-label">Status Pasangan *</label>
    <div class="col-md-6">
        <select class="form-control select2" name="pasangan_status" id="pasangan_status">
            <option></option>
            <?php foreach ($status as $r): ?>
                <?php $terpilih = $r==@$pasangan->pasangan_status ? 'selected' : '' ?>
                <?php echo '<option value="'.$r.'" '.$terpilih.'>'.$r.'</option>' ?>
            <?php endforeach; ?>
        </select>
    </div>
</div>

<script type="text/javascript">
$('.select2').select2({
    placeholder: 'Silahkan Pilih'
});
</script>
