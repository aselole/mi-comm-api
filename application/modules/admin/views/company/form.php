
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" onclick="event.preventDefault(); closeModal();"></button>
			<h4 class="modal-title"><?php echo $judul ?></h4>
		</div>
		<div class="modal-body">
            <form class="form-horizontal form_ajax" method="<?php echo $method ?>" action="<?php echo $action ?>">
                <?php echo form_hidden($csrf_name, $csrf); ?>
    			<div class="form-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Nama *</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="nama" value="<?php echo @$dt->nama ?>" placeholder="Nama Perusahaan">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Jenis *</label>
                        <div class="col-md-8">
                            <select class="form-control select2" name="jenis" style="width:100%">
                                <option></option>
                                <?php foreach ($jenis_company as $r): ?>
                                    <?php $terpilih = $r==@$dt->jenis ? 'selected' : '' ?>
                                    <?php echo '<option value="'.$r.'" '.$terpilih.'>'.$r.'</option>' ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label class="col-md-3 control-label">Alamat *</label>
                        <div class="col-md-8">
                            <textarea class="form-control" name="alamat" placeholder="Alamat"><?php echo @$dt->alamat ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Email</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="email" value="<?php echo @$dt->email ?>" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Telepon 1 *</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="telepon1" value="<?php echo @$dt->telepon1 ?>" placeholder="Telepon 1">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Telepon 2</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="telepon2" value="<?php echo @$dt->telepon2 ?>" placeholder="Telepon 2">
                        </div>
                    </div> -->
    			</div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn green">Submit</button>
                            <button type="button" class="btn default" onclick="event.preventDefault(); closeModal();">Cancel</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</div>
</div>
<script src="<?php echo base_url() ?>assets/custom/scripts/j_generalFormModal.js" type="text/javascript"></script>

<script type="text/javascript">

// begin - select2 modal wajib
$.fn.modal.Constructor.prototype.enforceFocus = function() {};
// end - select2 modal wajib

$('.select2').select2({
    placeholder: 'Silahkan Pilih'
});
</script>
