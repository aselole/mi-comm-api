<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <div class="page-toolbar">
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">
                                <?php echo $judul ?>
                                <button type="button" class="btn btn-outline btn-sm blue" onclick="event.preventDefault(); btn_add();"><i class="fa fa-plus"></i> Tambah</button>
                            </span>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" id="mytable">
                            <thead>
                                <tr role="row" class="heading">
                                    <th width="2%">No </th>
                                    <th>Nama</th>
                                    <th>Jenis</th>
                                    <!-- <th>Alamat</th> -->
                                    <!-- <th>Email</th> -->
                                    <!-- <th>Tlp1</th> -->
                                    <!-- <th>Tlp2</th> -->
                                    <th width="2%">Aksi</th>
                                </tr>
                                <tr role="row" class="filter">
                                    <td></td>
                                    <td><input type="text" class="form-control form-filter input-sm" name="nama"></td>
                                    <td>
                                        <select class="form-control form-filter input-sm" name="jenis" style="padding:0px;width:100%">
                                            <option value="">All</option>
                                            <?php foreach ($jenis_company as $r): ?>
                                                <?php echo '<option value="'.$r.'">'.$r.'</option>' ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                    <!-- <td><input type="text" class="form-control form-filter input-sm" name="alamat"></td> -->
                                    <!-- <td><input type="text" class="form-control form-filter input-sm" name="email"></td> -->
                                    <!-- <td><input type="text" class="form-control form-filter input-sm" name="telepon1"></td> -->
                                    <!-- <td><input type="text" class="form-control form-filter input-sm" name="telepon2"></td> -->
                                    <td>
                                        <div class="margin-bottom-5">
                                            <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                                <i class="fa fa-search"></i> Search
                                            </button>
                                        </div>
                                        <button class="btn btn-sm red btn-outline filter-cancel">
                                            <i class="fa fa-times"></i> Reset
                                        </button>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>

<div id="modal_form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<span id="site_url" data-site-url="<?php echo site_url() ?>"></span>

<script type="text/javascript">

var TableDatatablesAjax = function () {
    var grid = new Datatable();
    grid.init({
        src: $("#mytable"),
        dataTable: {
            "ajax": {
                "url": "<?php echo site_url('admin/company/getDatatable/') ?>", // ajax source
                data: function (d) {
                    d.<?php echo $csrf_name ?> = '<?php echo $csrf ?>';
                    d.nama                     = $('[name=nama]').val();
                    d.jenis                    = $('[name=jenis]').val();
                    // d.alamat                   = $('[name=alamat]').val();
                    // d.email                    = $('[name=email]').val();
                    // d.tlp1                     = $('[name=tlp1]').val();
                    // d.tlp2                     = $('[name=tlp2]').val();
                }
            },
            "order": [
                [1, "asc"]
            ]// set first column as a default sort by asc
        }
    });
}
jQuery(document).ready(function() {
   TableDatatablesAjax();
});

function btn_add() {
    $(".portlet").LoadingOverlay("show");
    $('#modal_form').load("<?php echo site_url('admin/company/create') ?>", function() {
        $(this).modal('show');
        $(".portlet").LoadingOverlay("hide");
    });
}

function btn_edit(id) {
    $(".portlet").LoadingOverlay("show");
    $('#modal_form').load("<?php echo site_url('admin/company/edit/') ?>"+id, function() {
        $(this).modal('show');
        $(".portlet").LoadingOverlay("hide");
    });
}

function btn_delete(id) {
    swal(
        {
            title: "Apakah Anda yakin?",
            text: "Semua data yang berkaitan dengan data perusahaan akan terhapus.",
            type: "warning",
            showCancelButton: true,
            cancelButtonClass: "btn-default",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, Delete it!",
            closeOnConfirm: true
        },
        function(){
            $.post('<?php echo site_url('admin/company/delete/') ?>'+id, {<?php echo $csrf_name ?>: '<?php echo $csrf ?>'}, function(res) {
                if (res.success) {
                    $('.filter-cancel').trigger('click');
                    toastr.success(res.message);
                } else {
                    toastr.error(res.message);
                }
            });
        }
    );
}

</script>
