<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <div class="page-toolbar">
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">
                                <?php echo $judul ?>
                            </span>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <form class="form-horizontal form_ajax" method="<?php echo $method ?>" action="<?php echo $action ?>">
                            <?php echo form_hidden($csrf_name, $csrf); ?>
                			<div class="form-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Promo *</label>
                                        <div class="col-md-6">
                                            <select class="form-control select2" name="promo_id" id="promo_id">
                                                <option></option>
                                                <?php foreach ($promos as $r): ?>
                                                    <?php echo '<option value="'.$r->id.'">'.$r->judul.'</option>' ?>
                                                <?php endforeach; ?>
                                            </select>
                                            <a href="#" id="btn_show_promo">Pilih Promo</a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Jumlah Voucher *</label>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" name="jml_voucher" value="" placeholder="Jumlah Voucher">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Panjang Voucher *</label>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" name="length_voucher" value="" placeholder="Panjang Voucher">
                                        </div>
                                    </div>
                                </div>
                			</div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Submit</button>
                                        <a href="<?php echo site_url('admin/voucher') ?>"><button type="button" class="btn default">Batal</button></a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>

<div id="modal_form" class="modal fade bs-modal-lg" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<span id="site_url" data-site-url="<?php echo site_url() ?>"></span>
<script src="<?php echo base_url() ?>assets/custom/scripts/j_generalForm.js" type="text/javascript"></script>

<script type="text/javascript">
$.fn.select2.defaults.set( "theme", "bootstrap" );
$('.select2').select2({
    placeholder: 'Silahkan Pilih'
});
$('.datepicker').datepicker({
    rtl: App.isRTL(),
    autoclose: true
});

$('#btn_show_promo').click(function(e) {
    e.preventDefault();
    $('#modal_form').load('<?php echo site_url('admin/voucher/modal_promo') ?>', function() {
        $(this).modal('show');
    });
});


</script>
