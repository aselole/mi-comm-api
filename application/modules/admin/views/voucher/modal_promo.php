<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" onclick="event.preventDefault(); closeModal();"></button>
			<h4 class="modal-title"><?php echo $judul ?></h4>
		</div>
		<div class="modal-body">
            <table class="table table-striped table-bordered table-hover dt-responsive" id="mytable">
                <thead>
                    <tr role="row" class="heading">
                        <th width="2%">No </th>
                        <th width="2%">Global</th>
                        <th width="2%">Merchant</th>
                        <th>Judul</th>
                        <th width="20%">Valid</th>
                        <th width="20%">Dibuat</th>
                        <th>Aksi</th>
                    </tr>
                    <tr role="row" class="filter">
                        <td></td>
                        <td>
                            <select class="form-control form-filter input-sm" name="is_global" style="padding:0px;width:100%">
                                <option value="">All</option>
                                <option value="y">y</option>
                                <option value="t">t</option>
                            </select>
                        </td>
                        <td>
                            <select class="form-control form-filter input-sm" name="merchant_id" style="padding:0px;width:100%">
                                <option value="">All</option>
                                <?php foreach ($merchants as $r): ?>
                                    <?php echo '<option value="'.$r->id.'">'.$r->nama.'</option>' ?>
                                <?php endforeach; ?>
                            </select>
                        </td>
                        <td><input type="text" class="form-control form-filter input-sm" name="judul"></td>
                        <td></td>
                        <td></td>
                        <td>
                            <div class="margin-bottom-5">
                                <button class="btn btn-xs green btn-outline filter-submit margin-bottom">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                            <button class="btn btn-xs red btn-outline filter-cancel">
                                <i class="fa fa-times"></i>
                            </button>
                        </td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
		</div>
	</div>
</div>

<script type="text/javascript">
var TableDatatablesAjax = function () {
    var grid = new Datatable();
    grid.init({
        src: $("#mytable"),
        dataTable: {
            "ajax": {
                "url": "<?php echo site_url('admin/voucher/datatable_promo/') ?>", // ajax source
                data: function (d) {
                    d.<?php echo $csrf_name ?> = '<?php echo $csrf ?>';
                    d.is_global                = $('[name=is_global]').val();
                    d.merchant_id              = $('[name=merchant_id]').val();
                    d.judul                    = $('[name=judul]').val();
                }
            },
            "order": [
                [1, "asc"]
            ]// set first column as a default sort by asc
        }
    });
}
jQuery(document).ready(function() {
   TableDatatablesAjax();
});

function closeModal() {
    $('#modal_form').modal('hide');
}

function btn_pilih(id) {
    $('#promo_id').val(id).change();
    closeModal();
}
</script>
