<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <div class="page-toolbar">
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">
                                <?php echo $judul ?>
                                <a href="<?php echo site_url('admin/voucher/create') ?>">
                                    <button type="button" class="btn btn-outline btn-sm blue"><i class="fa fa-plus"></i> Tambah</button>
                                </a>
                                <button type="button" class="btn btn-outline btn-sm yellow btn-cetak"><i class="fa fa-print"></i> Cetak Promo</button>
                            </span>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" id="mytable">
                            <thead>
                                <tr role="row" class="heading">
                                    <th width="2%">No </th>
                                    <th width="20%">Merchant</th>
                                    <th width="20%">Promo</th>
                                    <th>Global</th>
                                    <th width="20%">Valid Sampai</th>
                                    <th width="20%">Kode</th>
                                    <th>Dipakai</th>
                                    <th width="2%">Aksi</th>
                                </tr>
                                <tr role="row" class="filter">
                                    <td></td>
                                    <td><input type="text" class="form-control form-filter input-sm" name="merchant_nama"></td>
                                    <td><input type="text" class="form-control form-filter input-sm" name="promo_judul"></td>
                                    <td>
                                        <select class="form-control form-filter input-sm" name="promo_is_global" style="padding:0px;width:100%">
                                            <option value="">All</option>
                                            <option value="y">y</option>
                                            <option value="t">t</option>
                                        </select>
                                    </td>
                                    <td>
                                        <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                            <input type="text" class="form-control form-filter input-sm" id="promo_valid_until_start" readonly="" name="promo_valid_until_start" placeholder="From">
                                            <span class="input-group-btn">
                                                <button class="btn btn-sm default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                            <input type="text" class="form-control form-filter input-sm" id="promo_valid_until_end" readonly="" name="promo_valid_until_end" placeholder="To">
                                            <span class="input-group-btn">
                                                <button class="btn btn-sm default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </td>
                                    <td><input type="text" class="form-control form-filter input-sm" name="kode"></td>
                                    <td>
                                        <select class="form-control form-filter input-sm" name="is_used" style="padding:0px;width:100%">
                                            <option value="">All</option>
                                            <option value="y">y</option>
                                            <option value="t">t</option>
                                        </select>
                                    </td>
                                    <td>
                                        <div class="margin-bottom-5">
                                            <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                                <i class="fa fa-search"></i> Search
                                            </button>
                                        </div>
                                        <button class="btn btn-sm red btn-outline filter-cancel">
                                            <i class="fa fa-times"></i> Reset
                                        </button>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>

<div id="modal_form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<div id="modal_print" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<span id="site_url" data-site-url="<?php echo site_url() ?>"></span>

<script type="text/javascript">
$('.date-picker').datepicker({
    rtl: App.isRTL(),
    autoclose: true
});

$('.btn-cetak').click(function(event) {
    $('#modal_print').load('<?php echo site_url('admin/voucher/cetak_form') ?>',{
        <?php echo $csrf_name ?> :'<?php echo $csrf ?>'} ,
        function(){
        $('#modal_print').modal('show');
    });

});

$('#promo_valid_until_start').change(function(e) {
    var promo_valid_until_start = $('#promo_valid_until_start').val();
    var from_arr = promo_valid_until_start.split("/");
    var from = new Date(from_arr[2], from_arr[1]-1, from_arr[0]);

    var promo_valid_until_end = $('#promo_valid_until_end').val();
    var to_arr = promo_valid_until_end.split("/");
    var to = new Date(to_arr[2], to_arr[1]-1, to_arr[0]);

    if (from > to) {
        $('#promo_valid_until_end').val(promo_valid_until_start);
    }
});
$('#promo_valid_until_end').change(function(e) {
    var promo_valid_until_start = $('#promo_valid_until_start').val();
    var from_arr = promo_valid_until_start.split("/");
    var from = new Date(from_arr[2], from_arr[1]-1, from_arr[0]);

    var promo_valid_until_end = $('#promo_valid_until_end').val();
    var to_arr = promo_valid_until_end.split("/");
    var to = new Date(to_arr[2], to_arr[1]-1, to_arr[0]);

    if (to < from) {
        $('#promo_valid_until_start').val(promo_valid_until_end);
    }
});

var TableDatatablesAjax = function () {
    var grid = new Datatable();
    grid.init({
        src: $("#mytable"),
        dataTable: {
            "ajax": {
                "url": "<?php echo site_url('admin/voucher/ajax_list/') ?>", // ajax source
                data: function (d) {
                    d.<?php echo $csrf_name ?> = '<?php echo $csrf ?>';
                    d.merchant_nama            = $('[name=merchant_nama]').val();
                    d.promo_judul              = $('[name=promo_judul]').val();
                    d.promo_is_global          = $('[name=promo_is_global]').val();
                    d.kode                     = $('[name=kode]').val();
                    d.promo_valid_until_start  = $('[name=promo_valid_until_start]').val();
                    d.promo_valid_until_end    = $('[name=promo_valid_until_end]').val();
                    d.is_used                  = $('[name=is_used]').val();
                }
            },
            "order": [
                [1, "asc"]
            ]// set first column as a default sort by asc
        }
    });
}
jQuery(document).ready(function() {
   TableDatatablesAjax();
});

function btn_delete(id) {
    swal(
        {
            title: "Apakah Anda yakin?",
            text: "Voucher ini akan terhapus.",
            type: "warning",
            showCancelButton: true,
            cancelButtonClass: "btn-default",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, Delete it!",
            closeOnConfirm: true
        },
        function(){
            $.post('<?php echo site_url('admin/issue/delete/') ?>'+id, {<?php echo $csrf_name ?>: '<?php echo $csrf ?>'}, function(res) {
                if (res.success) {
                    $('.filter-cancel').trigger('click');
                    toastr.success(res.message);
                } else {
                    toastr.error(res.message);
                }
            });
        }
    );
}

</script>
