<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Cetak Voucher</title>
        <script type="text/javascript" language="javaScript">
        function tFunc() {
            window.print ();
            window.close ();
        }
        </script>
    </head>
    <body style="padding:0; margin:0 " onLoad="timerID=setTimeout('tFunc();', 1000);">
        <?php if ($voucher) : ?>
            <table>
                <tr>
                    <td>Nama Merchant</td>
                    <td>:</td>
                    <td><?php echo $merchant->nama ?></td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td><?php echo $merchant->alamat ?></td>
                </tr>
                <tr>
                    <td>Phone</td>
                    <td>:</td>
                    <td><?php echo $merchant->telepon1 ?></td>
                </tr>
            </table>
            <br>
            <?php foreach ($promo as $p) : ?>
                <br>
                <table  border="1">
                    <thead>
                        <tr>
                            <td colspan="3"><?php echo $p->judul ?></td>
                        </tr>
                        <tr>
                            <td colspan="3">Berlaku sampai : <?php echo date('d-m-Y', strtotime($p->valid_until)) ?></td>
                        </tr>
                        <tr>
                            <td width="10px">No</td>
                            <td width="150px">Kode Voucher</td>
                            <td width="30px">Status</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=0; ?>
                        <?php foreach ($voucher as $v) : ?>
                            <?php if ($v->judul == $p->judul): ?>
                                <tr>
                                    <td width="10px"><?php echo ++$i ?></td>
                                    <td width="150px"><?php echo $v->kode ?></td>
                                    <td width="30px"><?php echo $v->is_used ?></td>
                                </tr>
                            <?php endif ?>
                        <?php endforeach ?>
                    </tbody>
                </table>
            <?php endforeach ?>
        <?php else: ?>
            <tr>
                <td colspan="3">Belum ada Voucher</td>
            </tr>
        <?php endif ?>
    </body>
</html>

<style>
    table {
        border-collapse: collapse;
        padding: 10px;
    }
    td{
        padding-left: 5px;
    }
</style>