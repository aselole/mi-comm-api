
<div class="modal-dialog">
    <div class="modal-content c-square">
        <div class="modal-header">
            <button type="button" class="close" onclick="event.preventDefault(); closeModal();"></button>
            <h4 class="modal-title" id="exampleModalLabel">Form Cetak</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal">
                <div class="form-body">
                    <?php echo form_hidden($csrf_name, $csrf); ?>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Merchant *</label>
                        <div class="col-md-6">
                            <?php $merchant_dd = array ( '' => 'pilih' )+$merchant_dd ?>
                            <?php echo form_dropdown('merchant_id', $merchant_dd,'', "class='form-control select2 c-square'" ) ?>
                        </div>
                    </div>
                    <div class="form-group promo_dd_multi hide">
                        <label class="col-md-3 control-label">Promo *</label>
                        <div class="col-md-6">
                            <select name="promo_id[]" id="promo_dd_multi" class="form-control select2-multiple c-square" multiple>
                            </select>
                            <span class="help-block">Kosongi jika ingin mencetak semua voucher promo</span>

                        </div>
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <button type="submit" class="btn c-theme-btn c-btn-square c-btn-bold c-btn-uppercase kirim">Submit</button>
                <button type="button" class="btn c-theme-btn c-btn-border-2x c-btn-square c-btn-bold c-btn-uppercase" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>

    $('.kirim').click(function(e) {
        e.preventDefault();
        var merchant_id = $('select[name=merchant_id]').val();
        if ( merchant_id == '' ) {
            toastr.error('Merchant dibutuhkan.');
        }else{
            window.open('<?php echo site_url('admin/voucher/cetak_action?') ?>merchant_id='+merchant_id+'&promo_id='+$('#promo_dd_multi').val());
            $('#modal_print').modal('hide');
        }
    });


    $('.select2, .select2-multiple').select2({
        placeholder: 'Pilih',
        width: '100%'
    });

    $('select[name=merchant_id]').change(function(event) {
        $('.promo_dd_multi').removeClass('hide');
        var targetObj = $("#promo_dd_multi");
        var send_data = {merchant_id: $(this).val(), <?php echo $csrf_name ?>:'<?php echo $csrf ?>'};
        var url       = '<?php echo site_url('admin/promo/dropdownAjax') ?>';
        dropdownAjax (targetObj, send_data, url);
    });

    function dropdownAjax (targetObj, send_data, url) {
        targetObj.html("<option value='' >Loading...</option>");
        targetObj.select2("val", "");
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: send_data,
            beforeSend: function(e) {
                // targetObj.prop("disabled", true);
            }
        }).done(function(response) {
            console.log("success");
            if (response) {
                if (response.length > 0) {
                    targetObj.html("<option value='' >Pilih</option>");
                    for (i = 0; i < response.length; i++) {
                        var option = "<option value='" + response[i]['id'] + "' ";
                        option += " >" + response[i]['judul'] + "</option>";
                        targetObj.append(option);
                    }
                    // targetObj.prop("disabled", false);
                } else {
                    targetObj.html("<option value='' >Tidak ada data</option>");
                }
                targetObj.select2("val", "");
            }
        }).fail(function(response) {
            console.log('error');
            // main.alertMessage('Oops!', response.message, 'warning');
        }).always(function() {
            // console.log("complete");
        });
    }
</script>