<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo extends MX_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth'));
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('message', 'Anda belum login');
            redirect('/admin/login', 'refresh');
        } elseif (!$this->ion_auth->is_admin()) { // remove this elseif if you want to enable this for non-admins
            // redirect them to the home page because they must be an administrator to view this
            return show_error('You must be an administrator to view this page.');
        }

        $this->load->model(array('promo_model', 'merchant_model'));
        $this->load->model(array('promo_user_model', 'promo_claim_model', 'voucher_model'));
    }

    public function index()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Promo';
        $data['judul'] = '<i class="fa fa-bars"></i> Data Promo';
        $data['merchants'] = $this->merchant_model->fields('id, nama')->get_all();

        $this->template->load('layouts/template','promo/index', $data);
    }

    public function getDatatable()
    {
        $customActionName = $this->input->post('customActionName');
        $records          = array();

        if ($customActionName == "delete_checked") {
            $this->_delete_checked();
        }

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayStart  = intval($_REQUEST['start']);
        $sEcho          = intval($_REQUEST['draw']);

        $t              = $this->promo_model->get_limit_data($iDisplayStart, $iDisplayLength);
        $iTotalRecords  = $t['total_rows'];
        $get_data       = $t['get_db'];

        $records["data"] = array();
        $i=$iDisplayStart+1;
        if ($get_data) {
            foreach ($get_data as $d) {
                $checkbok= '<input type="checkbox" name="id[]" value="'.$d->id.'">';
                $view='<a href="#" class="btn btn-xs btn-outline green" onclick="event.preventDefault();btn_view('.$d->id.')" title="view">
                <i class="fa fa-search fa-lg"></i>
                </a> ';
                $edit='<a href="'.site_url('admin/promo/edit/'.$d->id).'" class="btn btn-xs btn-outline blue" title="edit">
                <i class="fa fa-pencil-square-o fa-lg"></i>
                </a> ';
                $delete='<a href="#" class="btn btn-xs btn-outline red" onclick="event.preventDefault();btn_delete('.$d->id.')" title="delete">
                <i class="fa fa-trash-o fa-lg"></i>
                </a> ';

                $checkbok  = '<div><input type="checkbox" name="id[]" value="'.$d->id.'"></div>';

                $records["data"][] = array(
                    $checkbok,
                    $i++,
                    $d->is_global,
                    $d->merchant_nama,
                    $d->judul,
                    date('d/m/Y', strtotime($d->published_at)),
                    date('d/m/Y', strtotime($d->valid_until)),
                    $d->active,
                    $view.$edit.$delete
                );
            }
        }
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        $this->output->set_content_type('application/json')->set_output(json_encode($records));
    }

    public function create()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Tambah Promo';
        $data['judul'] = '<i class="fa fa-plus"></i> Tambah Promo';

        $data['mode'] = 'add';
        $data['method'] = 'post';
        $data['action'] = site_url('admin/promo/store');
        $data['merchants'] = $this->merchant_model->fields('id, nama, alamat')->where('active', 'y')->get_all();

        $this->template->load('layouts/template','promo/form', $data);
    }

    public function store()
    {
        $config = array(
            array(
                'field'  => 'merchant_id',
                'label'  => 'Merchant',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'judul',
                'label'  => 'Judul',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'body',
                'label'  => 'Body',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'published_at',
                'label'  => 'Tgl Publish',
                'rules'  => 'required',
                'errors' => array(
                    'required'  => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'valid_until',
                'label'  => 'Valid Sampai',
                'rules'  => 'required',
                'errors' => array(
                    'required'  => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'active',
                'label'  => 'Aktif',
                'rules'  => 'required',
                'errors' => array(
                    'required'  => '%s tidak boleh kosong.',
                ),
            ),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $result['errors'] = $this->form_validation->error_array();
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        } else {
            $data = array(
                'is_global'   => $this->input->post('is_global'),
                'merchant_id' => $this->input->post('merchant_id'),
                'judul'       => $this->input->post('judul'),
                'body'        => $this->input->post('body'),
                'published_at' => date('Y-m-d', strtotime($this->input->post('published_at'))),
                'valid_until' => date('Y-m-d', strtotime($this->input->post('valid_until'))),
                'active'      => $this->input->post('active'),
            );

            try {
                if ( !empty($_FILES['featured_img']['name']) ) {
                    $upload = $this->upload_pic_promo();
                    if ($upload['success']) {
                        $data['featured_img'] = $upload['data']['file_name'];
                    } else {
                        $result = $upload;
                        throw new Exception('error');
                    }
                }

                $insert_id = $this->promo_model->insert($data);
                if ($insert_id) {
                    $result['success'] = true;
                    $result['message'] = 'Promo berhasil ditambahkan.';
                    $result['url']     = site_url('admin/promo');
                } else {
                    $result['success'] = false;
                    $result['message'] = 'Terjadi kesalahan.';
                }
            } catch (Exception $e) {
                $this->output
                    ->set_status_header(400);
            }
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }
    }

    public function edit($id)
    {
        $exist = $this->promo_model->get($id);
        if (!$exist) {
            $this->session->set_flashdata('notif_type', 'alert-warning');
            $this->session->set_flashdata('notif_msg', 'Data tidak ditemukan.');
            redirect('admin/promo');
        }

        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Edit Promo';
        $data['judul'] = '<i class="fa fa-edit"></i> Edit User';

        $data['dt']     = $this->promo_model->get($id);
        $data['mode']   = 'edit';
        $data['method'] = 'post';
        $data['action'] = site_url('admin/promo/update/'.$id);
        $data['merchants'] = $this->merchant_model->fields('id, nama, alamat')->where('active', 'y')->get_all();

        $this->template->load('layouts/template','promo/form', $data);
    }

    public function update($id)
    {
        $exist = $this->promo_model->get($id);
        if (!$exist) {
            $result['success'] = false;
            $result['message'] = 'Data tidak ditemukan.';
            $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
        } else {
            $config = array(
                array(
                    'field'  => 'merchant_id',
                    'label'  => 'Merchant',
                    'rules'  => 'required',
                    'errors' => array(
                        'required' => '%s tidak boleh kosong.',
                    ),
                ),
                array(
                    'field'  => 'judul',
                    'label'  => 'Judul',
                    'rules'  => 'required',
                    'errors' => array(
                        'required' => '%s tidak boleh kosong.',
                    ),
                ),
                array(
                    'field'  => 'body',
                    'label'  => 'Body',
                    'rules'  => 'required',
                    'errors' => array(
                        'required' => '%s tidak boleh kosong.',
                    ),
                ),
                array(
                    'field'  => 'published_at',
                    'label'  => 'Tgl Publish',
                    'rules'  => 'required',
                    'errors' => array(
                        'required'  => '%s tidak boleh kosong.',
                    ),
                ),
                array(
                    'field'  => 'valid_until',
                    'label'  => 'Valid Sampai',
                    'rules'  => 'required',
                    'errors' => array(
                        'required'  => '%s tidak boleh kosong.',
                    ),
                ),
                array(
                    'field'  => 'active',
                    'label'  => 'Aktif',
                    'rules'  => 'required',
                    'errors' => array(
                        'required'  => '%s tidak boleh kosong.',
                    ),
                ),
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE){
                $result['errors'] = $this->form_validation->error_array();
                $this->output
                    ->set_status_header(400)
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            } else {
                $data = array(
                    'merchant_id' => $this->input->post('merchant_id'),
                    'judul'       => $this->input->post('judul'),
                    'body'        => $this->input->post('body'),
                    'published_at' => date('Y-m-d', strtotime($this->input->post('published_at'))),
                    'valid_until' => date('Y-m-d', strtotime($this->input->post('valid_until'))),
                    'active'      => $this->input->post('active'),
                );

                try {
                    if ( !empty($_FILES['featured_img']['name']) ) {
                        $upload = $this->upload_pic_promo();
                        if ($upload['success']) {
                            $data['featured_img'] = $upload['data']['file_name'];
                            if (file_exists('./uploads/promo/'.$exist->featured_img)) {
                                unlink('./uploads/promo/'.$exist->featured_img);
                            }
                        } else {
                            $result = $upload;
                            throw new Exception('error');
                        }
                    }

                    $this->promo_model->update($data, $id);
                    $result['success'] = true;
                    $result['message'] = 'Promo berhasil diperbarui.';
                    $result['url']     = site_url('admin/promo');
                } catch (Exception $e) {
                    $result['success'] = false;
                    $result['message'] = 'Terjadi kesalahan.';
                    if ($e == 'error') {
                        $this->output->set_status_header(400);
                    }
                }
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            }
        }
    }

    public function read($id)
    {
        $exist = $this->promo_model->get($id);
        if (!$exist) {
            $this->session->set_flashdata('notif_type', 'alert-warning');
            $this->session->set_flashdata('notif_msg', 'Data tidak ditemukan.');
            redirect('admin/promo');
        }

        $data['judul'] = '<i class="fa fa-search"></i> View Promo';
        $data['dt']    = $this->promo_model->get($id);
        $this->load->view('promo/read', $data);
    }

    public function delete($id)
    {
        $exist = $this->promo_model->get($id);
        if (!$exist) {
            $result['success'] = false;
            $result['message'] = 'Data tidak ditemukan.';
            $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
        } else {
            if ($exist->is_global=='t') {
                $cek_promo_user = $this->promo_model->with_promo_user('where:`promo_id`='.$id, 'fields:*count*')->get($id);
                if (count($cek_promo_user->promo_user) == 0) {
                    $delete = $this->promo_model->delete($id);
                    $result['success'] = true;
                    $result['message'] = 'Promo berhasil dihapus.';
                } else {
                    $result['success'] = false;
                    $result['message'] = 'Promo ini memiliki Promo User.';
                }
            } elseif ($exist->is_global=='y') {
                $cek_promo_claim = $this->promo_model->with_promo_claim('where:`promo_id`='.$id, 'fields:*count*')->get($id);
                if (count($cek_promo_claim->promo_claim) == 0) {
                    $delete = $this->promo_model->delete($id);
                    $result['success'] = true;
                    $result['message'] = 'Promo berhasil dihapus.';
                } else {
                    $result['success'] = false;
                    $result['message'] = 'Promo ini memiliki Promo Claim.';
                }
            }
            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
    }

    public function upload_pic_promo()
    {
        $config['upload_path'] = './uploads/promo/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']  = '90000';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('featured_img')) {
            $error = array('error' => $this->upload->display_errors());
            $result['success'] = false;
            $result['message'] = $error['error'];
        } else {
            $upload_data = $this->upload->data();
            $result = array(
                'success' => true,
                'data' => $upload_data,
            );
        }
        return $result;
    }

    public function _delete_checked()
    {
        $id = $this->input->post('id');
        foreach ($id as $key => $value) {
            if ($this->promo_model->delete($value)) {
                $this->promo_user_model->delete( array ('promo_id' => $value ) );
                $this->promo_claim_model->delete( array ('promo_id' => $value ) );
                $this->voucher_model->delete( array ('promo_id' => $value ) );
            }
        }
    }

    public function dropdownAjax($value='')
    {
        $merchant_id  = $this->input->post('merchant_id');
        $res = $this->promo_model->fields('id,judul')->where('merchant_id', $merchant_id)->get_all();
        $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }

}
