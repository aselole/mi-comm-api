<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Voucher extends MX_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth'));
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('message', 'Anda belum login');
            redirect('/admin/login', 'refresh');
        } elseif (!$this->ion_auth->is_admin()) { // remove this elseif if you want to enable this for non-admins
            // redirect them to the home page because they must be an administrator to view this
            return show_error('You must be an administrator to view this page.');
        }

        $this->load->model(array('voucher_model', 'promo_model', 'merchant_model'));
    }

    public function index()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf'] = $this->security->get_csrf_hash();

        $data['title']       = 'Voucher';
        $data['judul']       = '<i class="fa fa-bars"></i> Data Voucher';

        $this->template->load('layouts/template','voucher/index', $data);
    }

    public function ajax_list()
    {
        $records = $this->voucher_model->ajax_list();
        $this->output->set_content_type('application/json')->set_output(json_encode($records));
    }

    public function datatable_promo()
    {
        $customActionName = $this->input->post('customActionName');
        $records          = array();

        if ($customActionName == "delete") {
            $records=$this-> delete_checked();
        }

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayStart  = intval($_REQUEST['start']);
        $sEcho          = intval($_REQUEST['draw']);

        $t              = $this->promo_model->promo_voucher($iDisplayStart, $iDisplayLength);
        $iTotalRecords  = $t['total_rows'];
        $get_data       = $t['get_db'];

        $records["data"] = array();
        $i=$iDisplayStart+1;
        if ($get_data) {
            foreach ($get_data as $d) {
                $checkbok= '<input type="checkbox" name="id[]" value="'.$d->id.'">';
                $pilih='<a href="#" class="btn btn-xs btn-outline green" onclick="event.preventDefault();btn_pilih('.$d->id.')" title="view">
                <i class="fa fa-check fa-lg"></i>
                </a> ';

                $records["data"][] = array(
                    $i++,
                    $d->is_global,
                    $d->merchant_nama,
                    $d->judul,
                    date('d/m/Y', strtotime($d->valid_until)),
                    date('d/m/Y', strtotime($d->created_at)),
                    $pilih
                );
            }
        }
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        $this->output->set_content_type('application/json')->set_output(json_encode($records));
    }

    public function modal_promo()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();
        $data['judul']  = '<i class="fa fa-search"></i> Pilih Promo';
        $data['merchants'] = $this->merchant_model->fields('id, nama')->get_all();

        $this->load->view('voucher/modal_promo', $data);
    }

    public function create()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Tambah Voucher';
        $data['judul'] = '<i class="fa fa-plus"></i> Tambah Voucher';

        $data['mode'] = 'add';
        $data['method'] = 'post';
        $data['action'] = site_url('admin/voucher/store');

        $data['promos'] = $this
                            ->promo_model
                            ->fields('id, judul')
                            ->where('active', 'y')
                            ->where('valid_until >=', date('Y-m-d'))
                            ->get_all();

        $this->template->load('layouts/template','voucher/form', $data);
    }

    public function store()
    {
        $config = array(
            array(
                'field'  => 'promo_id',
                'label'  => 'Promo',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'jml_voucher',
                'label'  => 'Jumlah Voucher',
                'rules'  => 'required|integer',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                    'integer' => '%s harus angka.',
                ),
            ),
            array(
                'field'  => 'length_voucher',
                'label'  => 'Panjang Voucher',
                'rules'  => 'required|integer',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                    'integer' => '%s harus angka.',
                ),
            ),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $result['errors'] = $this->form_validation->error_array();
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        } else {
            $promo_id = $this->input->post('promo_id');
            $length_voucher = $this->input->post('length_voucher');
            $jml_voucher = $this->input->post('jml_voucher');

            for ($i=0; $i < $jml_voucher; $i++) {
                $coupon = $this->get_coupon($length_voucher);
                $data = array(
                    'promo_id' => $promo_id,
                    'kode' => $coupon,
                );
                $this->voucher_model->insert($data);
            }
            $result = array(
                'success' => true,
                'message' => 'Voucher berhasil ditambahkan.',
                'url'     => site_url('admin/voucher')
            );
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }
    }

    public function get_coupon($length_voucher)
    {
        $coupon = generateRandomString($length_voucher);
        $lower_coupon = strtolower($coupon);
        $cek = $this->db->query("select * from vouchers where LOWER(kode) = '$lower_coupon'");
        while ($cek->num_rows() > 0) {
            $coupon = $this->get_coupon();
        }
        return $coupon;
    }

    public function cetak_form()
    {
        $data['csrf_name']   = $this->security->get_csrf_token_name();
        $data['csrf']        = $this->security->get_csrf_hash();

        $data['merchant_dd'] = $this->merchant_model->as_dropdown('nama')->get_all();
        $this->load->view('voucher/modal_cetak', $data, FALSE);
    }

    public function cetak_action()
    {
        $merchant_id = $this->input->get('merchant_id');
        if (($this->input->get('promo_id') != 'null')) {
            $promo_id_arr = $this->input->get('promo_id');
            $sql = "SELECT
                        merchants.id AS merchant_id,
                        merchants.nama,
                        promos.id AS promo_id,
                        promos.judul,
                        promos.valid_until,
                        vouchers.kode,
                        vouchers.is_used
                    FROM
                        merchants
                    LEFT JOIN promos ON promos.merchant_id = merchants.id
                    LEFT JOIN vouchers ON vouchers.promo_id = promos.id
                    WHERE promo_id in ($promo_id_arr)
                    ORDER BY
                        merchant_id ASC,
                        promo_id ASC";
            $sql2 = "SELECT
                        merchants.id AS merchant_id,
                        merchants.nama,
                        promos.id AS promo_id,
                        promos.judul,
                        promos.valid_until,
                        vouchers.kode,
                        vouchers.is_used
                    FROM
                        merchants
                    LEFT JOIN promos ON promos.merchant_id = merchants.id
                    LEFT JOIN vouchers ON vouchers.promo_id = promos.id
                    WHERE promo_id in ($promo_id_arr)
                    GROUP BY
                        promos.judul";
        }else{
            $sql = "SELECT
                        merchants.id AS merchant_id,
                        merchants.nama,
                        promos.id AS promo_id,
                        promos.judul,
                        promos.valid_until,
                        vouchers.kode,
                        vouchers.is_used
                    FROM
                        merchants
                    LEFT JOIN promos ON promos.merchant_id = merchants.id
                    LEFT JOIN vouchers ON vouchers.promo_id = promos.id
                    WHERE merchant_id = $merchant_id
                    ORDER BY
                        merchant_id ASC,
                        promo_id ASC";
            $sql2 = "SELECT
                        merchants.id AS merchant_id,
                        merchants.nama,
                        promos.id AS promo_id,
                        promos.judul,
                        promos.valid_until,
                        vouchers.kode,
                        vouchers.is_used
                    FROM
                        merchants
                    LEFT JOIN promos ON promos.merchant_id = merchants.id
                    LEFT JOIN vouchers ON vouchers.promo_id = promos.id
                    WHERE merchant_id = $merchant_id
                    GROUP BY
                        promos.judul";
        }
        $db = $this->db->query($sql);
        $data['voucher'] = false;
        if ($db->num_rows() > 0) {
            $data['promo']   = $this->db->query($sql2)->result();
            $data['voucher'] = $db->result();
        }
        $data['merchant'] = $this->merchant_model->get($merchant_id);

        $this->load->view('voucher/form_cetak', $data);
    }

}
