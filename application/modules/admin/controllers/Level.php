<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Level extends MX_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth'));
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('message', 'Anda belum login');
    		redirect('/admin/login', 'refresh');
		} elseif (!$this->ion_auth->is_admin()) { // remove this elseif if you want to enable this for non-admins
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

        $this->load->model(array('level_model'));
    }

    public function index()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Level';
        $data['judul'] = '<i class="fa fa-bars"></i> Data Level';

        $this->template->load('layouts/template','level/index', $data);
    }

    public function getDatatable()
    {
        $customActionName = $this->input->post('customActionName');
        $records          = array();

        if ($customActionName == "delete") {
            $records=$this-> delete_checked();
        }

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayStart  = intval($_REQUEST['start']);
        $sEcho          = intval($_REQUEST['draw']);

        $t              = $this->level_model->get_limit_data($iDisplayStart, $iDisplayLength);
        $iTotalRecords  = $t['total_rows'];
        $get_data       = $t['get_db'];

        $records["data"] = array();

        $i=$iDisplayStart+1;
        if ($get_data) {
            foreach ($get_data as $d) {
                $checkbok= '<input type="checkbox" name="id[]" value="'.$d->id.'">';
                $edit='<a href="#" class="btn btn-sm btn-outline blue" onclick="event.preventDefault();btn_edit('.$d->id.')" title="edit">
                <i class="fa fa-pencil-square-o fa-lg"></i>
                </a> ';
                $delete='<a href="#" class="btn btn-sm btn-outline red" onclick="event.preventDefault();btn_delete('.$d->id.')" title="delete">
                <i class="fa fa-trash-o fa-lg"></i>
                </a> ';

                $records["data"][] = array(
                    $i++,
                    $d->nama,
					$d->ket,
                    ''
                    // $edit.$delete
                );
            }
        }
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        $this->output->set_content_type('application/json')->set_output(json_encode($records));
    }

    public function create()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['mode']   = 'create';
        $data['judul']  = '<i class="fa fa-plus"></i> Tambah Level';
        $data['method'] = 'post';
        $data['action'] = site_url('admin/level/store');
        $this->load->view('level/form', $data);
    }

    public function store()
    {
        $config = array(
            array(
                'field'  => 'nama',
                'label'  => 'Nama',
                'rules'  => 'trim|required|is_unique[levels.nama]',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                    'is_unique' => '%s sudah ada.',
                ),
            ),
            array(
                'field'  => 'ket',
                'label'  => 'Keterangan',
                'rules'  => 'required',
                'errors' => array(
                    'required'  => '%s tidak boleh kosong.',
                ),
            ),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $result['errors'] = $this->form_validation->error_array();
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        } else {
            $data = array(
                'nama' => $this->input->post('nama'),
                'ket'  => $this->input->post('ket'),
            );
            $insert_id = $this->level_model->insert($data);
            if ($insert_id) {
                $result['success'] = true;
                $result['message'] = 'Level berhasil ditambahkan.';
            } else {
                $result['success'] = false;
            }
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }
    }

    public function edit($id)
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['mode']   = 'edit';
        $data['judul']  = '<i class="fa fa-edit"></i> Edit Level';
        $data['dt']     = $this->level_model->get($id);
        $data['method'] = 'post';
        $data['action'] = site_url('admin/level/update/'.$id);
        $this->load->view('level/form', $data);
    }

    public function update($id)
    {
        $is_unique = '';
        $exists = $this->level_model->get($id);
        if (!$exists) {
            $result['success'] = false;
            $result['message'] = 'Data tidak ditemukan.';
            $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
        } else {
            if ($this->input->post('nama') != $exists->nama) {
                $is_unique = '|is_unique[levels.nama]';
            }
            $config = array(
                array(
                    'field'  => 'nama',
                    'label'  => 'Nama',
                    'rules'  => 'trim|required'.$is_unique,
                    'errors' => array(
                        'required' => '%s tidak boleh kosong.',
                        'is_unique' => '%s sudah ada.',
                    ),
                ),
                array(
                    'field'  => 'ket',
                    'label'  => 'Keterangan',
                    'rules'  => 'required',
                    'errors' => array(
                        'required'  => '%s tidak boleh kosong.',
                    ),
                ),
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE){
                $result['errors'] = $this->form_validation->error_array();
                $this->output
                    ->set_status_header(400)
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            } else {
                $data = array(
                    'nama' => $this->input->post('nama'),
                    'ket'  => $this->input->post('ket'),
                );
                $update = $this->level_model->update($data, $id);
                if ($update) {
                    $result['success'] = true;
                    $result['message'] = 'Level berhasil diperbarui.';
                } else {
                    $result['success'] = false;
                }
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            }
        }
    }

    public function delete($id)
    {
        $exists = $this->level_model->get($id);
        if (!$exists) {
            $result['success'] = false;
            $result['message'] = 'Data tidak ditemukan.';
            $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
        } else {
            $delete = $this->level_model->delete($id);
            $result['success'] = true;
            $result['message'] = 'Level berhasil dihapus.';
            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
    }

}
