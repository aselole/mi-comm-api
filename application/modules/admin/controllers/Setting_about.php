<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_about extends MX_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth'));
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('message', 'Anda belum login');
    		redirect('/admin/login', 'refresh');
		} elseif (!$this->ion_auth->is_admin()) { // remove this elseif if you want to enable this for non-admins
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

    }

    public function index()
    {
        $kode = 'about';
        $exist = $this->db->query("select * from master_settings where kode='$kode'")->row();

        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Setting About';
        $data['judul'] = '<i class="fa fa-pencil"></i> Setting About';

        $data['dt']     = $exist;
        $data['mode']   = 'edit';
        $data['method'] = 'post';
        $data['action'] = site_url('admin/setting_about/update');

        $this->template->load('layouts/template','master_setting/form_about', $data);
    }

    public function update()
    {
        $kode = $this->input->post('kode');
        $config = array(
            array(
                'field'  => 'isi',
                'label'  => 'Isi',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $result['errors'] = $this->form_validation->error_array();
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        } else {
            $isi = $this->input->post('isi');
            $update = $this->db->query("update master_settings set isi='$isi' where kode='$kode'");
            $result = array(
                'success' => true,
                'message' => 'Setting About berhasil disimpan',
                'url' => site_url('admin/setting_about'),
            );
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }
    }

}
