<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_email extends MX_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth'));
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('message', 'Anda belum login');
    		redirect('/admin/login', 'refresh');
		} elseif (!$this->ion_auth->is_admin()) { // remove this elseif if you want to enable this for non-admins
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

        $this->load->model(array('master_email_model'));
    }

    public function index()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title']       = 'Master Email';
        $data['judul']       = '<i class="fa fa-bars"></i> Data Master Email';
        $data['master_emails'] = $this->master_email_model->get_all();

        $this->template->load('layouts/template','master_email/index', $data);
    }

    public function edit($id)
    {
        $exist = $this->master_email_model->get($id);
        if (!$exist) {
            $this->session->set_flashdata('notif_type', 'alert-warning');
            $this->session->set_flashdata('notif_msg', 'Data tidak ditemukan.');
            redirect('admin/master_email');
        }

        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Edit Master Email';
        $data['judul'] = '<i class="fa fa-pencil"></i> Edit Master Email';

        $data['dt']     = $exist;
        $data['mode']   = 'edit';
        $data['method'] = 'post';
        $data['action'] = site_url('admin/master_email/update');

        $this->template->load('layouts/template','master_email/form', $data);
    }

    public function update()
    {
        $id = $this->input->post('id');
        $id = 1;
        $config = array(
            array(
                'field'  => 'isi',
                'label'  => 'Body',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $result['errors'] = $this->form_validation->error_array();
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        } else {
            $data = array(
                'isi' => $this->input->post('isi'),
            );
            $exist = $this->master_email_model->get($id);
            if (!$exist) {
                $result['success'] = false;
                $result['message'] = 'Data tidak ditemukan.';
                $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
            } else {
                $this->master_email_model->update($data, $id);
                $result = array(
                    'success' => true,
                    'message' => 'Master Email berhasil disimpan',
                    'url' => site_url('admin/master_email'),
                );
            }
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }
    }

}
