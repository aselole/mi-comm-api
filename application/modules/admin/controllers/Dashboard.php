<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->library(array('ion_auth'));
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('message', 'Anda belum login');
    		redirect('/admin/login', 'refresh');
		}
	}

	public function index()
	{
		$this->load->model(array('community_model', 'member_model', 'bad_comment_model', 'promo_model'));
		$this->load->model('promo_claim_model');
		$this->load->model('promo_user_model');
		
		$data['title'] = 'Dashboard';
		if (!$this->ion_auth->logged_in()) {
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		} elseif (!$this->ion_auth->is_admin()) { // remove this elseif if you want to enable this for non-admins
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		} else {
			$data['jumlah_komunitas']      = $this->community_model->count_rows();
			$data['jumlah_member']         = $this->member_model->where('active','y')->count_rows();
			$data['jumlah_komentar_buruk'] = $this->bad_comment_model->count_rows();
            $date_now         = date('Y-m-d');
            $batas_date_valid = date('Y-m-d', strtotime($date_now));

            $where = array('active'=> 'y', 'valid_until  >= "'.$batas_date_valid.'"' => null, 'is_global' => 't');
			$data['promo_khusus'] = $this->promo_model->order_by('published_at', 'desc')->get_all($where);
            $where = array('active'=> 'y', 'valid_until  >= "'.$batas_date_valid.'"' => null, 'is_global' => 'y');
			$data['promo_global'] = $this->promo_model->order_by('published_at', 'desc')->get_all($where);

			$data['promo_claim'] = $this->promo_claim_model->list_dashboard();
			$data['promo_user']  = $this->promo_user_model->list_dashboard();
			$this->template->load('layouts/template_dashboard','dashboard/index', $data);
		}
	}

}

/* End of file Dashboard.php */
