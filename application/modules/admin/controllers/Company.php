<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends MX_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth'));
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('message', 'Anda belum login');
    		redirect('/admin/login', 'refresh');
		} elseif (!$this->ion_auth->is_admin()) { // remove this elseif if you want to enable this for non-admins
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

        $this->load->model(array('company_model', 'community_model'));
    }

    public function index()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Perusahaan';
        $data['judul'] = '<i class="fa fa-bars"></i> Data Perusahaan';
        $data['jenis_company'] = get_enum('company', 'jenis');

        $this->template->load('layouts/template','company/index', $data);
    }

    public function getDatatable()
    {
        $customActionName = $this->input->post('customActionName');
        $records          = array();

        if ($customActionName == "delete") {
            $records=$this-> delete_checked();
        }

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayStart  = intval($_REQUEST['start']);
        $sEcho          = intval($_REQUEST['draw']);

        $t              = $this->company_model->get_limit_data($iDisplayStart, $iDisplayLength);
        $iTotalRecords  = $t['total_rows'];
        $get_data       = $t['get_db'];

        $records["data"] = array();

        $i=$iDisplayStart+1;
        if ($get_data) {
            foreach ($get_data as $d) {
                $checkbok= '<input type="checkbox" name="id[]" value="'.$d->id.'">';
                $edit='<a href="#" class="btn btn-sm btn-outline blue" onclick="event.preventDefault();btn_edit('.$d->id.')" title="edit">
                <i class="fa fa-pencil-square-o fa-lg"></i>
                </a> ';
                $delete='<a href="#" class="btn btn-sm btn-outline red" onclick="event.preventDefault();btn_delete('.$d->id.')" title="delete">
                <i class="fa fa-trash-o fa-lg"></i>
                </a> ';

                $records["data"][] = array(
                    $i++,
                    $d->nama,
                    $d->jenis,
                    // $d->alamat,
                    // $d->email,
                    // $d->telepon1,
					// $d->telepon2,
                    $edit.$delete
                );
            }
        }
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        $this->output->set_content_type('application/json')->set_output(json_encode($records));
    }

    public function create()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['mode']          = 'create';
        $data['judul']         = '<i class="fa fa-plus"></i> Tambah Perusahaan';
        $data['method']        = 'post';
        $data['action']        = site_url('admin/company/store');
        $data['jenis_company'] = get_enum('company', 'jenis');

        $this->load->view('company/form', $data);
    }

    public function store()
    {
        $config = array(
            array(
                'field'  => 'nama',
                'label'  => 'Nama',
                'rules'  => 'trim|required|is_unique[company.nama]',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                    'is_unique' => '%s sudah ada.',
                ),
            ),
            array(
                'field'  => 'jenis',
                'label'  => 'Jenis',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            // array(
            //     'field'  => 'alamat',
            //     'label'  => 'Alamat',
            //     'rules'  => 'required',
            //     'errors' => array(
            //         'required'  => '%s tidak boleh kosong.',
            //     ),
            // ),
            // array(
            //     'field'  => 'telepon1',
            //     'label'  => 'Telepon 1',
            //     'rules'  => 'required',
            //     'errors' => array(
            //         'required'  => '%s tidak boleh kosong.',
            //     ),
            // ),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $result['errors'] = $this->form_validation->error_array();
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        } else {
            $data = array(
                'nama'     => $this->input->post('nama'),
                'jenis'    => $this->input->post('jenis'),
                // 'alamat'   => $this->input->post('alamat'),
                // 'email'    => $this->input->post('email'),
                // 'telepon1' => $this->input->post('telepon1'),
                // 'telepon2' => $this->input->post('telepon2'),
            );
            $insert_id = $this->company_model->insert($data);
            if ($insert_id) {
                $result['success'] = true;
                $result['message'] = 'Perusahaan berhasil ditambahkan.';
                $result['url'] = '';
            } else {
                $result['success'] = false;
            }
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }
    }

    public function edit($id)
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['mode']          = 'edit';
        $data['judul']         = '<i class="fa fa-edit"></i> Edit Perusahaan';
        $data['dt']            = $this->company_model->get($id);
        $data['method']        = 'post';
        $data['action']        = site_url('admin/company/update/'.$id);
        $data['jenis_company'] = get_enum('company', 'jenis');

        $this->load->view('company/form', $data);
    }

    public function update($id)
    {
        $is_unique = '';
        $exists = $this->company_model->get($id);
        if (!$exists) {
            $result['success'] = false;
            $result['message'] = 'Data tidak ditemukan.';
            $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
        } else {
            if ($this->input->post('nama') != $exists->nama) {
                $is_unique = '|is_unique[company.nama]';
            }
            $config = array(
                array(
                    'field'  => 'nama',
                    'label'  => 'Nama',
                    'rules'  => 'trim|required'.$is_unique,
                    'errors' => array(
                        'required' => '%s tidak boleh kosong.',
                        'is_unique' => '%s sudah ada.',
                    ),
                ),
                array(
                    'field'  => 'jenis',
                    'label'  => 'Jenis',
                    'rules'  => 'required',
                    'errors' => array(
                        'required' => '%s tidak boleh kosong.',
                    ),
                ),
                // array(
                //     'field'  => 'alamat',
                //     'label'  => 'Alamat',
                //     'rules'  => 'required',
                //     'errors' => array(
                //         'required'  => '%s tidak boleh kosong.',
                //     ),
                // ),
                // array(
                //     'field'  => 'telepon1',
                //     'label'  => 'Telepon 1',
                //     'rules'  => 'required',
                //     'errors' => array(
                //         'required'  => '%s tidak boleh kosong.',
                //     ),
                // ),
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE){
                $result['errors'] = $this->form_validation->error_array();
                $this->output
                    ->set_status_header(400)
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            } else {
                $data = array(
                    'nama'     => $this->input->post('nama'),
                    'jenis'    => $this->input->post('jenis'),
                    // 'alamat'   => $this->input->post('alamat'),
                    // 'email'    => $this->input->post('email'),
                    // 'telepon1' => $this->input->post('telepon1'),
                    // 'telepon2' => $this->input->post('telepon2'),
                );
                $update = $this->company_model->update($data, $id);
                if ($update) {
                    $result['success'] = true;
                    $result['message'] = 'Perusahaan berhasil diperbarui.';
                    $result['url'] = '';
                } else {
                    $result['success'] = false;
                }
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            }
        }
    }

    public function delete($id)
    {
        $exists = $this->company_model->get($id);
        if (!$exists) {
            $result['success'] = false;
            $result['message'] = 'Data tidak ditemukan.';
            $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
        } else {
            $cek_community = $this->community_model->where('company_id', $id)->get_all();
            if (!$cek_community) {
                $delete = $this->company_model->delete($id);
                $result['success'] = true;
                $result['message'] = 'Perusahaan berhasil dihapus.';
            } else {
                $result['success'] = false;
                $result['message'] = 'Perusahaan ini memiliki Community.';
            }
            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
    }

}
