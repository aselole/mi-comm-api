<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Private_issue extends MX_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth'));
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('message', 'Anda belum login');
    		redirect('/admin/login', 'refresh');
		} elseif (!$this->ion_auth->is_admin()) { // remove this elseif if you want to enable this for non-admins
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

        $this->load->model(array('private_issue_model', 'private_issue_comment_model'));
    }

    public function index()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title']       = 'Private Issue';
        $data['judul']       = '<i class="fa fa-bars"></i> Data Private Issue';
        $data['status']      = get_enum('private_issues', 'status');

        $this->template->load('layouts/template','private_issue/index', $data);
    }

    public function ajax_list()
	{
		$records = $this->private_issue_model->ajax_list();
		$this->output->set_content_type('application/json')->set_output(json_encode($records));
	}

    public function comment($id)
    {
        $sql = "
        select
            a.*,
            b.first_name as member_first_name,
            c.first_name as trainer_first_name
        from private_issues a
        join users b on (a.user_member_id=b.id)
        join users c on (a.user_trainer_id=c.id)
        where a.id = $id
        ";
        $exist = $this->db->query($sql);

        if ($exist->num_rows() == 0) {
            $this->session->set_flashdata('notif_type', 'alert-warning');
            $this->session->set_flashdata('notif_msg', 'Data tidak ditemukan.');
            redirect('admin/private_issues');
        }

        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Private Issue';
        $data['judul'] = '<i class="fa fa-commnt-o"></i> Manajemen Komentar Private Issue';

        $data['dt']          = $exist->row();
        $data['mode']        = 'comment';
        $data['comments'] = $this
                            ->private_issue_comment_model
                            ->fields('id, user_id, comment, read, created_at')
                            ->where(array('private_issue_id'=>$id, 'comment !='=>null))
                            ->order_by('created_at', 'asc')
                            ->with_user('fields:first_name,photo,level_id|join:true')
                            ->get_all();
        $this->template->load('layouts/template','private_issue/comment', $data);
    }

    public function edit($id)
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['mode']   = 'edit';
        $data['judul']  = '<i class="fa fa-edit"></i> Edit Private Issue';
        $data['dt']     = $this->private_issue_model->get($id);
        $data['method'] = 'post';
        $data['action'] = site_url('admin/private_issue/update/'.$id);
        $data['status'] = get_enum('private_issues', 'status');

        $this->load->view('private_issue/form', $data);
    }

    public function update($id)
    {
        $exists = $this->private_issue_model->get($id);
        if (!$exists) {
            $result['success'] = false;
            $result['message'] = 'Data tidak ditemukan.';
            $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
        } else {
            $config = array(
                array(
                    'field'  => 'status',
                    'label'  => 'Status',
                    'rules'  => 'required',
                    'errors' => array(
                        'required' => '%s tidak boleh kosong.',
                    ),
                ),
                array(
                    'field'  => 'active',
                    'label'  => 'Aktif',
                    'rules'  => 'required',
                    'errors' => array(
                        'required' => '%s tidak boleh kosong.',
                    ),
                ),
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE){
                $result['errors'] = $this->form_validation->error_array();
                $this->output
                    ->set_status_header(400)
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            } else {
                $data = array(
                    'status'     => $this->input->post('status'),
                    'active'    => $this->input->post('active'),
                );
                $update = $this->private_issue_model->update($data, $id);
                if ($update) {
                    $result['success'] = true;
                    $result['message'] = 'Private Issue berhasil diperbarui.';
                    $result['url']     = site_url('admin/private_issue/comment/'.$id);
                } else {
                    $result['success'] = false;
                }
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            }
        }
    }

    public function delete($id)
    {
        $exist = $this->private_issue_model->get($id);
        if (!$exist) {
            $result['success'] = false;
            $result['message'] = 'Data tidak ditemukan.';
            $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
        } else {
            $delete_comments = $this->private_issue_comment_model->delete(array('private_issue_id'=>$id));
            $delete = $this->private_issue_model->delete($id);
            if ($delete) {
                $result = array(
                    'success' => true,
                    'message' => 'Private Issue berhasil dihapus',
                );
            } else {
                $result['success'] = false;
                $result['message'] = 'Terjadi kesalahan.';
                $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
            }
            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
    }

}
