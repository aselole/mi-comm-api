<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bad_comment extends MX_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth'));
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('message', 'Anda belum login');
    		redirect('/admin/login', 'refresh');
		} elseif (!$this->ion_auth->is_admin()) { // remove this elseif if you want to enable this for non-admins
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

        $this->load->model(array('bad_comment_model', 'issue_comment_model'));
    }

    public function index()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title']       = 'Komentar Buruk';
        $data['judul']       = '<i class="fa fa-bars"></i> Data Komentar Buruk';
        $data['status']      = get_enum('issues', 'status');

        $this->template->load('layouts/template','bad_comment/index', $data);
    }

    public function ajax_list()
	{
		$records = $this->bad_comment_model->ajax_list();
		$this->output->set_content_type('application/json')->set_output(json_encode($records));
	}

    public function delete($id)
    {
        $exist = $this->bad_comment_model->fields('issue_comment_id')->get($id);
        if (!$exist) {
            $result = array(
                'success' => false,
                'message' => 'Data tidak ditemukan.',
            );
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }

        $issue_comment_id = $exist->issue_comment_id;
        $delete1 = $this->issue_comment_model->delete(array('parent_id'=>$issue_comment_id));
        $delete2 = $this->issue_comment_model->delete($issue_comment_id);
        $delete3 = $this->bad_comment_model->delete(array('issue_comment_id'=>$issue_comment_id));
        if ($delete3) {
            $result = array(
                'success' => true,
                'message' => 'Komentar berhasil dihapus',
            );
        } else {
            $result = array(
                'success' => false,
                'message' => 'Terjadi kesalahan.',
            );
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function abaikan($id)
    {
        $exist = $this->bad_comment_model->fields('issue_comment_id')->get($id);
        if (!$exist) {
            $result = array(
                'success' => false,
                'message' => 'Data tidak ditemukan.',
            );
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }

        $issue_comment_id = $exist->issue_comment_id;
        $delete = $this->bad_comment_model->delete(array('issue_comment_id'=>$issue_comment_id));
        if ($delete) {
            $result = array(
                'success' => true,
                'message' => 'Laporan tentang komentar buruk berhasil diabaikan.',
            );
        } else {
            $result = array(
                'success' => false,
                'message' => 'Terjadi kesalahan.',
            );
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

}
