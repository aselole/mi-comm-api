<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Issue extends MX_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth'));
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('message', 'Anda belum login');
    		redirect('/admin/login', 'refresh');
		} elseif (!$this->ion_auth->is_admin()) { // remove this elseif if you want to enable this for non-admins
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

        $this->load->model(array('issue_model', 'community_model', 'issue_comment_model'));
    }

    public function index()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title']       = 'Issue';
        $data['judul']       = '<i class="fa fa-bars"></i> Data Issue';
        $data['communities'] = $this->community_model->fields('id, nama')->get_all();
        $data['status']      = get_enum('issues', 'status');

        $this->template->load('layouts/template','issue/index', $data);
    }

    public function getDatatable()
    {
        $customActionName = $this->input->post('customActionName');
        $records          = array();

        if ($customActionName == "delete") {
            $records=$this-> delete_checked();
        }

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayStart  = intval($_REQUEST['start']);
        $sEcho          = intval($_REQUEST['draw']);

        $t              = $this->issue_model->get_limit_data($iDisplayStart, $iDisplayLength);
        $iTotalRecords  = $t['total_rows'];
        $get_data       = $t['get_db'];

        $records["data"] = array();

        $i=$iDisplayStart+1;
        if ($get_data) {
            foreach ($get_data as $d) {
                $checkbok= '<input type="checkbox" name="id[]" value="'.$d->id.'">';
                $edit='<a href="'.site_url('admin/issue/edit/'.$d->id).'" class="btn btn-sm btn-outline blue" title="edit">
                <i class="fa fa-pencil-square-o fa-lg"></i>
                </a> ';
                $comment='<a href="'.site_url('admin/issue/comment/'.$d->id).'" class="btn btn-sm btn-outline green" title="comment">
                <i class="fa fa-comment-o fa-lg"></i>
                </a> ';
                $delete='<a href="#" class="btn btn-sm btn-outline red" onclick="event.preventDefault();btn_delete('.$d->id.')" title="delete">
                <i class="fa fa-trash-o fa-lg"></i>
                </a> ';

                $records["data"][] = array(
                    $i++,
                    $d->community_nama,
                    $d->judul,
                    substr($d->body, 0, 100),
                    $d->author_first_name,
                    $d->status,
					$d->published_at ? date('d/m/Y', strtotime($d->published_at)) : '',
                    $d->active,
                    $edit.$comment.$delete
                );
            }
        }
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        $this->output->set_content_type('application/json')->set_output(json_encode($records));
    }

    public function create()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title']       = 'Tambah Issue';
        $data['mode']        = 'create';
        $data['judul']       = '<i class="fa fa-plus"></i> Tambah Issue';
        $data['method']      = 'post';
        $data['action']      = site_url('admin/issue/store');
        $data['communities'] = $this->community_model->fields('id, nama, ket')->get_all();
        $data['status']      = get_enum('issues', 'status');

        $this->template->load('layouts/template','issue/form', $data);
    }

    public function store()
    {
        $config = array(
            array(
                'field'  => 'community_id',
                'label'  => 'Community',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'judul',
                'label'  => 'Judul',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'body',
                'label'  => 'Body',
                'rules'  => 'required',
                'errors' => array(
                    'required'  => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'published_at',
                'label'  => 'Tgl Publish',
                'rules'  => 'required',
                'errors' => array(
                    'required'  => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'status',
                'label'  => 'Status',
                'rules'  => 'required',
                'errors' => array(
                    'required'  => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'active',
                'label'  => 'Aktif',
                'rules'  => 'required',
                'errors' => array(
                    'required'  => '%s tidak boleh kosong.',
                ),
            ),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $result['errors'] = $this->form_validation->error_array();
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        } else {
            $data = array(
                'community_id' => $this->input->post('community_id'),
                'judul'        => $this->input->post('judul'),
                'body'         => $this->input->post('body'),
                'published_at' => date('Y-m-d', strtotime($this->input->post('published_at'))),
                'author_id'    => $this->session->userdata('user_id'),
            );
            if ( !empty($_FILES['featured_img']['name']) ) {
                $upload = $this->issue_model->upload_pic_issue();
                if ($upload['success']) {
                    $data['featured_img'] = $upload['data']['file_name'];
                } else {
                    $result = $upload;
                    $this->output
                        ->set_status_header(400)
                        ->set_content_type('application/json')
                        ->set_output(json_encode($result));
                }
            }

            $insert_id = $this->issue_model->insert($data);
            if ($insert_id) {
                $result['success'] = true;
                $result['message'] = 'Issue berhasil ditambahkan.';
                $result['url']     = site_url('admin/issue');
            } else {
                $result['success'] = false;
                $result['message'] = 'Terjadi kesalahan.';
            }
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }
    }

    public function edit($id)
    {
        $exist = $this->issue_model->get($id);
        if (!$exist) {
            $this->session->set_flashdata('notif_type', 'alert-warning');
            $this->session->set_flashdata('notif_msg', 'Data tidak ditemukan.');
            redirect('admin/issue');
        }

        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Edit Issue';
        $data['judul'] = '<i class="fa fa-edit"></i> Edit Issue';

        $data['dt']          = $exist;
        $data['mode']        = 'edit';
        $data['method']      = 'post';
        $data['action']      = site_url('admin/issue/update/'.$id);
        $data['communities'] = $this->community_model->fields('id, nama, ket')->get_all();
        $data['status']      = get_enum('issues', 'status');

        $this->template->load('layouts/template','issue/form', $data);
    }

    public function update($id)
    {
        $exist = $this->issue_model->get($id);
        if (!$exist) {
            $result['success'] = false;
            $result['message'] = 'Data tidak ditemukan.';
            $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
        } else {
            $config = array(
                array(
                    'field'  => 'judul',
                    'label'  => 'Judul',
                    'rules'  => 'required',
                    'errors' => array(
                        'required' => '%s tidak boleh kosong.',
                    ),
                ),
                array(
                    'field'  => 'body',
                    'label'  => 'Body',
                    'rules'  => 'required',
                    'errors' => array(
                        'required'  => '%s tidak boleh kosong.',
                    ),
                ),
                array(
                    'field'  => 'published_at',
                    'label'  => 'Tgl Publish',
                    'rules'  => 'required',
                    'errors' => array(
                        'required'  => '%s tidak boleh kosong.',
                    ),
                ),
                array(
                    'field'  => 'status',
                    'label'  => 'Status',
                    'rules'  => 'required',
                    'errors' => array(
                        'required'  => '%s tidak boleh kosong.',
                    ),
                ),
                array(
                    'field'  => 'active',
                    'label'  => 'Aktif',
                    'rules'  => 'required',
                    'errors' => array(
                        'required'  => '%s tidak boleh kosong.',
                    ),
                ),
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE){
                $result['errors'] = $this->form_validation->error_array();
                $this->output
                    ->set_status_header(400)
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            } else {
                $data = array(
                    'judul'        => $this->input->post('judul'),
                    'body'         => $this->input->post('body'),
                    'published_at' => date('Y-m-d', strtotime($this->input->post('published_at'))),
                    'status'       => $this->input->post('status'),
                    'active'       => $this->input->post('active'),
                );

                if ( !empty($_FILES['featured_img']['name']) ) {
                    $upload = $this->issue_model->upload_pic_issue();
                    if ($upload['success']) {
                        $data['featured_img'] = $upload['data']['file_name'];
                        if (file_exists('./uploads/issue/'.$exist->featured_img)) {
                            unlink('./uploads/issue/'.$exist->featured_img);
                        }
                    } else {
                        $result = $upload;
                        $this->output
                            ->set_status_header(400)
                            ->set_content_type('application/json')
                            ->set_output(json_encode($result));
                    }
                }

                $update = $this->issue_model->update($data, $id);
                if ($update) {
                    $result['success'] = true;
                    $result['message'] = 'Issue berhasil diperbarui.';
                    $result['url']     = site_url('admin/issue');
                } else {
                    $result['success'] = false;
                    $result['message'] = 'Terjadi kesalahan.';
                }
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            }
        }
    }

    public function delete($id)
    {
        $exist = $this->issue_model->get($id);
        if (!$exist) {
            $result['success'] = false;
            $result['message'] = 'Data tidak ditemukan.';
            $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
        } else {
            $delete_comments = $this->issue_comment_model->delete(array('issue_id'=>$id));
            if ($exist->featured_img && file_exists('./uploads/issue/'.$exist->featured_img)) {
                unlink('./uploads/issue/'.$exist->featured_img);
            }
            $delete = $this->issue_model->delete($id);
            if ($delete) {
                $result = array(
                    'success' => true,
                    'message' => 'Issue berhasil dihapus',
                );
            } else {
                $result['success'] = false;
                $result['message'] = 'Terjadi kesalahan.';
                $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
            }
            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
    }

    public function comment($id)
    {
        $fields = '
        issues.id,
        issues.community_id,
        issues.judul,
        issues.body,
        issues.featured_img,
        issues.author_id,
        issues.status,
        issues.active,
        issues.published_at
        ';
        $exist = $this
                    ->issue_model
                    ->fields($fields)
                    ->with_author('fields:first_name')
                    ->with_issue_comment('fields:*count*')
                    ->get($id);
        if (!$exist) {
            $this->session->set_flashdata('notif_type', 'alert-warning');
            $this->session->set_flashdata('notif_msg', 'Data tidak ditemukan.');
            redirect('admin/issue');
        }

        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Issue';
        $data['judul'] = '<i class="fa fa-commnt-o"></i> Manajemen Comment Issue';

        $data['dt']          = $exist;
        $data['mode']        = 'comment';
        $data['comments'] = $this
                            ->issue_comment_model
                            ->fields('id, comment, created_at')
                            ->where(array('issue_id'=>$id, 'parent_id'=>0))
                            ->order_by('created_at', 'asc')
                            ->with_user('fields:first_name,photo')
                            ->with_issue_comment_child(
                                array(
                                    'fields'=>'id, comment, created_at',
                                    'order_inside'=>'created_at asc',
                                    'with'=>array('relation'=>'user', 'fields'=>'first_name,photo')
                                ))
                            ->get_all();
        $this->template->load('layouts/template','issue/comment', $data);
    }

    public function form_modal_comment($issue_id, $parent_id)
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf'] = $this->security->get_csrf_hash();

        $data['issue_id']  = $issue_id;
        $data['parent_id'] = $parent_id;
        $data['judul']     = '<i class="fa fa-reply"></i> Balas Komentar';
        $data['method']    = 'POST';
        $data['action']    = site_url('admin/issue/store_comment');
        $this->load->view('issue/form_modal_comment', $data);
    }

    public function store_comment()
    {
        $config = array(
            array(
                'field'  => 'comment',
                'label'  => 'Komentar',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $result['errors'] = $this->form_validation->error_array();
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        } else {
            $issue_id = $this->input->post('issue_id');
            $data = array(
                'issue_id'   => $issue_id,
                'user_id'    => $this->session->userdata('user_id'),
                'comment'    => $this->input->post('comment'),
                'parent_id'  => $this->input->post('parent_id'),
                'created_at' => date('Y-m-d H:i:s'),
            );

            $insert = $this->issue_comment_model->insert($data);
            if ($insert) {
                $result['success'] = true;
                $result['message'] = 'Komentar berhasil ditambahkan.';
                $result['url']     = site_url('admin/issue/comment/'.$issue_id);
            } else {
                $result['success'] = false;
                $result['message'] = 'Terjadi kesalahan.';
            }
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }
    }

    public function delete_comment()
    {
        $comment_id = $this->input->post('comment_id');
        $delete = $this->issue_comment_model->delete(array('parent_id'=>$comment_id));
        $delete = $this->issue_comment_model->delete($comment_id);
        if ($delete) {
            $result = array(
                'success' => true,
                'message' => 'Komentar berhasil dihapus',
            );
        } else {
            $result = array(
                'success' => false,
                'message' => 'Terjadi kesalahan.',
            );
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }
}
