<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller{

    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('username', 'username', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
    }

    public function index()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();
        if($this->security->get_csrf_hash() == FALSE){
            show_404();
        }

        if ($this->input->server('REQUEST_METHOD') == 'POST' AND $this->form_validation->run() == TRUE){
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $data = array(
                $username => $username,
                $password => $password
            );
            $this->security->xss_clean($data);

			if ($this->ion_auth->login($username, $password, false)) {
				//if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
                if ($this->session->userdata('level_id')==1) {
                    redirect('/admin/dashboard', 'refresh');
                } else {
                    redirect('/', 'refresh');
                }
			} else {
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('/admin/login', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries
            }
        }
        $this->load->view('login', $data);
    }

    public function logout()
	{
		// log the user out
		$logout = $this->ion_auth->logout();

		// redirect them to the login page
		$this->session->set_flashdata('message', $this->ion_auth->messages());
		redirect('/', 'refresh');
	}

}
