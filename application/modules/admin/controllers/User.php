<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MX_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth'));
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('message', 'Anda belum login');
    		redirect('/admin/login', 'refresh');
		} elseif (!$this->ion_auth->is_admin()) { // remove this elseif if you want to enable this for non-admins
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

        $params['salt_prefix'] = $this->config->item('salt_prefix', 'ion_auth');
        $this->load->library('bcrypt',$params);

        $this->load->model('user_model');
        $this->load->model('level_model');
        $this->load->model('product_model');
        $this->load->model('global_model');
        $this->load->model('community_model');
        $this->load->model('member_model');
        $this->load->model('bad_comment_model');
        $this->load->model('issue_comment_model');
        $this->load->model('issue_model');
        $this->load->model('pasangan_model');
        $this->load->model('private_issue_comment_model');
        $this->load->model('private_issue_model');
        $this->load->model('promo_claim_model');
        $this->load->model('promo_user_model');
    }

    public function index()
    {
        $data['static_prefix'] = $this->config->item('static_prefix');
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'User';
        $data['judul'] = '<i class="fa fa-bars"></i> Data Users';
        $data['member_jenis'] = get_enum('users', 'member_jenis');
        $data['levels'] = $this->level_model->get_all();

        $this->template->load('layouts/template','user/index', $data);
    }

    public function getDatatable()
    {
        $customActionName = $this->input->post('customActionName');
        $records          = array();

        if ($customActionName == "aktifkan_checked") {
            $this->_aktifkan_checked();
        }

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayStart  = intval($_REQUEST['start']);
        $sEcho          = intval($_REQUEST['draw']);

        $t              = $this->user_model->get_limit_data($iDisplayStart, $iDisplayLength);
        $iTotalRecords  = $t['total_rows'];
        $get_data       = $t['get_db'];

        $records["data"] = array();

        $i=$iDisplayStart+1;
        if ($get_data) {
            foreach ($get_data as $d) {
                $checkbok= '<input type="checkbox" name="id[]" value="'.$d->id.'">';
                $send_activation = '';
                $aktifkan = '';
                $delete = '';

                if ($d->active==0 && $d->level_id!=1) {
                    $send_activation='<a href="#" class="btn btn-xs btn-outline green" onclick="event.preventDefault();btn_send_activation('.$d->id.')" title="kirim aktifasi">
                    <i class="fa fa-plane fa-lg"></i>
                    </a> ';
                    $aktifkan='<a href="#" class="btn btn-xs btn-outline green" onclick="event.preventDefault();btn_aktifkan('.$d->id.',1)" title="aktifkan">
                    <i class="fa fa-check fa-lg"></i>
                    </a> ';
                } else if ($d->active==1 && $d->level_id!=1) {
                    $aktifkan='<a href="#" class="btn btn-xs btn-outline yellow" onclick="event.preventDefault();btn_aktifkan('.$d->id.',0)" title="non aktifkan">
                    <i class="fa fa-check fa-exclamation-triangle"></i>
                    </a> ';
                }
                // $view    = anchor(site_url('admin/group/read/'.$d->id),'<i class="fa fa-eye fa-lg"></i> Lihat Konten',array('title'=>'detail','class'=>'btn btn-outline green'));
                $edit='<a href="'.site_url('admin/user/edit/'.$d->id).'" class="btn btn-xs btn-outline blue" title="edit">
                <i class="fa fa-pencil-square-o fa-lg"></i>
                </a> ';
                if ($d->level_id != 1) {
                    $delete='<a href="#" class="btn btn-xs btn-outline red" onclick="event.preventDefault();btn_delete('.$d->id.')" title="delete">
                    <i class="fa fa-trash-o fa-lg"></i>
                    </a> ';
                }

                $checkbox  = '<div><input type="checkbox" name="id[]" value="'.$d->id.'"></div>';   

                $records["data"][] = array(
                    $checkbox,
                    $d->level_nama,
                    $d->member_jenis,
                    $d->first_name,
                    $d->email,
                    $d->phone,
                    $d->active,
					$d->last_login ? date('d/m/Y H:i', strtotime($d->last_login)) : '',
                    $send_activation.$aktifkan.$edit.$delete
                );
            }
        }
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        $this->output->set_content_type('application/json')->set_output(json_encode($records));
    }

    public function create_member()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Tambah User Member';
        $data['judul'] = '<i class="fa fa-plus"></i> Tambah User Member';

        $data['mode'] = 'add';
        $data['method'] = 'post';
        $data['action'] = site_url('admin/user/store');
        $data['kokab'] = $this->db->query("select a.kokab_nama, b.provinsi_nama from master_kokab a left join master_provinsi b on (a.provinsi_id=b.provinsi_id)");
        // $data['communities'] = $this->community_model->fields('id, nama, ket, member_jenis')->get_all();
        $data['product'] = $this->product_model->fields('id, nama')->where('active', 'y')->get_all();
        $data['member_jenis'] = get_enum('users', 'member_jenis');

        $this->template->load('layouts/template','user/form_member', $data);
    }

    public function create_trainer()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Tambah User Trainer';
        $data['judul'] = '<i class="fa fa-plus"></i> Tambah User Trainer';

        $data['mode'] = 'add';
        $data['method'] = 'post';
        $data['action'] = site_url('admin/user/store');
        $data['kokab'] = $this->db->query("select a.kokab_nama, b.provinsi_nama from master_kokab a left join master_provinsi b on (a.provinsi_id=b.provinsi_id)");
        $data['communities'] = $this->community_model->fields('id, nama, ket, member_jenis')->get_all();

        $this->template->load('layouts/template','user/form_trainer', $data);
    }

    public function get_community_member($member_jenis)
    {
        $data['communities'] = $this->community_model->fields('id, nama, ket, member_jenis')->where('member_jenis', $member_jenis)->get_all();
        $this->load->view('user/select2member', $data);
    }

    public function store()
    {
        $config = array(
            array(
                'field'  => 'first_name',
                'label'  => 'Nama',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'gender',
                'label'  => 'Jenis Kemain',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'card_barcode',
                'label'  => 'ID Barcode',
                'rules'  => 'required|is_unique[users.card_barcode]',
                'errors' => array(
                    'required'  => '%s tidak boleh kosong.',
                    'is_unique' => '%s tersebut sudah dipakai.',
                ),
            ),
            array(
                'field'  => 'email',
                'label'  => 'Email',
                'rules'  => 'required|is_unique[users.email]',
                'errors' => array(
                    'required'  => '%s tidak boleh kosong.',
                    'is_unique' => '%s tersebut sudah dipakai.',
                ),
            ),
            array(
                'field'  => 'password',
                'label'  => 'Password',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
        );

        $level_id = $this->input->post('level_id');
        $custom_rules = [];
        if ($level_id == 3 ) {
            $custom_rules = array(
                'field'  => 'a_community_id[]',
                'label'  => 'Komunitas',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            );
        }elseif ($level_id == 2){
            $custom_rules = array(
                'field'  => 'community_id',
                'label'  => 'Komunitas',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            );
        }
        if (count($custom_rules) > 0) {
            array_push($config, $custom_rules);
        }
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $result['errors'] = $this->form_validation->error_array();
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        } else {
            $password      = $this->input->post('password');
            $hash_password = $this->hash_password($password);
            $data = array(
                'first_name'     => $this->input->post('first_name'),
                'place_birthday' => $this->input->post('place_birthday'),
                'gender'         => $this->input->post('gender'),
                'phone'          => $this->input->post('phone'),
                'address'        => $this->input->post('address'),
                'description'    => $this->input->post('description'),
                'username'       => $this->input->post('card_barcode'),
                'card_barcode'   => $this->input->post('card_barcode'),
                'email'          => $this->input->post('email'),
                'password'       => $hash_password,
                'level_id'       => $level_id,
                'active'         => 0,
            );
            if ($this->input->post('date_birthday')) {
                $data['date_birthday'] = date('Y-m-d', strtotime($this->input->post('date_birthday')));
            }

            if ($level_id == 1) {
                // admin 
                $data['product_id'] = null;
                $data['member_jenis'] = null;
            } elseif ($level_id == 2) {
                // member 
                $data['product_id'] = $this->input->post('product_id');
                $data['member_jenis'] = $this->input->post('member_jenis');
            } elseif ($level_id == 3) {
                // trainer 
                $data['urut'] = $this->input->post('urut');
                $data['product_id'] = null;
                $data['member_jenis'] = null;
            }

            try {
                $email = $this->input->post('email');
                if ( !empty($_FILES['photo']['name']) ) {
                    $upload = $this->user_model->upload_pic_user($email);
    				if ($upload['success']) {
                        $data['photo'] = $upload['data']['file_name'];
                    } else {
                        $result = $upload;
                        throw new Exception('error');
                    }
                }

                $insert_id = $this->user_model->insert($data);
                if ($insert_id) {
                    // begin generate key
                    $key = $this->global_model->generateKey($insert_id);
                    $data_key = array(
                        'user_id' => $insert_id,
                        'key'     => $key,
                    );
                    $this->db->insert('keys', $data_key);
                    // end generate key

                    // generate activation code
                    $activation_code = $this->global_model->generateActivationCode($insert_id);
                    $data_activation = array(
                        'activation_code' => $activation_code,
                    );
                    $this->db->where('id', $insert_id);
                    $this->db->update('users', $data_activation);
                    // end generate activation code

                    // $send_email = $this->user_model->send_email_activation($insert_id);

                    // create member
                    if ($level_id == 2) {
                        if ($this->input->post('community_id')) {
                            $data = array(
                                'community_id'  => $this->input->post('community_id'),
                                'user_id'       => $insert_id,
                                'tgl_bergabung' => date('Y-m-d'),
                            );
                            $this->member_model->insert($data);
                        }
                    }
                    if ($level_id == 3) {
                        $community_arr = $this->input->post('a_community_id');
                        if (!empty($community_arr) ) {
                            foreach ($community_arr as $key => $v) {
                                $data = array(
                                    'community_id'  => $v,
                                    'user_id'       => $insert_id,
                                    'tgl_bergabung' => date('Y-m-d'),
                                );
                                $this->member_model->insert($data);
                            }
                        }
                    }

                    $result['success'] = true;
                    $result['message'] = 'User berhasil ditambahkan.';
                    $result['url']     = site_url('admin/user');
                } else {
                    $result['success'] = false;
                    $result['message'] = 'Terjadi kesalahan.';
                }
            } catch (Exception $e) {
                $this->output
                    ->set_status_header(400);
            }
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }
    }

    public function edit($id)
    {
        $exist = $this->user_model->get($id);
        if (!$exist) {
            $this->session->set_flashdata('notif_type', 'alert-warning');
            $this->session->set_flashdata('notif_msg', 'Data tidak ditemukan.');
            redirect('admin/user');
        }

        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $dt             = $this->user_model->get($id);
        $data['dt']     = $dt;
        $data['kokab'] = $this->db->query("select a.kokab_nama, b.provinsi_nama from master_kokab a left join master_provinsi b on (a.provinsi_id=b.provinsi_id)");
        $data['mode']   = 'edit';
        $data['method'] = 'post';
        $data['action'] = site_url('admin/user/update/'.$id);

        if ($exist->level_id == 2) {
            $data['title'] = 'Edit User Member';
            $data['judul'] = '<i class="fa fa-edit"></i> Edit User Member';
            $data['product'] = $this->product_model->fields('id, nama')->where('active', 'y')->get_all();
            $data['dc']      = $this->member_model->get(array('user_id' => $dt->id));

            $community_id = $data['dc']->community_id;
            $data['community_nama'] = $this->db->query("select nama from communities where id=$community_id")->row()->nama;
            $this->template->load('layouts/template','user/form_member', $data);
        } elseif ($exist->level_id == 3) {
            $data['title'] = 'Edit User Trainer';
            $data['judul'] = '<i class="fa fa-edit"></i> Edit User Trainer';
            $dc = array();
            if ($dd = $this->member_model->get_all( array('user_id' => $dt->id))) {
                foreach ($dd as $key) {
                    $dc[] =  $key->community_id;
                }
            }
            $data['dc'] = $dc;
            $data['communities'] = $this->community_model->fields('id, nama, ket')->get_all();

            $this->template->load('layouts/template','user/form_trainer', $data);
        } elseif ($exist->level_id == 1) {
            $data['title'] = 'Edit User Admin';
            $data['judul'] = '<i class="fa fa-edit"></i> Edit User Admin';

            $this->template->load('layouts/template','user/form', $data);
        }

    }

    public function update($id)
    {
        $level_id = $this->input->post('level_id');
        $is_unique_username = '';
        $is_unique_email = '';
        $exist = $this->user_model->get($id);
        if (!$exist) {
            $result['success'] = false;
            $result['message'] = 'Data tidak ditemukan.';
            $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
        } else {
            // if ($this->input->post('username') != $exist->username) {
            //     $is_unique_username = '|is_unique[users.username]';
            // }
            if ($this->input->post('email') != $exist->email) {
                $is_unique_email = '|is_unique[users.email]';
            }
            $config = array(
                array(
                    'field'  => 'first_name',
                    'label'  => 'Nama',
                    'rules'  => 'required',
                    'errors' => array(
                        'required' => '%s tidak boleh kosong.',
                    ),
                ),
                array(
                    'field'  => 'gender',
                    'label'  => 'Jenis Kemain',
                    'rules'  => 'required',
                    'errors' => array(
                        'required' => '%s tidak boleh kosong.',
                    ),
                ),
                // array(
                //     'field'  => 'username',
                //     'label'  => 'Username',
                //     'rules'  => 'required'.$is_unique_username,
                //     'errors' => array(
                //         'required'  => '%s tidak boleh kosong.',
                //         'is_unique' => '%s tersebut sudah dipakai.',
                //     ),
                // ),
                array(
                    'field'  => 'email',
                    'label'  => 'Email',
                    'rules'  => 'required'.$is_unique_email,
                    'errors' => array(
                        'required'  => '%s tidak boleh kosong.',
                        'is_unique' => '%s tersebut sudah dipakai.',
                    ),
                ),
                array(
                    'field'  => 'password',
                    'label'  => 'Password',
                    'rules'  => '',
                    'errors' => array(
                        'required' => '%s tidak boleh kosong.',
                    ),
                ),
            );
            $this->form_validation->set_rules($config);
            if ($level_id == 3 ) {
                $this->form_validation->set_rules('a_community_id[]', 'Komunitas', 'required');
            }

            if ($this->form_validation->run() == FALSE){
                $result['errors'] = $this->form_validation->error_array();
                $this->output
                    ->set_status_header(400)
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            } else {
                $data = array(
                    'first_name'     => $this->input->post('first_name'),
                    'place_birthday' => $this->input->post('place_birthday'),
                    'gender'         => $this->input->post('gender'),
                    'phone'          => $this->input->post('phone'),
                    'address'        => $this->input->post('address'),
                    'description'    => $this->input->post('description'),
                    'username'       => $this->input->post('username'),
                    'email'          => $this->input->post('email'),
                );
                if ($this->input->post('date_birthday')) {
                    $data['date_birthday'] = date('Y-m-d', strtotime($this->input->post('date_birthday')));
                }
                $password      = trim($this->input->post('password'));
                if ($password != '') {
                    $hash_password = $this->hash_password($password);
                    $data['password'] = $hash_password;
                }

                try {
                    $email = $this->input->post('email');
                    if ( !empty($_FILES['photo']['name']) ) {
                        // if ($exist->photo != null && file_exists('./uploads/user/'.$exist->photo)) {
                        //     unlink('./uploads/user/'.$exist->photo);
                        // }
                        $upload = $this->user_model->upload_pic_user($email);
        				if ($upload['success']) {
                            $data['photo'] = $upload['data']['file_name'];
                        } else {
                            $result = $upload;
                            throw new Exception('error');
                        }
                    }

                    if ($level_id == 3) {
                        $data['urut'] = $this->input->post('urut');
                    }

                    if ($this->user_model->update($data, $id)) {
                        if ($level_id == 3) {
                            $this->member_model->delete(array('user_id' => $id));
                            $community_arr = $this->input->post('a_community_id');
                            if (!empty($community_arr) ) {
                                foreach ($community_arr as $key => $v) {
                                    $data = array(
                                        'community_id'  => $v,
                                        'user_id'       => $id,
                                        'tgl_bergabung' => date('Y-m-d'),
                                    );
                                    $this->member_model->insert($data);
                                }
                            }
                        }
                    }
                    $result['success'] = true;
                    $result['message'] = 'User berhasil diperbarui.';
                    $result['url']     = site_url('admin/user');
                } catch (Exception $e) {
                    $result['success'] = false;
                    $result['message'] = 'Terjadi kesalahan.';
                    if ($e == 'error') {
                        $this->output->set_status_header(400);
                    }
                }
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            }
        }

    }



    public function hash_password($password)
    {
		// bcrypt
        return $this->bcrypt->hash($password);
    }



    public function send_activation()
    {
        $id = $this->input->post('id');
        $send_activation = $this->user_model->send_email_activation($id);
        if (!$send_activation) {
            $result = array(
                'success' => false,
                // 'message' => $send_activation,
                'message' => 'Email aktivasi gagal dikirim. Periksa koneksi internet Anda dan email tujuan.',
            );
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }
        $result = array(
            'success' => true,
            'message' => 'Email aktivasi berhasil dikirim.',
        );
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function aktifkan()
    {
        $id = $this->input->post('id');
        $aksi = $this->input->post('aksi');
        try {
            $data['active'] = $aksi;
            if ($aksi == 1) {
                $data['activation_code'] = null;
            } else {
                $data['activation_code'] = $this->global_model->generateActivationCode($id);
            }
            
            if ($this->user_model->update($data, $id)) {
                if ($aksi == 1) {
                    $result['message'] = 'User berhasil diaktifkan.';
                } else {
                    $result['message'] = 'User berhasil dinon-aktifkan.';
                }
                $result['success'] = true;
                // $result['url']     = site_url('admin/user');
            }
        } catch (Exception $e) {
            $result['success'] = false;
            $result['message'] = 'Terjadi kesalahan.';
            if ($e == 'error') {
                $this->output->set_status_header(400);
            }
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function delete($id)
    {
        $exist = $this->user_model->get($id);
        if (!$exist) {
            $result = array(
                'success' => false,
            );
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        } else {
            // bad comments
            $cek_bad_comments = $this->bad_comment_model->where('pelapor_id', $id)->get_all();
            if ($cek_bad_comments) {
                $this->bad_comment_model->where('pelapor_id', $id)->delete();
            }

            // member 
            $cek_member = $this->member_model->where('user_id', $id)->get_all();
            if ($cek_member) {
                $this->member_model->where('user_id', $id)->delete();
            }

            // issue comment
            $cek_issue_comment = $this->issue_comment_model->where('user_id', $id)->get_all();
            if ($cek_issue_comment) {
                $this->issue_comment_model->where('user_id', $id)->delete();
            }

            // issue
            $cek_issue = $this->issue_model->where('author_id', $id)->get_all();
            if ($cek_issue) {
                $this->issue_model->where('author_id', $id)->delete();
            }

            // pasangan
            $cek_pasangan = $this->pasangan_model->where('utama_user_id', $id)->or_where('pasangan_user_id', $id)->get_all();
            if ($cek_pasangan) {
                $this->pasangan_model->where('utama_user_id', $id)->or_where('pasangan_user_id', $id)->delete();
            }

            // private_issue comment
            $cek_private_issue_comment = $this->private_issue_comment_model->where('user_id', $id)->get_all();
            if ($cek_private_issue_comment) {
                $this->private_issue_comment_model->where('user_id', $id)->delete();
            }

            // private_issue
            $cek_private_issue = $this->private_issue_model->where('user_member_id', $id)->or_where('user_trainer_id', $id)->get_all();
            if ($cek_private_issue) {
                $this->private_issue_model->where('user_member_id', $id)->or_where('user_trainer_id', $id)->delete();
            }

            // promo_claim
            $cek_promo_claim = $this->promo_claim_model->where('user_id', $id)->get_all();
            if ($cek_promo_claim) {
                $cek_promo_claim = $this->promo_claim_model->where('user_id', $id)->delete();
            }

            // promo_user
            $cek_promo_user = $this->promo_user_model->where('user_id', $id)->get_all();
            if ($cek_promo_user) {
                $cek_promo_user = $this->promo_user_model->where('user_id', $id)->delete();
            }

            $this->user_model->delete($id);
            $result = array(
                'success' => true,
                'message' => 'User berhasil dihapus.',
            );
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function _aktifkan_checked()
    {
        $id = $this->input->post('id');

        $data['active'] =    1;
        $data['activation_code'] = null;
        foreach ($id as $key => $value) {
            $this->user_model->update($data, $value);
        }
    }

}
