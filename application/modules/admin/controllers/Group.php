<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends MX_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth'));
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('message', 'Anda belum login');
    		redirect('/admin/login', 'refresh');
		} elseif (!$this->ion_auth->is_admin()) { // remove this elseif if you want to enable this for non-admins
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

        $this->load->model(array('group_model'));
    }

    public function index()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Group';
        $data['judul'] = '<i class="fa fa-bars"></i> Data Groups';

        $this->template->load('layouts/template','group/index', $data);
    }

    public function getDatatable()
    {
        $customActionName = $this->input->post('customActionName');
        $records          = array();

        if ($customActionName == "delete") {
            $records=$this-> delete_checked();
        }

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayStart  = intval($_REQUEST['start']);
        $sEcho          = intval($_REQUEST['draw']);

        $t              = $this->group_model->get_limit_data($iDisplayStart, $iDisplayLength);
        $iTotalRecords  = $t['total_rows'];
        $get_data       = $t['get_db'];

        $records["data"] = array();

        $i=$iDisplayStart+1;
        if ($get_data) {
            foreach ($get_data as $d) {
                $checkbok= '<input type="checkbox" name="id[]" value="'.$d->id.'">';
                // $view    = anchor(site_url('admin/group/read/'.$d->id),'<i class="fa fa-eye fa-lg"></i> Lihat Konten',array('title'=>'detail','class'=>'btn btn-outline green'));
                $edit='<a href="#" class="btn btn-sm btn-outline blue" onclick="event.preventDefault();btn_edit('.$d->id.')" title="edit">
                <i class="fa fa-pencil-square-o fa-lg"></i>
                </a> ';
                $delete='<a href="#" class="btn btn-sm btn-outline red" onclick="event.preventDefault();btn_delete('.$d->id.')" title="delete">
                <i class="fa fa-trash-o fa-lg"></i>
                </a> ';

                $records["data"][] = array(
                    $i++,
                    $d->name,
					$d->description,
                    $edit.$delete
                );
            }
        }
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        $this->output->set_content_type('application/json')->set_output(json_encode($records));
    }

    // public function list()
	// {
	// 	$records = $this->Groups->list();
	// 	$this->output->set_content_type('application/json')->set_output(json_encode($records));
	// }

    public function create()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['mode']   = 'create';
        $data['judul']  = '<i class="fa fa-plus"></i> Tambah Group';
        $data['method'] = 'post';
        $data['action'] = site_url('admin/group/store');
        $this->load->view('group/form', $data);
    }

    public function store()
    {
        $this->form_validation->set_rules('name', 'Name',
            'required|is_unique[groups.name]',
            array(
                'is_unique' => 'This %s already exists.'
            )
        );
        $this->form_validation->set_rules('description', 'Description', 'required');

        if ($this->form_validation->run() == FALSE){
            $result['errors'] = $this->form_validation->error_array();
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        } else {
            $data = array(
                'name'        => $this->input->post('name'),
                'description' => $this->input->post('description'),
            );
            $insert_id = $this->Groups->insert($data);
            if ($insert_id) {
                $result['success'] = true;
                $result['message'] = 'Group berhasil ditambahkan.';
            } else {
                $result['success'] = false;
            }
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }
    }

    public function edit($id)
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['mode']   = 'edit';
        $data['judul']  = '<i class="fa fa-pencil"></i> Edit Group';
        $data['dt']     = $this->group_model->get($id);
        $data['method'] = 'post';
        $data['action'] = site_url('admin/group/update/'.$id);
        $this->load->view('group/form', $data);
    }

    public function update($id)
    {
        $is_unique = '';
        $exists = $this->Groups->get_by(array('id'=>$id));
        if (!$exists) {
            $result['success'] = false;
            $result['message'] = 'Data tidak ditemukan.';
            $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
        } else {
            if ($this->input->post('name') != $exists['name']) {
                $is_unique = '|is_unique[groups.name]';
            }
            $this->form_validation->set_rules('name', 'Name', 'required'.$is_unique,
                array(
                    'is_unique' => 'This %s already exists.'
                )
            );
            $this->form_validation->set_rules('description', 'Description', 'required');

            if ($this->form_validation->run() == FALSE){
                $result['errors'] = $this->form_validation->error_array();
                $this->output
                    ->set_status_header(400)
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            } else {
                $data = array(
                    'name'        => $this->input->post('name'),
                    'description' => $this->input->post('description'),
                );
                $update = $this->Groups->update($id, $data);
                if ($update) {
                    $result['success'] = true;
                    $result['message'] = 'Group berhasil diperbarui.';
                } else {
                    $result['success'] = false;
                }
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            }
        }
    }

    public function delete($id)
    {
        // $exists = $this->Groups->get_by(array('id'=>100));
        // if (!$exists) {
        //     $result['success'] = false;
        //     $result['message'] = 'Data tidak ditemukan.';
        //     $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
        // } else {
        //     $delete = $this->Groups->delete($id);
        // }
    }

}
