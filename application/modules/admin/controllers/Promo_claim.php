<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo_claim extends MX_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth'));
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('message', 'Anda belum login');
            redirect('/admin/login', 'refresh');
        } elseif (!$this->ion_auth->is_admin()) { // remove this elseif if you want to enable this for non-admins
            // redirect them to the home page because they must be an administrator to view this
            return show_error('You must be an administrator to view this page.');
        }

        $this->load->model('promo_model');
        $this->load->model('promo_claim_model');
    }

    public function index()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Promo Global Claim';
        $data['judul'] = '<i class="fa fa-bars"></i> Data Promo Global Claim';

        $this->template->load('layouts/template','promo_claim/index', $data);
    }

    public function ajax_list()
	{
		$records = $this->promo_claim_model->ajax_list();
		$this->output->set_content_type('application/json')->set_output(json_encode($records));
	}

    public function getDatatable()
    {
        $customActionName = $this->input->post('customActionName');
        $records          = array();

        if ($customActionName == "delete") {
            $records=$this-> delete_checked();
        }

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayStart  = intval($_REQUEST['start']);
        $sEcho          = intval($_REQUEST['draw']);

        $t              = $this->promo_claim_model->get_limit_data($iDisplayStart, $iDisplayLength);
        $iTotalRecords  = $t['total_rows'];
        $get_data       = $t['get_db'];
        $records["data"] = array();

        $i=$iDisplayStart+1;
        if ($get_data) {
            foreach ($get_data as $d) {
                $checkbok= '<input type="checkbox" name="id[]" value="'.$d->id.'">';
                $view='<a href="#" class="btn btn-xs btn-outline green" onclick="event.preventDefault();btn_view('.$d->id.')" title="view">
                <i class="fa fa-search fa-lg"></i>
                </a> ';
                $records["data"][] = array(
                    $i++,
                    $d->promo_judul,
                    $d->user_first_name,
                    $d->claimed_at ? date('d/m/Y', strtotime($d->claimed_at)) : '',
                    $view
                );
            }
        }
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        $this->output->set_content_type('application/json')->set_output(json_encode($records));
    }

}
