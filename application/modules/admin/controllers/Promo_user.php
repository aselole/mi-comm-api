<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo_user extends MX_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth'));
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('message', 'Anda belum login');
            redirect('/admin/login', 'refresh');
        } elseif (!$this->ion_auth->is_admin()) { // remove this elseif if you want to enable this for non-admins
            // redirect them to the home page because they must be an administrator to view this
            return show_error('You must be an administrator to view this page.');
        }

        $this->load->model('promo_model');
        $this->load->model('user_model');
        $this->load->model('promo_user_model');
        $this->load->model('global_model');
    }

    public function index()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Promo Private User';
        $data['judul'] = '<i class="fa fa-bars"></i> Data Promo Private User';

        $this->template->load('layouts/template','promo_user/index', $data);
    }

    public function getDatatable()
    {
        $customActionName = $this->input->post('customActionName');
        $records          = array();

        if ($customActionName == "delete") {
            $records=$this-> delete_checked();
        }

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayStart  = intval($_REQUEST['start']);
        $sEcho          = intval($_REQUEST['draw']);

        $t              = $this->promo_user_model->get_limit_data($iDisplayStart, $iDisplayLength);
        $iTotalRecords  = $t['total_rows'];
        $get_data       = $t['get_db'];
        $records["data"] = array();

        $i=$iDisplayStart+1;
        if ($get_data) {
            foreach ($get_data as $d) {
                $delete = '';
                $checkbok= '<input type="checkbox" name="id[]" value="'.$d->id.'">';
                $view='<a href="#" class="btn btn-xs btn-outline green" onclick="event.preventDefault();btn_view('.$d->id.')" title="view">
                <i class="fa fa-search fa-lg"></i>
                </a> ';
                $edit='<a href="'.site_url('admin/promo_user/edit/'.$d->id).'" class="btn btn-xs btn-outline blue" title="edit">
                <i class="fa fa-pencil-square-o fa-lg"></i>
                </a> ';
                if ($d->claimed_at==null && $d->is_claimed==null) {
                    $delete='<a href="#" class="btn btn-xs btn-outline red" onclick="event.preventDefault();btn_delete('.$d->id.')" title="delete">
                    <i class="fa fa-trash-o fa-lg"></i>
                    </a> ';
                }
                $records["data"][] = array(
                    $i++,
                    $d->promo_judul,
                    $d->user_first_name,
                    date('d/m/Y', strtotime($d->created_at)),
                    $d->is_claimed ? $d->is_claimed : 't',
                    $d->claimed_at ? date('d/m/Y', strtotime($d->claimed_at)) : '',
                    $d->active,
                    $view.$edit.$delete
                );
            }
        }
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        $this->output->set_content_type('application/json')->set_output(json_encode($records));
    }

    public function detail($promo_id)
    {
        $data['promo'] = $this->promo_model->with_merchant('fields:nama, alamat|join:true')->get($promo_id);
        $this->load->view('promo_user/detail_promo', $data);
    }

    public function create()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Tambah Promo Private User';
        $data['mode']   = 'create';
        $data['judul']  = '<i class="fa fa-plus"></i> Tambah Promo Private User';
        $data['method'] = 'post';
        $data['action'] = site_url('admin/promo_user/store');

        $data['promo'] = $this->promo_model->where(array('active' => 'y', 'is_global' => 't'))->get_all();
        $data['user'] = $this->user_model->fields('id, first_name, email')->where(array('level_id' => 2, 'active' => 1))->get_all();

        $this->template->load('layouts/template','promo_user/form', $data);
    }

    public function store()
    {
        $config = array(
            array(
                'field'  => 'promo_id',
                'label'  => 'Promo',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'ar_user_id',
                'label'  => 'User Member',
                'rules'  => 'required',
                'errors' => array(
                    'required'  => '%s tidak boleh kosong.',
                ),
            ),
            // array(
            //     'field'  => 'active',
            //     'label'  => 'Aktif',
            //     'rules'  => 'required',
            //     'errors' => array(
            //         'required'  => '%s tidak boleh kosong.',
            //     ),
            // ),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $result['errors'] = $this->form_validation->error_array();
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        } else {
            $date_now   = date('Y-m-d H:i:s');
            $active     = $this->input->post('active');
            $promo_id   = $this->input->post('promo_id');
            $ar_user_id = $this->input->post('ar_user_id');
            $ar_user_id = explode(',', $ar_user_id);

            $multi_data = array();
            $message_error = '';
            $ok = true;
            foreach ($ar_user_id as $user_id) {
                $data = array(
                    'promo_id' => $promo_id,
                    'user_id'  => $user_id,
                );
                $cek = $this->promo_user_model->where($data)->count_rows();
                if ($cek > 0) {
                    $ok = false;
                    $user = $this->user_model->fields('email, first_name')->get($user_id);
                    $message_error .= $user->first_name.' ('.$user->email.') <br> ';
                } else {
                    $data['active']     = $active;
                    $data['created_at'] = $date_now;
                    array_push($multi_data, $data);
                }
            }

            if ($ok == false) {
                $result['success'] = false;
                $result['message'] = $message_error.' sudah mendapatkan promo ini.';
                $this->output
                    ->set_status_header(400)
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            } else {
                $this->promo_user_model->insert($multi_data);
                $result['success'] = true;
                $result['message'] = 'Promo User berhasil ditambahkan.';
                $result['url']     = site_url('admin/promo_user');

                // firebase
                if ($active == 'y') {
                    $promo       = $this->promo_model->fields('judul, valid_until')->get($promo_id);
                    $valid_until = date('d M Y', strtotime($promo->valid_until));
                    $title       = "Promo Khusus untuk Anda";
                    $message     = $promo->judul.'. Berlaku sampai '.$valid_until;
                    $data_firebase = array(
                        'id'    => "$promo_id",
                        'jenis' => 'promo',
                        'title' => "Promo Khusus untuk Anda",
                        'body' => $promo->judul.'. Berlaku sampai '.$valid_until
                    );
                    foreach ($ar_user_id as $user_id) {
                        $member_jenis = 'micomm';
                        $member_jenis = $this->user_model->fields('member_jenis')->where(['id'=>$user_id])->get()->member_jenis;
                        $token_firebase = $this->user_model->fields('token_firebase')->get($user_id)->token_firebase;
                        $this->global_model->push_notif_firebase($token_firebase, $title, $message, $data_firebase, $member_jenis);
                        $this->global_model->push_notif_fcm($token_firebase, $title, $message, $data_firebase, $member_jenis, true);
                    }
                }
            }
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }
    }

    public function edit($id)
    {
        $exist = $this->promo_user_model->get($id);
        if (!$exist) {
            $this->session->set_flashdata('notif_type', 'alert-warning');
            $this->session->set_flashdata('notif_msg', 'Data tidak ditemukan.');
            redirect('admin/promo_user');
        }

        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Edit Promo Private User';
        $data['judul'] = '<i class="fa fa-edit"></i> Edit Promo Private User';

        $data['dt']     = $this->promo_user_model->get($id);
        $data['mode']   = 'edit';
        $data['method'] = 'post';
        $data['action'] = site_url('admin/promo_user/update/'.$id);

        $data['promo'] = $this->promo_model->get($exist->promo_id);
        $data['user'] = $this->user_model->fields('id, first_name, email')->get($exist->user_id);

        $this->template->load('layouts/template','promo_user/form', $data);
    }

    public function update($id)
    {
        $config = array(
            array(
                'field'  => 'active',
                'label'  => 'Aktif',
                'rules'  => 'required',
                'errors' => array(
                    'required'  => '%s tidak boleh kosong.',
                ),
            ),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $result['errors'] = $this->form_validation->error_array();
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        } else {
            $active     = $this->input->post('active');
            $data = array('active' => $active);

            $update = $this->promo_user_model->update($data, $id);
            if ($update) {
                $result['success'] = true;
                $result['message'] = 'Promo User berhasil diperbarui.';
                $result['url'] = site_url('admin/promo_user');

                if ($active == 'y') {
                    $promo       = $this->promo_model->fields('judul, valid_until, user_id')->get($promo_id);
                    $valid_until = date('d M Y', strtotime($promo->valid_until));
                    $title       = "Promo Khusus untuk Anda";
                    $message     = $promo->judul.'. Berlaku sampai '.$valid_until;
                    $data_firebase = array(
                        'jenis' => 'promo',
                        'id'    => "$promo_id",
                    );
                    $token_firebase = $this->user_model->fields('token_firebase')->get($promo->user_id)->token_firebase;
                    $this->global_model->push_notif_firebase($token_firebase, $title, $message, $data_firebase);
                }
            } else {
                $result['success'] = false;
            }
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }
    }

    public function delete($id)
    {
        $exist = $this->promo_user_model->get($id);
        if (!$exist) {
            $this->session->set_flashdata('notif_type', 'alert-warning');
            $this->session->set_flashdata('notif_msg', 'Data tidak ditemukan.');
            redirect('admin/promo_user');
        } else {
            $delete = $this->promo_user_model->delete($id);
            $result['success'] = true;
            $result['message'] = 'Promo User berhasil dihapus.';
            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
    }

}
