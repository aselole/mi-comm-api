<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Community extends MX_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth'));
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('message', 'Anda belum login');
    		redirect('/admin/login', 'refresh');
		} elseif (!$this->ion_auth->is_admin()) { // remove this elseif if you want to enable this for non-admins
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

        $this->load->model(array('community_model', 'company_model', 'member_model'));
    }

    public function index()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Community';
        $data['judul'] = '<i class="fa fa-bars"></i> Data Community';
        $data['member_jenis'] = get_enum('communities', 'member_jenis');
        $data['company'] = $this->company_model->fields('id, nama')->get_all();

        $this->template->load('layouts/template','community/index', $data);
    }

    public function getDatatable()
    {
        $customActionName = $this->input->post('customActionName');
        $records          = array();

        if ($customActionName == "delete") {
            $records=$this-> delete_checked();
        }

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayStart  = intval($_REQUEST['start']);
        $sEcho          = intval($_REQUEST['draw']);

        $t              = $this->community_model->get_limit_data($iDisplayStart, $iDisplayLength);
        $iTotalRecords  = $t['total_rows'];
        $get_data       = $t['get_db'];

        $records["data"] = array();

        $i=$iDisplayStart+1;
        if ($get_data) {
            foreach ($get_data as $d) {
                $checkbok= '<input type="checkbox" name="id[]" value="'.$d->id.'">';
                $edit='<a href="#" class="btn btn-sm btn-outline blue" onclick="event.preventDefault();btn_edit('.$d->id.')" title="edit">
                <i class="fa fa-pencil-square-o fa-lg"></i>
                </a> ';
                $delete='<a href="#" class="btn btn-sm btn-outline red" onclick="event.preventDefault();btn_delete('.$d->id.')" title="delete">
                <i class="fa fa-trash-o fa-lg"></i>
                </a> ';

                $records["data"][] = array(
                    $i++,
                    $d->member_jenis,
                    $d->company_nama,
                    $d->nama,
                    $d->ket,
                    $d->alamat,
                    $d->email,
                    $d->telepon1,
					$d->telepon2,
					date('d/m/Y', strtotime($d->created_at)),
                    $edit.$delete
                );
            }
        }
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        $this->output->set_content_type('application/json')->set_output(json_encode($records));
    }

    public function create()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['mode']    = 'create';
        $data['judul']   = '<i class="fa fa-plus"></i> Tambah Community';
        $data['method']  = 'post';
        $data['action']  = site_url('admin/community/store');
        $data['company'] = $this->company_model->fields('id, jenis, nama, alamat')->get_all();
        $data['member_jenis'] = get_enum('communities', 'member_jenis');

        $this->load->view('community/form', $data);
    }

    public function store()
    {
        $config = array(
            array(
                'field'  => 'member_jenis',
                'label'  => 'Jenis',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'company_id',
                'label'  => 'Perusahaan',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'nama',
                'label'  => 'Nama',
                'rules'  => 'trim|required|is_unique[communities.nama]',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                    'is_unique' => '%s sudah ada.',
                ),
            ),
            array(
                'field'  => 'ket',
                'label'  => 'Keterangan',
                'rules'  => 'required',
                'errors' => array(
                    'required'  => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'alamat',
                'label'  => 'Alamat',
                'rules'  => 'required',
                'errors' => array(
                    'required'  => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'telepon1',
                'label'  => 'Telepon 1',
                'rules'  => 'required',
                'errors' => array(
                    'required'  => '%s tidak boleh kosong.',
                ),
            ),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $result['errors'] = $this->form_validation->error_array();
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        } else {
            $data = array(
                'member_jenis' => $this->input->post('member_jenis'),
                'company_id'   => $this->input->post('company_id'),
                'nama'         => $this->input->post('nama'),
                'ket'          => $this->input->post('ket'),
                'alamat'       => $this->input->post('alamat'),
                'email'        => $this->input->post('email'),
                'telepon1'     => $this->input->post('telepon1'),
                'telepon2'     => $this->input->post('telepon2'),
            );
            if ( !empty($_FILES['logo']['name']) ) {
                $upload = $this->community_model->upload_pic_community();
                // var_dump($upload);
                if ($upload['success']) {
                    $data['logo'] = $upload['data']['file_name'];
                } else {
                    $result = $upload;
                    $this->output
                        ->set_status_header(400)
                        ->set_content_type('application/json')
                        ->set_output(json_encode($result));
                }
            }
            $insert_id = $this->community_model->insert($data);
            if ($insert_id) {
                $result['success'] = true;
                $result['message'] = 'Community berhasil ditambahkan.';
                $result['url'] = site_url('admin/community');
            } else {
                $result['success'] = false;
            }
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }
    }

    public function edit($id)
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['mode']    = 'edit';
        $data['judul']   = '<i class="fa fa-edit"></i> Edit Community';
        $data['dt']      = $this->community_model->with_company('fields:nama')->get($id);
        $data['method']  = 'post';
        $data['action']  = site_url('admin/community/update/'.$id);
        $data['company'] = $this->company_model->fields('id, jenis, nama, alamat')->get_all();

        $this->load->view('community/form', $data);
    }

    public function update($id)
    {
        $is_unique = '';
        $exists = $this->community_model->get($id);
        if (!$exists) {
            $result['success'] = false;
            $result['message'] = 'Data tidak ditemukan.';
            $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
        } else {
            if ($this->input->post('nama') != $exists->nama) {
                $is_unique = '|is_unique[communities.nama]';
            }
            $config = array(
                array(
                    'field'  => 'nama',
                    'label'  => 'Nama',
                    'rules'  => 'trim|required'.$is_unique,
                    'errors' => array(
                        'required' => '%s tidak boleh kosong.',
                        'is_unique' => '%s sudah ada.',
                    ),
                ),
                array(
                    'field'  => 'ket',
                    'label'  => 'Keterangan',
                    'rules'  => 'required',
                    'errors' => array(
                        'required'  => '%s tidak boleh kosong.',
                    ),
                ),
                array(
                    'field'  => 'alamat',
                    'label'  => 'Alamat',
                    'rules'  => 'required',
                    'errors' => array(
                        'required'  => '%s tidak boleh kosong.',
                    ),
                ),
                array(
                    'field'  => 'telepon1',
                    'label'  => 'Telepon 1',
                    'rules'  => 'required',
                    'errors' => array(
                        'required'  => '%s tidak boleh kosong.',
                    ),
                ),
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE){
                $result['errors'] = $this->form_validation->error_array();
                $this->output
                    ->set_status_header(400)
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            } else {
                $data = array(
                    'nama'       => $this->input->post('nama'),
                    'ket'        => $this->input->post('ket'),
                    'alamat'   => $this->input->post('alamat'),
                    'email'    => $this->input->post('email'),
                    'telepon1' => $this->input->post('telepon1'),
                    'telepon2' => $this->input->post('telepon2'),
                );
                if ( !empty($_FILES['logo']['name']) ) {
                    $upload = $this->community_model->upload_pic_community();
                    if ($upload['success']) {
                        $data['logo'] = $upload['data']['file_name'];
                        if ($exists->logo != null && file_exists('./uploads/community/'.$exists->logo)) {
                            unlink('./uploads/community/'.$exists->logo);
                        }
                    } else {
                        $result = $upload;
                        $this->output
                            ->set_status_header(400)
                            ->set_content_type('application/json')
                            ->set_output(json_encode($result));
                    }
                }
                $update = $this->community_model->update($data, $id);
                if ($update) {
                    $result['success'] = true;
                    $result['message'] = 'Community berhasil diperbarui.';
                    $result['url'] = '';
                } else {
                    $result['success'] = false;
                }
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            }
        }
    }

    public function delete($id)
    {
        $exists = $this->community_model->get($id);
        if (!$exists) {
            $result['success'] = false;
            $result['message'] = 'Data tidak ditemukan.';
            $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
        } else {
            $cek_member = $this->member_model->where('community_id', $id)->get_all();
            if ($cek_member) {
                $this->member_model->where('community_id', $id)->delete();
            }

            if ($exists->logo != null && file_exists('./uploads/community/'.$exists->logo)) {
                unlink('./uploads/community/'.$exists->logo);
            }
            $delete = $this->community_model->delete($id);
            $result['success'] = true;
            $result['message'] = 'Community berhasil dihapus.';
            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
    }

}
