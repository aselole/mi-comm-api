<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends MX_Controller{

    const AR_FOR_WHOM = ['micomm', 'thecoach'];

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth'));
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('message', 'Anda belum login');
    		redirect('/admin/login', 'refresh');
		} elseif (!$this->ion_auth->is_admin()) { // remove this elseif if you want to enable this for non-admins
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

        $this->load->model(array('news_model', 'image_news_model'));
    }

    public function index()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'News';
        $data['judul'] = '<i class="fa fa-bars"></i> Data News';

        $this->template->load('layouts/template','news/index', $data);
    }

    public function getDatatable()
    {
        $customActionName = $this->input->post('customActionName');
        $records          = array();

        if ($customActionName == "delete") {
            $records=$this-> delete_checked();
        }

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayStart  = intval($_REQUEST['start']);
        $sEcho          = intval($_REQUEST['draw']);

        $t              = $this->news_model->get_limit_data($iDisplayStart, $iDisplayLength);
        $iTotalRecords  = $t['total_rows'];
        $get_data       = $t['get_db'];

        $records["data"] = array();

        $i=$iDisplayStart+1;
        if ($get_data) {
            foreach ($get_data as $d) {
                $checkbok= '<input type="checkbox" name="id[]" value="'.$d->id.'">';
                $edit='<a href="'.site_url('admin/news/edit/'.$d->id).'" class="btn btn-sm btn-outline blue" title="edit">
                <i class="fa fa-pencil-square-o fa-lg"></i>
                </a> ';
                $delete='<a href="#" class="btn btn-sm btn-outline red" onclick="event.preventDefault();btn_delete('.$d->id.')" title="delete">
                <i class="fa fa-trash-o fa-lg"></i>
                </a> ';

                $records["data"][] = array(
                    $i++,
                    strtoupper($d->for_whom),
                    $d->judul,
                    substr($d->body, 0, 100),
                    $d->user_first_name,
                    date('d/m/Y', strtotime($d->created_at)),
					date('d/m/Y', strtotime($d->published_at)),
                    $d->active,
                    $edit.$delete
                );
            }
        }
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        $this->output->set_content_type('application/json')->set_output(json_encode($records));
    }

    public function create()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['mode']   = 'create';
        $data['title']  = 'Tambah News';
        $data['judul']  = '<i class="fa fa-plus"></i> Tambah News';
        $data['method'] = 'post';
        $data['ar_for_whom'] = self::AR_FOR_WHOM;
        $data['action'] = site_url('admin/news/store');

        $this->template->load('layouts/template','news/form', $data);
    }

    public function upload_img()
    {
        $news_id = $this->input->post('news_id');
        if ( !empty($_FILES['featured_img']['name']) ) {
            $upload = $this->upload_pic_news();
            if ($upload['success']) {
                $data['image'] = $upload['data']['file_name'];
                if ($news_id == 0) {
                    $data_news['judul'] = 'Tanpa Judul';
                    $data_news['published_at'] = null;
                    $data_news['author_id'] = $this->session->userdata('user_id');
                    $news_id = $this->news_model->insert($data_news);
                    if ($news_id) {
                        $data['news_id'] = $news_id;
                        $data['is_featured'] = 'y';
                        $insert_image = $this->image_news_model->insert($data);
                        $result = array(
                            'success' => true,
                            'message' => 'Gambar berhasil diupload',
                            'news_id' => $news_id,
                        );
                    } else {
                        $result = array(
                            'success' => false,
                            'message' => 'Terjadi kesalahan',
                        );
                        $this->output
                            ->set_status_header(400)
                            ->set_content_type('application/json')
                            ->set_output(json_encode($result));
                    }
                } else {
                    $data['news_id'] = $news_id;
                    $cek = $this->image_news_model->where(['news_id'=>$news_id, 'is_featured'=>'y'])->get_all();
                    if (!$cek) {
                        $data['is_featured'] = 'y';
                    }
                    $insert_image = $this->image_news_model->insert($data);
                    $result = array(
                        'success' => true,
                        'message' => 'Gambar berhasil diupload',
                        'news_id' => $news_id,
                    );
                }
            } else {
                $result = $upload;
                $this->output
                    ->set_status_header(400)
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            }
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        } else {
            $result = array(
                'success' => false,
                'message' => 'Tidak ada gambar yang diupload.',
            );
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }
    }

    public function upload_pic_news()
    {
        $config['upload_path'] = './uploads/news/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']  = '90000';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('featured_img')) {
            $error = array('error' => $this->upload->display_errors());
            $result['success'] = false;
            $result['message'] = $error['error'];
        } else {
            $upload_data = $this->upload->data();
            $result = array(
                'success' => true,
                'data' => $upload_data,
            );
        }
        return $result;
    }

    public function get_image_news($news_id)
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf'] = $this->security->get_csrf_hash();
        $data['image_news'] = $this->image_news_model->where('news_id', $news_id)->get_all();
        $this->load->view('news/content_image_news', $data);
    }

    public function store()
    {
        $id = $this->input->post('id');
        $config = array(
            array(
                'field'  => 'for_whom',
                'label'  => 'For',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'judul',
                'label'  => 'Judul',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'body',
                'label'  => 'Body',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'published_at',
                'label'  => 'Tgl Publish',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'active',
                'label'  => 'Aktif',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $result['errors'] = $this->form_validation->error_array();
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        } else {
            $data = array(
                'for_whom'     => $this->input->post('for_whom'),
                'judul'        => $this->input->post('judul'),
                'body'         => $this->input->post('body'),
                'published_at' => date('Y-m-d', strtotime($this->input->post('published_at'))),
                'author_id'    => $this->session->userdata('user_id'),
                'active'       => $this->input->post('active'),
            );
            if ($id == 0) {
                $insert_id = $this->news_model->insert($data);
                if (!$insert_id) {
                    $result = array(
                        'success' => false,
                    );
                    $this->output
                        ->set_status_header(400)
                        ->set_content_type('application/json')
                        ->set_output(json_encode($result));
                } else {
                    $result = array(
                        'success' => true,
                        'message' => 'News berhasil disimpan',
                        'url' => site_url('admin/news'),
                    );
                }
            } else {
                $exist = $this->news_model->get($id);
                if (!$exist) {
                    $result['success'] = false;
                    $result['message'] = 'Data tidak ditemukan.';
                    $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
                } else {
                    $this->news_model->update($data, $id);
                    $result = array(
                        'success' => true,
                        'message' => 'News berhasil disimpan',
                        'url' => site_url('admin/news'),
                    );
                }
            }
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }
    }

    public function edit($id)
    {
        $exist = $this->news_model->get($id);
        if (!$exist) {
            $this->session->set_flashdata('notif_type', 'alert-warning');
            $this->session->set_flashdata('notif_msg', 'Data tidak ditemukan.');
            redirect('admin/news');
        }

        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Edit News';
        $data['judul'] = '<i class="fa fa-edit"></i> Edit News';

        $data['dt']     = $this->news_model->get($id);
        $data['mode']   = 'edit';
        $data['method'] = 'post';
        $data['ar_for_whom'] = self::AR_FOR_WHOM;
        $data['action'] = site_url('admin/news/store');

        $this->template->load('layouts/template','news/form', $data);
    }

    public function delete($id)
    {
        $exist = $this->news_model->get($id);
        if (!$exist) {
            $result['success'] = false;
            $result['message'] = 'Data tidak ditemukan.';
            $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
        } else {
            $images = $this->image_news_model->where('news_id',$id)->get_all();
            if ($images) {
                foreach ($images as $r) {
                    if (file_exists('./uploads/news/'.$r->image)) {
                        unlink('./uploads/news/'.$r->image);
                    }
                    $delete = $this->image_news_model->delete($r->id);
                }
            }
            $delete = $this->news_model->delete($id);

            $result['success'] = true;
            $result['message'] = 'News berhasil dihapus.';
            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
    }

    public function delete_image()
    {
        $id = $this->input->post('id');
        $exist = $this->image_news_model->get($id);
        if (!$exist) {
            $result = array(
                'success' => false,
                'message' => 'Gambar tidak ditemukan.',
            );
        }
        if ($exist->image && file_exists('./uploads/news/'.$exist->image)) {
            unlink('./uploads/news/'.$exist->image);
        }
        $this->image_news_model->delete($id);
        $result = array(
            'success' => true,
            'message' => 'Gambar berhasil dihapus.',
        );
        $cek1 = $this->image_news_model->where(['news_id'=>$exist->news_id])->get_all();
        $cek2 = $this->image_news_model->where(['news_id'=>$exist->news_id, 'is_featured'=>'y'])->get_all();
        if ($cek1 && !$cek2) {
            $data['is_featured'] = 'y';
            $pertama = $this->image_news_model->where(['news_id'=>$exist->news_id])->get();
            $this->image_news_model->update($data, array('id' => $pertama->id));
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function edit_image()
    {
        $id = $this->input->post('id');
        $is_featured = $this->input->post('is_featured');
        $exist = $this->image_news_model->get($id);
        if (!$exist) {
            $result = array(
                'success' => false,
                'message' => 'Gambar tidak ditemukan.',
            );
        }
        if ($is_featured=='y') {
            $news_id = $exist->news_id;
            $data = array('is_featured' => 't');
            $this->image_news_model->update($data, array('news_id' => $news_id));

            $data2 = array('is_featured' => 'y');
            $this->image_news_model->update($data2, $id);
        } else {
            $data = array('is_featured' => 't');
            $this->image_news_model->update($data, $id);
        }
        $result = array(
            'success' => true,
            'message' => 'Gambar berhasil diperbarui.',
        );
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }
}
