<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends MX_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth'));
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('message', 'Anda belum login');
    		redirect('/admin/login', 'refresh');
		} elseif (!$this->ion_auth->is_admin()) { // remove this elseif if you want to enable this for non-admins
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

        $this->load->model(array('member_model', 'community_model', 'level_model', 'user_model'));
    }

    public function index()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title']       = 'Member';
        $data['judul']       = '<i class="fa fa-bars"></i> Data Member';
        $data['communities'] = $this->community_model->fields('id, nama')->get_all();
        $data['levels']      = $this->level_model->fields('id, nama')->where('id', array(2,3))->get_all();
        $data['member_jenis'] = get_enum('users', 'member_jenis');

        $this->template->load('layouts/template','member/index', $data);
    }

    public function getDatatable()
    {
        $customActionName = $this->input->post('customActionName');
        $records          = array();

        if ($customActionName == "delete") {
            $records=$this-> delete_checked();
        }

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayStart  = intval($_REQUEST['start']);
        $sEcho          = intval($_REQUEST['draw']);

        $t              = $this->member_model->get_limit_data($iDisplayStart, $iDisplayLength);
        $iTotalRecords  = $t['total_rows'];
        $get_data       = $t['get_db'];

        $records["data"] = array();

        $i=$iDisplayStart+1;
        if ($get_data) {
            foreach ($get_data as $d) {
                $checkbok= '<input type="checkbox" name="id[]" value="'.$d->id.'">';
                $edit='<a href="#" class="btn btn-sm btn-outline blue" onclick="event.preventDefault();btn_edit('.$d->id.')" title="edit">
                <i class="fa fa-pencil-square-o fa-lg"></i>
                </a> ';
                $delete='<a href="#" class="btn btn-sm btn-outline red" onclick="event.preventDefault();btn_delete('.$d->id.')" title="delete">
                <i class="fa fa-trash-o fa-lg"></i>
                </a> ';

                $level_nama = $this->level_model->fields('nama')->get($d->user_level_id)->nama;
                $records["data"][] = array(
                    $i++,
                    $d->user_member_jenis,
                    $d->community_nama,
                    $d->user_first_name,
                    $d->user_email,
                    $level_nama,
					$d->tgl_bergabung ? date('d/m/Y', strtotime($d->tgl_bergabung)) : '',
                    strtoupper($d->active),
                    ''
                    // $edit.$delete
                );
            }
        }
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        $this->output->set_content_type('application/json')->set_output(json_encode($records));
    }

    public function create()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['mode']    = 'create';
        $data['judul']   = '<i class="fa fa-plus"></i> Tambah Member';
        $data['method']  = 'post';
        $data['action']  = site_url('admin/member/store');

        $data['communities'] = $this->community_model->fields('id, nama, ket')->get_all();
        $data['users'] = $this->user_model->fields('id, first_name, email, level_id')->with_level('fields:nama|join:true')->get_all();

        $this->load->view('member/form', $data);
    }

    public function store()
    {
        $config = array(
            array(
                'field'  => 'community_id',
                'label'  => 'Community',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'ar_user_id',
                'label'  => 'User',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'tgl_bergabung',
                'label'  => 'Tgl Bergabung',
                'rules'  => 'required',
                'errors' => array(
                    'required'  => '%s tidak boleh kosong.',
                ),
            ),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $result['errors'] = $this->form_validation->error_array();
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        } else {
            $community_id  = $this->input->post('community_id');
            $tgl_bergabung = date('Y-m-d', strtotime($this->input->post('tgl_bergabung')));

            $ar_user_id = $this->input->post('ar_user_id');
            $ar_user_id = explode(',', $ar_user_id);

            $multi_data = array();
            $message_error = '';
            $ok = true;

            foreach ($ar_user_id as $user_id) {
                $user = $this->user_model->fields('email, first_name, level_id')->get($user_id);
                $level_id = $user->level_id;
                if ($level_id==2) {
                    $data_cek_member = array(
                        'user_id'       => $user_id,
                    );
                    $cek = $this->member_model->where($data_cek_member)->count_rows();
                } elseif ($level_id==3) {
                    $data_cek_trainer = array(
                        'community_id'  => $community_id,
                        'user_id'       => $user_id,
                    );
                    $cek = $this->member_model->where($data_cek_trainer)->count_rows();
                }
                if ($cek > 0) {
                    $ok = false;
                    $user = $this->user_model->fields('email, first_name')->get($user_id);
                    $message_error .= $user->first_name.' ('.$user->email.') <br> ';
                } else {
                    $data = array(
                        'community_id'  => $community_id,
                        'user_id'       => $user_id,
                        'tgl_bergabung' => $tgl_bergabung,
                    );
                    array_push($multi_data, $data);
                }
            }

            if ($ok == false) {
                $result['success'] = false;
                $result['message'] = $message_error.' sudah terdaftar di member community.';
                $this->output
                    ->set_status_header(400)
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            } else {
                $this->member_model->insert($multi_data);
                $result['success'] = true;
                $result['message'] = 'Member berhasil ditambahkan.';
                $result['url'] = site_url('admin/member');
            }
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }
    }

    public function edit($id)
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['mode']    = 'edit';
        $data['judul']   = '<i class="fa fa-edit"></i> Edit Member';
        $data['dt']      = $this->member_model->get($id);
        $data['method']  = 'post';
        $data['action']  = site_url('admin/member/update/'.$id);

        $data['communities'] = $this->community_model->fields('id, nama, ket')->get_all();
        $data['users']       = $this->user_model->fields('id, first_name, email, level_id')->with_level('fields:nama|join:true')->get_all();

        $this->load->view('member/form', $data);
    }

    public function update($id)
    {
        $is_unique = '';
        $exists = $this->member_model->get($id);
        if (!$exists) {
            $result['success'] = false;
            $result['message'] = 'Data tidak ditemukan.';
            $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
        } else {
            $config = array(
                array(
                    'field'  => 'tgl_bergabung',
                    'label'  => 'Tgl Bergabung',
                    'rules'  => 'required',
                    'errors' => array(
                        'required' => '%s tidak boleh kosong.',
                    ),
                ),
                array(
                    'field'  => 'active',
                    'label'  => 'Aktif',
                    'rules'  => 'required',
                    'errors' => array(
                        'required' => '%s tidak boleh kosong.',
                    ),
                ),
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE){
                $result['errors'] = $this->form_validation->error_array();
                $this->output
                    ->set_status_header(400)
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            } else {
                $tgl_bergabung = date('Y-m-d', strtotime($this->input->post('tgl_bergabung')));
                $data = array(
                    'tgl_bergabung' => $tgl_bergabung,
                    'active'        => $this->input->post('active'),
                );
                $update = $this->member_model->update($data, $id);
                if ($update) {
                    $result['success'] = true;
                    $result['message'] = 'Member berhasil diperbarui.';
                    $result['url'] = site_url('admin/member');
                } else {
                    $result['success'] = false;
                }
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            }
        }
    }

    public function delete($id)
    {
        $exists = $this->member_model->get($id);
        if (!$exists) {
            $result['success'] = false;
            $result['message'] = 'Data tidak ditemukan.';
            $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
        } else {
            $delete = $this->member_model->delete($id);
            $result['success'] = true;
            $result['message'] = 'Member berhasil dihapus.';
            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
    }

}
