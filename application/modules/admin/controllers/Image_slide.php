<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Image_slide extends MX_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth'));
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('message', 'Anda belum login');
    		redirect('/admin/login', 'refresh');
		} elseif (!$this->ion_auth->is_admin()) { // remove this elseif if you want to enable this for non-admins
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

        $this->load->model('image_slide_model');
    }

    public function index()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Image Slide';
        $data['judul'] = '<i class="fa fa-bars"></i> Data Image Slide';

        $this->template->load('layouts/template','image_slide/index', $data);
    }

    public function getDatatable()
    {
        $customActionName = $this->input->post('customActionName');
        $records          = array();

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayStart  = intval($_REQUEST['start']);
        $sEcho          = intval($_REQUEST['draw']);

        $t              = $this->image_slide_model->get_limit_data($iDisplayStart, $iDisplayLength);
        $iTotalRecords  = $t['total_rows'];
        $get_data       = $t['get_db'];

        $records["data"] = array();

        $i=$iDisplayStart+1;
        if ($get_data) {
            foreach ($get_data as $d) {
                $edit='<a href="'.site_url('admin/image_slide/edit/'.$d->id).'" class="btn btn-xs btn-outline blue" title="edit">
                <i class="fa fa-pencil-square-o fa-lg"></i>
                </a> ';
                $delete = '';
                if ($d->paten=='t') {
                    $delete='<a href="#" class="btn btn-xs btn-outline red" onclick="event.preventDefault();btn_delete('.$d->id.')" title="delete">
                    <i class="fa fa-trash-o fa-lg"></i>
                    </a> ';
                }

                $file = '<img src="'.base_url('assets/pages/img/page_general_search/02.jpg').'" style="width:80px;height:80px">';
                if ($d->file!=null && file_exists('./uploads/image_slide/'.$d->file)) {
                    $file = '<img src="'.base_url('uploads/image_slide/'.$d->file).'" style="width:80px;height:80px">';
                }
                $records["data"][] = array(
                    $i++,
                    $file,
                    $d->order,
                    strtoupper($d->publish),
                    $edit.$delete
                );
            }
        }
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        $this->output->set_content_type('application/json')->set_output(json_encode($records));
    }

    public function create()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Tambah Image Slide';
        $data['judul'] = '<i class="fa fa-plus"></i> Tambah Image Slide';

        $data['mode']          = 'add';
        $data['method']        = 'post';
        $data['action']        = site_url('admin/image_slide/store');

        $this->template->load('layouts/template','image_slide/form', $data);
    }

    public function store()
    {
        $config = array(
            array(
                'field'  => 'file',
                'label'  => 'file',
                'rules'  => '',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'order',
                'label'  => 'order',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'publish',
                'label'  => 'publish',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $result['errors'] = $this->form_validation->error_array();
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        } else {
            $data = array(
                'order' => $this->input->post('order'),
                'publish'    => $this->input->post('publish'),
            );

            try {
                if ( !empty($_FILES['file']['name']) ) {
                    $upload = $this->upload_pic_logo();
    				if ($upload['success']) {
                        $data['file'] = $upload['data']['file_name'];
                    } else {
                        $result = $upload;
                        throw new Exception('error');
                    }
                }

                $insert_id = $this->image_slide_model->insert($data);
                if ($insert_id) {
                    $result['success'] = true;
                    $result['message'] = 'Image slide berhasil ditambahkan.';
                    $result['url']     = site_url('admin/image_slide');
                } else {
                    $result['success'] = false;
                    $result['message'] = 'Terjadi kesalahan.';
                }
            } catch (Exception $e) {
                $this->output
                    ->set_status_header(400);
            }
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }
    }

    public function edit($id)
    {
        $exist = $this->image_slide_model->get($id);
        if (!$exist) {
            $this->session->set_flashdata('notif_type', 'alert-warning');
            $this->session->set_flashdata('notif_msg', 'Data tidak ditemukan.');
            redirect('admin/image_slide');
        }

        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Edit Image Slide';
        $data['judul'] = '<i class="fa fa-edit"></i> Edit Image Slide';

        $data['dt']     = $this->image_slide_model->get($id);
        $data['mode']   = 'edit';
        $data['method'] = 'post';
        $data['action'] = site_url('admin/image_slide/update/'.$id);

        $this->template->load('layouts/template','image_slide/form', $data);
    }

    public function update($id)
    {
        $exist = $this->image_slide_model->get($id);
        if (!$exist) {
            $result['success'] = false;
            $result['message'] = 'Data tidak ditemukan.';
            $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
        } else {
            $config = array(
                array(
                    'field'  => 'order',
                    'label'  => 'order',
                    'rules'  => 'required',
                    'errors' => array(
                        'required' => '%s tidak boleh kosong.',
                    ),
                ),
                array(
                    'field'  => 'publish',
                    'label'  => 'Publish',
                    'rules'  => 'required',
                    'errors' => array(
                        'required' => '%s tidak boleh kosong.',
                    ),
                ),
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE){
                $result['errors'] = $this->form_validation->error_array();
                $this->output
                    ->set_status_header(400)
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            } else {
                $data = array(
                    'order'           => $this->input->post('order'),
                    'publish'         => $this->input->post('publish'),
                );

                try {
                    if ( !empty($_FILES['file']['name']) ) {
                        $upload = $this->upload_pic_logo();
        				if ($upload['success']) {
                            $data['file'] = $upload['data']['file_name'];
                            if (file_exists('./uploads/image_slide/'.$exist->file)) {
                                unlink('./uploads/image_slide/'.$exist->file);
                            }
                        } else {
                            $result = $upload;
                            throw new Exception('error');
                        }
                    }

                    $update = $this->image_slide_model->update($data, $id);
                    if ($update) {
                        $result['success'] = true;
                        $result['message'] = 'Image Slide berhasil diperbarui.';
                        $result['url']     = site_url('admin/image_slide');
                    } else {
                        $result['success'] = false;
                        $result['message'] = 'Terjadi kesalahan.';
                    }
                } catch (Exception $e) {
                    $this->output
                        ->set_status_header(400);
                }
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            }
        }
    }

    public function delete($id)
    {
        $exist = $this->image_slide_model->get($id);
        if (!$exist) {
            $result['success'] = false;
            $result['message'] = 'Data tidak ditemukan.';
            $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
        } else {
            if ($exist->file != null && file_exists('./uploads/image_slide/'.$exist->file)) {
                unlink('./uploads/image_slide/'.$exist->file);
            }
            $this->image_slide_model->delete($id);
            $result['success'] = true;
            $result['message'] = 'Image Slide berhasil dihapus.';
            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
    }

    public function upload_pic_logo()
    {
        $config['upload_path'] = './uploads/image_slide/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']  = '90000';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('file')) {
            $error = array('error' => $this->upload->display_errors());
            $result['success'] = false;
            $result['message'] = $error['error'];
        } else {
            $upload_data = $this->upload->data();
            $result = array(
                'success' => true,
                'data' => $upload_data,
            );
        }
        return $result;
    }

}
