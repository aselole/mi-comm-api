<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasangan extends MX_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth'));
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('message', 'Anda belum login');
    		redirect('/admin/login', 'refresh');
		} elseif (!$this->ion_auth->is_admin()) { // remove this elseif if you want to enable this for non-admins
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

        $params['salt_prefix'] = $this->config->item('salt_prefix', 'ion_auth');
        $this->load->library('bcrypt',$params);

        $this->load->model('user_model');
        $this->load->model('member_model');
        $this->load->model('pasangan_model');
        $this->load->model('company_model');
        $this->load->model('community_model');
    }

    public function index()
    {
        $data['static_prefix'] = $this->config->item('static_prefix');
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Pasangan';
        $data['judul'] = '<i class="fa fa-bars"></i> Data Pasangan';
        $data['company'] = $this->company_model->get_all();
        $data['community'] = $this->community_model->get_all();
        $data['status']      = get_enum('pasangans', 'utama_status');

        $this->template->load('layouts/template','pasangan/index', $data);
    }

    public function ajax_list()
	{
		$records = $this->pasangan_model->ajax_list();
		$this->output->set_content_type('application/json')->set_output(json_encode($records));
	}

    public function create()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Tambah Pasangan';
        $data['judul'] = '<i class="fa fa-plus"></i> Tambah Pasangan';

        $data['mode'] = 'create';
        $data['method'] = 'post';
        $data['action'] = site_url('admin/pasangan/store');
        $sql_community = "
        select
            a.*,
            b.nama as company_nama
        from communities a
        join company b on (a.company_id=b.id)
        where member_jenis='coach'
        ";
        $data['community'] = $this->db->query($sql_community)->result();

        $this->template->load('layouts/template','pasangan/form', $data);
    }

    public function get_form_user($community_id,$pasangan_id=null)
    {
        $member = $this->member_model->where('community_id',$community_id)->get_all();
        $ar_user_id = array(0);
        if ($member) {
            foreach ($member as $r) {
                array_push($ar_user_id, $r->user_id);
            }
        }
        $data['user'] = $this->user_model->where('id', $ar_user_id)->where('level_id',2)->get_all();
        $data['status'] = get_enum('pasangans', 'utama_status');

        if ($pasangan_id) {
            $data['pasangan'] = $this->pasangan_model->where('id', $pasangan_id)->get();
        }
        $this->load->view('pasangan/select_user', $data);
    }

    public function store()
    {
        $config = array(
            array(
                'field'  => 'community_id',
                'label'  => 'Community',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $result['errors'] = $this->form_validation->error_array();
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        } else {
            $utama_user_id = $this->input->post('utama_user_id');
            $pasangan_user_id = $this->input->post('pasangan_user_id');
            $cek = $this->db->query("select id from pasangans where utama_user_id=$utama_user_id or utama_user_id=$pasangan_user_id or pasangan_user_id=$utama_user_id or pasangan_user_id=$pasangan_user_id");
            if ($cek->num_rows() > 0) {
                $result['message'] = 'Pasangan tersebut sudah ada.';
                $this->output
                    ->set_status_header(400)
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            } else {
                $data = array(
                    'utama_user_id'    => $this->input->post('utama_user_id'),
                    'utama_status'     => $this->input->post('utama_status'),
                    'pasangan_user_id' => $this->input->post('pasangan_user_id'),
                    'pasangan_status'  => $this->input->post('pasangan_status'),
                );

                try {
                    $insert_id = $this->pasangan_model->insert($data);
                    if ($insert_id) {
                        $result['success'] = true;
                        $result['message'] = 'Pasangan berhasil ditambahkan.';
                        $result['url']     = site_url('admin/pasangan');
                    } else {
                        $result['success'] = false;
                        $result['message'] = 'Terjadi kesalahan.';
                    }
                } catch (Exception $e) {
                    $this->output
                    ->set_status_header(400);
                }

                $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
            }
        }
    }

    public function edit($id)
    {
        $exist = $this->pasangan_model->get($id);
        if (!$exist) {
            $this->session->set_flashdata('notif_type', 'alert-warning');
            $this->session->set_flashdata('notif_msg', 'Data tidak ditemukan.');
            redirect('admin/pasangan');
        }

        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Edit Pasangan';
        $data['judul'] = '<i class="fa fa-edit"></i> Edit Pasangan';

        $dt             = $this->user_model->get($id);
        $data['dt']     = $dt;
        $data['mode']   = 'edit';
        $data['method'] = 'post';
        $data['action'] = site_url('admin/pasangan/update/'.$id);

        $sql_community = "
        select
            a.*,
            b.community_id,
            c.nama as community_nama,
            d.nama as company_nama
        from pasangans a
        join members b on (a.utama_user_id=b.user_id)
        join communities c on (b.community_id=c.id)
        join company d on (c.company_id=d.id)
        ";
        $data['community'] = $this->db->query($sql_community)->row();

        $this->template->load('layouts/template','pasangan/form', $data);
    }

    public function update($id)
    {
        $exist = $this->pasangan_model->get($id);
        if (!$exist) {
            $result['success'] = false;
            $result['message'] = 'Data tidak ditemukan.';
            $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
        } else {
            $utama_user_id = $this->input->post('utama_user_id');
            $utama_status = $this->input->post('utama_status');
            $pasangan_user_id = $this->input->post('pasangan_user_id');
            $pasangan_status = $this->input->post('pasangan_status');
            $cek = $this->db->query("select id from pasangans where (utama_user_id=$utama_user_id or utama_user_id=$pasangan_user_id or pasangan_user_id=$utama_user_id or pasangan_user_id=$pasangan_user_id) and id!=$id");
            if ($cek->num_rows() > 0) {
                $result['message'] = 'Pasangan tersebut sudah ada.';
                $this->output
                    ->set_status_header(400)
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            } else {
                $data = array(
                    'utama_user_id'    => $this->input->post('utama_user_id'),
                    'utama_status'     => $this->input->post('utama_status'),
                    'pasangan_user_id' => $this->input->post('pasangan_user_id'),
                    'pasangan_status'  => $this->input->post('pasangan_status'),
                );

                try {
                    // $update = $this->pasangan_model->update($data, $id);
                    $update = $this->db->query("
                    update pasangans
                    set
                    utama_user_id = $utama_user_id,
                    utama_status = '$utama_status',
                    pasangan_user_id = $pasangan_user_id,
                    pasangan_status = '$pasangan_status'
                    where id = $id
                    ");
                    if ($update) {
                        $result['success'] = true;
                        $result['message'] = 'Pasangan berhasil diperbarui.';
                        $result['url']     = site_url('admin/pasangan');
                    } else {
                        $result['success'] = false;
                        $result['message'] = 'Terjadi kesalahan.';
                    }
                } catch (Exception $e) {
                    $this->output
                    ->set_status_header(400);
                }

                $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
            }
        }

    }

    public function delete($id)
    {
        $exist = $this->user_model->get($id);
        if (!$exist) {
            $result = array(
                'success' => false,
            );
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        } else {
            $this->user_model->delete($id);
            $result = array(
                'success' => true,
                'message' => 'User berhasil dihapus.',
            );
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

}
