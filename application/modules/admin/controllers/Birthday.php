<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Birthday extends MX_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth'));
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('message', 'Anda belum login');
    		redirect('/admin/login', 'refresh');
		} elseif (!$this->ion_auth->is_admin()) { // remove this elseif if you want to enable this for non-admins
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

        $this->load->model('birthday_model');
        $this->load->model('user_model');
    }

    public function index()
    {
        $data['static_prefix'] = $this->config->item('static_prefix');
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Next Birthday';
        $data['judul'] = '<i class="fa fa-bars"></i> Data Next Birthday';

        $this->template->load('layouts/template','birthday/index', $data);
    }

    public function ajax_list()
	{
		$records = $this->birthday_model->ajax_list();
		$this->output->set_content_type('application/json')->set_output(json_encode($records));
	}

    public function edit_gift($user_id)
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['mode']          = 'edit';
        $data['judul']         = '<i class="fa fa-gift"></i> History Hadiah';
        $data['dt']            = $this->user_model->get($user_id);
        $data['method']        = 'post';
        $data['action']        = site_url('admin/birthday/update/'.$user_id);

        $this->load->view('birthday/form', $data);
    }

    public function update($id)
    {
        $is_unique = '';
        $exists = $this->user_model->get($id);
        if (!$exists) {
            $result['success'] = false;
            $result['message'] = 'Data tidak ditemukan.';
            $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
        } else {
            $config = array(
                array(
                    'field'  => 'gift',
                    'label'  => 'Hadiah',
                    'rules'  => 'trim|required',
                    'errors' => array(
                        'required' => '%s tidak boleh kosong.',
                    ),
                ),
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE){
                $result['errors'] = $this->form_validation->error_array();
                $this->output
                    ->set_status_header(400)
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            } else {
                $data = array(
                    'gift'     => $this->input->post('gift'),
                );
                $update = $this->user_model->update($data, $id);
                if ($update) {
                    $result['success'] = true;
                    $result['message'] = 'Hadiah berhasil disimpan.';
                    $result['url'] = '';
                } else {
                    $result['success'] = false;
                }
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            }
        }
    }

}
