<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partner extends MX_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth'));
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('message', 'Anda belum login');
    		redirect('/admin/login', 'refresh');
		} elseif (!$this->ion_auth->is_admin()) { // remove this elseif if you want to enable this for non-admins
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

        $this->load->model('partner_model');
        $this->load->model('promo_model');
    }

    public function index()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Partner';
        $data['judul'] = '<i class="fa fa-bars"></i> Data partner';

        $this->template->load('layouts/template','partner/index', $data);
    }

    public function getDatatable()
    {
        $customActionName = $this->input->post('customActionName');
        $records          = array();

        if ($customActionName == "delete") {
            $records=$this-> delete_checked();
        }

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayStart  = intval($_REQUEST['start']);
        $sEcho          = intval($_REQUEST['draw']);

        $t              = $this->partner_model->get_limit_data($iDisplayStart, $iDisplayLength);
        $iTotalRecords  = $t['total_rows'];
        $get_data       = $t['get_db'];

        $records["data"] = array();

        $i=$iDisplayStart+1;
        if ($get_data) {
            foreach ($get_data as $d) {
                $checkbok= '<input type="checkbox" name="id[]" value="'.$d->id.'">';
                // $view    = anchor(site_url('admin/group/read/'.$d->id),'<i class="fa fa-eye fa-lg"></i> Lihat Konten',array('title'=>'detail','class'=>'btn btn-outline green'));
                $edit='<a href="'.site_url('admin/partner/edit/'.$d->id).'" class="btn btn-xs btn-outline blue" title="edit">
                <i class="fa fa-pencil-square-o fa-lg"></i>
                </a> ';
                $delete='<a href="#" class="btn btn-xs btn-outline red" onclick="event.preventDefault();btn_delete('.$d->id.')" title="delete">
                <i class="fa fa-trash-o fa-lg"></i>
                </a> ';

                $logo = '<img src="'.base_url('assets/pages/img/page_general_search/02.jpg').'" style="width:80px;height:80px">';
                if ($d->logo_partner!=null && file_exists('./uploads/partner/'.$d->logo_partner)) {
                    $logo = '<img src="'.base_url('uploads/partner/').$d->logo_partner.'" style="width:80px;height:80px">';
                }
                $records["data"][] = array(
                    $logo,
                    $d->nama,
                    $d->alamat,
                    $d->telepon1,
                    $d->email,
                    strtoupper($d->active),
                    $edit.$delete
                );
            }
        }
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        $this->output->set_content_type('application/json')->set_output(json_encode($records));
    }

    public function create()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Tambah partner';
        $data['judul'] = '<i class="fa fa-plus"></i> Tambah partner';

        $data['mode']          = 'add';
        $data['method']        = 'post';
        $data['action']        = site_url('admin/partner/store');
        // $data['jenis_company'] = get_enum('partners', 'jenis_company');

        $this->template->load('layouts/template','partner/form', $data);
    }

    public function store()
    {
        $config = array(
            array(
                'field'  => 'nama',
                'label'  => 'Nama partner',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'ket',
                'label'  => 'Keterangan',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'alamat',
                'label'  => 'Alamat',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'telepon1',
                'label'  => 'Telepon 1',
                'rules'  => 'required',
                'errors' => array(
                    'required'  => '%s tidak boleh kosong.',
                ),
            ),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $result['errors'] = $this->form_validation->error_array();
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        } else {
            $data = array(
                'nama'             => $this->input->post('nama'),
                'ket'           => $this->input->post('ket'),
                'alamat'           => $this->input->post('alamat'),
                'telepon1'         => $this->input->post('telepon1'),
                'telepon2'         => $this->input->post('telepon2'),
                'email'            => $this->input->post('email'),
                'lattitude'         => $this->input->post('lattitude'),
                'longitude'        => $this->input->post('longitude'),
                'active'           => $this->input->post('active'),
            );

            try {
                if ( !empty($_FILES['logo_partner']['name']) ) {
                    $upload = $this->upload_pic_logo();
    				if ($upload['success']) {
                        $data['logo_partner'] = $upload['data']['file_name'];
                    } else {
                        $result = $upload;
                        throw new Exception('error');
                    }
                }

                $insert_id = $this->partner_model->insert($data);
                if ($insert_id) {
                    $result['success'] = true;
                    $result['message'] = 'partner berhasil ditambahkan.';
                    $result['url']     = site_url('admin/partner');
                } else {
                    $result['success'] = false;
                    $result['message'] = 'Terjadi kesalahan.';
                }
            } catch (Exception $e) {
                $this->output
                    ->set_status_header(400);
            }
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }
    }

    public function edit($id)
    {
        $exist = $this->partner_model->get($id);
        if (!$exist) {
            $this->session->set_flashdata('notif_type', 'alert-warning');
            $this->session->set_flashdata('notif_msg', 'Data tidak ditemukan.');
            redirect('admin/partner');
        }

        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $data['title'] = 'Edit partner';
        $data['judul'] = '<i class="fa fa-edit"></i> Edit partner';

        $data['dt']     = $this->partner_model->get($id);
        $data['mode']   = 'edit';
        $data['method'] = 'post';
        $data['action'] = site_url('admin/partner/update/'.$id);
        // $data['jenis_company'] = get_enum('partners', 'jenis_company');

        $this->template->load('layouts/template','partner/form', $data);
    }

    public function update($id)
    {
        $exist = $this->partner_model->get($id);
        if (!$exist) {
            $result['success'] = false;
            $result['message'] = 'Data tidak ditemukan.';
            $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
        } else {
            $config = array(
                array(
                    'field'  => 'nama',
                    'label'  => 'Nama partner',
                    'rules'  => 'required',
                    'errors' => array(
                        'required' => '%s tidak boleh kosong.',
                    ),
                ),
                array(
                    'field'  => 'ket',
                    'label'  => 'Keterangan',
                    'rules'  => 'required',
                    'errors' => array(
                        'required' => '%s tidak boleh kosong.',
                    ),
                ),
                array(
                    'field'  => 'alamat',
                    'label'  => 'Alamat',
                    'rules'  => 'required',
                    'errors' => array(
                        'required' => '%s tidak boleh kosong.',
                    ),
                ),
                array(
                    'field'  => 'telepon1',
                    'label'  => 'Telepon 1',
                    'rules'  => 'required',
                    'errors' => array(
                        'required'  => '%s tidak boleh kosong.',
                    ),
                ),
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE){
                $result['errors'] = $this->form_validation->error_array();
                $this->output
                    ->set_status_header(400)
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            } else {
                $data = array(
                    'nama'             => $this->input->post('nama'),
                    'ket'           => $this->input->post('ket'),
                    'alamat'           => $this->input->post('alamat'),
                    'telepon1'         => $this->input->post('telepon1'),
                    'telepon2'         => $this->input->post('telepon2'),
                    'email'            => $this->input->post('email'),
                    'lattitude'        => $this->input->post('lattitude'),
                    'longitude'        => $this->input->post('longitude'),
                    'active'           => $this->input->post('active'),
                );

                try {
                    if ( !empty($_FILES['logo_partner']['name']) ) {
                        $upload = $this->upload_pic_logo();
        				if ($upload['success']) {
                            $data['logo_partner'] = $upload['data']['file_name'];
                            if ($exist->logo_partner!=null && file_exists('./uploads/partner/'.$exist->logo_partner)) {
                                unlink('./uploads/partner/'.$exist->logo_partner);
                            }
                        } else {
                            $result = $upload;
                            throw new Exception('error');
                        }
                    }

                    $update = $this->partner_model->update($data, $id);
                    if ($update) {
                        $result['success'] = true;
                        $result['message'] = 'partner berhasil diperbarui.';
                        $result['url']     = site_url('admin/partner');
                    } else {
                        $result['success'] = false;
                        $result['message'] = 'Terjadi kesalahan.';
                    }
                } catch (Exception $e) {
                    $this->output
                        ->set_status_header(400);
                }
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            }
        }
    }

    public function delete($id)
    {
        $exist = $this->partner_model->get($id);
        if (!$exist) {
            $result['success'] = false;
            $result['message'] = 'Data tidak ditemukan.';
            $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
        } else {
                if ($exist->logo_partner!=null && file_exists('./uploads/partner/'.$exist->logo_partner)) {
                    unlink('./uploads/partner/'.$exist->logo_partner);
                }
                $delete = $this->partner_model->delete($id);
                $result['success'] = true;
                $result['message'] = 'partner berhasil dihapus.';
                $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
    }

    public function upload_pic_logo()
    {
        $config['upload_path'] = './uploads/partner/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']  = '90000';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('logo_partner')) {
            $error = array('error' => $this->upload->display_errors());
            $result['success'] = false;
            $result['message'] = $error['error'];
        } else {
            $upload_data = $this->upload->data();
            $result = array(
                'success' => true,
                'data' => $upload_data,
            );
        }
        return $result;
    }

}
