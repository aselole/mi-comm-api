<div class="c-layout-breadcrumbs-1 c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
    <div class="container">
        <div class="c-page-title c-pull-left">
            <h3 class="c-font-uppercase c-font-sbold">Merchant</h3>
        </div>
    </div>
</div>
<!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->
<!-- BEGIN: PAGE CONTENT -->
<!-- BEGIN: BLOG LISTING -->
<div id="modal_merchant"></div>

<div id="map" style="height: 400px"></div>

<div class="c-content-box c-size-md" >
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="c-content-blog-post-card-1-grid">
                    <div class="row list-promo-private">
                        <div class="c-content-title-1">
                            <h3 class="c-center c-font-uppercase c-font-bold">Merchant</h3>
                            <div class="c-line-center c-theme-bg"></div>
                        </div>
                        <?php if ($merchants): ?>
                            <?php foreach ($merchants as $m): ?>
                                <div class="col-md-3">
                                    <div class="c-content-blog-post-card-1 c-option-2 c-bordered">
                                        <div class="c-media c-content-overlay">
                                            <a href="javascript:read_merchant('<?php echo $m->id ?>')">
                                                <img class="c-overlay-object img-responsive" src="<?php echo get_merchant_img($m->logo_merchant) ?>" alt="">
                                            </a>
                                        </div>
                                        <div class="c-body hide">
                                            <div class="c-title c-font-bold c-font-uppercase">
                                                <a href="javascript:read_merchant('<?php echo $m->id ?>')"><?php echo $m->nama ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach;?>
                        <?php else: ?>
                            <p>Belum ada Merchant</p>
                        <?php endif?>
                    </div>
                    <div class="row list-promo-private">
                        <div class="c-content-title-1">
                            <h3 class="c-center c-font-uppercase c-font-bold">Partner</h3>
                            <div class="c-line-center c-theme-bg"></div>
                        </div>
                        <?php if ($partners): ?>
                            <?php foreach ($partners as $m): ?>
                                <div class="col-md-3">
                                    <div class="c-content-blog-post-card-1 c-option-2 c-bordered">
                                        <div class="c-media c-content-overlay">
                                            <a href="javascript:read_partner('<?php echo $m->id ?>')">
                                                <img class="c-overlay-object img-responsive" src="<?php echo get_partner_img($m->logo_partner) ?>" alt="">
                                            </a>
                                        </div>
                                        <div class="c-body hide">
                                            <div class="c-title c-font-bold c-font-uppercase">
                                                <a href="javascript:read_partner('<?php echo $m->id ?>')"><?php echo $m->nama ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach;?>
                        <?php else: ?>
                            <p>Belum ada Partner</p>
                        <?php endif?>

                    </div>

                </div>
            </div>
            <!-- END: CONTENT/BLOG/BLOG-SIDEBAR-1 -->
        </div>
    </div>
</div>



<script>

function initMap() {

    var locations = <?php echo $loc ?>;

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: new google.maps.LatLng( -7.275973, 112.808304),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map
        });

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));
    }
}

var siteUrl = '<?php site_url()?>';



</script>
