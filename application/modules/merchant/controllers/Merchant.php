<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Merchant extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('merchant_model');
        $this->load->model('partner_model');
        //Codeigniter : Write Less Do More
    }

    public function index()
    {
        $data['title']       = 'Merchant';

        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf']      = $this->security->get_csrf_hash();

        $data['merchants']   = $this->merchant_model->get_all(array('active' => 'y'));
        $data['partners']   = $this->partner_model->get_all(array('active' => 'y'));
        $loc                 = array();
        foreach ($data['merchants'] as $v) {
            $loc[] = array("<b>$v->nama</b><br>$v->alamat", $v->lattitude, $v->longitude);
        }
        foreach ($data['partners'] as $v) {
            $loc[] = array("<b>$v->nama</b><br>$v->alamat", $v->lattitude, $v->longitude);
        }
        $data['loc'] = json_encode($loc);

        $this->template->load('global/frontLayout/index', 'merchant/index', $data);
    }

    public function read($merchant_id)
    {
        $data['merchant'] = $this->merchant_model->get(array('id' => $merchant_id, 'active' => 'y'));

        $this->load->view('read_modal', $data, false);
        // $this->template->load('global/frontLayout/index', 'promo/read', $data);
    }


}
