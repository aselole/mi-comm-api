<div class="modal fade in mdl-promo" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title"><?php echo $promo->judul ?></h4>
            </div>
            <div class="modal-body">
                <span>
                <img align="middle" src="<?php echo get_promo_img($promo->featured_img) ?>"
                    style="width:100%;
                        display: block;
                        margin-left: auto;
                        margin-right: auto ">
                        <?php if ($is_claimed): ?>
                            <div class="ribbon red" style="margin-right:20px"><span>claimed</span></div>
                        <?php endif ?>
                    </span>

                        <br>
                <p> <b><i class="icon-calendar"></i> Berlaku sampai <?php echo date('d/m/Y', strtotime($promo->valid_until)) ?></b></p>
                <p> <?php echo $promo->body ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn c-btn-dark c-btn-border-2x c-btn-square c-btn-bold c-btn-uppercase" data-dismiss="modal">Tutup</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script type="text/javascript">
    $('.mdl-promo').modal('show');
</script>
