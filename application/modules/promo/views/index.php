<div class="c-layout-breadcrumbs-1 c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
    <div class="container">
        <div class="c-page-title c-pull-left">
            <h3 class="c-font-uppercase c-font-sbold">Promo</h3>
        </div>
    </div>
</div>
<!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->
<!-- BEGIN: PAGE CONTENT -->
<!-- BEGIN: BLOG LISTING -->
<div id="modal_promo"></div>
<?php if (logged_user() && logged_user('level_id') == 2): ?>
<div class="c-content-box c-size-md">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-uppercase c-font-bold">Promo Khusus
                    </h3>
                    <div class="c-line-center c-theme-bg"></div>
                </div>
                <div class="c-content-blog-post-card-1-grid">
                    <div class="row list-promo-private">
                    <?php if ($promo_khusus): ?>

                        <?php foreach ($promo_khusus as $pk): ?>
                            <?php $pk_url = $pk->id . '-' . strtolower(str_replace(' ', '-', $pk->judul))?>
                            <div class="col-md-4">
                                <div class="c-content-blog-post-card-1 c-option-2 c-bordered">
                                    <div class="c-media c-content-overlay">
                                        <a href="javascript:readp('<?php echo $pk->id ?>')">
                                        <img class="c-overlay-object img-responsive img-c" src="<?php echo get_promo_img($pk->featured_img) ?>" alt="promo">
                                        <?php if ( $pk->is_claimed == 'y'): ?>
                                            <div class="ribbon red"><span>claimed</span></div>
                                        <?php endif ?>
                                        </a>
                                    </div>
                                    <div class="c-body">
                                        <div class="c-title c-font-bold c-font-uppercase">
                                            <a href="javascript:readp('<?php echo $pk->id ?>')"><?php echo $pk->judul ?></a>
                                        </div>
                                        <span><?php echo $pk->merchant_nama ?></span>
                                        <div class="c-author ">
                                            Berlaku sampai
                                            <span class="c-font-uppercase c-font-blue-2 bold"><?php echo date('d/m/Y', strtotime($pk->valid_until)) ?></span>
                                        </div>
                                        <p>
                                            <?php echo ellipsize($pk->body, 100) ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;?>
                    <?php else: ?>
                        <p>belum ada promo khusus</p>
                    <?php endif?>

                    </div>
                    <?php if (count($promo_khusus) >= 6 ): ?>

                    <div id="loadMore-container" class="cbp-l-loadMore-button c-margin-t-60">
                        <a class="cbp-l-loadMore-link btn c-btn-square c-btn-border-2x c-btn-dark c-btn-bold c-btn-uppercase btn-load-private">
                            <span class="cbp-l-loadMore-defaultText">LOAD MORE</span>
                            <span class="cbp-l-loadMore-loadingText">LOADING...</span>
                            <span class="cbp-l-loadMore-noMoreLoading">NO MORE WORKS</span>
                        </a>
                    </div>
                    <?php endif?>

                </div>
            </div>
                <!-- END: CONTENT/BLOG/BLOG-SIDEBAR-1 -->
            </div>
    </div>
</div>
<?php endif?>
<!-- <div class="c-content-divider c-divider-sm">
</div> -->

<div class="c-content-box c-size-md">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-uppercase c-font-bold">Promo Global
                    </h3>
                    <div class="c-line-center c-theme-bg"></div>
                </div>
                <div class="c-content-blog-post-card-1-grid">
                    <div class="row list-promo-global">
                        <?php foreach ($promos->result() as $p): ?>
                            <?php $p_url = $p->id . '-' . strtolower(str_replace(' ', '-', $p->judul))?>
                            <div class="col-md-4">
                                <div class="c-content-blog-post-card-1 c-option-2 c-bordered">
                                    <div class="c-media c-content-overlay">
                                        <a href="javascript:readg('<?php echo $p->id ?>')">
                                            <img class="c-overlay-object img-responsive img-c" src="<?php echo get_promo_img($p->featured_img) ?>" alt="">
                                            <?php if ($p->is_claimed=='y'): ?>
                                                <div class="ribbon red"><span>claimed</span></div>
                                            <?php endif ?>

                                        </a>
                                    </div>
                                    <div class="c-body">
                                        <div class="c-title c-font-bold c-font-uppercase">
                                            <a href="javascript:readg('<?php echo $p->id ?>')"><?php echo $p->judul ?></a>
                                        </div>
                                        <span><?php echo $p->merchant_nama ?></span>
                                        <div class="c-author ">
                                            Berlaku sampai
                                            <span class="c-font-uppercase c-font-blue-2 bold"><?php echo date('d/m/Y', strtotime($p->valid_until)) ?></span>
                                        </div>
                                        <p>
                                            <?php echo ellipsize($p->body, 100) ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;?>

                    </div>
                    <?php if (count($promos) >= 6): ?>

                    <div id="loadMore-container" class="cbp-l-loadMore-button c-margin-t-60">
                        <a class="cbp-l-loadMore-link btn c-btn-square c-btn-border-2x c-btn-dark c-btn-bold c-btn-uppercase btn-load-global">
                            <span class="cbp-l-loadMore-defaultText ">LOAD MORE</span>
                            <span class="cbp-l-loadMore-loadingText ">LOADING...</span>
                            <span class="cbp-l-loadMore-noMoreLoading ">NO MORE</span>
                        </a>
                    </div>
                    <?php endif?>

                </div>
            </div>
                <!-- END: CONTENT/BLOG/BLOG-SIDEBAR-1 -->
            </div>
        </div>
    </div>
</div>
<!-- END: BLOG LISTING  -->
<script type="text/javascript">
    var siteUrl = '<?php site_url()?>';
    var baseUrl = '<?php base_url()?>';
    var csrf    = {<?php echo $csrf_name ?>:'<?php echo $csrf ?>'};
    var pStart  = 0;;


    $('.btn-load-private').click(function(event) {
        pStart  = pStart+1;
        $.ajax({
            url: siteUrl + 'promo/list_private_call/' + pStart,
            type: 'post',
            dataType: 'json',
            data: csrf
        })
        .done(function(res) {
            $.each(res.data, function(index, v) {
                var url = v.judul
                    .toLowerCase()
                    .replace(/ /g,'-')
                    .replace(/[^\w-]+/g,'');
                var url = v.id+'-'+url;
                var claimed = '';
                if (v.is_claimed == 'y') {
                    claimed = '<div class="ribbon red"><span>claimed</span></div>';
                }

                $('.list-promo-private').append(
                    '<div class="col-md-4">'+
                        '<div class="c-content-blog-post-card-1 c-option-2 c-bordered">'+
                            '<div class="c-media c-content-overlay">'+
                                '<a href="javascript:readp(\''+v.id+'\')"><img class="c-overlay-object img-responsive img-c" src="'+baseUrl+'uploads/promo/'+v.featured_img+'" alt="">'+

                                claimed +

                                '</a>'+
                            '</div>'+
                            '<div class="c-body">'+
                                '<div class="c-title c-font-bold c-font-uppercase">'+
                                    '<a href="javascript:readp(\''+v.id+'\')">'+v.judul+'</a>'+
                                '</div>'+
                                '<span>' + v.merchant_nama + '</span>'+
                                '<div class="c-author"> Berlaku sampai '+
                                    '<span class="c-font-uppercase c-font-blue-2 bold">'+fotmatTanggal(v.valid_until)+'</span>'+
                                '</div>'+
                                '<p>'+ v.body.slice(1, 100) +'</p>'+
                            '</div>'+
                        '</div>'+
                    '</div>'
                )
            });
        })
        .fail(function() {
            console.log("error");
        })
        .always(function(res) {
            $('.btn-load-private').find('.cbp-l-loadMore-loadingText').removeClass('show');
            $('.btn-load-private').find('.cbp-l-loadMore-defaultText').addClass('show');
            if (res.finish == true) {
                $('.btn-load-private').find('.cbp-l-loadMore-defaultText').html('No More');
                // $('.btn-load-private').find('.cbp-l-loadMore-noMoreLoading').addClass('show');
                console.log("complete");
            }
        });
    });


    var gStart = 0;

    $('.btn-load-global').click(function(event) {
        gStart = gStart+1;
        $.ajax({
            url: siteUrl + 'promo/list_global_call/' + gStart,
            type: 'post',
            dataType: 'json',
            data: csrf,
        })
        .done(function(res) {
            $.each(res.data, function(index, v) {
            console.log("load promo gobal");
                var url = v.judul
                    .toLowerCase()
                    .replace(/ /g,'-')
                    .replace(/[^\w-]+/g,'');
                var url = v.id+'-'+url;

                var claimed = '';
                if (v.is_claimed == true) {
                    claimed = '<div class="ribbon red"><span>claimed</span></div>';
                }
                $('.list-promo-global').append(
                    '<div class="col-md-4">'+
                        '<div class="c-content-blog-post-card-1 c-option-2 c-bordered">'+
                            '<div class="c-media c-content-overlay">'+
                                '<a href="javascript:readg(\''+url+'\')"><img class="c-overlay-object img-responsive img-c" src="'+baseUrl+'uploads/promo/'+v.featured_img+'" alt=""></a>'+
                                claimed +

                            '</div>'+
                            '<div class="c-body">'+
                                '<div class="c-title c-font-bold c-font-uppercase">'+
                                    '<a href="javascript:readg(\''+url+'\')">'+v.judul+'</a>'+
                                '</div>'+
                                '<span>'+v.merchant.nama+'</span>'+
                                '<div class="c-author"> Berlaku sampai '+
                                    '<span class="c-font-uppercase c-font-blue-2 bold">'+fotmatTanggal(v.valid_until)+'</span>'+
                                '</div>'+
                                '<p>'+ v.body.slice(1, 100) +'</p>'+
                            '</div>'+
                        '</div>'+
                    '</div>'
                )
            });
        })
        .fail(function() {
            console.log("error");
        })
        .always(function(res) {
            $('.btn-load-global').find('.cbp-l-loadMore-defaultText').html('Load More');
            if (res.finish == true) {
                $('.btn-load-global').find('.cbp-l-loadMore-defaultText').html('No More');
                console.log("complete");
            }
        });
    });

    function readp(promo_id) {
        var crsf = {<?php echo $csrf_name ?> : '<?php echo $csrf ?>'};
        $('#modal_promo').load(siteUrl + 'promo/read/' + promo_id,crsf,function() {
        });

    }

    function readg(promo_id) {
        var crsf = {<?php echo $csrf_name ?> : '<?php echo $csrf ?>'};
        $('#modal_promo').load(siteUrl + 'promo/read/' + promo_id + '/t',crsf,function() {
        });

    }

    function fotmatTanggal(tanggal) {
        var tahun = tanggal.slice(1, 4);
        var bulan = tanggal.slice(6, 7);
        var hari = tanggal.slice(9, 10);
        return hari+'/'+bulan+'/'+tahun;
    }

</script>
