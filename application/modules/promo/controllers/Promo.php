<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Promo extends MX_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('promo_model');
        $this->load->model('promo_user_model');
        $this->load->model('promo_claim_model');
        $this->load->helper('text');

        //Codeigniter : Write Less Do More
    }

    public function index()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf']      = $this->security->get_csrf_hash();

        $date_now = date('Y-m-d');
        $batas_date_valid = date('Y-m-d', strtotime($date_now. ' + 7 days'));

        $data['title']  = 'Promo';

        $data['promos'] = $this->promo_model->list_global();
        $data['promo_khusus'] = $this->promo_model->list_private();

        $this->template->load('global/frontLayout/index', 'promo/index', $data);
    }

    public function list_private_call($start = 0)
    {
        $res['data'] = $this->promo_model->list_private($start);
        $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }

    public function list_global_call($start = 0)
    {
        $res['data'] = $this->promo_model->list_global($start);
        $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }


    public function read($promo_id, $private = 'y')
    {
        $data['promo'] = $this->promo_model->get(array('id' => $promo_id, 'active' => 'y'));
        if (!$data['promo']) {
            show_404();
        }
        if ($private == 'y') {
            $data['is_claimed'] = $this->promo_user_model->get( array('user_id' => logged_user('id'), 'promo_id'=> $promo_id, 'is_claimed' => 'y' ));
        } else {
            $data['is_claimed'] = $this->promo_claim_model->get( array('user_id' => logged_user('id'), 'promo_id'=> $promo_id, 'is_claimed' => 'y' ));
        }

        $data['title'] = $data['promo']->judul;

        $this->load->view('read_modal', $data, false);
        // $this->template->load('global/frontLayout/index', 'promo/read', $data);
    }

}
