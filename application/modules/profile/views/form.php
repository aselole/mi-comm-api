<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
    <div class="container">
        <div class="c-page-title c-pull-left">
            <h3 class="c-font-uppercase c-font-sbold">Profil</h3>
        </div>
    </div>
</div>
<!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
<div class="container">
    <div class="c-layout-sidebar-menu c-theme ">
        <!-- BEGIN: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
        <div class="c-sidebar-menu-toggler">
            <h3 class="c-title c-font-uppercase c-font-bold">My Dashboard</h3>
            <a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
                <span class="c-line"></span>
                <span class="c-line"></span>
                <span class="c-line"></span>
            </a>
        </div>
        <ul class="c-sidebar-menu collapse " id="sidebar-menu-1">

            <li class="c-dropdown c-open">
                <img src="<?php echo get_user_img(logged_user('photo')) ?>" alt="" width="270px" height="270px">
            </li>
            <li class="c-dropdown c-open">
                <a href="javascript:;" class="c-toggler">Akun
                    <span class="c-arrow"></span>
                </a>
                <ul class="c-dropdown-menu">
                    <li class="c-active">
                        <a href="<?php echo site_url('profile') ?>">Profil</a>
                    </li>
                    <li class="">
                        <a href="<?php echo site_url('issue_private') ?>">Pesan
                        <?php if (count_unread_msg()): ?>
                            <span class="badge c-bg-blue"><?php echo count_unread_msg(); ?></span>
                        <?php endif; ?></a>
                    </li>
                    <?php if (logged_user('level_id') == 2): ?>
                        <li class="">
                            <a href="<?php echo site_url('transaksi') ?>">Transaksi
                            </a>
                        </li>
                    <?php endif; ?>

                </ul>
            </li>
        </ul>
        <!-- END: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
    </div>

    <div id="modal_form" class="modal  modal-profile" tabindex="-1" data-backdrop="static" data-keyboard="true">
    </div>

    <div class="c-layout-sidebar-content ">
        <!-- BEGIN: PAGE CONTENT -->
        <div class="c-content-title-1">
            <h3 class="c-font-uppercase c-font-bold">Edit Profile</h3>
            <div class="c-line-left"></div>
        </div>
        <form class="c-shop-form-1 form_ajax" action="<?php echo $action ?>" method="post" enctype="multipart/form-data">
            <!-- BEGIN: ADDRESS FORM -->
            <div class="">
                <?php echo form_hidden($csrf_name, $csrf); ?>

                <!-- BEGIN: BILLING ADDRESS -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                        <?php if (@$user->photo != null && file_exists('./uploads/user/'.@$user->photo)): ?>
                                            <img src="<?php echo base_url().'uploads/user/'.@$user->photo ?>">
                                        <?php endif; ?>
                                    </div>
                                    <div>
                                        <span class="btn c-btn-red-2 c-btn-uppercase c-btn-bold c-btn-border-2x btn-file">
                                            <span class="fileinput-new"> Pilih Foto </span>
                                            <span class="fileinput-exists"> Ganti </span>
                                            <input type="file" name="photo"> </span>
                                        <a href="javascript:;" class="btn c-btn-red-2 fileinput-exists" data-dismiss="fileinput"> Hapus </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="control-label">Nama <span class="c-font-red">*</span></label>
                                <input type="text" value="<?php echo $user->first_name ?>" name="first_name" class="form-control c-square c-theme" placeholder="Nama"> </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="control-label">Alamat <span class="c-font-red">*</span></label>
                        <textarea type="text" name="address" class="form-control c-square c-theme" placeholder="Alamat"><?php echo $user->address ?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="control-label">Jenis Kelamin <span class="c-font-red">*</span></label>
                        <div>
                            <?php
                            $checkedl = '';
                            $checkedp = '';
                            if ( $user->gender == 'L' || $user->gender == 'l')
                            {
                                $checkedl = 'checked';
                            }else{
                                $checkedp = 'checked';
                            }
                             ?>
                            <label class="radio-inline">
                                <input <?php echo $checkedl ?> name="gender" id="inlineRadio1" value="L" type="radio"> Laki-laki </label>
                            <label class="radio-inline">
                                <input <?php echo $checkedp ?> name="gender" id="inlineRadio2" value="P" type="radio"> Perempuan </label>
                        </div>
                    </div>
                </div>
                <?php if (logged_user('level_id') == 3): ?>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label class="control-label">Deskripsi</label>
                            <textarea type="text" name="description" class="form-control c-square c-theme" placeholder="deskripsi"><?php echo $user->description ?></textarea>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="control-label">Email </label>
                                <input type="text" value="<?php echo $user->email ?>" name="email" readonly class="form-control c-square c-theme" placeholder="Email Address">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label">Phone</label>
                                <input type="text" value="<?php echo $user->phone ?>" name="phone" class="form-control c-square c-theme" placeholder="Phone">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END: BILLING ADDRESS -->
                <!-- BEGIN: PASSWORD -->

                <!-- <h3>Akun</h3>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="control-label">Username</label>
                        <input type="text" name="username" value="<?php echo $user->username ?>" class="form-control c-square c-theme" placeholder="Password"> </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label class="control-label">Ubah Password</label>
                        <input type="password" name="password" class="form-control c-square c-theme" placeholder="Password">
                        <p class="help-block">* kosongi password jika tidak diubah. </p>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label">Ulangi Password</label>
                        <input type="password" name="re_password" class="form-control c-square c-theme" placeholder="Password">
                    </div>
                </div>-->
                <div class="row c-margin-t-30">
                    <div class="form-group col-md-12" role="group">
                        <button type="submit" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Submit</button>
                        <a href="<?php echo site_url('profile') ?>" class="btn btn-lg btn-default c-btn-square c-btn-uppercase c-btn-bold">Cancel</a>
                    </div>
                </div>
            </div>
            <!-- END: ADDRESS FORM -->
        </form>

    </div>
</div>
<script src="<?php echo base_url() ?>assets/custom/scripts/j_front_issue_comment.js" type="text/javascript"></script>
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
