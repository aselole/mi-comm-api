<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
    <div class="container">
        <div class="c-page-title c-pull-left">
            <h3 class="c-font-uppercase c-font-sbold">Profil</h3>
        </div>
    </div>
</div>
<!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
<div class="container">
    <div class="c-layout-sidebar-menu c-theme ">
        <!-- BEGIN: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
        <div class="c-sidebar-menu-toggler">
            <h3 class="c-title c-font-uppercase c-font-bold">My Dashboard</h3>
            <a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
                <span class="c-line"></span>
                <span class="c-line"></span>
                <span class="c-line"></span>
            </a>
        </div>
        <ul class="c-sidebar-menu collapse " id="sidebar-menu-1">

            <li class="c-dropdown c-open">
                <img src="<?php echo get_user_img(logged_user('photo')) ?>" alt="" width="270px" height="270px">
            </li>
            <li class="c-dropdown c-open">
                <a href="javascript:;" class="c-toggler">Akun
                    <span class="c-arrow"></span>
                </a>
                <ul class="c-dropdown-menu">
                    <li class="c-active">
                        <a href="<?php echo site_url('profile') ?>">Profil</a>
                    </li>
                    <li class="">
                        <a href="<?php echo site_url('issue_private') ?>">Pesan
                        <?php if (count_unread_msg()): ?>
                            <span class="badge c-bg-blue"><?php echo count_unread_msg(); ?></span>
                        <?php endif; ?>
                        </a>
                    </li>
                    <?php if (logged_user('level_id') == 2): ?>
                        <li class="">
                            <a href="<?php echo site_url('transaksi') ?>">Transaksi
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if (FALSE): ?>
                        <li class="">
                            <a href="<?php echo site_url('issue_community') ?>">Issue
                        </li>
                    <?php endif; ?>
                </ul>
            </li>
        </ul>
        <!-- END: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
    </div>

    <div id="modal_form" class="modal  modal-profile" tabindex="-1" data-backdrop="static" data-keyboard="true">
    </div>

    <div class="c-layout-sidebar-content ">
        <!-- BEGIN: PAGE CONTENT -->
        <div class="profil">
            <div class="c-content-title-1">
                <h3 class="c-font-uppercase c-font-bold">Profil
                    <a href="<?php echo site_url('profile/form') ?>" class="c-font-lowercase btn btn-xs btn-success btn-outline ">
                        <i class="fa fa-edit"></i> edit
                    </a>
                </h3>
                <div class="c-line-left"></div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 c-margin-b-20">
                    <h3 class="c-font-uppercase c-font-bold"><?php echo logged_user('first_name') ?></h3>
                    <table>
                        <tr>
                            <td>Alamat</td>
                            <td>&nbsp;:&nbsp;</td>
                            <td><?php echo logged_user('address') ? logged_user('address'):'-' ?></td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td>&nbsp;:&nbsp;</td>
                            <td><?php echo logged_user('phone') ? logged_user('phone'):'-' ?></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>&nbsp;:&nbsp;</td>
                            <td><?php echo logged_user('email') ? logged_user('email'):'-' ?></td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr >
                            <td style="vertical-align: top">Kominitas</td>
                            <td style="vertical-align: top">&nbsp;:&nbsp;</td>
                            <td style="vertical-align: top">
                                <?php if ($communities): ?>
                                    <?php foreach ($communities as $c): ?>
                                        <?php echo $c->community->nama ?><br>
                                    <?php endforeach ?>
                                <?php else: ?>
                                    -
                                <?php endif ?>
                            </td>
                        <tr>
                            <td>Peserta</td>
                            <td>&nbsp;:&nbsp;</td>
                            <td> <?php echo $product->nama ?> </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top">ID</td>
                            <td style="vertical-align: top">&nbsp;:&nbsp;</td>
                            <td style="vertical-align: top">
                                <?php echo logged_user('card_barcode') ?>
                                <br>
                                <?php
                                $img            =   code128BarCode(logged_user('card_barcode'), 1);
                                ob_start();
                                imagepng($img);
                                $output_img     =   ob_get_clean();
                                echo '<img src="data:image/png;base64,' . base64_encode($output_img) . '" />'; ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>        <!-- END: PAGE CONTENT -->
        </div>

    </div>
</div>
