<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/Barcode.php';
class Profile extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            redirect('home', 'refresh');
        }
        $this->load->model(array('user_model', 'product_model', 'member_model'));
        //Codeigniter : Write Less Do More
    }

    function index()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf']      = $this->security->get_csrf_hash();

        $data['title']       ='Profil';
        $data['product']     = $this->product_model->get(logged_user('product_id'));
        $data['communities'] = $this->member_model->with_community('fields:nama')->get_all( array('user_id' => logged_user('id')) );
        $this->template->load('global/frontLayout/index', 'profile/index', $data);
    }

    public function form()
    {

        $data['title'] ='Edit Profil';
        $id   = logged_user('id');
        $user = $this->user_model->get($id);

        if (!$user) {
            redirect('home');
        }

        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();

        $user           = $this->user_model->get($id);
        $data['user']   = $user;
        $data['action'] = site_url('profile/form_action');

        $this->template->load('global/frontLayout/index', 'profile/form', $data);
    }

    public function form_action()
    {
        $is_unique_username = '';
        $is_unique_email    = '';

        $id    = logged_user('id');
        $user = $this->user_model->get($id);
        if (!$user) {
            $result['success'] = false;
            $result['message'] = 'Data tidak ditemukan.';
            $this->output->set_status_header(400)->set_content_type('application/json')->set_output(json_encode($result));
        } else {
            if ($this->input->post('username') != $user->username) {
                $is_unique_username = '|is_unique[users.username]';
            }
            if ($this->input->post('email') != $user->email) {
                $is_unique_email = '|is_unique[users.email]';
            }
            $config = array(
                array(
                    'field'  => 'first_name',
                    'label'  => 'Nama',
                    'rules'  => 'required|min_length[3]',
                    'errors' => array(
                        'required' => '%s tidak boleh kosong.',
                        'min_length' => '%s minimal 3 huruf.',
                    ),
                ),
                array(
                    'field'  => 'address',
                    'label'  => 'Alamat',
                    'rules'  => 'required|min_length[10]',
                    'errors' => array(
                        'required' => '%s tidak boleh kosong.',
                        'min_length' => '%s minimal 10 huruf.',
                    ),
                ),
                array(
                    'field'  => 'gender',
                    'label'  => 'Jenis Kemain',
                    'rules'  => 'required',
                    'errors' => array(
                        'required' => '%s tidak boleh kosong.',
                    ),
                ),
                array(
                    'field'  => 'email',
                    'label'  => 'Email',
                    'rules'  => 'valid_email|required'.$is_unique_email,
                    'errors' => array(
                        'required'  => '%s tidak boleh kosong.',
                        'is_unique' => '%s tersebut sudah dipakai.',
                        'valid_email' => '%s tidak benar.',
                    ),
                ),
                array(
                    'field'  => 'phone',
                    'label'  => 'No Hp',
                    'rules'  => 'numeric|min_length[6]',
                    'errors' => array(
                        'numeric'  => '%s hanya mengandung angka.',
                        'min_length'  => '%s kurang benar.'
                    ),
                ),
                // array(
                //     'field'  => 'username',
                //     'label'  => 'Username',
                //     'rules'  => 'required'.$is_unique_username,
                //     'errors' => array(
                //         'required'  => '%s tidak boleh kosong.',
                //         'is_unique' => '%s tersebut sudah dipakai.',
                //     ),
                // ),
                // array(
                //     'field'  => 'password',
                //     'label'  => 'New Password',
                //     'rules'  => '',
                // ),
                // array(
                //     'field'  => 'photo',
                //     'label'  => 'Foto',
                //     'rules'  => 'required',
                //     'errors' => array(
                //         'required'  => '%s tidak boleh kosong.',
                //     ),
                // ),
                // array(
                //     'field'  => 're_password',
                //     'label'  => 'Password',
                //     'rules'  => 'matches[password]',
                //     'errors' => array(
                //         'matches'  => '%s tidak cocok.',
                //     ),
                // ),
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE){
                $result['errors'] = $this->form_validation->error_array();
                $this->output
                    ->set_status_header(400)
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            } else {
                $password      = $this->input->post('password');
                $hash_password = $this->hash_password($password);
                $data = array(
                    'first_name'  => $this->input->post('first_name'),
                    'gender'      => $this->input->post('gender'),
                    'phone'       => $this->input->post('phone'),
                    'address'     => $this->input->post('address'),
                    'description' => $this->input->post('description'),
                    'email'       => $this->input->post('email'),
                    // 'username'    => $this->input->post('username'),
                    // 'password'    => $hash_password,
                );

                try {
                    if ( !empty($_FILES['photo']['name']) ) {
                        $upload = $this->upload_pic_user();
        				if ($upload['success']) {
                            $data['photo'] = $upload['data']['file_name'];
                            if ($user->photo != null && file_exists('./uploads/user/'.$user->photo)) {
                                unlink('./uploads/user/'.$user->photo);
                            }
                        } else {
                            $result = $upload;
                            throw new Exception('error');
                        }
                    }

                    $this->user_model->update($data, $id);
                    $result['success'] = true;
                    $result['message'] = 'User berhasil diperbarui.';
                    $result['url']     = site_url('profile');
                } catch (Exception $e) {
                    $result['success'] = false;
                    $result['message'] = 'Terjadi kesalahan.';
                    if ($e == 'error') {
                        $this->output->set_status_header(400);
                    }
                }
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            }
        }

    }



    public function hash_password($password)
    {
		// bcrypt
        return $this->bcrypt->hash($password);
    }

    public function upload_pic_user()
    {
        $config['upload_path'] = './uploads/user/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']  = '90000';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('photo')) {
            $error = array('error' => $this->upload->display_errors());
            $result['success'] = false;
            $result['message'] = $error['error'];
        } else {
            $upload_data = $this->upload->data();
            $result = array(
                'success' => true,
                'data' => $upload_data,
            );
        }
        return $result;
    }


}
