<div class="c-layout-breadcrumbs-1 c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
    <div class="container">
        <div class="c-page-title c-pull-left">
            <h3 class="c-font-uppercase c-font-sbold">Issue
                <?php if (logged_user('level_id' == 3)): ?>
                    <button class="btn btn-xs btn-success btn-outline btn_create" >
                        <i class="fa fa-plus"></i> baru
                    </button>
                <?php endif; ?>
            </h3>
        </div>
        <ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
            <li>
                <a href="<?php echo site_url('') ?>">Home</a>
            </li>
            <li>/</li>
            <li>
                <a href="<?php echo site_url('Issue') ?>">Issue</a>
            </li>
            <li>/</li>
            <li class="c-state_active"><?php echo $title ?></li>
        </ul>
    </div>
</div>

<div id="modal_form" class="modal  modal-create" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>
<!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->
<!-- BEGIN: PAGE CONTENT -->
<!-- BEGIN: BLOG LISTING -->
<div class="c-content-box c-size-md">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="c-content-blog-post-1-view">
                    <div class="c-content-blog-post-1">
                        <?php if ($this->session->flashdata('notif_type')): ?>
                            <div class="alert <?php echo $this->session->flashdata('notif_type') ?> alert-dismissible" role="alert">
                                <?php echo $this->session->flashdata('notif_msg'); ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                        <?php endif; ?>
                        <div class="c-media">
                            <div class="c-content-media-2-slider" data-slider="owl" data-single-item="true" data-auto-play="4000">
                                <div class="owl-carousel owl-theme c-theme owl-single">
                                        <div class="item">
                                            <div class="c-content-media-2" style="background-image: url(<?php echo get_issue_img($issue->featured_img) ?>); min-height: 460px;"> </div>
                                            <?php if ( $issue->status == 'open'): ?>
                                                <div class="ribbon blue"><span>open</span></div>
                                            <?php endif ?>
                                            <?php if ( $issue->status == 'closed'): ?>
                                                <div class="ribbon green"><span>closed</span></div>
                                            <?php endif ?>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div class="c-title c-font-bold c-font-uppercase">
                            <a href="#"><?php echo $issue->judul ?></a>
                        </div>
                        <div class="c-panel c-margin-b-30">
                            <div class="c-author">
                                <a href="#">Oleh
                                    <span class="c-font-uppercase"><?php echo $issue->author->first_name ?></span>
                                </a>
                            </div>
                            <div class="c-date">
                                <span class="c-font-uppercase"><?php echo date('d/m/Y', strtotime($issue->published_at)) ?></span>
                            </div>
                            <div class="c-comments ">
                            <?php $jumlah_komentar = isset($issue->issue_comment[0]->counted_rows) ? $issue->issue_comment[0]->counted_rows : 0 ; ?>
                                <a href="#komentar">
                                    <i class="icon-speech"></i> <?php echo $jumlah_komentar ?> Komentar</a>
                            </div>
                        </div>
                        <div class="c-desc">
                            <?php echo $issue->body ?>
                        </div>
                        <div id="komentar"><br><br></div>
                        <div class="c-comments">
                            <div class="c-content-title-1">
                                <h3 class="c-font-uppercase c-font-bold">Komentar(<?php echo $jumlah_komentar ?>)</h3>
                                <div class="c-line-left"></div>
                            </div>
                            <?php if ($jumlah_komentar != 0): ?>
                                <div class="c-comment-list">
                                    <?php if ($comments): ?>
                                    <?php foreach ($comments as $r): ?>
                                        <div class="media">
                                            <div class="media-left">
                                                <?php if ($r->user->photo && file_exists('./uploads/user/'.$r->user->photo)): ?>
                                                    <img class="media-object" alt="" src="<?php echo base_url('uploads/user/'.$r->user->photo) ?>">
                                                <?php else: ?>
                                                    <img class="media-object" alt="" src="<?php echo base_url('assets/pages/media/profile/avatar.png') ?>">
                                                <?php endif; ?>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">
                                                    <a ><?php echo $r->user->first_name ?></a> on
                                                    <span class="c-date"><?php echo date('d F Y, H:i', strtotime($r->created_at)) ?></span>
                                                </h4>
                                                <?php echo $r->comment ?>
                                                <br>
                                                <a href="#" data-issue-id="<?php echo $issue->id ?>" data-parent-id="<?php echo $r->id ?>" class="btn_reply font-kecil"> balas</a>
                                                <?php if (logged_user('id') != $r->user->id): ?>
                                                     | <a href="#" data-comment-id="<?php echo $r->id ?>" data-parent='y' class="btn_lapor font-kecil">laporkan</a> 
                                                <?php endif; ?>
                                                <?php if (logged_user('id') == $r->user->id): ?>
                                                     | <a href="#" data-comment-id="<?php echo $r->id ?>" data-pesan="<?php echo $r->comment ?>" data-parent='y' class="btn_hapus font-kecil">hapus</a>
                                                <?php endif; ?>
                                                <?php if (isset($r->issue_comment_child)): ?>
                                                    <?php foreach ($r->issue_comment_child as $rr): ?>
                                                        <div class="media">
                                                            <div class="media-left">
                                                                <?php if ($rr->user->photo && file_exists('./uploads/user/'.$rr->user->photo)): ?>
                                                                    <img class="media-object" alt="" src="<?php echo base_url('uploads/user/'.$rr->user->photo) ?>">
                                                                <?php else: ?>
                                                                    <img class="media-object" alt="" src="<?php echo base_url('assets/pages/media/profile/avatar.png') ?>">
                                                                <?php endif; ?>
                                                            </div>
                                                            <div class="media-body">
                                                                <h4 class="media-heading">
                                                                    <a href=""><?php echo $rr->user->first_name ?></a> on
                                                                    <span class="c-date"><?php echo date('d F Y, H:i', strtotime($rr->created_at)) ?></span>
                                                                </h4>
                                                                <?php echo $rr->comment ?>
                                                                <br>
                                                                <?php if (logged_user('id') != $rr->user->id): ?>
                                                                    <a href="" data-comment-id="<?php echo $rr->id ?>" data-parent='t' class="btn_lapor font-kecil">laporkan</a> 
                                                                <?php endif; ?>
                                                                <?php if (logged_user('id') == $rr->user->id): ?>
                                                                    <a href="" data-comment-id="<?php echo $rr->id ?>" data-pesan="<?php echo $rr->comment ?>" data-parent='t' class="btn_hapus font-kecil">hapus</a>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                </div>
                            <?php endif ?>
                            <?php if ($issue->status == 'open'): ?>
                                <h3 class="sbold blog-comments-title">Tinggalkan Komentar</h3>
                                <form method="post" class="form_ajax" action="<?php echo site_url('issue/store_comment') ?>">
                                    <?php echo form_hidden($csrf_name, $csrf) ?>
                                    <input type="hidden" name="issue_id" value="<?php echo $issue->id ?>">
                                    <input type="hidden" name="parent_id" value="0">
                                    <input type="hidden" name="url" value="<?php echo $this->uri->segment(3) ?>">
                                    <div class="form-group">
                                        <textarea rows="8" name="comment" placeholder="Tulis komentar disini ..." class="form-control c-square"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn c-theme-btn c-btn-uppercase btn-md c-btn-sbold btn-block c-btn-square">Kirim Pesan</button>
                                    </div>
                                </form>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <!-- BEGIN: CONTENT/BLOG/BLOG-SIDEBAR-1 -->
                <div class="c-content-tab-1 c-theme c-margin-t-30">
                    <div class="nav-justified">
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active">
                                <a href="#blog_recent_posts" data-toggle="tab">Issue Terbaru</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="blog_recent_posts">
                                <ul class="c-content-recent-posts-1">
                                    <?php foreach ($issues as $is): ?>
                                    <li>
                                        <div class="c-post">
                                            <?php $judul = strtolower(str_replace(' ', '-', $is->judul));
                                            $find  = array ('\'', '(', ')', '/' , '\/', '%', '$', '^', '*', '=', '!', '@','#', ':', ';', '"', '?','<', '>', ',', '.', '{', '}',
                                            '[', ']', '|', '_', '&' );
                                            $url_p = str_replace($find, '', $judul); 
                                            
                                            $url = 'issue/read/' . $is->id . '-' . $url_p ?>
                                            <a href="<?php echo site_url($url) ?>" class="c-title"> <?php echo $is->judul ?> </a>
                                            <div class="c-date"><?php echo date('d/m/Y', strtotime($is->published_at)) ?></div>
                                        </div>
                                    </li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal_form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>

<style media="screen">
    .font-kecil{
        font-size: 12px;
        color: #a7a7a7;
    }
</style>
<!-- <script src="<?php echo base_url() ?>assets/global/scripts/app.js" type="text/javascript"></script> -->
<script src="<?php echo base_url() ?>assets/custom/scripts/j_front_issue_comment.js" type="text/javascript"></script>

<script type="text/javascript">
    $('.btn_reply').click(function(e) {
        e.preventDefault();
        var issue_id = $(this).data('issue-id');
        var parent_id = $(this).data('parent-id');
        $('#modal_form').load('<?php echo site_url() ?>'+'issue/form_modal_comment/'+issue_id+'/'+parent_id, function() {
            $('#modal_form').modal('show');
        });
    });
    $('.btn_lapor').click(function(e) {
        e.preventDefault();
        var comment_id = $(this).data('comment-id');

        $.post('<?php echo site_url('issue/cek_laporan_bad_comment') ?>', {'<?php echo $csrf_name ?>':'<?php echo $csrf ?>', comment_id:comment_id}, function(res) {
            if (res.success) {
                msg = 'Komentar ini mengandung konten yang terlarang.';
                swal(
                    {
                        title: "Apakah Anda yakin?",
                        text: msg,
                        type: "warning",
                        showCancelButton: true,
                        cancelButtonClass: "btn-default",
                        confirmButtonClass: "btn c-theme-btn c-btn-uppercase btn-md c-btn-sbold btn-block c-btn-square",
                        confirmButtonText: "Laporkan Komentar!",
                        closeOnConfirm: false
                    },
                    function(){
                        $.post('<?php echo site_url('issue/laporkan_comment') ?>', {<?php echo $csrf_name ?>: '<?php echo $csrf ?>', comment_id:comment_id}, function(ress) {
                            if (ress.success) {
                                swal(
                                    {
                                        title: "Berhasil!",
                                        text: ress.message,
                                        type: "success",
                                        confirmButtonClass: "btn-success",
                                        confirmButtonText: "Success",
                                        closeOnConfirm: true
                                    }
                                );
                            } else {
                                toastr.error(ress.message);
                            }
                        });
                    }
                );
            } else {
                swal(
                    {
                        title: "Laporan pernah dilakukan.",
                        text: res.message,
                        type: "warning",
                        confirmButtonClass: "btn c-theme-btn c-btn-uppercase btn-md c-btn-sbold btn-block c-btn-square",
                        confirmButtonText: "Ok.",
                        closeOnConfirm: true
                    }
                );
            }
        });

    });
    $('.btn_hapus').click(function(e) {
        e.preventDefault();
        var comment_id = $(this).data('comment-id');
        var pesan = $(this).data('pesan');

        swal(
            {
                title: "Hapus komentar ini ?",
                text: pesan,
                type: "warning",
                showCancelButton: true,
                cancelButtonClass: "btn-default",
                confirmButtonClass: "btn c-theme-btn c-btn-uppercase btn-md c-btn-sbold btn-block c-btn-square",
                confirmButtonText: "Hapus Komentar!",
                closeOnConfirm: false
            },
            function(){
                $.post('<?php echo site_url('issue/hapus') ?>', {<?php echo $csrf_name ?>: '<?php echo $csrf ?>', comment_id:comment_id,current_url: '<?php echo current_url() ?>'}, function(ress) {
                    if (ress.success) {
                        swal("Deleted!", ress.message, "success");
                        location.reload();
                    } else {
                        toastr.error(ress.message);
                    }
                });
            }
        );
    });

    $('.btn_create').click(function(e) {
        e.preventDefault();
        var crsf = {<?php echo $csrf_name ?> : '<?php echo $csrf ?>'};
        $('.modal-create').load('<?php echo site_url('issue_community/create') ?>', crsf ,function() {
            $('.modal-create').modal('show');
        });
    });


</script>
