<div class="c-layout-breadcrumbs-1 c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
    <div class="container">
        <div class="c-page-title c-pull-left">
            <h3 class="c-font-uppercase c-font-sbold">Issue
                <?php if (logged_user('level_id' == 3)): ?>
                    <button class="btn btn-xs btn-success btn-outline btn_create" >
                        <i class="fa fa-plus"></i> baru
                    </button>
                <?php endif; ?>
            </h3>
        </div>
    </div>
</div>
<!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->
<!-- BEGIN: PAGE CONTENT -->
<!-- BEGIN: BLOG LISTING -->
<div id="modal_form" class="modal  modal-create" tabindex="-1" data-backdrop="static" data-keyboard="false">
</div>

<div class="c-content-box c-size-md">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="c-content-blog-post-card-1-grid">
                    <div class="row isi">
                        <?php if ($issues): ?>
                            <?php foreach ($issues as $i):
                                $judul = strtolower(str_replace(' ', '-', $i->judul));
                                $find  = array ('\'', '(', ')', '/' , '\/', '%', '$', '^', '*', '=', '!', '@','#', ':', ';', '"', '?','<', '>', ',', '.', '{', '}',
                                        '[', ']', '|', '_', '&' );
                                $url_p = str_replace($find, '', $judul); ?>

                                <?php $url = 'issue/read/' . $i->id . '-' . $url_p ?>
                                <div class="col-md-4">
                                    <div class="c-content-blog-post-card-1 c-option-2 c-bordered">
                                        <div class="c-media c-content-overlay ">
                                            <a href="<?php echo site_url($url) ?>">
                                            <img class="c-overlay-object img-responsive img-def" src="<?php echo get_issue_img($i->featured_img)?>" alt="">
                                            <?php if ( $i->status == 'open'): ?>
                                                <div class="ribbon blue"><span>open</span></div>
                                            <?php endif ?>
                                            <?php if ( $i->status == 'closed'): ?>
                                                <div class="ribbon green"><span>closed</span></div>
                                            <?php endif ?>
                                            </a>
                                        </div>
                                        <div class="c-body">
                                            <div class="c-title c-font-bold c-font-uppercase">
                                                <a href="<?php echo site_url($url) ?>"><?php echo ellipsize($i->judul, 27) ?></a>
                                            </div>
                                            <div class="c-author"> oleh
                                                <span class="c-font-uppercase"><?php echo $i->first_name ?></span>
                                                on
                                                <span class="c-font-uppercase"><?php echo date('d/m/Y', strtotime($i->published_at)) ?></span>
                                            </div>
                                            <div class="c-panel">
                                                <div class="c-comments">
                                                    <?php $cou = isset($i->jumlah_komentar) ? $i->jumlah_komentar : 0 ; ?>
                                                    <i class="icon-speech"></i> <?php echo $cou ?> Komentar
                                                </div>
                                            </div>
                                            <p style="height:60px"> <?php echo ellipsize($i->body, 100) ?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        <?php else: ?>
                            belum ada issues
                        <?php endif ?>

                    </div>
                    <?php if (count($issues) >= 6): ?>

                        <div id="loadMore-container" class="cbp-l-loadMore-button c-margin-t-60">
                            <a class="cbp-l-loadMore-link btn c-btn-square c-btn-border-2x c-btn-dark c-btn-bold c-btn-uppercase btn-load">
                                <span class="cbp-l-loadMore-defaultText">LOAD MORE</span>
                                <span class="cbp-l-loadMore-loadingText">LOADING...</span>
                                <span class="cbp-l-loadMore-noMoreLoading">NO MORE</span>
                            </a>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: BLOG LISTING  -->
<!-- END: PAGE CONTENT -->
<script type="text/javascript">
    var siteUrl = '<?php site_url()?>';
    var baseUrl = '<?php base_url()?>';
    var csrf    = {<?php echo $csrf_name ?>:'<?php echo $csrf ?>'};
    var start  = 0;;


    $('.btn-load').click(function(event) {
        start = start+1;
        $.ajax({
            url: siteUrl + 'issue/list_issues_community_call/' + start,
            type: 'post',
            dataType: 'json',
            data: csrf,
        })
        .done(function(res) {
            console.log("oko");
            $.each(res.data, function(index, v) {
                var url = v.judul
                    .toLowerCase()
                    .replace(/ /g,'-')
                    .replace(/[^\w-]+/g,'');
                var url = v.id+'-'+url;
                var status = '';
                if (v.status == 'open') {
                    status = '<div class="ribbon blue"><span>open</span></div>';
                }
                $('.isi').append(
                    '<div class="col-md-4">'+
                        '<div class="c-content-blog-post-card-1 c-option-2 c-bordered">'+
                            '<div class="c-media c-content-overlay">'+
                                '<a href="'+siteUrl+'issue/read/'+url+'"><img class="c-overlay-object img-responsive img-def" src="'+baseUrl+'uploads/issues/'+v.featured_img+'" alt=""></a>'+
                                status +
                            '</div>'+
                            '<div class="c-body">'+
                                '<div class="c-title c-font-bold c-font-uppercase">'+
                                    '<a href="'+siteUrl+'issue/read/'+url+'">'+v.judul.slice(1,27)+'</a>'+
                                '</div>'+
                                '<div class="c-author"> oleh '+
                                    '<span class="c-font-uppercase">'+v.first_name+'</span>'+
                                    '<span class="c-font-uppercase">'+fotmatTanggal(v.published_at)+'</span>'+
                                '</div>'+
                                '<div class="c-panel">'+
                                    '<div class="c-comments">'+
                                        '<i class="icon-speech"></i> '+v.jumlah_komentar+' Komentar'+
                                    '</div>'+
                                '</div>'+
                                '<p style="height:60px">'+ v.body.slice(1, 100) +'</p>'+
                            '</div>'+
                        '</div>'+
                    '</div>'
                )
            });
        })
        .fail(function() {
            console.log("error");
        })
        .always(function(res) {
            $('.btn-load-global').find('.cbp-l-loadMore-defaultText').html('Load More');
            if (res.finish == true) {
                $('.btn-load-global').find('.cbp-l-loadMore-defaultText').html('No More');
                console.log("complete");
            }
        });
    });



    function fotmatTanggal(tanggal) {
        var tahun = tanggal.slice(1, 4);
        var bulan = tanggal.slice(6, 7);
        var hari = tanggal.slice(9, 10);
        return hari+'/'+bulan+'/'+tahun;
    }

    $('.btn_create').click(function(e) {
        e.preventDefault();
        var crsf = {<?php echo $csrf_name ?> : '<?php echo $csrf ?>'};
        $('.modal-create').load('<?php echo site_url('issue_community/create') ?>', crsf ,function() {
            $('.modal-create').modal('show');
        });
    });
</script>
