<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Issue extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            redirect('home', 'refresh');
        }
        $this->load->model('issue_model');
        $this->load->model('issue_comment_model');
        $this->load->model('bad_comment_model');
        $this->load->helper('text');
    }

    public function index()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf']      = $this->security->get_csrf_hash();

        $data['title']  = 'Issue';
        $where          = array('issues.active' => 'y');

        $data['issues'] = $this->issue_model->get_issue_comunity();
        // var_dump($data['issues']);

        $this->template->load('global/frontLayout/index', 'issue/index', $data);
    }

    public function list_issues_community_call($start = 0)
    {
        $res['data'] = $this->issue_model->get_issue_comunity($start);
        $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }


    public function read($url=0)
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf']      = $this->security->get_csrf_hash();


        $urlArr   = explode('-', $url);
        $issue_id = $urlArr[0];

        unset($urlArr[0]);
        $label    = implode('-',$urlArr);

        $issue    = $this->issue_model
                        ->where(array('issues.id' => $issue_id, 'issues.active' => 'y'))
                        ->with_author()
                        ->with_issue_comment('fields:*count*')
                        ->get();

        if (!$issue) {
            show_404();
        }else{
            $judul = strtolower(str_replace(' ', '-', $issue->judul));
            $find  = array ('\'', '(', ')', '/' , '\/', '%', '$', '^', '*', '=', '!', '@','#', ':', ';', '"', '?','<', '>', ',', '.', '{', '}',
                    '[', ']', '|', '_', '&' );
            $url_p = str_replace($find, '', $judul);
            if ( $url_p != $label  ) {
                show_404();

            }
        }
        $data['issue'] = $issue;

        $data['comments'] = $this
                            ->issue_comment_model
                            ->fields('id, comment, created_at')
                            ->where(array('issue_id'=>$issue_id, 'parent_id'=>0))
                            ->order_by('created_at', 'asc')
                            ->with_user('fields:first_name,photo')
                            ->with_issue_comment_child(
                                array(
                                    'fields'=>'id, comment, created_at',
                                    'order_inside'=>'created_at asc',
                                    'with'=>array('relation'=>'user', 'fields'=>'first_name,photo')
                                ))
                            ->get_all();

        $data['issues'] = $this->issue_model
                                ->fields('id,judul,published_at')
                                ->where(array('active' => 'y'))
                                ->order_by('published_at', 'desc')
                                ->limit(6)
                                ->get_all();

        if (!$data['issue']) {
            show_404();
        }
        $data['title'] = $data['issue']->judul;

        $this->template->load('global/frontLayout/index', 'issue/read', $data);
    }

    public function form_modal_comment($issue_id, $parent_id)
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf'] = $this->security->get_csrf_hash();

        $data['issue_id']  = $issue_id;
        $data['parent_id'] = $parent_id;
        $data['judul']     = '<i class="fa fa-reply"></i> Balas Komentar';
        $data['method']    = 'POST';
        $data['action']    = site_url('issue/store_comment');
        $this->load->view('admin/issue/form_modal_comment', $data);
    }

    public function store_comment()
    {
        $config = array(
            array(
                'field'  => 'comment',
                'label'  => 'Komentar',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $result['errors'] = $this->form_validation->error_array();
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        } else {
            $issue_id = $this->input->post('issue_id');
            $data = array(
                'issue_id'   => $issue_id,
                'user_id'    => $this->session->userdata('user_id'),
                'comment'    => $this->input->post('comment'),
                'parent_id'  => $this->input->post('parent_id'),
                'created_at' => date('Y-m-d H:i:s'),
            );
            $iss = $this->issue_model->get($issue_id);
            $url = $iss->id.'-'.strtolower(str_replace(' ', '-', $iss->judul));
            $insert = $this->issue_comment_model->insert($data);
            if ($insert) {
                $this->session->set_flashdata('notif_type', 'alert-info');
                $this->session->set_flashdata('notif_msg', 'Komentar berhasil ditambahkan.');

                $result['success'] = true;
                $result['message'] = 'Komentar berhasil ditambahkan.';
                $result['url']     = site_url('issue/read/'.$url);
            } else {
                $result['success'] = false;
                $result['message'] = 'Terjadi kesalahan.';
            }
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }
    }

    public function cek_laporan_bad_comment()
    {
        $filter = array(
            'issue_comment_id' => $this->input->post('comment_id'),
            'pelapor_id'       => $this->session->userdata('user_id'),
        );
        $cek = $this->bad_comment_model->where($filter)->get();
        if (!$cek) {
            // belum pernah
            $result = array('success' => true);
        } else {
            $result = array('success' => false, 'message' => 'Anda sudah pernah melaporkan komentar ini.');
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function laporkan_comment()
    {
        $comment_id = $this->input->post('comment_id');
        $data = array(
            'issue_comment_id' => $comment_id,
            'pelapor_id'       => $this->session->userdata('user_id'),
            'created_at'       => date('Y-m-d H:i:s'),
        );
        $insert = $this->bad_comment_model->insert($data);
        if (!$insert) {
            $result = array(
                'success' => false,
                'message' => 'Terjadi kesalahan.',
            );
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }

        $this->session->set_flashdata('notif_type', 'alert-info');
        $this->session->set_flashdata('notif_msg', 'Laporan Anda berhasil kami terima.');

        $result = array(
            'success' => true,
            'message' => 'Laporan Anda berhasil kami terima.',
        );
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function hapus()
    {
        $result = array(
            'success' => false,
            'message' => 'Terjadi kesalahan.',
        );

        $comment_id = $this->input->post('comment_id');
        if ($komen = $this->issue_comment_model->get($comment_id)) {
            if ($komen->user_id == logged_user('id')) {
                $this->issue_comment_model->delete($comment_id);
                $this->issue_comment_model->delete( array('parent_id' => $comment_id));
                $result = array(
                    'url' => $this->input->post('current_url'),
                    'success' => true,
                    'message' => 'Komentar dihapus.',
                );
            }
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

}
