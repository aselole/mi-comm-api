<?php
defined('BASEPATH') or exit('No direct script access allowed');

class News extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('news_model', 'image_news_model'));
        $this->load->helper('text');
        $this->load->library('pagination');
    }

    public function index($page=null)
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf']      = $this->security->get_csrf_hash();
        $data['title']     = 'Berita';
        // $events = $this->db->query("select a.*, b.image from news a left join image_news b on (b.news_id=a.id and b.is_featured='y') order by a.published_at desc limit 6");
        // $data['events']    = $this->news_model->with_image_news('where:`is_featured`=\'y\'')->order_by('published_at', 'desc')->limit(6, 0)->get_all(array('active' => 'y'));

        // pagination
        $config['full_tag_open'] = "<ul class='c-content-pagination c-theme'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='c-active'><a href='#'>";
        $config['cur_tag_close'] = "</a></li>";
        $config['next_tag_open'] = "<li class='c-next'>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li class='c-prev'>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        $jumlah_data = $this->news_model->jumlah_data_pagination();
        $config['base_url'] = site_url().'news/index/';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 6;
        $from = $this->uri->segment(3);
        $this->pagination->initialize($config);
        $data['events'] = $this->news_model->data_pagination($config['per_page'],$from);
        $this->template->load('global/frontLayout/index', 'news/index', $data);
    }

    function ajax_list() {
        $limit         = 6;
        $start         = $this->input->post('start')*$limit;

        $res['finish'] = false;
        $res['data']   = $this->news_model->with_image_news('where:`is_featured` =\'y\'')->order_by('published_at', 'desc')->limit($limit, $start)->get_all(array('active' => 'y'));;

        if (count($res['data']) < $limit) {
            $res['finish'] = true;
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($res));

    }

    public function read($url=NULL)
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf']      = $this->security->get_csrf_hash();

        $urlArr   = explode('-', $url);
        $event_id = $urlArr[0];
        unset($urlArr[0]);
        $label    = implode('-',$urlArr);

        $event = $this->news_model->get(array('id' => $event_id, 'active' => 'y'));
        $image_news = $this->image_news_model->where('news_id', $event_id)->get_all();
        // $event = $this->news_model->with_image_news('order_by:`is_featured`=\'desc\'')->get(array('id' => $event_id, 'active' => 'y'));
        if (!$event) {
            show_404();
        }else{
            $judul = strtolower(str_replace(' ', '-', $event->judul));
            $find  = array ('\'', '(', ')', '/' , '\/', '%', '$', '^', '*', '=', '!', '@','#', ':', ';', '"', '?','<', '>', ',', '.', '{', '}',
                    '[', ']', '|', '_', '&' );
            $url_p = str_replace($find, '', $judul);
            if ( $url_p != $label  ) {
                show_404();
            }
        }
        // $data['events'] = $this->news_model->limit(5)->order_by('published_at', 'desc')->get_all();
        $data['events'] = $this->db->query("select a.*, b.image from news a left join image_news b on (b.news_id=a.id and b.is_featured='y') order by a.published_at desc limit 5");
        $data['event']  = $event;
        $data['image_news']  = $image_news;
        $data['title']  = $data['event']->judul;

        $this->template->load('global/frontLayout/index', 'news/read', $data);
    }

}
