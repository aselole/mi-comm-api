<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends MX_Controller{

    public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            redirect('home', 'refresh');
        }
        $this->load->model(array('promo_claim_model','promo_user_model'));
        $this->load->helper('text');
        //Codeigniter : Write Less Do More
    }

    function index()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf']      = $this->security->get_csrf_hash();

        $data['title'] ='Transaksi';
        $where = array('user_id' => logged_user('id'), 'is_claimed' => 'y');
        $data['promop'] = $this->promo_user_model->with_promo()->get_all($where);
        $data['promog'] = $this->promo_claim_model->with_promo()->get_all($where);
        // $this->template->load('global/frontLayout/index', 'team/index', $data);
        $this->template->load('global/frontLayout/index', 'transaksi/index', $data);
        // $this->load->view('transaksi/index', $data);
    }


}
