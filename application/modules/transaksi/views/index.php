<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
    <div class="container">
        <div class="c-page-title c-pull-left">
            <h3 class="c-font-uppercase c-font-sbold">Profil</h3>
        </div>
    </div>
</div>
<div id="modal_promo"></div>
<!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
<div class="container">
    <div class="c-layout-sidebar-menu c-theme ">
        <!-- BEGIN: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
        <div class="c-sidebar-menu-toggler">
            <h3 class="c-title c-font-uppercase c-font-bold">My Dashboard</h3>
            <a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
                <span class="c-line"></span>
                <span class="c-line"></span>
                <span class="c-line"></span>
            </a>
        </div>
        <ul class="c-sidebar-menu collapse " id="sidebar-menu-1">

            <li class="c-dropdown c-open">
                <img src="<?php echo get_user_img(logged_user('photo')) ?>" alt="" width="270px" height="270px">
            </li>
            <li class="c-dropdown c-open">
                <a href="javascript:;" class="c-toggler">Akun
                    <span class="c-arrow"></span>
                </a>
                <ul class="c-dropdown-menu">
                    <li class="">
                        <a href="<?php echo site_url('profile') ?>">Profil</a>
                    </li>
                    <li class="">
                        <a href="<?php echo site_url('issue_private') ?>">Pesan
                        <?php if (count_unread_msg()): ?>
                            <span class="badge c-bg-blue"><?php echo count_unread_msg(); ?></span>
                        <?php endif; ?>
                        </a>
                    </li>
                   <li class="c-active">
                        <a href="<?php echo site_url('transaksi') ?>">Transaksi
                        </a>
                    </li>
                    <?php if (FALSE): ?>
                        <li class="">
                            <a href="<?php echo site_url('issue_community') ?>">Issue
                        </li>
                    <?php endif; ?>
                </ul>
            </li>
        </ul>
        <!-- END: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
    </div>

    <div id="modal_form" class="modal  modal-profile" tabindex="-1" data-backdrop="static" data-keyboard="true">
    </div>

    <div class="c-layout-sidebar-content ">
        <!-- BEGIN: PAGE CONTENT -->
        <div class="profil">
            <div class="c-content-title-1">
                <h3 class="c-font-uppercase c-font-bold">Histori Transaksi
                </h3>
                <div class="c-line-left"></div>
            </div>
            <div class="c-shop-wishlist-1">
                <?php if (!$promop && !$promog): ?>
                    Anda belum ambil promo !
                <?php endif ?>
                <?php if ($promop): ?>
                    <?php foreach ($promop as $p): ?>
                    <div class="c-border-bottom c-row-item">
                        <div class="row">
                            <div class="col-md-3 col-sm-12">
                                <div class="c-content-overlay">
                                    <div class="c-bg-img-top-center c-overlay-object" data-height="height">
                                        <img width="100%" class="img-responsive" src="<?php echo get_promo_img($p->promo->featured_img) ?>"> </div>
                                </div>
                            </div>
                            <div class="col-md-9 col-sm-12">
                                <ul class="c-list list-unstyled">
                                <li>
                                    <?php echo date('d F Y', strtotime($p->claimed_at)) ?>
                                    <span class="c-content-label c-font-uppercase c-font-bold c-bg-yellow-2 c-font-slim">CLAIMED</span>
                                </li>
                                    <li class="c-margin-b-25">
                                        <a href="shop-product-details-2.html" class="c-font-bold c-font-22 c-theme-link"><?php echo $p->promo->judul ?></a>
                                    </li>
                                    <li class="c-margin-b-10"><?php echo ellipsize($p->promo->body, 100) ?></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>
                <?php endif ?>
                <?php if ($promog): ?>
                    <?php foreach ($promog as $p): ?>
                    <div class="c-border-bottom c-row-item">
                        <div class="row">
                            <div class="col-md-3 col-sm-12">
                                <div class="c-content-overlay">
                                    <div class="c-bg-img-top-center c-overlay-object" data-height="height">
                                        <img width="100%" class="img-responsive" src="<?php echo get_promo_img($p->promo->featured_img) ?>"> </div>
                                </div>
                            </div>
                            <div class="col-md-9 col-sm-12">
                                <ul class="c-list list-unstyled">
                                <li>
                                    <?php echo date('d F Y', strtotime($p->claimed_at)) ?>
                                    <span class="c-content-label c-font-uppercase c-font-bold c-bg-yellow-2 c-font-slim">CLAIMED</span>
                                </li>
                                    <li class="c-margin-b-25">
                                        <a href="shop-product-details-2.html" class="c-font-bold c-font-22 c-theme-link"><?php echo $p->promo->judul ?></a>
                                    </li>
                                    <li class="c-margin-b-10"><?php echo ellipsize($p->promo->body, 100) ?></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>
                <?php endif ?>
            </div>        <!-- END: PAGE CONTENT -->
        </div>

    </div>
</div>

<script type="text/javascript">
function readp(promo_id) {
    var crsf = {<?php echo $csrf_name ?> : '<?php echo $csrf ?>'};
    $('#modal_promo').load(siteUrl + 'promo/read/' + promo_id,crsf,function() {
    });

}
</script>
