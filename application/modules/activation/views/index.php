    <!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->
    <div class="c-layout-breadcrumbs-1 c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
        <div class="container">
            <div class="c-page-title c-pull-left">
                <h3 class="c-font-uppercase c-font-sbold">Aktifasi</h3>
            </div>
        </div>
    </div>
    <!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->
    <!-- BEGIN: PAGE CONTENT -->
    <!-- BEGIN: BLOG LISTING -->
    <div class="c-content-box c-size-md">
        <div class="container">
	        <div class="c-content-title-1">
                <h3 class="c-center c-font-uppercase c-font-bold">Halo <?php echo $dt->first_name ?></h3>
                <div class="c-line-center c-theme-bg"></div>
            </div>
            <div class="row">
                <div class="col-md-12">
	                <div class="c-content-panel">
                        <div class="c-body">
							<form class="form_ajax form-horizontal" action="<?php echo $action ?>" method="<?php echo $method ?>">
							    <?php echo form_hidden($csrf_name, $csrf) ?>

							    <div class="form-group">
                                    <label for="inputEmail3" class="col-md-4 control-label">New Password</label>
                                    <div class="col-md-6">
                                        <input type="password" class="form-control  c-square c-theme" id="inputEmail3" name="password" placeholder="New Password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-md-4 control-label">Re-type New Password</label>
                                    <div class="col-md-6">
                                        <input type="password" class="form-control  c-square c-theme" id="inputEmail3" name="re_password" placeholder="Re-type Password">
                                    </div>
                                </div>
                                <div class="form-group c-margin-t-40">
                                    <div class="col-sm-offset-4 col-md-8">
                                        <button type="submit" class="btn c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Submit</button>
                                        <a href="<?php echo site_url('home') ?>" class="btn btn-default c-btn-square c-btn-uppercase c-btn-bold">Cancel</a>
                                    </div>
                                </div>
							</form>
						</div>
					</div>

                </div>
            </div>
        </div>
    </div>
    <!-- END: BLOG LISTING  -->
    <!-- END: PAGE CONTENT -->
<!-- <script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script> -->
<script src="<?php echo base_url() ?>assets/custom/scripts/j_generalFormModal.js" type="text/javascript"></script>
