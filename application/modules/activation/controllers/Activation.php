<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activation extends MX_Controller{

    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
        $this->load->model('user_model');
    }

    public function index($user_id, $activation_code)
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
		$data['csrf'] = $this->security->get_csrf_hash();
        $data['title'] = 'Aktifasi akun';
        $user = $this->user_model->fields('id, first_name, email, activation_code')->get($user_id);
        if ($user) {
            if ($user->activation_code == $activation_code) {
                if($this->ion_auth->activate_n_login($user->id)){
                    redirect("");
                } else {
                    $data['method'] = 'POST';
                    $data['action'] = site_url('activation/activation_submit/'.$user->id);

                    $data['dt'] = $user;
                    $this->template->load('global/frontLayout/index', 'activation/index', $data);
                }
            } else {
                redirect('404');
            }
        } else {
            redirect('404');
        }
    }

    public function activation_submit($id)
    {
        $config = array(
            array(
                'field'  => 'password',
                'label'  => 'New Password',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 're_password',
                'label'  => 'Re-type Password',
                'rules'  => 'required|matches[password]',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                    'matches'  => '%s tidak cocok.',
                ),
            ),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $result['errors'] = $this->form_validation->error_array();
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        } else {
            $data = array('active' => 1, 'activation_code' => null);
            $update = $this->user_model->update($data, $id);
            if ($update) {
                $result['success'] = true;
                $result['url'] = site_url();
                $result['message'] = 'Akun Anda sudah aktif. Silahkan login.';
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            } else {
                $result['success'] = false;
                $result['message'] = 'terjadi kesalahan';
                $this->output
                    ->set_status_header(400)
                    ->set_content_type('application/json')
                    ->set_output(json_encode($result));
            }
        }
    }
}
