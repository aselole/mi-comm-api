<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page_404 extends MX_Controller{

    public function index()
    {
        $data['title'] = 'Halaman tidak ada';
        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf']      = $this->security->get_csrf_hash();

        $this->template->load('global/frontLayout/index', 'page_404/page_404', $data);
    }

}
