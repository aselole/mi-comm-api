<div class="c-layout-breadcrumbs-1 c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
    <div class="container">
        <div class="c-page-title c-pull-left">
            <h3 class="c-font-uppercase c-font-sbold">Page 404</h3>
        </div>
    </div>
</div>
<!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->
<!-- BEGIN: PAGE CONTENT -->
<!-- BEGIN: BLOG LISTING -->
<div class="c-content-box c-size-md">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-uppercase c-font-bold">Halaman yang anda tuju tidak ada
                    </h3>
                    <div class="c-line-center c-theme-bg"></div>
                </div>

            </div>
        </div>
    </div>
    <!-- END: BLOG LISTING  -->
