<link href="<?php echo base_url() ?>assets/global/css/components-md.css" rel="stylesheet" type="text/css" />
<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
    <div class="container">
        <div class="c-page-title c-pull-left">
            <h3 class="c-font-uppercase c-font-sbold">Pesan</h3>
        </div>
    </div>
</div>
<!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
<div class="container">
    <div class="c-layout-sidebar-menu c-theme ">
        <!-- BEGIN: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
        <div class="c-sidebar-menu-toggler">
            <h3 class="c-title c-font-uppercase c-font-bold">My Profile</h3>
            <a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
                <span class="c-line"></span>
                <span class="c-line"></span>
                <span class="c-line"></span>
            </a>
        </div>
        <ul class="c-sidebar-menu collapse " id="sidebar-menu-1">

            <li class="c-dropdown c-open">
                <img src="<?php echo get_user_img(logged_user('photo')) ?>" alt="" width="270px" height="270px">
            </li>
            <li class="c-dropdown c-open">
                <a href="javascript:;" class="c-toggler">Akun
                    <span class="c-arrow"></span>
                </a>
                <ul class="c-dropdown-menu">
                    <li class="">
                        <a href="<?php echo site_url('profile') ?>">Profil</a>
                    </li>
                    <li class="">
                        <a href="<?php echo site_url('issue_private') ?>">Pesan
                            <?php if (count_unread_msg()): ?>
                                <span class="badge c-bg-blue"><?php echo count_unread_msg(); ?></span>
                            <?php endif; ?>
                        </a>
                    </li>
                    <?php if (logged_user('level_id') == 3): ?>
                        <li class="c-active">
                            <a href="<?php echo site_url('issue_community') ?>">Issue</a>
                        </li>
                    <?php endif; ?>
                </ul>
            </li>
        </ul>
        <!-- END: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
    </div>
    <div class="c-layout-sidebar-content ">
        <!-- BEGIN: PAGE CONTENT -->
        <div class="profil">
            <div class="c-content-title-1">
                <h3 class="c-font-uppercase c-font-bold">Pesan
                    <!-- <button class="btn btn-xs btn-success btn-outline ">
                        <i class="fa fa-plus"></i> Pesan baru
                    </button> -->
                </h3>
                <div class="c-line-left"></div>
            </div>
            <div class="row">
                <div class="col-md-10 col-sm-6 col-xs-12 c-margin-b-20">
                    <div class="page-content-wrapper">
                        <!-- BEGIN CONTENT BODY -->
                        <div class="page-content">
                            <!-- BEGIN PAGE HEADER-->
                            <!-- BEGIN PAGE BAR -->
                            <div class="page-bar">
                                <div class="page-toolbar">
                                </div>
                            </div>
                            <!-- END PAGE BAR -->
                            <!-- BEGIN PAGE BASE CONTENT -->
                            <div class="blog-page blog-content-2">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="blog-single-content bordered blog-container">
                                            <div class="blog-single-head">
                                                <h1 class="blog-single-head-title"><?php echo $issue->judul ?> </h1>
                                                <div class="blog-single-head-date">
                                                    <i class="icon-calendar font-blue"></i>
                                                    <a href="javascript:;"><?php echo date('d F Y, H:i', strtotime($issue->created_at)) ?></a>
                                                    <br>
                                                    <i class="fa fa-user-md font-blue"></i>
                                                    <a href="javascript:;"><?php echo $issue->trainer->first_name ?></a>
                                                    <br>
                                                    <a href="javascript:;">Status: <?php echo $issue->status ?></a>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="blog-single-desc">
                                                <p>
                                                    <?php echo $issue->body ?>
                                                </p>
                                            </div>

                                            <div class="blog-comments">
                                                <?php $jml_komen = $comments ? count($comments) : 0 ?>
                                                <h3 class="sbold blog-comments-title">Komentar (<?php echo $jml_komen ?>)</h3>
                                                <br>
                                                <?php if ($this->session->flashdata('notif_type')): ?>
                                                    <div class="alert <?php echo $this->session->flashdata('notif_type') ?> alert-dismissible" role="alert">
                                                        <?php echo $this->session->flashdata('notif_msg'); ?>
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                <?php endif; ?>
                                                <div class="c-comment-list">
                                                    <div class="scroller" style="height: 525px;" data-always-visible="1" data-rail-visible1="1">
                                                        <?php if ($comments): ?>
                                                            <ul class="chats">
                                                                <?php foreach ($comments as $r): ?>
                                                                    <?php
                                                                    $photo = base_url('assets/pages/media/profile/avatar.png');
                                                                    if ($r->user_photo && file_exists('./uploads/user/'.$r->user_photo)) {
                                                                        $photo = base_url('uploads/user/'.$r->user_photo);
                                                                    }

                                                                    $read = '<span class="label label-sm label-warning">unread</span>';
                                                                    if ($r->read == 1) {
                                                                        $read = '<span class="label label-sm label-success">read</span>';
                                                                    }

                                                                    $inout = $r->user_level_id == 3 ? 'in' : 'out';
                                                                    if ( logged_user('level_id') == 3 )
                                                                    {
                                                                        $inout = $r->user_level_id == 3 ? 'out' : 'in';
                                                                    }
                                                                    ?>
                                                                    <li class="<?php echo $inout ?>">
                                                                        <img class="avatar" alt="" src="<?php echo $photo ?>" />
                                                                        <div class="message">
                                                                            <span class="arrow"> </span>
                                                                            <a href="javascript:;" class="name"> <?php echo $r->user_first_name ?> </a>
                                                                            <span class="datetime"> at <?php echo date('d/m/Y H:i', strtotime($r->created_at)) ?> <?php echo $read ?></span>
                                                                            <span class="body"> <?php echo $r->comment ?> </span>
                                                                        </div>
                                                                    </li>
                                                                <?php endforeach; ?>

                                                            </ul>
                                                        <?php endif; ?>
                                                        <?php if ($issue->status == 'open'): ?>

                                                            <div class="chat-form">
                                                                <form method="post" class="form_ajax" action="<?php echo site_url('issue_private/store_comment') ?>">
                                                                    <div class="input-cont">
                                                                        <?php echo form_hidden($csrf_name, $csrf) ?>
                                                                        <input type="hidden" name="private_issue_id" value="<?php echo $issue->id ?>">
                                                                        <input type="hidden" name="url" value="<?php echo $this->uri->segment(3) ?>">
                                                                        <input name="comment" class="form-control" type="text" placeholder="Type a message here..." />
                                                                    </div>
                                                                    <div class="btn-cont">
                                                                        <span class="arrow"> </span>
                                                                        <button type="submit" class="btn blue icn-only">
                                                                            <i class="fa fa-check icon-white"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END PAGE BASE CONTENT -->
                        </div>
                        <!-- END CONTENT BODY -->
                    </div>


                    <div id="modal_form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                    </div>
                    <span id="site_url" data-site-url="<?php echo site_url() ?>"></span>

                </div>
            </div>        <!-- END: PAGE CONTENT -->
        </div>

    </div>
</div>

<script src="<?php echo base_url() ?>assets/custom/scripts/j_front_issue_comment.js" type="text/javascript"></script>

<script type="text/javascript">
function btn_edit(id) {
    $('#modal_form').load('<?php echo site_url('admin/private_issue/edit/') ?>'+id, function() {
        $(this).modal('show');
    });
}

$('.btn_delete').click(function(e) {
    e.preventDefault();
    var comment_id = $(this).data('comment-id');
    var parent = $(this).data('parent');
    var msg = 'Komentar ini akan dihapus.';
    if (parent=='y') {
        msg = 'Komentar ini dan balasannya akan terhapus.';
    }
    swal(
        {
            title: "Apakah Anda yakin?",
            text: msg,
            type: "warning",
            showCancelButton: true,
            cancelButtonClass: "btn-default",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, Delete it!",
            closeOnConfirm: true
        },
        function(){
            $.post('<?php echo site_url('admin/issue/delete_comment') ?>', {<?php echo $csrf_name ?>: '<?php echo $csrf ?>', comment_id:comment_id}, function(res) {
                if (res.success) {
                    $('.filter-cancel').trigger('click');
                    toastr.success(res.message);
                    location.reload();
                } else {
                    toastr.error(res.message);
                }
            });
        }
    );
});

</script>
