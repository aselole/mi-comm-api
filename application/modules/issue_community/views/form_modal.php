
<div class="modal-dialog">
    <div class="modal-content c-square">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="exampleModalLabel">Issue Baru</h4>
        </div>
        <div class="modal-body">
            <form action="<?php echo site_url('issue_community/store') ?>" class="form_ajax" method="post" enctype="multipart/form-data">
                <div class="form-body">
                    <?php echo form_hidden($csrf_name, $csrf); ?>

                    <div class="form-group">
                        <span class="help-block">Best View 345 x 300 pixel</span>
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                <img src="">
                            </div>
                            <div>
                                <span class="btn c-btn-red-2 c-btn-uppercase c-btn-bold c-btn-border-2x btn-file">
                                    <span class="fileinput-new"> Pilih Foto </span>
                                    <span class="fileinput-exists"> Ganti </span>
                                    <input type="file" name="featured_img">
                                </span>
                                <a class="btn c-btn-red-2 fileinput-exists" data-dismiss="fileinput"> Hapus </a>
                            </div>
                        </div>
                        <br>
                        <label for="recipient-name" class="control-label">Ke:</label>
                        <?php if (count($community) > 1): ?>
                            <?php $community = array ( '' => 'pilih' )+$community ?>
                        <?php endif ?>
                        <?php echo form_dropdown('community_id', $community,'', "class='form-control  c-square'" ) ?>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Judul:</label>
                            <textarea class="form-control  c-square" id="message-text" name="judul"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Isi:</label>
                            <textarea class="form-control  c-square" id="message-text" name="body"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn c-theme-btn c-btn-square c-btn-bold c-btn-uppercase">Submit</button>
                    <button type="button" class="btn c-theme-btn c-btn-border-2x c-btn-square c-btn-bold c-btn-uppercase" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>assets/custom/scripts/j_generalFormModal.js" type="text/javascript"></script>
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
