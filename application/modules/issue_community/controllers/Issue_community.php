<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Issue_community extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            redirect('home', 'refresh');
        }
        $this->load->model(array('issue_model', 'issue_comment_model', 'community_model'));
        $this->load->helper('text');
    }

    function index()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf']      = $this->security->get_csrf_hash();

        $data['title'] ='Issue Community';
        $data['issue'] = $this->issue_model->get_data();

        $this->template->load('global/frontLayout/index', 'issue_community/index', $data);
    }


    public function create()
    {
        $this->load->model(array('member_model'));
        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf']      = $this->security->get_csrf_hash();

        $q = "SELECT
                    communities.id as com_id,
                    communities.nama
                FROM
                    `members`
                JOIN `communities` ON `members`.`community_id` = `communities`.`id`
                WHERE
                    `user_id` = ".logged_user('id');

        $komunitas = $this->db->query($q);
        $dd = array();
        if ($komunitas->num_rows() != 0) {
            foreach ($komunitas->result() as $v) {
                $dd[$v->com_id] = $v->nama;
            }
        }

        $data['community'] = $dd;
        $this->load->view('form_modal', $data);
    }

    public function store()
    {
        $config = array(
            array(
                'field'  => 'community_id',
                'label'  => 'Community',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'judul',
                'label'  => 'Judul',
                'rules'  => 'required',
                'errors' => array(
                    'required' => '%s tidak boleh kosong.',
                ),
            ),
            array(
                'field'  => 'body',
                'label'  => 'Body',
                'rules'  => 'required',
                'errors' => array(
                    'required'  => '%s tidak boleh kosong.',
                ),
            ),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $result['errors'] = $this->form_validation->error_array();
            $this->output
                ->set_status_header(400)
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        } else {
            $data = array(
                'community_id' => $this->input->post('community_id'),
                'judul'        => $this->input->post('judul'),
                'body'         => $this->input->post('body'),
                'published_at' => date('Y-m-d'),
                'author_id'    => logged_user('id'),
            );
            if ( !empty($_FILES['featured_img']['name']) ) {
                $upload = $this->issue_model->upload_pic_issue();
                if ($upload['success']) {
                    $data['featured_img'] = $upload['data']['file_name'];
                } else {
                    $result = $upload;
                    $this->output
                        ->set_status_header(400)
                        ->set_content_type('application/json')
                        ->set_output(json_encode($result));
                }
            }

            $insert_id = $this->issue_model->insert($data);
            if ($insert_id) {
                $judul = strtolower(str_replace(' ', '-', $this->input->post('judul')));
                $find  = array ('\'', '(', ')', '/' , '\/', '%', '$', '^', '*', '=', '!', '@','#', ':', ';', '"', '?','<', '>', ',', '.', '{', '}',
                        '[', ']', '|', '_', '&' );
                $url_p = str_replace($find, '', $judul);

                $url = 'issue/read/' . $insert_id . '-' . $url_p;
                $result['success'] = true;
                $result['message'] = 'Issue berhasil ditambahkan.';
                $result['url']     = site_url($url);
            } else {
                $result['success'] = false;
                $result['message'] = 'Terjadi kesalahan.';
            }
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }
    }



}
