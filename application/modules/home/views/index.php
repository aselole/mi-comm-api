<section class="c-layout-revo-slider c-layout-revo-slider-12">
    <div class="tp-banner-container tp-fullscreen tp-fullscreen-mobile c-arrow-darken" data-bullets-pos="center">
        <div class="tp-banner rev_slider" data-version="5.0">
            <ul>
                <?php if ($sliders): ?>
                    <?php foreach ($sliders as $s): ?>
                        <?php if (!empty($s->file)): ?>
                            <li data-transition="fade" data-slotamount="1" data-masterspeed="1000">
                                <img alt="" src="<?php echo base_url('uploads/image_slide/'.$s->file)?>" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
                                <div class="tp-caption customin customout" data-x="center" data-y="center" data-hoffset="" data-voffset="-50" data-speed="500" data-start="1000" data-transform_in="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:500;e:Back.easeInOut;"
                                data-transform_out="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:600;e:Back.easeInOut;" data-easing="Back.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1"
                                data-endelementdelay="0.1" data-endspeed="600"></div>
                                <div class="tp-caption lft" data-x="center" data-y="center" data-voffset="140" data-speed="900" data-start="1500" data-transform_in="x:0;y:-50;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;s:900;e:Expo.easeInOut;"
                                data-transform_out="x:0;y:-50;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;s:900;e:Expo.easeInOut;"></div>
                            </li>
                        <?php endif ?>
                    <?php endforeach ?>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</section>


<!-- END: LAYOUT/SLIDERS/REVO-SLIDER-12 -->

<!-- EVENT -->
<div class="c-content-box c-size-md c-bg-white">
    <div class="container">
        <!-- Begin: Testimonals 1 component -->
        <div class="c-content-blog-post-card-1-slider" data-slider="owl" data-items="3" data-auto-play="8000">
            <!-- Begin: Title 1 component -->
            <div class="c-content-title-1">
                <h3 class="c-center c-font-uppercase c-font-bold">Berita
                    <!-- <a href="#" class="btn c-btn-blue-2 c-btn-uppercase c-btn-bold c-btn-border-2x c-btn-square btn-xs">lihat semua</a> -->
                    <a href="<?php echo site_url('event') ?>" class="link-kecil">lihat semua</a>
                </h3>
                <div class="c-line-center c-theme-bg"></div>
            </div>
            <!-- End-->
            <!-- Begin: Owlcarousel -->
            <div class="owl-carousel owl-theme c-theme">
                <?php if ($news): ?>
                    <?php foreach ($news->result() as $n): ?>
                        <?php $event_url = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '',str_replace(' ', '-', $n->judul))) ?>
                        <div class="item">
                            <div class="c-content-blog-post-card-1 c-option-2">
                                <?php if ($n->image != null): ?>
                                    <div class="c-media c-content-overlay">
                                        <a href="<?php echo site_url('event/read/'.$n->id.'-'.$event_url)?>">
                                            <img class="c-overlay-object img-responsive img-flat" src="<?php echo get_news_img($n->image)?>" alt="event">
                                        </a>
                                    </div>
                                <?php endif; ?>

                                <div class="c-body">
                                    <div class="c-title c-font-uppercase c-font-bold">
                                        <a href="<?php echo site_url('event/read/'.$n->id.'-'.$event_url)?>"><?php echo $n->judul ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
            <!-- End-->
        </div>
        <!-- End-->
    </div>
</div>
<!-- EVENT end -->

<div class="c-content-box c-size-md c-bg-grey-1">
    <div class="container">
        <!-- Begin: Testimonals 1 component -->
        <div class="c-content-blog-post-card-1-slider" data-slider="owl" data-items="3" data-auto-play="8000">
            <!-- Begin: Title 1 component -->
            <div class="c-content-title-1">
                <h3 class="c-center c-font-uppercase c-font-bold">Promo
                    <a href="<?php echo site_url('promo') ?>" class="link-kecil">lihat semua</a>
                </h3>
                <div class="c-line-center c-theme-bg"></div>
            </div>
            <!-- End-->
            <!-- Begin: Owlcarousel -->
            <div class="owl-carousel owl-theme c-theme">
                <?php if ($promos): ?>
                    <?php foreach ($promos->result() as $p): ?>
                        <?php $promo_url = strtolower(str_replace(' ', '-', $p->judul)) ?>
                        <?php
                            // $claimed_users = array();
                            // if (isset($p->promo_claim))
                            // {
                            //     foreach ($p->promo_claim as $k => $v) {
                            //         $claimed_users[] = $v->user_id;
                            //     }
                            // }
                        ?>
                        <div class="item">
                            <div class="c-content-blog-post-card-1 c-option-2">
                                <div class="c-media c-content-overlay">
                                    <a href="javascript:read_promo('<?php echo $p->id ?>')">
                                    <img class="c-overlay-object img-responsive img-flat" src="<?php echo get_promo_img($p->featured_img)?>" alt="promo">
                                    <?php if ($p->is_claimed=='y'): ?>
                                        <div class="ribbon red"><span>claimed</span></div>
                                    <?php endif ?>
                                </a>
                                </div>
                                <div class="c-body">
                                    <div class="c-title c-font-uppercase c-font-bold">
                                        <a href="javascript:read_promo('<?php echo $p->id ?>')"><?php echo $p->judul ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
            <!-- End-->
        </div>
        <!-- End-->
    </div>
</div>
<!-- PROMO end -->

<!-- BEGIN: CONTENT/SLIDERS/CLIENT-LOGOS-2 -->
<div class="c-content-box c-size-md c-bg-white">
    <div class="container">
        <!-- Begin: Testimonals 1 component -->
        <div class="c-content-client-logos-slider-1  c-bordered" data-slider="owl" data-items="6" data-desktop-items="4"
            data-desktop-small-items="3" data-tablet-items="3" data-mobile-small-items="2" data-auto-play="5000">
            <!-- Begin: Title 1 component -->
            <div class="c-content-title-1">
                <h3 class="c-center c-font-uppercase c-font-bold">Merchant
                    <a href="<?php echo site_url('merchant') ?>" class="link-kecil">lihat semua</a>
                </h3>
                <div class="c-line-center c-theme-bg"></div>
            </div>
            <!-- End-->
            <!-- Begin: Owlcarousel -->
            <div class="owl-carousel owl-theme c-theme owl-bordered1">
                <?php foreach ($merchants as $m): ?>
                    <div class="item">
                        <a href="javascript:read_merchant('<?php echo $m->id ?>')">
                            <img src="<?php echo get_merchant_img($m->logo_merchant) ?>" width="180px" alt="logo-<?php echo $m->nama ?>" />
                        </a>
                    </div>
                <?php endforeach?>
            </div>
            <!-- End-->
            <br>
            <!-- Begin: Title 1 component -->
            <div class="c-content-title-1">
                <h3 class="c-center c-font-uppercase c-font-bold">Partner
                    <a href="<?php echo site_url('merchant') ?>" class="link-kecil">lihat semua</a>
                </h3>
                <div class="c-line-center c-theme-bg"></div>
            </div>
            <!-- End-->
            <div class="owl-carousel owl-theme c-theme owl-bordered1">
                <?php foreach ($partners as $m): ?>
                    <div class="item">
                        <a href="javascript:read_partner('<?php echo $m->id ?>')">
                            <img src="<?php echo get_partner_img($m->logo_partner) ?>" width="180px" alt="logo-<?php echo $m->nama ?>" />
                        </a>
                    </div>
                <?php endforeach?>
            </div>
            <!-- End-->
        </div>
        <!-- End-->
    </div>
</div>
                    <!-- END: CONTENT/SLIDERS/CLIENT-LOGOS-2 -->
<div class="c-content-box c-size-md c-bg-grey-1">
    <div class="container">
        <!-- Begin: Testimonals 1 component -->
        <div class="c-content-person-1-slider" data-slider="owl" data-items="3" data-auto-play="8000">
            <!-- Begin: Title 1 component -->
            <div class="c-content-title-1 wow animated fadeIn">
                <h3 class="c-center c-font-uppercase c-font-bold">Our Support Team
                    <a href="<?php echo site_url('team') ?>" class="link-kecil">lihat semua</a>
                </h3>
                <div class="c-line-center c-theme-bg"></div>
            </div>
            <!-- End-->
            <!-- Begin: Owlcarousel -->
            <div class="owl-carousel owl-theme c-theme wow animated fadeInUp">
                <?php if (TRUE): ?>
                    <?php foreach ($teams as $t): ?>
                        <div class="item">
                            <div class="c-content-person-1 c-option-2">
                                <div class="c-media c-content-overlay">
                                    <a href="javascript:read_team('<?php echo $t->id ?>')">
                                        <img src="<?php echo get_user_img($t->photo) ?>" class="img-responsive c-overlay-object img-c" alt="team">
                                    </a>
                                </div>
                                <div class="c-body">
                                    <div class="c-head">
                                        <div class="c-name c-font-uppercase c-font-bold">
                                            <a href="javascript:read_team('<?php echo $t->id ?>')"><?php echo $t->first_name .' '. $t->last_name ?></a>
                                        </div>
                                    </div>
                                    <!-- <div class="c-position"> CEO, JANGO Inc. </div> -->
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
            <!-- End-->
        </div>
        <!-- End-->
    </div>
</div>
<?php if ( !logged_user() ): ?>
    <!-- Start of LiveChat (www.livechatinc.com) code -->
    <script type="text/javascript">
    // window.__lc = window.__lc || {};
    // window.__lc.license = 8709761;
    // (function() {
    //   var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
    //   lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
    //   var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
    // })();
    // </script>
    <!-- End of LiveChat code -->
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/55067fd7e1dea9e226605e81/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
<?php endif ?>

<!-- END: PAGE CONTENT -->
<style media="screen">
.link-kecil{
    font-size: 14px;
    color:grey;
}
</style>
