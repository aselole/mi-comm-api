<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('news_model', 'image_news_model'));
        $this->load->model('promo_model');
        $this->load->model('merchant_model');
        $this->load->model('partner_model');
        $this->load->model('user_model');
        $this->load->model('image_slide_model');

    }

    public function index()
    {
        $data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf']      = $this->security->get_csrf_hash();
        $data['title']     = 'Home';
        $date_now          = date('Y-m-d');
        $batas_date_valid  = date('Y-m-d', strtotime($date_now. ' + 7 days'));
        // $data['news']      = $this->news_model->with_image_news('where:`is_featured`=\'y\'')->order_by('published_at', 'desc')->limit(6, 0)->get_all(array('active' => 'y'));
        $data['news']      = $this->db->query("select a.*, b.image from news a left join image_news b on (b.news_id=a.id and b.is_featured='y') where for_whom='micomm' and active='y' order by a.published_at desc limit 6");
        $data['promos']    = $this->promo_model->list_global(0,100);
        $data['sliders']   = $this->image_slide_model->order_by('order', 'asc')->limit(10, 0)->get_all( array('publish' => 'y') );

        $data['merchants'] = $this->merchant_model->get_all(array('active' => 'y'));
        $data['partners'] = $this->partner_model->get_all(array('active' => 'y'));
        $data['teams']     = $this->user_model->get_all( array ('level_id' => 3 , 'active' => 1) );
        $this->template->load('global/frontLayout/index-home', 'home/index', $data);
    }

}
